/*
 * Copyright 2005-2011 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * @file adv7180.c
 *
 * @brief Analog Device ADV7180 video decoder functions
 *
 * @ingroup Camera
 */
//#define DEBUG
#define USE_RAW
#define EASY_FRAMERATE
// which interlacing type do we want to report? can't detect this...
#define V4L2_FIELD_DEFINE	V4L2_FIELD_SEQ_TB

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/ctype.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/i2c.h>
#include <linux/wait.h>
#include <linux/videodev2.h>
#include <linux/workqueue.h>
#include <linux/regulator/consumer.h>
#include <linux/fsl_devices.h>
#include <media/v4l2-chip-ident.h>
#include <media/v4l2-int-device.h>
#include "mxc_v4l2_capture.h"
#include <linux/proc_fs.h>

#ifdef EASY_FRAMERATE
  #define FRAMERATE_NUMERATOR 1
  #define FRAMERATE_DENOMITATOR 30
#else
  #define FRAMERATE_NUMERATOR 1001
  #define FRAMERATE_DENOMITATOR 30000
#endif

#if 0 //def DEBUG
#include <linux/v4l2debug.h>
#else
#define DUMP_V4L2PIXFORMAT(x)
#endif

extern void gpio_sensor_active(void);
extern void gpio_sensor_inactive(void);

static void adv7180_init_device(struct v4l2_int_device *, bool );
static int adv7180_probe(struct i2c_client *adapter, const struct i2c_device_id *id);
static int adv7180_detach(struct i2c_client *client);

static const struct i2c_device_id adv7180_id[] = {
	{"adv7180", 0},
	{},
};

MODULE_DEVICE_TABLE(i2c, adv7180_id);

static struct i2c_driver adv7180_i2c_driver = {
	.driver = {
		   .owner = THIS_MODULE,
		   .name = "adv7180",
		   },
	.probe = adv7180_probe,
	.remove = adv7180_detach,
	.id_table = adv7180_id,
};

/*! List of input video formats supported. The video formats is corresponding
 * with v4l2 id in video_fmt_t
 */
typedef enum {
	ADV7180_NTSC = 0,	/*!< Locked on (M) NTSC video signal. */
	ADV7180_PAL,		/*!< (B, G, H, I, N)PAL video signal. */
	ADV7180_NOT_LOCKED,	/*!< Not locked on a signal. */
} video_fmt_idx;

/*! Number of video standards supported (including 'not locked' signal). */
#define ADV7180_STD_MAX		(ADV7180_PAL + 1)

/*! Video format structure. */
typedef struct {
	int v4l2_id;		/*!< Video for linux ID. */
	char name[16];		/*!< Name (e.g., "NTSC", "PAL", etc.) */
	u16 raw_width;		/*!< Raw width. */
	u16 raw_height;		/*!< Raw height. */
	u16 active_width;	/*!< Active width. */
	u16 active_height;	/*!< Active height. */
} video_fmt_t;

/*! Description of video formats supported.
 *
 *  PAL: raw=720x625, active=720x576.
 *  NTSC: raw=720x525, active=720x480.
 */
static const video_fmt_t video_fmts[] = {
	{			/*! NTSC */
	 .v4l2_id = V4L2_STD_NTSC,
	 .name = "NTSC",
	 .raw_width = 720,	/* SENS_FRM_WIDTH */
	 .raw_height = 525,	/* SENS_FRM_HEIGHT */
	 .active_width = 720,	/* ACT_FRM_WIDTH plus 1 */
	 .active_height = 480,	/* ACT_FRM_WIDTH plus 1 */
	 },
	{			/*! (B, G, H, I, N) PAL */
	 .v4l2_id = V4L2_STD_PAL,
	 .name = "PAL",
	 .raw_width = 720,
	 .raw_height = 625,
	 .active_width = 720,
	 .active_height = 576,
	 },
	{			/*! Unlocked standard */
	 .v4l2_id = V4L2_STD_ALL,
	 .name = "Autodetect",
	 .raw_width = 720,
	 .raw_height = 625,
	 .active_width = 720,
	 .active_height = 576,
	 },
};

/* supported controls */
static const struct v4l2_queryctrl adv7180_qctrl[] = {
	{
	.id = V4L2_CID_BRIGHTNESS,
	.type = V4L2_CTRL_TYPE_INTEGER,
	.name = "Brightness",
	.minimum = 0,		/* check this value */
	.maximum = 255,		/* check this value */
	.step = 1,		/* check this value */
	.default_value = 0,	/* check this value */
	.flags = 0,
	}, {
	.id = V4L2_CID_CONTRAST,
	.type = V4L2_CTRL_TYPE_INTEGER,
	.name = "Contrast",
	.minimum = 0,		/* check this value */
	.maximum = 255,		/* check this value */
	.step = 0x1,		/* check this value */
	.default_value = 0x80,	/* check this value */
	.flags = 0,
	}, {
	.id = V4L2_CID_SATURATION,
	.type = V4L2_CTRL_TYPE_INTEGER,
	.name = "Saturation",
	.minimum = 0,		/* check this value */
	.maximum = 255,		/* check this value */
	.step = 0x1,		/* check this value */
	.default_value = 0x80,	/* check this value */
	.flags = 0,
	}, {
	.id = V4L2_CID_HUE,
	.type = V4L2_CTRL_TYPE_INTEGER,
	.name = "Hue",
	.minimum = 0,		/* check this value */
	.maximum = 255,		/* check this value */
	.step = 0x1,		/* check this value */
	.default_value = 0,	/* check this value */
	.flags = 0,
	}
};

//static DECLARE_MUTEX(mutex);

struct private_data {
	// add list handling here for multiple devices
	struct mxc_tvin_platform_data *tvin_plat;
	struct v4l2_int_device *v4l2_int_device;
	struct i2c_client *i2c_client;
	struct regulator *dvddio_regulator;
	struct regulator *dvdd_regulator;
	struct regulator *avdd_regulator;
	struct regulator *pvdd_regulator;
	bool on;
/*!* Standard index of ADV7180. */
	video_fmt_idx video_idx;
	enum v4l2_field field;

	struct proc_dir_entry *proc_dir;
};
struct private_data *adv7180_private_data = NULL; // head


#define IF_NAME				"adv7180"
#define ADV7180_INPUT_CTL		0x00	/* Input Control */
#define ADV7180_OUTPUT_CTL		0x03	/* output control */
#define ADV7180_EXT_OUTPUT_CTL		0x04	/* extended output control */
#define ADV7180_AUTODETECT_EN		0x07	/* autodetct enable */
#define ADV7180_CONTRAST		0x08	/* contrast */
#define ADV7180_BRIGHTNESS		0x0a	/* Brightness */
#define ADV7180_HUE			0x0b	/* Hue */
#define ADV7180_DEFAULT_Y		0x0c	/* default value Y */
#define ADV7180_DEFAULT_C		0x0d	/* defalt value C */
#define ADV7180_ADI_CTL_1		0x0e	/* ADI Control 1 */
#define ADV7180_PWR_MNG			0x0f     /* Power Management */
#define ADV7180_STATUS_1		0x10	/* Status #1 */
#define ADV7180_IDENT			0x11	/* IDENT */
#define ADV7180_STATUS_2		0x12	/* Status #2 */
#define ADV7180_STATUS_3		0x13	/* Status #3 */
#define ADV7180_ANALOG_CLAMP_CTL	0x14	/* Analog Clamp Control */
#define ADV7180_DIGITAL_CLAMP_CTL	0x15	/* Digital Clamp Control */
#define ADV7180_SHAPING_FILTER_CTL_1	0x17	/* Shaping Filter Control #1 */
#define ADV7180_SHAPING_FILTER_CTL_2	0x18	/* Shaping Filter Control #2 */
#define ADV7180_COMB_FILTER_CTL		0x19	/* Comb Filter Control */
#define ADV7180_ADI_CTL_2		0x1d	/* ADI Control 2 */
#define ADV7180_PIXEL_DELAY_CTL		0x27	/* Pixel Delay Control */
#define ADV7180_MISC_GAIN_CTL		0x2b	/* Misc Gain Control */
#define ADV7180_AGC_MODE_CTL		0x2c	/* AGC Mode Control */
#define ADV7180_CHROMA_GAIN_CTL_1	0x2d	/* Chroma Gain Control 1, Chroma Gain 1 */
#define ADV7180_CHROMA_GAIN_CTL_2	0x2e	/* Chroma Gain Control 2, Chroma Gain 2 */
#define ADV7180_LUMA_GAIN_CTL_1		0x2f	/* Luma Gain Control 1, Luma Gain 1 */
#define ADV7180_LUMA_GAIN_CTL_2		0x30	/* Luma Gain Control 2, Luma Gain 2 */
#define ADV7180_VSYNC_FIELD_CTL_1	0x31	/* VSYNC Field Control #1 */
#define ADV7180_VSYNC_FIELD_CTL_2	0x32	/* VSYNC Field Control #2 */
#define ADV7180_VSYNC_FIELD_CTL_3	0x33	/* VSYNC Field Control #3 */
#define ADV7180_HSYNC_POSITION_CTL_1	0x34	/* HS Position Control #1 */
#define ADV7180_HSYNC_POSITION_CTL_2	0x35	/* HS Position Control #2 */
#define ADV7180_HSYNC_POSITION_CTL_3	0x36	/* HS Position Control #3 */
#define ADV7180_POLARITY		0x37	/* Polarity */
#define ADV7180_NTSC_COMB_CTL		0x38	/* NTSC Comb Control */
#define ADV7180_PAL_COMB_CTL		0x39	/* PAL Comb Control */
#define ADV7180_ADC_CTL			0x3a	/* ADC Control */
#define ADV7180_MANUAL_WIN_CTL		0x3d	/* Manual Window Control */
#define ADV7180_GEMSTAR_CTL_1		0x48	/* Gemstar Control 1 */
#define ADV7180_GEMSTAR_CTL_2		0x49	/* Gemstar Control 2 */
#define ADV7180_GEMSTAR_CTL_3		0x4a	/* Gemstar Control 3 */
#define ADV7180_GEMSTAR_CTL_4		0x4b	/* Gemstar Control 4 */
#define ADV7180_GEMSTAR_CTL_5		0x4c	/* Gemstar Control 5 */
#define ADV7180_CTI_DNR_1		0x4d	/* CTI DNR Control 1 */
#define ADV7180_CTI_DNR_2		0x4e	/* CTI DNR Control 2 */
#define ADV7180_CTI_DNR_4		0x50	/* CTI DNR Control 4 */
#define ADV7180_LOCK_COUNT		0x51	/* Lock count */
#define ADV7180_VSYNC_PIN_CTL		0x58	/* VSYNC Pin control */
#define ADV7180_GPOUT			0x59	/* General Purpose Outputs */
#define ADV7180_FREERUN_LINE_LENGTH_1	0x8f	/* Free-run line length 1 */
#define ADV7180_CCAP1			0x99	/* CCAP1 */
#define ADV7180_CCAP2			0x9a	/* CCAP2 */
#define ADV7180_LETTERBOX_1		0x9b	/* Letterbox 1 */
#define ADV7180_LETTERBOX_2		0x9c	/* Letterbox 2 */
#define ADV7180_LETTERBOX_3		0x9d	/* Letterbox 3 */
#define ADV7180_CRC_EN			0xb2	/* CRC Enable */
#define ADV7180_ADC_SWITCH_1		0xc3	/* ADC Switch 1 */
#define ADV7180_ADC_SWITCH_2		0xc4	/* ADC Switch 2 */
#define ADV7180_LETTERBOX_CTL_1		0xdc	/* Letterbox Control 1 */
#define ADV7180_LETTERBOX_CTL_2		0xdd	/* Letterbox Control 2 */
#define ADV7180_ST_NOISE_READ_1		0xde	/* ST Noise Readback 1 */
#define ADV7180_ST_NOISE_READ_2		0xdf	/* ST Noise Readback 2 */
#define ADV7180_SD_OFFSET_CB		0xe1	/* SD Offset Cb */
#define ADV7180_SD_OFFSET_CR		0xe2	/* SD Offset Cr */
#define ADV7180_SD_SATURATION_CB	0xe3	/* SD Saturation Cb */
#define ADV7180_SD_SATURATION_CR	0xe4	/* SD Saturation Cr */
#define ADV7180_NTSC_VBIT_BEGIN		0xe5	/* NTSC V-bit Begin */
#define ADV7180_NTSC_VBIT_END		0xe6	/* NTSC V-bit End */
#define ADV7180_NTSC_FBIT_TOGGLE	0xe7	/* NTSC F-bit Toggle */
#define ADV7180_PAL_VBIT_BEGIN		0xe8	/* PAL V-bit Begin */
#define ADV7180_PAL_VBIT_END		0xe9	/* PAL V-bit End */
#define ADV7180_PAL_FBIT_TOGGLE		0xea	/* PAL F-bit Toggle */
#define ADV7180_VBLANK_CTL_1		0xeb	/* VBlank Control 1 */
#define ADV7180_VBLANK_CTL_2		0xec	/* VBlank Control 2 */
#define ADV7180_AFE_CTL_1		0xf3	/* AFE Control 1 */
#define ADV7180_DRIVE_STRENGTH		0xf4	/* Drive Strength */
#define ADV7180_IF_COMP_CTL		0xf8	/* IF Comp Control */
#define ADV7180_VS_MODE_CTL		0xf9	/* VSYNC Mode Control */
#define ADV7180_PEAKING_CTL		0xfb
#define ADV7180_CORING_THRESH		0xfc

#define ADV7180_INTTERRUPT_CONFIG_1	0x40
#define ADV7180_RESAMPEL_CTL		0x41	/* Resample Control */
#define ADV7180_INTERRUPT_STATUS_1	0x42
#define ADV7180_INTERRUPT_CLEAR_1	0x43
#define ADV7180_INTERRUPT_MASK_1	0x44
#define ADV7180_RAW_STATUS_1		0x45
#define ADV7180_INTERRUPT_STATUS_2	0x46
#define ADV7180_INTERRUPT_CLEAR_2	0x47
#define ADV7180_INTERRUPT_MASK_2	0x48
#define ADV7180_RAW_STATUS_2		0x49
#define ADV7180_INTERRUPT_STATUS_3	0x4a
#define ADV7180_INTERRUPT_CLEAR_3	0x4b
#define ADV7180_INTERRUPT_MASK_3	0x4c
#define ADV7180_INTERRUPT_STATUS_4	0x4e
#define ADV7180_INTERRUPT_CLEAR_4	0x4f
#define ADV7180_INTERRUPT_MASK_4	0x50


/***********************************************************************
 * I2C transfert.
 ***********************************************************************/

/*! Read one register from a ADV7180 i2c slave device.
 *
 *  @param *reg		register in the device we wish to access.
 *
 *  @return		       0 if success, an error code otherwise.
 */
static inline int adv7180_read(struct i2c_client *i2c_client, u8 reg)
{
	int val;
	val = i2c_smbus_read_byte_data(i2c_client, reg);
	if (val < 0) {
		dev_dbg(&i2c_client->dev,
			"%s:read reg error: reg=%2x \n", __func__, reg);
		return -1;
	}
	return val;
}

/*! Write one register of a ADV7180 i2c slave device.
 *
 *  @param *reg		register in the device we wish to access.
 *
 *  @return		       0 if success, an error code otherwise.
 */
static int adv7180_write_reg(struct i2c_client *i2c_client, u8 reg, u8 val)
{
	if (i2c_smbus_write_byte_data(i2c_client, reg, val) < 0) {
		dev_dbg(&i2c_client->dev,
			"%s:write reg error:reg=%2x,val=%2x\n", __func__,
			reg, val);
		return -1;
	}
	return 0;
}

/***********************************************************************
 * mxc_v4l2_capture interface.
 ***********************************************************************/

/*!
 * Return attributes of current video standard.
 * Since this device autodetects the current standard, this function also
 * sets the values that need to be changed if the standard changes.
 * There is no set std equivalent function.
 *
 *  @return		None.
 */
static void adv7180_get_std(struct v4l2_int_device *s, v4l2_std_id *std)
{
	struct private_data *p = (struct private_data *)s->priv;
	int wait = 30;
	u8 tmp;

	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );

	/* Make sure power on */
	if (p->tvin_plat->pwdn)
		p->tvin_plat->pwdn(0);

	/* Read the AD_RESULT to get the detect output video standard */
	tmp = adv7180_read(p->i2c_client, ADV7180_STATUS_1);
	while (( tmp & 0x1 ) == 0 ) { // (looking for lock)
		msleep(33);
		tmp = adv7180_read(p->i2c_client, ADV7180_STATUS_1);
		if ( wait-- == 0 ) { // timed out looking for lock
			*std = V4L2_STD_ALL;
			p->video_idx = ADV7180_NOT_LOCKED;
			return;
		}
	}
	tmp &= 0x70; // mask off unrelated bits

//	down(&mutex);
	if (tmp == 0x40) {
		/* PAL */
		dev_dbg(&p->i2c_client->dev, "->detected PAL\n");
		*std = V4L2_STD_PAL;
		p->video_idx = ADV7180_PAL;
	} else if (tmp == 0) {
		/*NTSC*/
		dev_dbg(&p->i2c_client->dev, "->detected NTSC\n");
		*std = V4L2_STD_NTSC;
		p->video_idx = ADV7180_NTSC;
	} else {
		dev_dbg(&p->i2c_client->dev, "Got invalid video standard! (%04x) \n", tmp);
		*std = V4L2_STD_ALL;
		p->video_idx = ADV7180_NOT_LOCKED;
	}
	// check for interlaced
	tmp = adv7180_read(p->i2c_client, ADV7180_STATUS_3);
	if ( tmp & 0x40 ) { // field seq detected
		dev_dbg(&p->i2c_client->dev, "->detected interlaced video\n");
		p->field = V4L2_FIELD_DEFINE;
	}  else {
		p->field = V4L2_FIELD_NONE;
	}

//	up(&mutex);
}

/***********************************************************************
 * IOCTL Functions from v4l2_int_ioctl_desc.
 ***********************************************************************/

/*!
 * ioctl_g_ifparm - V4L2 sensor interface handler for vidioc_int_g_ifparm_num
 * s: pointer to V4L2 device structure
 * p: pointer to V4L2 vidioc_int_g_ifparm_num ioctl structure
 *
 * Gets slave interface parameters.
 * Calculates the required xclk value to support the requested
 * clock parameters in p.  This value is returned in the p
 * parameter.
 *
 * vidioc_int_g_ifparm returns platform-specific information about the
 * interface settings used by the sensor.
 *
 * Called on open.
 */
static int ioctl_g_ifparm(struct v4l2_int_device *s, struct v4l2_ifparm *ifp)
{
	struct private_data *p = s->priv;
	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );

	if (s == NULL) {
		pr_err("   ERROR!! no slave device set!\n");
		return -1;
	}

	/* Initialize structure to 0s then set any non-0 values. */
	memset(ifp, 0, sizeof(*ifp));
	ifp->if_type = V4L2_IF_TYPE_BT656; /* This is the only possibility. */
	ifp->u.bt656.mode = V4L2_IF_TYPE_BT656_MODE_NOBT_8BIT;
	ifp->u.bt656.nobt_hs_inv = 1;

	/* ADV7180 has a dedicated clock so no clock settings needed. */

	return 0;
}

/*!
 * Sets the camera power.
 *
 * s  pointer to the camera device
 * on if 1, power is to be turned on.  0 means power is to be turned off
 *
 * ioctl_s_power - V4L2 sensor interface handler for vidioc_int_s_power_num
 * @s: pointer to V4L2 device structure
 * @on: power state to which device is to be set
 *
 * Sets devices power state to requrested state, if possible.
 * This is called on open, close, suspend and resume.
 */
static int ioctl_s_power(struct v4l2_int_device *s, int on)
{
	struct private_data *p = (struct private_data *)s->priv;

	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );

	if (on && !p->on) {
		gpio_sensor_active();

		/* Make sure power on */
		if (p->tvin_plat->pwdn)
			p->tvin_plat->pwdn(0);

		if (adv7180_write_reg(p->i2c_client, ADV7180_PWR_MNG, 0) != 0)
			return -EIO;
	} else if (!on && p->on) {
		if (adv7180_write_reg(p->i2c_client, ADV7180_PWR_MNG, 0x24) != 0)
			return -EIO;

		/* Make sure power off */
		if (p->tvin_plat->pwdn)
			p->tvin_plat->pwdn(1);

		gpio_sensor_inactive();
	}

	p->on = on;

	return 0;
}

/*!
 * ioctl_g_parm - V4L2 sensor interface handler for VIDIOC_G_PARM ioctl
 * @s: pointer to V4L2 device structure
 * @a: pointer to V4L2 VIDIOC_G_PARM ioctl structure
 *
 * Returns the sensor's video CAPTURE parameters.
 */
static int ioctl_g_parm(struct v4l2_int_device *s, struct v4l2_streamparm *a)
{
	struct private_data *p = (struct private_data *)s->priv;
	struct v4l2_captureparm *cparm = &a->parm.capture;

	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );

	switch (a->type) {
	/* These are all the possible cases. */
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:
		pr_debug("   type is V4L2_BUF_TYPE_VIDEO_CAPTURE\n");
		memset(a, 0, sizeof(*a));
		a->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		cparm->capability = 0; // sensor->streamcap.capability;
		cparm->timeperframe.denominator = 0; //sensor->streamcap.timeperframe;
		cparm->timeperframe.numerator = 0; //sensor->streamcap.timeperframe;
		cparm->capturemode = 0; //sensor->streamcap.capturemode;
		break;

	default:
		pr_debug("ioctl_g_parm:type is unknown %d\n", a->type);
	case V4L2_BUF_TYPE_VIDEO_OUTPUT:
	case V4L2_BUF_TYPE_VIDEO_OVERLAY:
	case V4L2_BUF_TYPE_VBI_CAPTURE:
	case V4L2_BUF_TYPE_VBI_OUTPUT:
	case V4L2_BUF_TYPE_SLICED_VBI_CAPTURE:
	case V4L2_BUF_TYPE_SLICED_VBI_OUTPUT:
		return -EINVAL;
	}

	return 0;
}

/*!
 * ioctl_s_parm - V4L2 sensor interface handler for VIDIOC_S_PARM ioctl
 * @s: pointer to V4L2 device structure
 * @a: pointer to V4L2 VIDIOC_S_PARM ioctl structure
 *
 * Configures the sensor to use the input parameters, if possible.  If
 * not possible, reverts to the old parameters and returns the
 * appropriate error code.
 *
 * This driver cannot change these settings.
 */
static int ioctl_s_parm(struct v4l2_int_device *s, struct v4l2_streamparm *a)
{
	struct private_data *p = (struct private_data *)s->priv;
	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );

	switch (a->type) {
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:
		break;
	default:
		pr_debug("   type is unknown - %d\n", a->type);
	case V4L2_BUF_TYPE_VIDEO_OUTPUT:
	case V4L2_BUF_TYPE_VIDEO_OVERLAY:
	case V4L2_BUF_TYPE_VBI_CAPTURE:
	case V4L2_BUF_TYPE_VBI_OUTPUT:
	case V4L2_BUF_TYPE_SLICED_VBI_CAPTURE:
	case V4L2_BUF_TYPE_SLICED_VBI_OUTPUT:
		return -EINVAL;
	}

	return 0;
}

/*!
 * ioctl_queryctrl - V4L2 sensor interface handler for VIDIOC_QUERYCTRL ioctl
 * @s: pointer to V4L2 device structure
 * @qc: V4L2 VIDIOC_QUERYCTRL ioctl structure
 *
 * If the requested control is supported, returns the control information
 * from the video_control[] array.  Otherwise, returns -EINVAL if the
 * control is not supported.
 */
static int ioctl_queryctrl(struct v4l2_int_device *s,
			   struct v4l2_queryctrl *qc)
{
	int i;
	struct private_data *p = (struct private_data *)s->priv;

	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );

	for (i = 0; i < ARRAY_SIZE(adv7180_qctrl); i++)
		if (qc->id && qc->id == adv7180_qctrl[i].id) {
			memcpy(qc, &(adv7180_qctrl[i]), sizeof(*qc));
			return 0;
		}

	return -EINVAL;
}

/*!
 * ioctl_g_ctrl - V4L2 sensor interface handler for VIDIOC_G_CTRL ioctl
 * @s: pointer to V4L2 device structure
 * @vc: V4L2 VIDIOC_G_CTRL ioctl structure
 *
 * If the requested control is supported, returns the control's current
 * value from the video_control[] array.  Otherwise, returns -EINVAL
 * if the control is not supported.
 */
static int ioctl_g_ctrl(struct v4l2_int_device *s, struct v4l2_control *vc)
{
	int ret = 0;
	struct private_data *p = (struct private_data *)s->priv;

	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );

	/* Make sure power on */
	if (p->tvin_plat->pwdn)
		p->tvin_plat->pwdn(0);

	switch (vc->id) {
	case V4L2_CID_BRIGHTNESS:
		dev_dbg(&p->i2c_client->dev,"   V4L2_CID_BRIGHTNESS\n");
		vc->value = adv7180_read(p->i2c_client, ADV7180_BRIGHTNESS);
		break;
	case V4L2_CID_CONTRAST:
		dev_dbg(&p->i2c_client->dev,"   V4L2_CID_CONTRAST\n");
		vc->value = adv7180_read(p->i2c_client, ADV7180_CONTRAST);
		break;
	case V4L2_CID_SATURATION:
		dev_dbg(&p->i2c_client->dev,"   V4L2_CID_SATURATION\n");
		vc->value = adv7180_read(p->i2c_client, ADV7180_SD_SATURATION_CB);
		break;
	case V4L2_CID_HUE:
		dev_dbg(&p->i2c_client->dev,"   V4L2_CID_HUE\n");
		vc->value = adv7180_read(p->i2c_client, ADV7180_HUE); 
		break;

	default:
		dev_dbg(&p->i2c_client->dev, "   Default case\n");
		vc->value = 0;
		ret = -EPERM;
		break;
	}

	return ret;
}

/*!
 * ioctl_s_ctrl - V4L2 sensor interface handler for VIDIOC_S_CTRL ioctl
 * @s: pointer to V4L2 device structure
 * @vc: V4L2 VIDIOC_S_CTRL ioctl structure
 *
 * If the requested control is supported, sets the control's current
 * value in HW (and updates the video_control[] array).  Otherwise,
 * returns -EINVAL if the control is not supported.
 */
static int ioctl_s_ctrl(struct v4l2_int_device *s, struct v4l2_control *vc)
{
	int retval = 0;
	struct private_data *p = (struct private_data *)s->priv;
	u8 tmp;

	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );

	/* Make sure power on */
	if (p->tvin_plat->pwdn)
		p->tvin_plat->pwdn(0);

	switch (vc->id) {
	case V4L2_CID_BRIGHTNESS:
		dev_dbg(&p->i2c_client->dev,"   V4L2_CID_BRIGHTNESS\n");
		tmp = vc->value;
		adv7180_write_reg(p->i2c_client, ADV7180_BRIGHTNESS, tmp);
		break;
	case V4L2_CID_CONTRAST:
		dev_dbg(&p->i2c_client->dev,"   V4L2_CID_CONTRAST\n");
		tmp = vc->value;
		adv7180_write_reg(p->i2c_client, ADV7180_CONTRAST, tmp);
		break;
	case V4L2_CID_SATURATION:
		dev_dbg(&p->i2c_client->dev,"   V4L2_CID_SATURATION\n");
		tmp = vc->value;
		adv7180_write_reg(p->i2c_client, ADV7180_SD_SATURATION_CB, tmp);
		adv7180_write_reg(p->i2c_client, ADV7180_SD_SATURATION_CR, tmp);
		break;
	case V4L2_CID_HUE:
		dev_dbg(&p->i2c_client->dev,"   V4L2_CID_HUE\n");
		tmp = vc->value,
		adv7180_write_reg(p->i2c_client, ADV7180_HUE, tmp);
		break;

	case V4L2_CID_CHROMA_GAIN:
	case V4L2_CID_CHROMA_AGC:
	default:
		dev_dbg(&p->i2c_client->dev,"   Default case\n");
		retval = -EPERM;
		break;
	}

	return retval;
}

/*!
 * ioctl_g_chip_ident - V4L2 sensor interface handler for
 *			VIDIOC_DBG_G_CHIP_IDENT ioctl
 * @s: pointer to V4L2 device structure
 * @id: pointer to int
 *
 * Return 0.
 */
static int ioctl_g_chip_ident(struct v4l2_int_device *s, int *id)
{
	((struct v4l2_dbg_chip_ident *)id)->match.type = V4L2_CHIP_MATCH_I2C_DRIVER;
	strcpy(((struct v4l2_dbg_chip_ident *)id)->match.name, "adv7180_decoder");
	((struct v4l2_dbg_chip_ident *)id)->ident = V4L2_IDENT_ADV7180;

	return 0;
}

/*!
 * ioctl_init - V4L2 sensor interface handler for VIDIOC_INT_INIT
 * @s: pointer to V4L2 device structure
 */
static int ioctl_init(struct v4l2_int_device *s)
{
	struct private_data *p = (struct private_data *)s->priv;
	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );
	return 0;
}

/*!
 * ioctl_dev_init - V4L2 sensor interface handler for vidioc_int_dev_init_num
 * @s: pointer to V4L2 device structure
 *
 * Initialise the device when slave attaches to the master.
 */
static int ioctl_dev_init(struct v4l2_int_device *s)
{
	struct private_data *p = (struct private_data *)s->priv;
	dev_dbg(&p->i2c_client->dev, "%s@%d\n", __func__, __LINE__ );
	adv7180_init_device(s, p->tvin_plat->cvbs);
	return 0;
}

/*!
 * ioctl_int_enum_frameintervals - V4L2 sensor interface handler for
 *			   VIDIOC_INT_ENUM_FRAMEINTERVALS ioctl
 * @s: pointer to V4L2 device structure
 * @fsize: V4L2 VIDIOC_INT_ENUM_FRAMEINTERVAL ioctl structure
 *
 * Return 0 if successful, otherwise -EINVAL.
 */
static int ioctl_int_enum_frameintervals(struct v4l2_int_device *s, struct v4l2_frmivalenum *f)
{
	int retval = 0;
	struct private_data *p = (struct private_data *)s->priv;
	dev_dbg(&p->i2c_client->dev, "%s@%d (idx %d)\n", __func__, __LINE__, f->index );
	if (f->index == 0) {
		f->type = V4L2_FRMIVAL_TYPE_DISCRETE;
		f->pixel_format = V4L2_PIX_FMT_UYVY;
#ifdef USE_RAW
		f->width = video_fmts[p->video_idx].raw_width;
		f->height = video_fmts[p->video_idx].raw_height;
#else
		f->width = video_fmts[p->video_idx].active_width;
		f->height = video_fmts[p->video_idx].active_height;
#endif
		f->discrete.numerator = FRAMERATE_NUMERATOR;
		f->discrete.denominator = FRAMERATE_DENOMITATOR;
	} else {
		retval = -EINVAL;
	}
	return retval;
}

/*!
 * ioctl_enum_framesizes - V4L2 sensor interface handler for
 *			   VIDIOC_ENUM_FRAMESIZES ioctl
 * @s: pointer to V4L2 device structure
 * @fsize: V4L2 VIDIOC_ENUM_FRAMESIZES ioctl structure
 *
 * Return 0 if successful, otherwise -EINVAL.
 */
static int ioctl_enum_framesizes(struct v4l2_int_device *s, struct v4l2_frmsizeenum *fsize)
{
	int retval = 0;
	struct private_data *p = (struct private_data *)s->priv;
	dev_dbg(&p->i2c_client->dev, "%s@%d (idx %d)\n", __func__, __LINE__, fsize->index);
	if (fsize->index == 0) {
		fsize->pixel_format = V4L2_PIX_FMT_UYVY;
		fsize->type = V4L2_FRMSIZE_TYPE_DISCRETE;
#ifdef USE_RAW
		fsize->discrete.width = video_fmts[p->video_idx].raw_width;
		fsize->discrete.height = video_fmts[p->video_idx].raw_height;
#else
		fsize->discrete.width = video_fmts[p->video_idx].active_width;
		fsize->discrete.height = video_fmts[p->video_idx].active_height;
#endif
	} else {
		retval = -EINVAL;
	}
	return retval;
}

/*!
 * ioctl_enum_fmt_cap - V4L2 sensor interface handler for VIDIOC_ENUM_FMT
 * @s: pointer to V4L2 device structure
 * @fmt: pointer to V4L2 fmt description structure
 *
 * Return 0.
 */
static struct v4l2_fmtdesc adv7180_fmtdesc = {
	.index = 0,
	.type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
	.description = "YUV 4:2:2",
	.pixelformat = V4L2_PIX_FMT_UYVY,
};

static int ioctl_enum_fmt_cap(struct v4l2_int_device *s, struct v4l2_fmtdesc *fmt)
{
	int retval = 0;
	struct private_data *p = (struct private_data *)s->priv;
	dev_dbg(&p->i2c_client->dev, "%s@%d (idx %d)\n", __func__, __LINE__, fmt->index);
        if (fmt->index == 0) {
		fmt->type = adv7180_fmtdesc.type;
		fmt->flags = 0;
		strcpy( fmt->description, adv7180_fmtdesc.description );
		fmt->pixelformat = adv7180_fmtdesc.pixelformat;
	} else {
                retval = -EINVAL;
	}
	return retval;
}

/*!
 * ioctl_s_fmt_cap - V4L2 sensor interface handler for ioctl_s_fmt_cap
 * 		     set camera output format and resolution format
 *
 * @s: pointer to V4L2 device structure
 * @arg: pointer to parameter, according this to set camera
 *
 * Returns 0 if set succeed, else return -1
 */
static int ioctl_s_fmt_cap(struct v4l2_int_device *s, struct v4l2_format *f)
{
	int retval = 0;
	struct private_data *p = (struct private_data *)s->priv;
	struct v4l2_pix_format *pix = &f->fmt.pix;

	if ( f->type == V4L2_BUF_TYPE_VIDEO_CAPTURE ) {
		v4l2_std_id std;
		adv7180_get_std(s, &std);

		pr_debug("%s: Before\n", __func__ );
		DUMP_V4L2PIXFORMAT(pix);

#ifdef USE_RAW
		pix->width = video_fmts[p->video_idx].raw_width;
		pix->height = video_fmts[p->video_idx].raw_height;
#else
		pix->width = video_fmts[p->video_idx].active_width;
		pix->height = video_fmts[p->video_idx].active_height;
#endif
		pix->pixelformat = V4L2_PIX_FMT_UYVY;
		pix->field = p->field;
		pix->bytesperline = pix->width * 2;
		pix->sizeimage = pix->bytesperline * pix->height;
		pix->colorspace = V4L2_COLORSPACE_SMPTE170M;
		pix->priv = 1;

		pr_debug("%s: After\n", __func__ );
		DUMP_V4L2PIXFORMAT(pix);
	} else {
		retval = -EINVAL;
	}
	return retval;
}

/*!
 * ioctl_g_fmt_cap - V4L2 sensor interface handler for ioctl_g_fmt_cap
 * @s: pointer to V4L2 device structure
 * @f: pointer to V4L2 v4l2_format structure
 *
 * Returns the sensor's current pixel format in the v4l2_format
 * parameter.
 */
static int ioctl_g_fmt_cap(struct v4l2_int_device *s, struct v4l2_format *f)
{
	int retval = 0;
	struct private_data *p = (struct private_data *)s->priv;
	struct v4l2_pix_format *pix = &f->fmt.pix;

	switch (f->type) {
	case V4L2_BUF_TYPE_VIDEO_CAPTURE: {
		v4l2_std_id std;
dev_dbg(&p->i2c_client->dev, "%s@%d V4L2_BUF_TYPE_VIDEO_CAPTURE\n", __func__, __LINE__);
		adv7180_get_std(s, &std);
#ifdef USE_RAW
		pix->width = video_fmts[p->video_idx].raw_width;
		pix->height = video_fmts[p->video_idx].raw_height;
#else
		pix->width = video_fmts[p->video_idx].active_width;
		pix->height = video_fmts[p->video_idx].active_height;
#endif
		pix->pixelformat = V4L2_PIX_FMT_UYVY;
		pix->field = p->field;
		pix->bytesperline = pix->width * 2;
		pix->sizeimage = pix->bytesperline * pix->height;
		pix->colorspace = V4L2_COLORSPACE_SMPTE170M;
		pix->priv = 1;
		DUMP_V4L2PIXFORMAT(pix);
		break;
	}
	case V4L2_BUF_TYPE_PRIVATE: {
		v4l2_std_id std;
dev_dbg(&p->i2c_client->dev, "%s@%d V4L2_BUF_TYPE_PRIVATE\n", __func__, __LINE__ );
		adv7180_get_std(s, &std);
		f->fmt.pix.pixelformat = (u32)std;
		break;
	}
	default:
pr_debug( "%s:%s@%d: type %d\n", IF_NAME, __func__, __LINE__, f->type );
		retval = -EINVAL;
		break;
	}

	return retval;
}

/*!
 * This structure defines all the ioctls for this module.
 */
static struct v4l2_int_ioctl_desc adv7180_ioctl_desc[] = {

	{vidioc_int_dev_init_num, (v4l2_int_ioctl_func*)ioctl_dev_init},

	/*!
	 * Delinitialise the dev. at slave detach.
	 * The complement of ioctl_dev_init.
	 */
/*	{vidioc_int_dev_exit_num, (v4l2_int_ioctl_func *)ioctl_dev_exit}, */

	{vidioc_int_s_power_num, (v4l2_int_ioctl_func*)ioctl_s_power},

	{vidioc_int_g_ifparm_num, (v4l2_int_ioctl_func*)ioctl_g_ifparm},

/*	{vidioc_int_g_needs_reset_num, (v4l2_int_ioctl_func *)ioctl_g_needs_reset}, */

/*	{vidioc_int_reset_num, (v4l2_int_ioctl_func *)ioctl_reset}, */
	{vidioc_int_init_num, (v4l2_int_ioctl_func*)ioctl_init},

	{vidioc_int_queryctrl_num, (v4l2_int_ioctl_func*)ioctl_queryctrl},

	{vidioc_int_g_parm_num, (v4l2_int_ioctl_func*)ioctl_g_parm},
	{vidioc_int_s_parm_num, (v4l2_int_ioctl_func*)ioctl_s_parm},

	{vidioc_int_g_ctrl_num, (v4l2_int_ioctl_func*)ioctl_g_ctrl},
	{vidioc_int_s_ctrl_num, (v4l2_int_ioctl_func*)ioctl_s_ctrl},

	{vidioc_int_enum_framesizes_num, (v4l2_int_ioctl_func *)ioctl_enum_framesizes},
	{vidioc_int_enum_frameintervals_num, (v4l2_int_ioctl_func *)ioctl_int_enum_frameintervals},

	{vidioc_int_enum_fmt_cap_num, (v4l2_int_ioctl_func *)ioctl_enum_fmt_cap},
	{vidioc_int_g_fmt_cap_num, (v4l2_int_ioctl_func*)ioctl_g_fmt_cap},
	{vidioc_int_s_fmt_cap_num, (v4l2_int_ioctl_func *)ioctl_s_fmt_cap},
	{vidioc_int_try_fmt_cap_num, (v4l2_int_ioctl_func *)ioctl_s_fmt_cap},

	{vidioc_int_g_chip_ident_num, (v4l2_int_ioctl_func *)ioctl_g_chip_ident},
};

static struct v4l2_int_slave adv7180_slave = {
	.ioctls = adv7180_ioctl_desc,
	.num_ioctls = ARRAY_SIZE(adv7180_ioctl_desc),
};

static struct v4l2_int_device adv7180_int_device = {
	.module = THIS_MODULE,
	.name = "adv7180",
	.type = v4l2_int_type_slave,
	.u = {
		.slave = &adv7180_slave,
	},
};

static int adv7180_procDump_std( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	struct private_data *p = (struct private_data *)data_param;
	struct v4l2_int_device *s = p->v4l2_int_device;
	v4l2_std_id std;

	adv7180_get_std(s, &std);
	if ( std == V4L2_STD_PAL ) {
		len += sprintf( buf+len, "PAL" );
	} else if ( std == V4L2_STD_NTSC ) {
		len += sprintf( buf+len, "NTSC" );
	} else {
		len += sprintf( buf+len, "no-lock" );
	}

	len += sprintf( buf+len, "\n");
	*eof = 1;
	return len;
}


/***********************************************************************
 * I2C client and driver.
 ***********************************************************************/

/*! ADV7180 Reset function.
 *
 *  @return		None.
 */
static void adv7180_init_device(struct v4l2_int_device *s, bool cvbs)
{
	struct private_data *p = (struct private_data *)s->priv;
	u8 tmp;
	pr_debug("In adv7180:adv7180_init_device\n");

// 0x00 is autodetect on Euro-PAL and NTSC-J
// 0x10 is autodetect on Euro-PAL and NTSC-M
	if (p->tvin_plat->mode) {
		adv7180_write_reg(p->i2c_client, ADV7180_INPUT_CTL, p->tvin_plat->mode);
	} else if (cvbs) {
		/* Set CVBS input on AIN1 */
		adv7180_write_reg(p->i2c_client, ADV7180_INPUT_CTL, 0x00);
	} else {
		/*
		 * Set YPbPr input on AIN1,4,5 and normal
		 * operations(autodection of all stds).
		 */
		adv7180_write_reg(p->i2c_client, ADV7180_INPUT_CTL, 0x09);
	}

	// set in-lock detection to 1000
	tmp = adv7180_read(p->i2c_client, ADV7180_LOCK_COUNT);
	tmp &= 0x7;
	tmp |= 0x6;
	adv7180_write_reg(p->i2c_client, ADV7180_LOCK_COUNT, tmp);

	// the following settings are recommended by the datasheet
	// for CVBS input on the BCPZ package
	adv7180_write_reg(p->i2c_client, ADV7180_INPUT_CTL, 0x10);
	adv7180_write_reg(p->i2c_client, ADV7180_EXT_OUTPUT_CTL, 0x57);
	adv7180_write_reg(p->i2c_client, ADV7180_SHAPING_FILTER_CTL_1, 0x41);
	adv7180_write_reg(p->i2c_client, ADV7180_VSYNC_FIELD_CTL_1, 0x02);
	adv7180_write_reg(p->i2c_client, ADV7180_MANUAL_WIN_CTL, 0xa2);

	// these registers/value are undocumented
	adv7180_write_reg(p->i2c_client, 0x3e, 0x6a);
	adv7180_write_reg(p->i2c_client, 0x3f, 0xa0);
	adv7180_write_reg(p->i2c_client, ADV7180_ADI_CTL_1, 0x80);
	adv7180_write_reg(p->i2c_client, 0x55, 0x81);
	adv7180_write_reg(p->i2c_client, ADV7180_ADI_CTL_1, 0x00);
}

/*! ADV7180 I2C attach function.
 *
 *  @param *adapter	struct i2c_adapter *.
 *
 *  @return		Error code indicating success or failure.
 */

/*!
 * ADV7180 I2C probe function.
 * Function set in i2c_driver struct.
 * Called by insmod.
 *
 *  @param *adapter	I2C adapter descriptor.
 *
 *  @return		Error code indicating success or failure.
 */
static int adv7180_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int rev_id;
	int ret = 0;
	struct private_data *p = adv7180_private_data;

	p->tvin_plat = client->dev.platform_data;
	p->i2c_client = client;
	p->on = true;
	p->video_idx = ADV7180_NTSC;

	adv7180_int_device.priv = (void *)p;
	p->v4l2_int_device = &adv7180_int_device;

	//printk(KERN_DEBUG "%s@%d\n",__func__,__LINE__);

	if (p->tvin_plat->platform_init)
		p->tvin_plat->platform_init();

	if (p->tvin_plat->dvddio_reg) {
		p->dvddio_regulator =
		    regulator_get(&client->dev, p->tvin_plat->dvddio_reg);
		if (!IS_ERR_VALUE((unsigned long)p->dvddio_regulator)) {
			regulator_set_voltage(p->dvddio_regulator, 3300000, 3300000);
			if (regulator_enable(p->dvddio_regulator) != 0)
				return -ENODEV;
		}
	}

	if (p->tvin_plat->dvdd_reg) {
		p->dvdd_regulator =
		    regulator_get(&client->dev, p->tvin_plat->dvdd_reg);
		if (!IS_ERR_VALUE((unsigned long)p->dvdd_regulator)) {
			regulator_set_voltage(p->dvdd_regulator, 1800000, 1800000);
			if (regulator_enable(p->dvdd_regulator) != 0)
				return -ENODEV;
		}
	}

	if (p->tvin_plat->avdd_reg) {
		p->avdd_regulator =
		    regulator_get(&client->dev, p->tvin_plat->avdd_reg);
		if (!IS_ERR_VALUE((unsigned long)p->avdd_regulator)) {
			regulator_set_voltage(p->avdd_regulator, 1800000, 1800000);
			if (regulator_enable(p->avdd_regulator) != 0)
				return -ENODEV;
		}
	}

	if (p->tvin_plat->pvdd_reg) {
		p->pvdd_regulator =
		    regulator_get(&client->dev, p->tvin_plat->pvdd_reg);
		if (!IS_ERR_VALUE((unsigned long)p->pvdd_regulator)) {
			regulator_set_voltage(p->pvdd_regulator, 1800000, 1800000);
			if (regulator_enable(p->pvdd_regulator) != 0)
				return -ENODEV;
		}
	}

	if (p->tvin_plat->pwdn)
		p->tvin_plat->pwdn(0);

	if (p->tvin_plat->reset)
		p->tvin_plat->reset();

	gpio_sensor_active();

	dev_dbg(&p->i2c_client->dev,
		"%s:adv7180 probe i2c address is 0x%02X \n",
		__func__, p->i2c_client->addr);

	/*! Read the revision ID of the tvin chip */
	rev_id = adv7180_read(p->i2c_client, ADV7180_IDENT);
	dev_dbg(&p->i2c_client->dev,
		"%s:Analog Device adv7%2X0 detected! \n", __func__,
		rev_id);

	/*! ADV7180 initialization. */
	adv7180_init_device( &adv7180_int_device, p->tvin_plat->cvbs);

	pr_debug("   type is %d (expect %d)\n",
		 adv7180_int_device.type, v4l2_int_type_slave);
	pr_debug("   num ioctls is %d\n",
		 adv7180_int_device.u.slave->num_ioctls);

	/* This function attaches this structure to the /dev/video0 device.
	 * The pointer in priv points to the mt9v111_data structure here.*/
	ret = v4l2_int_device_register(&adv7180_int_device);

	{
		char str[64];
		sprintf( str,"driver/adv7180-%02x", p->i2c_client->addr );
		p->proc_dir = proc_mkdir( str, NULL );

		sprintf( str,"std" );
		create_proc_read_entry( str,
				0,
				p->proc_dir,
				adv7180_procDump_std,
				p );
	}
	return ret;
}

/*!
 * ADV7180 I2C detach function.
 * Called on rmmod.
 *
 *  @param *client	struct i2c_client*.
 *
 *  @return		Error code indicating success or failure.
 */
static int adv7180_detach(struct i2c_client *client)
{
	struct private_data *p = adv7180_private_data;

	dev_dbg(&client->dev,
		"%s:Removing %s video decoder @ 0x%02X from adapter %s \n",
		__func__, IF_NAME, client->addr << 1, client->adapter->name);

	if (p->tvin_plat->pwdn)
		p->tvin_plat->pwdn(1);

	if (p->dvddio_regulator) {
		regulator_disable(p->dvddio_regulator);
		regulator_put(p->dvddio_regulator);
	}

	if (p->dvdd_regulator) {
		regulator_disable(p->dvdd_regulator);
		regulator_put(p->dvdd_regulator);
	}

	if (p->avdd_regulator) {
		regulator_disable(p->avdd_regulator);
		regulator_put(p->avdd_regulator);
	}

	if (p->pvdd_regulator) {
		regulator_disable(p->pvdd_regulator);
		regulator_put(p->pvdd_regulator);
	}

	v4l2_int_device_unregister(&adv7180_int_device);

	gpio_sensor_inactive();

	if (p->tvin_plat->platform_exit)
		p->tvin_plat->platform_exit();

	return 0;
}

/*!
 * ADV7180 init function.
 * Called on insmod.
 *
 * @return    Error code indicating success or failure.
 */
static __init int adv7180_init(void)
{
	u8 err = 0;

	//printk(KERN_DEBUG "%s@%d: Built %s %s\n",__func__,__LINE__, __DATE__, __TIME__ );
	adv7180_private_data = (struct private_data *)kmalloc( sizeof(struct private_data), GFP_KERNEL );
	memset(adv7180_private_data, 0, sizeof(struct private_data));

	/* Tells the i2c driver what functions to call for this driver. */
	err = i2c_add_driver(&adv7180_i2c_driver);
	if (err != 0)
		pr_err("%s:driver registration failed, error=%d \n",
			__func__, err);

	return err;
}

/*!
 * ADV7180 cleanup function.
 * Called on rmmod.
 *
 * @return   Error code indicating success or failure.
 */
static void __exit adv7180_clean(void)
{
	pr_debug("In adv7180_clean\n");
	i2c_del_driver(&adv7180_i2c_driver);

}

module_init(adv7180_init);
module_exit(adv7180_clean);

MODULE_AUTHOR("Freescale Semiconductor");
MODULE_DESCRIPTION("Anolog Device ADV7180 video decoder driver");
MODULE_LICENSE("GPL");
