//#define DEBUG

#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/phy.h>
#include <linux/if_ether.h>
#include <linux/if_swcmd.h>
#include <linux/if_vlan.h>
#include <linux/proc_fs.h>
//#include <linux/bpdu.h>
#include <linux/netdevice.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <asm/uaccess.h>
#include <linux/log2.h>
#include <net/marvellswitch_platform.h>
#include "marvell6161.h"

#pragma pack(1)

/** local defines **********************************************************/
#ifndef BIT
	#define BIT(x) (1<<(x))
#endif
#ifndef MAX
	#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif
#ifndef MIN
	#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif    
#ifndef MASK
	#define MASK(x,y) ((BIT((MAX((x),(y)))+1)-1)^(BIT(MIN((x),(y)))-1))
#endif
#ifndef BITS_SET
	#define BITS_SET(x,y)   (((x)&(y))==(y))
#else
	#warning BITS_SET already defined!    
#endif
#ifndef BITS_CLEAR
	#define BITS_CLEAR(x,y)   (((x)&(y))==0)
#else
	#warning BITS_CLEAR already defined!    
#endif
#ifdef DEBUG
	#define db_printf   printk
#else
	#define db_printf(...)
#endif    

enum {
	PHYOPT_PHYINIT_BIT = 0,
	PHYOPT_DEFAULT_INIT_BIT,
	PHYOPT_PACKETTAGGING_BIT,
	PHYOPT_VLAN_BIT,
};

#define PHYOPT_PHYINIT          BIT(PHYOPT_PHYINIT_BIT)
#define PHYOPT_DEFAULT_INIT     (BIT(PHYOPT_DEFAULT_INIT_BIT)-1)
//#define PHYOPT_PACKETTAGGING    BIT(PHYOPT_PACKETTAGGING_BIT)
//#define PHYOPT_VLAN             BIT(PHYOPT_VLAN_BIT)
#define PHYOPT_ATU_ACCESS
#define PHYOPT_TEMPERATURE_SENSOR


#define ERRATA_PPULINKUPDATEINTERVAL_MS 1000 // supposed to be 500, but so what.
#define DEFAULT_PORTSTATE	SPCR_PortState_Forwarding
#ifndef NUMBER_OF_SWITCHES
	#define NUMBER_OF_SWITCHES 1
#endif
#ifndef SWITCH_TIMEOUT
	#define SWITCH_TIMEOUT 1
#endif

/** local defines **********************************************************/
#define ERRATA_PPULINKUPDATEINTERVAL_MS 1000 // supposed to be 500, but so what.
#define SWITCHID_6161 (unsigned short)0x1610
#define SWITCHID_6123 (unsigned short)0x1210
#define SWITCHID_6352 (unsigned short)0x3520


/** local variables ********************************************************/
static const char *port_state_str[7] = {
	"DISABLED",
	"BLOCKED",
	"LEARNING",
	"FORWARDING",
	"FORCE MDI",
	"FORCE MDIX",
	"AUTO MDI/X"

};

static const char  *NamesSwitchRegs[] = {
	"PortStat    ",					    /*  00  */
	"PCS Cntrl   ",					    /*  01  */
	"JammingCntrl",					    /*  02  */
	"Switch ID   ",					    /*  03  */
	"PortCntrl   ",					    /*  04  */
	"PortCntrl 1 ",					    /*  05  */
	"PrtBasedVLAN",					    /*  06  */
	"VlanID&Pri  ",					    /*  07  */
	"PortCntrl2  ",					    /*  08  */
	"E-RateCntrl ",					    /*  09  */
	"E-RateCntrl2",					    /*  10  */
	"PrtAssocVect",					    /*  11  */
	"PortATUCntrl",					    /*  12  */
	"PriorityOvrd",					    /*  13  */
	"Policy Ctrl ",					    /*  14  */
	"Port E Type ",					    /*  15  */
	"InDiscardsLo",					    /*  16  */
	"InDiscardsHi",					    /*  17  */
	"InFiltrdCntr",					    /*  18  */
	"OutFiltrdCnt",					    /*  19  */
	"Reserved  20",					    /*  20  */
	"Reserved  21",					    /*  21  */
	"Reserved  22",					    /*  22  */
	"Reserved  23",					    /*  23  */
	"TagRemap 3:0",					    /*  24  */
	"TagRemap 7:4",					    /*  25  */
	"Reserved  26",					    /*  26  */
	"QueCntrReg  ",					    /*  27  */
	"Reserved  28",					    /*  28  */
	"Reserved  29",					    /*  29  */
	"Reserved  30",					    /*  30  */
	"Reserved  31",					    /*  31  */
};
//#define NUM_SWITCHPORT_REGS    (sizeof(NamesSwitchRegs)/sizeof(NamesSwitchRegs[0]))
#define NUM_SWITCHPORT_REGS 28 // dont want to see the rest

static const char  *NamesGlobalRegs[] = {
	"GlobalStatus",					    /*  00  */
	"ATU FID Reg ",					    /*  01  */
	"VTU FID Reg ",					    /*  02  */
	"VTU SID Reg ",					    /*  03  */
	"GlobalCntrl ",					    /*  04  */
	"VTUOperation",					    /*  05  */
	"VTU VID Reg ",					    /*  06  */
	"VTUDataPrt03",					    /*  07  */
	"VTUDataPrt45",					    /*  08  */
	"VTUDataVTUOp",					    /*  09  */
	"ATUCntrl    ",					    /*  10  */
	"ATUOperation",					    /*  11  */
	"ATUData     ",					    /*  12  */
	"ATU MAC 0&1 ",					    /*  13  */
	"ATU MAC 2&3 ",					    /*  14  */
	"ATU MAC 4&5 ",					    /*  15  */
	"IP PRI 0    ",					    /*  16  */
	"IP PRI 1    ",					    /*  17  */
	"IP PRI 2    ",					    /*  18  */
	"IP PRI 3    ",					    /*  19  */
	"IP PRI 4    ",					    /*  20  */
	"IP PRI 5    ",					    /*  21  */
	"IP PRI 6    ",					    /*  22  */
	"IP PRI 7    ",					    /*  23  */
	"IEEE-PRI    ",					    /*  24  */
	"Reserved,   ",					    /*  25  */
	"MonitorCntrl",					    /*  26  */
	"TotalFreeCnt",					    /*  27  */
	"ClobalCntrl2",					    /*  28  */
	"StatsOperat ",					    /*  29  */
	"StatsData3:2",					    /*  30  */
	"StatsData1:0"					    /*  31  */
};
#define NUM_GLOBAL_REGS    (sizeof(NamesGlobalRegs)/sizeof(NamesGlobalRegs[0]))

static const char  *NamesGlobal2Regs[] = {
	"Intr Source ",					    /*  00  */
	"Intr Mask   ",					    /*  01  */
	"MGMTEnable2x",					    /*  02  */
	"MGMTEnable0x",					    /*  03  */
	"FlowCntrlDly",					    /*  04  */
	"SwitchMGMT  ",					    /*  05  */
	"DevMapTablRg",					    /*  06  */
	"TrunkMaskTbl",					    /*  07  */
	"TrunkMmbrTbl",					    /*  08  */
	"InRateCmdReg",					    /*  09  */
	"InRateDatReg",					    /*  10  */
	"CrsChpPtVLAN",					    /*  11  */
	"CrsChpPtVDat",					    /*  12  */
	"Switch MAC  ",					    /*  13  */
	"ATU Stats   ",					    /*  14  */
	"PriOvrdTable",					    /*  15  */
	"Reserved,   ",					    /*  16  */
	"Reserved,   ",					    /*  17  */
	"Reserved,   ",					    /*  18  */
	"Reserved,   ",					    /*  19  */
	"Reserved,   ",					    /*  20  */
	"Reserved,   ",					    /*  21  */
	"Reserved,   ",					    /*  22  */
	"Reserved,   ",					    /*  23  */
	"SMI PHY Cmd ",					    /*  24  */
	"SMI PHY Data",					    /*  25  */
	"Reserved,   ",					    /*  26  */
	"WtchDogCntrl",					    /*  27  */
	"Reserved,   ",					    /*  28  */
	"SDETPolarity",					    /*  29  */
	"Reserved  30",					    /*  30  */
	"Reserved  31",					    /*  31  */
};
#define NUM_GLOBAL2_REGS (sizeof(NamesGlobal2Regs)/sizeof(NamesGlobal2Regs[0]))

static const char *NamesPIRLRegs[] __attribute__((unused)) = {
	"BucketConf#0",					/* 00 */
	"BucketConf#1",					/* 01 */
	"BucketConf#2",					/* 02 */
	"BucketConf#3",					/* 03 */
	"BucketConf#4",					/* 04 */
	"BucketConf#5",					/* 05 */
	"BucketConf#6",					/* 06 */
	"BucketConf#7",					/* 07 */
};
#define NUM_PIRL_REGS (sizeof(NamesPIRLRegs)/sizeof(NamesPIRLRegs[0]))

static const char *NamesPTPRegs[] __attribute__((unused)) = {
	"PTP EthType ",					/* 00 */
	"PTP MsgID   ",					/* 01 */
	"PTPTimeStamp",					/* 02 */
	"PTPPortArrIE",					/* 03 */
	"PTPPortDptIE",					/* 04 */
	"PTPGlblConf ",					/* 05 */
	"reserved  06",					/* 06 */
	"reserved  07",					/* 07 */
	"PTP Interupt",					/* 08 */
	"GlblTime 1&0",					/* 09 */
	"GlblTime 3&2",					/* 10 */
};
#define NUM_PTP_REGS (sizeof(NamesPTPRegs)/sizeof(NamesPTPRegs[0]))

static const char *NamesPTPStatRegs[] __attribute__((unused)) = {
	"ArTm0PrtSta",					/* 00 */
	"ArTm0Byt1&0",					/* 01 */
	"ArTm0Byt3&2",					/* 02 */
	"Ar0 Seq ID ",					/* 03 */
	"ArTm1PrtSta",					/* 04 */
	"ArTm1Byt1&0",					/* 05 */
	"ArTm1Byt3&2",					/* 06 */
	"Ar1 Seq ID ",					/* 07 */
	"DptTm Stat ",					/* 08 */
	"DptTmByt1&0",					/* 09 */
	"DptTmByt3&2",					/* 10 */
	"Dpt Seq ID ",					/* 11 */
	"reserved 12",					/* 12 */
	"Port Status",					/* 13 */
};
#define NUM_PTPSTAT_REGS (sizeof(NamesPTPStatRegs)/sizeof(NamesPTPStatRegs[0]))

static const char  *NamesPhyRegs[] = {
	"PHY Cntrl   ",					    /*  00  */
	"PHY Status  ",					    /*  01  */
	"PHY Ident 1 ",					    /*  02  */
	"PHY Ident 2 ",					    /*  03  */
	"AutoNeg Ad  ",					    /*  04  */
	"LinkPartner ",					    /*  05  */
	"AutoNegExpan",					    /*  06  */
	"NextPage    ",					    /*  07  */
	"LnkPrtnrNext",					    /*  08  */
	"M/S Cntrl   ",					    /*  09  */
	"M/S Status  ",					    /*  0A  */
	"Reserved    ",					    /*  0B  */
	"Reserved    ",					    /*  0C  */
	"Reserved    ",					    /*  0D  */
	"Reserved    ",					    /*  0E  */
	"ExtendedStat",					    /*  0F  */
	"PHYSpcCntrl1",					    /*  10  */
	"PHY Status 1",					    /*  11  */
	"PHY Intr En ",					    /*  12  */
	"PHY Status 2",					    /*  13  */
	"PHY Cntrl 3 ",					    /*  14  */
	"RcvErrorCntr",					    /*  15  */
	"Page Address",					    /*  16  */
	"Intr Status ",					    /*  17  */
	"Vendor 0x18 ",					    /*  18  */
	"Vendor 0x19 ",					    /*  19  */
	"Vendor 0x1A ",					    /*  1A  */
	"Vendor 0x1B ",					    /*  1B  */
	"PHY Cntrl 2 ",					    /*  1C  */
	"Vendor 0x1D ",					    /*  1D  */
	"Vendor 0x1E ",					    /*  1E  */
	"Vendor 0x1F "					    /*  1F  */
};
#define NUM_PHY_REGS    (sizeof(NamesPhyRegs)/sizeof(NamesPhyRegs[0]))

static const char *NamesSERDESRegs[] = {
	"SERDES Cntrl",					    /*  00  */
	"SERDESStatus",					    /*  01  */
	"PHY Ident 1 ",					    /*  02  */
	"PHY Ident 2 ",					    /*  03  */
	"AutoNeg Ad  ",					    /*  04  */
	"LinkPartner ",					    /*  05  */
	"AutoNegExpan",					    /*  06  */
	"NextPage    ",					    /*  07  */
	"ExtndedStat1",					    /*  08  */
	"Reserved  09",					    /*  09  */
	"Reserved  0A",					    /*  0A  */
	"Reserved  0B",					    /*  0B  */
	"Reserved  0C",					    /*  0C  */
	"Reserved  0D",					    /*  0D  */
	"Reserved  0E",					    /*  0E  */
	"ExtndedStat2",					    /*  0F  */
	"Control Reg1",					    /*  10  */
	"Status Reg 1",					    /*  11  */
	"Interrupt En",					    /*  12  */
	"Inter Status",					    /*  13  */
	"Reserved  14",					    /*  14  */
	"RcvErrorCntr",					    /*  15  */
	"Reserved  16",					    /*  16  */
	"Reserved  17",					    /*  17  */
	"CRC Counters",					    /*  18  */
	"Packet Gen  ",					    /*  19  */
	"Control Reg2",					    /*  1A  */
	"Vendor 0x1B ",					    /*  1B  */
	"Vendor 0x1C ",					    /*  1C  */
	"Vendor 0x1D ",					    /*  1D  */
	"Vendor 0x1E ",					    /*  1E  */
	"Vendor 0x1F "					    /*  1F  */
};
#define NUM_SERDES_REGS (sizeof(NamesSERDESRegs)/sizeof(NamesSERDESRegs[0]))

static const char  *NamesPortCounters[] = {
	"InGoodOctetsLo     ",	   /*  00  */
	"InGoodOctetsHi     ",	   /*  01  */
	"InBadOctets        ",	   /*  02  */
	"OutFCSErr          ",	   /*  03  */
	"InUniCast          ",	   /*  04  */
	"Deferred           ",	   /*  05  */
	"InBroadcasts       ",	   /*  06  */
	"InMulticasts       ",	   /*  07  */
	"Frm64Octs          ",	   /*  08  */
	"Frm_65_127_Oct     ",	   /*  09  */
	"Frm_128_255_Oct    ",	   /*  A	*/
	"Frm_256_to_511_Oct ",	   /*  B	*/
	"Frm_512_to_1023_Oct",	   /*  C	*/
	"Frm_1024_to_Max_Oct",	   /*  D	*/
	"OutOctetsLo        ",	   /*  E	*/
	"OutOctetsHigh      ",	   /*  F	*/
	"OutUnicast         ",	   /*  10	*/
	"Excessive          ",	   /*  11	*/
	"OutMuiltcast       ",	   /*  12	*/
	"OutBroadcast       ",	   /*  13	*/
	"Single             ",	   /*  14	*/
	"OutPause           ",	   /*  15	*/
	"InPause            ",	   /*  16	*/
	"Multiple           ",	   /*  17	*/
	"InUndersize        ",	   /*  18	*/
	"InFragments        ",	   /*  19	*/
	"InOversize         ",	   /*  1A	*/
	"InJabber           ",	   /*  1B	*/
	"InRxErr            ",	   /*  1C	*/
	"InFCSErr           ",	   /*  1D	*/
	"Collisions         ",	   /*  1E	*/
	"LateColls          ",	   /*  1F	*/
};
#define NUM_PORT_COUNTERS    (sizeof(NamesPortCounters)/sizeof(NamesPortCounters[0]))

struct marvell_private_data_t;

typedef struct marvell_procDump_data_t {
	struct marvell_private_data_t *priv;
	char		name[32];
	char		description[32];
	unsigned short	phy_start;
	unsigned short	phy_count;
	unsigned short	reg_start;
	unsigned short	reg_count;
	bool		ppu_disable;
} marvell_procDump_data_t;

typedef struct marvel_statDump_data_t {
	struct marvell_private_data_t *priv;
	char		name[32];
	char		description[32];
	marvell_PortCounters_t portStats;
	unsigned short	port_num;
} marvell_statDump_data_t;

struct marvell_private_data_t {
	struct list_head list; // for multiple instances
	marvell_procDump_data_t *procDump_entries;
	marvell_statDump_data_t *statDump_entries;
	struct mii_bus *mii_bus;
	int flags;
	int switch_smida;
	int switch_identifier;
	int management_port;
	int port_mask;
	int port_count;
	struct proc_dir_entry *proc_dir;
} marvell_private_data_t;

typedef struct packet_header_TYPE {
	unsigned char   h_dest[ETH_ALEN];	   /* destination eth addr	*/
	unsigned char   h_source[ETH_ALEN];	   /* source ether addr	*/
	unsigned short  h_proto;
	unsigned char  DSAP;
	unsigned char  SSAP;
	unsigned char  cntl;
	unsigned short protocol_id    __attribute__ ((packed));
	unsigned char  protocol_vid;
	unsigned char  bpdu_type;
} packet_header_TYPE;

typedef struct vlan_packet_header_TYPE {
	unsigned char   h_dest[ETH_ALEN];	   /* destination eth addr	*/
	unsigned char   h_source[ETH_ALEN];	   /* source ether addr	*/
	unsigned short  h_proto;
	unsigned short  h_vlan_TCI;		   /* Encapsulates priority and VLAN ID */
	unsigned short  h_vlan_encapsulated_proto; /* packet type ID field (or len) */
	unsigned char  DSAP;
	unsigned char  SSAP;
	unsigned char  cntl;
	unsigned short protocol_id    __attribute__ ((packed));
	unsigned char  protocol_vid;
	unsigned char  bpdu_type;
} vlan_packet_header_TYPE;

/** local function prototypes **********************************************/
static const char *print_phyRegisterName( SMI_DeviceAddress_t phy_param, SMI_Register_t reg_param );
static int print_marvellSmiRegs_bylist( void *, char *, SMI_DeviceAddress_t, unsigned short, SMI_Register_t, unsigned short );
static int marvell_procDump_regs( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param );
static int marvell_procDump_counters( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param );
static int marvell_UpdatePort_Counters(marvell_statDump_data_t *);

static void marvell_protected_write( void *private_param, unsigned short phy_param, unsigned short reg_param, unsigned short data_param );
static unsigned short marvell_read( void *private_param, unsigned short phy_param, unsigned short reg_param  );
static void marvell_write( void *private_param, unsigned short phy_param, unsigned short reg_param, unsigned short data_param );

static int marvell_find( struct marvell_private_data_t *private );
static int marvell_port_control( void *, int port_param, int state_param );
static int marvell_set_portMode( void *, int port, port_mode_t mode );
static int marvell_ppu_control( void *private_param, int mask_param, int state_param );

#if defined(PHYOPT_PACKETTAGGING)
int marvell_tag_insert( void *, struct sk_buff **skb );
int marvell_tag_remove( void *, struct sk_buff *skb );
#endif
#if defined(PHYOPT_ATU_ACCESS)
static void marvell_atuWrite( void *, unsigned short atu_op, atuEntry_t *entry_param );
static void marvell_atuRead( void *, atuEntry_t *entry_param ) __attribute__ ((unused));
static int marvell_procDump_atu( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param );
#endif

int marvell_m88e6352_status( struct phy_device *phydev );
int marvell_m88e6352_config_aneg( struct phy_device *phydev );
int marvell_m88e6352_config_init( struct phy_device *phydev );
int marvell_set_config( void *, struct ifmap *map );
int marvell_phy_ioctl( void *, int cmd, struct ifreq *ifr );

static int marvellsw_ioctl(struct inode *, struct file *, unsigned int, unsigned long);
static int marvellsw_open(struct inode *inode, struct file *filp);
static int marvellsw_release (struct inode *, struct file *);

int marvell_m88exxxx_probe(struct phy_device *phydev);


/** global variables *******************************************************/
static struct marvell_private_data_t *p_marvell_private;

static struct phy_driver marvell_m88e6161_driver = {
	.phy_id         = 0x1610,	// 0x0141 0C[B,0]0
	.phy_id_mask    = 0xfff0,	// 0xffff fff0
	.name           = "Marvell 88E6161",
//	.features = ,
//	.flags = ,

//	int (*config_init)(struct phy_device *phydev);
//	.config_init = ,
//	int (*probe)(struct phy_device *phydev);
	.probe          = marvell_m88exxxx_probe,
//	void (*remove)(struct phy_device *phydev);
//	.remove = ,

	/* PHY Power Management */
//	int (*suspend)(struct phy_device *phydev);
//	int (*resume)(struct phy_device *phydev);

	/*
	 * Configures the advertisement and resets
	 * autonegotiation if phydev->autoneg is on,
	 * forces the speed to the current settings in phydev
	 * if phydev->autoneg is off
	 */
//	int (*config_aneg)(struct phy_device *phydev);

	/* Determines the negotiated speed and duplex */
//	int (*read_status)(struct phy_device *phydev);

	/* Clears any pending interrupts */
//	int (*ack_interrupt)(struct phy_device *phydev);

	/* Enables or disables interrupts */
//	int (*config_intr)(struct phy_device *phydev);

	/*
	 * Checks if the PHY generated an interrupt.
	 * For multi-PHY devices with shared PHY interrupt pin
	 */
//	int (*did_interrupt)(struct phy_device *phydev);

	/* Clears up any memory if needed */

	.driver         = { .owner = THIS_MODULE },
};


// The NIU configuration does not have a PHY between the
// MX51 and the Marvell switch.
// As the FEC driver is expecting a PHY driver,
// this 6352 driver is used to give the FEC the illusion that
// a PHY is present.
// this SHOULD NOT be used for any other platforms!!!
static struct phy_driver marvell_m88e6352_driver = {
	.phy_id         = 0x3520,
	.phy_id_mask    = 0xfff0,
	.name           = "Marvell 88E6352",
	.features       = SUPPORTED_100baseT_Full | SUPPORTED_MII,

	.config_init    = marvell_m88e6352_config_init,
	.probe          = marvell_m88exxxx_probe,

	/* basic functions */
	.config_aneg	= marvell_m88e6352_config_aneg,
	.read_status	= marvell_m88e6352_status,

	.driver         = { .owner = THIS_MODULE },
};


/** display functions ********************************************************/
static const char *print_phyRegisterName( SMI_DeviceAddress_t phy_param, SMI_Register_t reg_param ) {
//	db_printf("%s@%d: phy_param %02X, reg_param %02X\n", __FUNCTION__, __LINE__, phy_param, reg_param );
	switch ( phy_param ) {
	case SMIDA_PHY0_REG ... SMIDA_LASTPHY_REG: {
			return NamesPhyRegs[reg_param];
		}
	case SMIDA_PHYC_REG ... SMIDA_PHYD_REG: {
			return NamesSERDESRegs[reg_param];
		}
	case SMIDA_SwitchPort0_REG ... SMIDA_LASTPORT_REG: {
			return NamesSwitchRegs[reg_param];
		}
	case SMIDA_SwitchGlobal_REG: {
			return NamesGlobalRegs[reg_param];
		}
	case SMIDA_SwitchGlobal2_REG: {
			return NamesGlobal2Regs[reg_param];
		}
	default:
		return "Unknown PHY ";
	}
}

static int print_marvellSmiRegs_bylist( void *private_param, char *buf,
					SMI_DeviceAddress_t phy_start, unsigned short phy_cnt,
					SMI_Register_t reg_start, unsigned short reg_cnt  ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	SMI_DeviceAddress_t phy_idx;
	SMI_Register_t reg_idx;
	int len = 0;

	len += sprintf( buf+len, "\n\tSMI Device Address\n               ");
	for ( phy_idx = 0; phy_idx < phy_cnt; phy_idx++ ) {
		len += sprintf( buf+len, "%5X", phy_start + phy_idx );	// Write out the COL header as a ROW
	}

	for ( reg_idx = 0; reg_idx < reg_cnt; reg_idx++ ) {
		for ( phy_idx = 0; phy_idx < phy_cnt; phy_idx++ ) {
			if ( phy_idx == 0 ) {	  // print ROW Title
				len += sprintf( buf+len, "\n%2X:%s ", reg_start + reg_idx,
					print_phyRegisterName( phy_start + phy_idx, reg_start + reg_idx ));
			}
			len += sprintf( buf+len, " %04X",
				marvell_read( private_data, phy_start + phy_idx, reg_start + reg_idx ));
		}
	}
	len += sprintf( buf+len, "\n");
	return len;
}

static int marvell_procDump_regs( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param ) {
	int len = 0;
	marvell_procDump_data_t *dd = (marvell_procDump_data_t *)data_param;

	if ( dd->ppu_disable ) {
		marvell_ppu_control( dd->priv, SGCR_PPU_ENABLE, 0 );
	}

	len += sprintf( buf+len, "\n\tPort Name: %s, Description: %s", dd->name, dd->description );
	len += print_marvellSmiRegs_bylist( 	dd->priv, buf+len,
						dd->phy_start, dd->phy_count,
						dd->reg_start, dd->reg_count );

	if ( dd->ppu_disable ) {
		marvell_ppu_control( dd->priv, SGCR_PPU_ENABLE, SGCR_PPU_ENABLE );  
	}

	len += sprintf( buf+len, "\n");
	*eof = 1;
	return len;
}

static int marvell_procWrite_page( struct file *file, const char *buffer_param, unsigned long count, void *data_param ) {
	char buffer[4];
	int page, size;
	marvell_procDump_data_t *dd = (marvell_procDump_data_t *)data_param;

	if ( count > sizeof(buffer))
		size = sizeof(buffer);
	else
		size = count;
	if ( copy_from_user( buffer, buffer_param, size )) {
		return -EFAULT;
	}
	page = simple_strtoul( buffer, NULL, 10 );
	pr_debug( "%s: set page to %d\n", __func__, page );
	
	if ( dd->ppu_disable ) {
		marvell_ppu_control( dd->priv, SGCR_PPU_ENABLE, 0 );
	}

	marvell_write( dd->priv, dd->phy_start, SIR_PageAddress_REG, page );

	if ( dd->ppu_disable ) {
		marvell_ppu_control( dd->priv, SGCR_PPU_ENABLE, SGCR_PPU_ENABLE );  
	}
	return size;
}

static int marvell_UpdatePort_Counters(marvell_statDump_data_t *sd) {
	struct marvell_private_data_t *private_data = sd->priv;
	unsigned int port_num = sd->port_num;
	unsigned int counter_num;
	unsigned short temp, timeout;

//	for ( port_num = 0; port_num < NUM_SWITCHPORTS; port_num++ )
	{
		temp = SOR_StatsBusy | SOR_StatsOp_CapAllCntPort | SOR_HM_CountALL | (SOR_StatsPtr_Mask & port_num);
		marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_StatsOperation_REG, temp );

		//poll Busy bit to see if Op is finished.
		timeout =0;
		do {
			udelay(10);
			if ( ++timeout > 500 ) {
				printk( KERN_ERR "%s@%d: timeout\n", __func__, __LINE__ );
				return -1;
			}
			temp = marvell_read(private_data, SMIDA_SwitchGlobal_REG, SGR_StatsOperation_REG);
		} while ( BITS_SET(temp, SOR_StatsBusy ));
//		db_printf("\nCounters updated @%d\n", timeout); 

		// Read Counters and place into structure.
		for (counter_num =0; counter_num < NUM_PORT_COUNTERS; counter_num++ ) {
			temp = SOR_StatsBusy | SOR_StatsOp_ReadCounter | (SOR_StatsPtr_Mask & counter_num);
			marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_StatsOperation_REG, temp);

			//poll Busy bit to see if Op is finished.
			timeout =0;
			do {
				udelay(10);
				if ( ++timeout > 500 ) {
					printk( KERN_ERR "%s@%d: timeout\n", __func__, __LINE__ );
					return -1;
				}
				temp = marvell_read(private_data, SMIDA_SwitchGlobal_REG, SGR_StatsOperation_REG);
			} while ( BITS_SET(temp, SOR_StatsBusy ));
//			db_printf("Read of Port:%d Reg:%d @%d\n", port_num, counter_num, timeout);

			sd->portStats.raw[counter_num] = ((	marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_StatsDataBytes32_REG) << 16) | 
							 	marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_StatsDataBytes10_REG) );
		}
	}
	return 0;
}

const u8 IngressCounter_List[] = {
	STATS_INGOODOCTETSLO,
	STATS_INGOODOCTETSHI,
	STATS_INBADOCTETS,
	STATS_INUNICAST,
	STATS_INBROADCASTS,
	STATS_INMULTICASTS,
	STATS_INPAUSE,
	STATS_INUNDERSIZE,
	STATS_INFRAGMENTS,
	STATS_INOVERSIZE,
	STATS_INJABBER,
	STATS_INRXERR,
	STATS_INFCSERR,
};

const u8 EgressCounter_List[] = {
	STATS_OUTOCTETSLO,
	STATS_OUTOCTETSHI,
	STATS_OUTUNICAST,
	STATS_OUTMULTICASTS,
	STATS_OUTBROADCASTS,
	STATS_OUTPAUSE,
	STATS_EXCESSIVE,
	STATS_COLLISIONS,
	STATS_DEFERRED,
	STATS_SINGLE,
	STATS_MULTIPLE,
	STATS_OUTFCSERR,
	STATS_LATE,
};

const u8 HistogramCounter_List[] = {
	STATS_64OCTETS,
	STATS_65to127OCTETS,
	STATS_128to255OCTETS,
	STATS_256to511OCTETS,
	STATS_512to1023OCTETS,
	STATS_1024toMAXOCTETS,
};

static char *scaleReallyBigNumber( u64 rbn )
{
	static char buf[16];
	char *units[] = { " B", "KB", "MB", "GB", "TB", "PB", "EB" };
	int idx;
	u32 mantissa = 0;
	for ( idx = 0; rbn > 1024; idx++ ) {
		mantissa = (rbn & 0x3ff)*1000; // bottom 10 bits
		mantissa /= 1024; // convert to base 10
		rbn >>= 10;
	}
	if ( idx > 0 ) {
		sprintf(buf," %llu.%03u%s", rbn, mantissa, units[idx] );
	} else {
		sprintf(buf," %llu%s", rbn, units[idx] );
	}
	return buf;
}	

static int marvell_procDump_counters( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param ) {
	u64 reallyBigNumber;
	marvell_statDump_data_t *sd = (marvell_statDump_data_t *)data_param;
	struct marvell_private_data_t *private_data = sd->priv;
	unsigned int port_num = sd->port_num;
	int len =0, idx;
	off_t begin =0;

	// Fill Port Counter Stats struct
	if ( marvell_UpdatePort_Counters(sd) < 0 ) {
		len += sprintf(buf+len, "Read timed out\n");
		*eof = 1;
		goto done;
	}

	len += sprintf(buf+len, "\nMarvell Link Street M88E6161 Port %d Statistics\n", port_num);
	len += sprintf(buf+len, "\tPort Name: %s, Description: %s\n", sd->name, sd->description );
	// Now that we have all the info we need dump it to the user.
//	for(counter_num=0; counter_num < NUM_PORT_COUNTERS; counter_num++ )
	{
		len += sprintf(buf+len,"\nIngress Counters:");
		reallyBigNumber = sd->portStats.raw[STATS_INGOODOCTETSHI];
		reallyBigNumber <<= 32;
		reallyBigNumber = sd->portStats.raw[STATS_INGOODOCTETSLO];
		len += sprintf(buf+len,"\n %-19s: %10s","InGoodOctets", scaleReallyBigNumber( reallyBigNumber ));
		for ( idx = 2; idx < ARRAY_SIZE(IngressCounter_List); idx++ ) {
			len += sprintf(buf+len,"\n %s: %10d", NamesPortCounters[IngressCounter_List[idx]],
				sd->portStats.raw[IngressCounter_List[idx]]);
		}
		len += sprintf(buf+len,"\n\nEgress Counters:");
		reallyBigNumber = sd->portStats.raw[STATS_OUTOCTETSHI];
		reallyBigNumber <<= 32;
		reallyBigNumber = sd->portStats.raw[STATS_OUTOCTETSLO];
		len += sprintf(buf+len,"\n %-19s: %10s","OutOctets", scaleReallyBigNumber( reallyBigNumber ));
		for ( idx = 2; idx < ARRAY_SIZE(EgressCounter_List); idx++ ) {
			len += sprintf(buf+len,"\n %s: %10d", NamesPortCounters[EgressCounter_List[idx]],
				sd->portStats.raw[EgressCounter_List[idx]]);
		}
		len += sprintf(buf+len,"\n\nHistogram Counters:");
		for ( idx = 0; idx < ARRAY_SIZE(HistogramCounter_List); idx++ ) {
			len += sprintf(buf+len,"\n %s: %10d", NamesPortCounters[HistogramCounter_List[idx]],
				sd->portStats.raw[HistogramCounter_List[idx]]);
		}

		len += sprintf(buf+len,"\n\nRMON Statistics Counters:");
		reallyBigNumber = marvell_read( private_data, SMIDA_SwitchPort0_REG + port_num, SPR_InDiscardHiFrameCounter_REG );
		reallyBigNumber <<=16;
		reallyBigNumber |= marvell_read( private_data, SMIDA_SwitchPort0_REG + port_num, SPR_InDiscardLoFrameCounter_REG );
		len += sprintf(buf+len, "\n %-19s: %10d",
			"InDiscards",
			(int)reallyBigNumber );
		len += sprintf(buf+len, "\n %-19s: %10d",
			print_phyRegisterName( SMIDA_SwitchPort0_REG + port_num, SPR_InFilteredFrameCounter_REG ),
			marvell_read( private_data, SMIDA_SwitchPort0_REG + port_num, SPR_InFilteredFrameCounter_REG ));
		len += sprintf(buf+len, "\n %-19s: %10d",
			print_phyRegisterName( SMIDA_SwitchPort0_REG + port_num, SPR_OutFilteredFrameCounter_REG ),
			marvell_read( private_data, SMIDA_SwitchPort0_REG + port_num, SPR_OutFilteredFrameCounter_REG ));


		if (len+begin > offset_param+count)
			goto done;
		if (len+begin < offset_param) {
			begin += len;
			len = 0;
		}
	}
	len += sprintf(buf+len,"\n");
	*eof = 1;
done:
	if (offset_param >= len+begin)
		return 0;
	*start = buf + (offset_param-begin);
	return ((count < begin+len-offset_param) ? count : begin+len-offset_param);
}

#ifdef PHYOPT_TEMPERATURE_SENSOR
static int marvell_procDump_temperature( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param ) {
	int len = 0, port_idx, temp, degC;
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)data_param;

	marvell_ppu_control( private_data, SGCR_PPU_ENABLE, 0 );

	len += sprintf( buf+len, "\nPHY Temperature:\n" );

	// turn on temperature sensor, registers on page 6
	for ( port_idx = 0; port_idx < NUM_PHYS; port_idx++ ) {
		marvell_write( private_data, port_idx, SIR_PageAddress_REG, 6 );
		temp = marvell_read( private_data, port_idx, SIR_Vendor_1A_REG );
		temp |= BIT(5);
		marvell_write( private_data, port_idx, SIR_Vendor_1A_REG, temp );
	}
	// wait for temperature to stabilize
	mdelay(10);
	// turn off temperature sensor
	for ( port_idx = 0; port_idx < NUM_PHYS; port_idx++ ) {
		temp = marvell_read( private_data, port_idx, SIR_Vendor_1A_REG );
		temp &= ~BIT(5);
		marvell_write( private_data, port_idx, SIR_Vendor_1A_REG, temp );
	}
	// read values
	for ( port_idx = 0; port_idx < NUM_PHYS; port_idx++ ) {
		temp = marvell_read( private_data, port_idx, SIR_Vendor_1A_REG );
		temp = (int)(temp & MASK(4,0));
		degC = (temp - 5)*5;
		len += sprintf( buf+len, "\tPort %d: %d deg C (%2d)\n", port_idx, degC, temp );
	}

	marvell_ppu_control( private_data, SGCR_PPU_ENABLE, SGCR_PPU_ENABLE );  

	len += sprintf( buf+len, "\n");
	*eof = 1;
	return len;
}
#endif

/** misc switch functions ****************************************************/
static void marvell_protected_write( void *private_param, unsigned short phy_param, unsigned short reg_param, unsigned short data_param ) {
	unsigned short register_data;
	register_data = marvell_read( private_param, phy_param, reg_param );
	if ( register_data != data_param ) {
		marvell_write( private_param, phy_param, reg_param, data_param );
	}
}

static void marvell_write( void *private_param, unsigned short phy_param, unsigned short reg_param, unsigned short data_param ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	struct mii_bus *mii_bus = private_data->mii_bus;

	unsigned short register_data;
	int tcount;

	if ( mii_bus == NULL ) {
		printk( KERN_ERR "%s@%d: uninitialized mii_bus\n", __func__, __LINE__ );
		return;
	}

	if ( private_data->switch_smida != 0 ) {
#if 0
		// sanity check; fist just see if the data register is writable. if not, no chip.
		mii_bus->write( mii_bus, private_data->switch_smida, SMIDA_SMIDATA_REG, 0xdead );
		// read data to holding register
		register_data = mii_bus->read( mii_bus, private_data->switch_smida, SMIDA_SMIDATA_REG );
		if ( register_data != 0xdead ) {
			db_printf("%s@%d: multi-mode switch not found at SMIDA %d\n", __func__, __LINE__,
				private_data->switch_smida );
			return;
		}
#endif

		// wait until SMI engine ready
		tcount = 0;
		while ( 1 ) {
			register_data = mii_bus->read( mii_bus, private_data->switch_smida, SMIDA_SMICOMMAND_REG );
			if ( 0xffff == register_data ) { // no chip?
				db_printf("%s@%d: no device at SMIDA %d\n", __func__, __LINE__, 
					private_data->switch_smida );
				return;
			}
			if ( BITS_CLEAR( register_data , SSR_SMIBUSY )) {
				break;
			}
			if ( tcount++ > 100 ) {
				printk("%s@%d: timeout\n", __func__, __LINE__ );
				return;
			}
			udelay( 10 );
		}

		// write data to holding register
		mii_bus->write( mii_bus, private_data->switch_smida, SMIDA_SMIDATA_REG, data_param );

		// write command to command register
		mii_bus->write( mii_bus, private_data->switch_smida, SMIDA_SMICOMMAND_REG, 
			SSR_SMIBUSY|SSR_SMIMODE_22|SSR_SMIOP( SSR_SMIOP_WRDATA )|
			SSR_DEVADDR( phy_param )|SSR_REGADDR( reg_param ) );
	} else {
		mii_bus->write( mii_bus, phy_param, reg_param, data_param );
	}
	pr_debug( KERN_DEBUG "%s@%d: 0x%02x:0x%02x <- 0x%04x\n", __func__, __LINE__, phy_param, reg_param, data_param );
}

static unsigned short marvell_read( void *private_param, unsigned short phy_param, unsigned short reg_param  ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	struct mii_bus *mii_bus = private_data->mii_bus;

	unsigned short register_data;
	int tcount;

	if ( mii_bus == NULL ) {
		printk( KERN_ERR "%s@%d: uninitialized mii_bus\n", __func__, __LINE__ );
		return -1;
	}

	if ( private_data->switch_smida != 0 ) {
#if 0
		// sanity check; fist just see if the data register is writable. if not, no chip.
		mii_bus->write( mii_bus, private_data->switch_smida, SMIDA_SMIDATA_REG, 0xdead );
		// read data to holding register
		register_data = mii_bus->read( mii_bus, private_data->switch_smida, SMIDA_SMIDATA_REG );
		if ( register_data != 0xdead ) {
			db_printf("%s@%d: multi-mode switch not found at SMIDA %d\n", __func__, __LINE__,
				private_data->switch_smida );
			return -1;
		}
#endif		
		// wait until SMI engine ready
		tcount = 0;
		while ( 1 ) {
			register_data = mii_bus->read( mii_bus, private_data->switch_smida, SMIDA_SMICOMMAND_REG );
			if ( 0xffff == register_data ) { // no chip?
				db_printf("%s@%d: no device at SMIDA %d\n", __func__, __LINE__,
					private_data->switch_smida );
				return -1;
			}
			if ( BITS_CLEAR( register_data , SSR_SMIBUSY )) {
				break;
			}
			if ( tcount++ > 100 ) {
				printk("%s@%d: timeout\n", __func__, __LINE__ );
				return -1;
			}
			udelay( 10 );
		}

		// write command to command register
		mii_bus->write( mii_bus, private_data->switch_smida, SMIDA_SMICOMMAND_REG,
			SSR_SMIBUSY|SSR_SMIMODE_22|SSR_SMIOP( SSR_SMIOP_RDDATA )|
			SSR_DEVADDR( phy_param )|SSR_REGADDR( reg_param ));

		// wait until SMI engine ready
		tcount = 0;
		while ( 1 ) {
			register_data = mii_bus->read( mii_bus, private_data->switch_smida, SMIDA_SMICOMMAND_REG );
			if ( 0xffff == register_data ) {
				db_printf("%s@%d: no device at SMIDA %d\n", __func__, __LINE__,
					private_data->switch_smida );
				return -1;
			}
			if ( BITS_CLEAR( register_data , SSR_SMIBUSY )) {
				break;
			}
			if ( tcount++ > 100 ) {
				printk("%s@%d: timeout\n", __func__, __LINE__ );
				return -1;
			}
			udelay( 10 );
		}

		// read data to holding register
		register_data = mii_bus->read( mii_bus, private_data->switch_smida, SMIDA_SMIDATA_REG );
	} else {
		register_data = mii_bus->read( mii_bus, phy_param, reg_param );
	}
	pr_debug( KERN_DEBUG "%s@%d: 0x%02x:0x%02x -> 0x%04x\n", __func__, __LINE__, phy_param, reg_param, register_data );
	return register_data;
}


static int marvell_find( struct marvell_private_data_t *private ) {
	struct mii_bus *dev = private->mii_bus;
	unsigned short SMIDA, switchID;
	for ( SMIDA = 0; SMIDA < 0x20; SMIDA++ ) {
		private->switch_smida = SMIDA;
		switchID = marvell_read( private, SMIDA_SwitchPort0_REG, SPR_SwitchIdentifier_REG );
		private->switch_identifier = switchID & SSIR_DeviceID; 
		switch ( switchID & SSIR_DeviceID ) {
		case SWITCHID_6161:
			printk("(%s): Found Marvell 88e6161(rev %x) at SMI Device Address 0x%02X\n",
				dev->name, switchID & SSIR_RevID, SMIDA );
			return 0;
		case SWITCHID_6123:
			printk("(%s): Found Marvell 88e6123(rev %x) at SMI Device Address 0x%02X\n",
				dev->name, switchID & SSIR_RevID, SMIDA );
			return 0;
		case SWITCHID_6352:
			printk("(%s): Found Marvell 88e6352(rev %x) at SMI Device Address 0x%02X\n",
				dev->name, switchID & SSIR_RevID, SMIDA );
			return 0;
		default:
			db_printf("%s@%d: read %04x\n", __func__, __LINE__, switchID );
			break;
		}
	}
	return -1;
}

static int marvell_port_control( void *private_param, int port_param, int state_param ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	unsigned short temp;
	int port_reg = SMIDA_SwitchPort0_REG + port_param;
	printk( KERN_INFO "%s@%d: port %02x %s\n", __FUNCTION__, __LINE__, port_param, port_state_str[state_param] );

	temp = marvell_read( private_data, port_reg, SPR_PortControl_REG );
	temp &= ~SPCR_PortState_MASK;			    //clear BIT0&1 to disable the port
	temp |=  state_param;
	marvell_protected_write( private_data, port_reg, SPR_PortControl_REG, temp );

	return 0;
}

static int marvell_phy_control( void *private_param, int port_param, int state_param ) {
	unsigned short temp;
	int port_reg = SMIDA_PHY0_REG + port_param;
	printk( KERN_INFO "%s@%d: port %02x %s\n", __FUNCTION__, __LINE__, port_param, port_state_str[state_param] );

	// Disable ppu
	marvell_ppu_control(private_param, SGCR_PPU_ENABLE, 0 ); 

	temp = marvell_read(private_param, port_reg, PHY_SpecificControl_REG );
   	temp &= ~PSCR_AutoMDI;
   	temp |= state_param;
	marvell_write(private_param, port_reg, PHY_SpecificControl_REG, temp );

	// Reset the phy so the change takes
	temp = marvell_read(private_param, port_reg, PHY_Control_REG );
	temp |= PCR_SWReset;
	marvell_write(private_param, port_reg, PHY_Control_REG, temp );

   	// Enable ppu
	marvell_ppu_control( private_param, SGCR_PPU_ENABLE, SGCR_PPU_ENABLE );   
	return 0;
}


static int marvell_set_portMode( void *private_data, int port_param, port_mode_t mode_param ) {
	switch ( mode_param ) {
	case PORT_DISABLE:
		return marvell_port_control( private_data, port_param, SPCR_PortState_Disabled );
	case PORT_BLOCK:
	case PORT_LISTEN:
		return marvell_port_control( private_data, port_param, SPCR_PortState_Blocking );
	case PORT_LEARNING:
		return marvell_port_control( private_data, port_param, SPCR_PortState_Learning );
	case PORT_FORWARD:		/* normal port operation*/
		return marvell_port_control( private_data, port_param, SPCR_PortState_Forwarding );
	case PORT_FORCE_MDI: {		/* normal port operation*/
		return marvell_phy_control( private_data, port_param, PSCR_ForceMDI );
	}
	case PORT_FORCE_MDIX: {
		return marvell_phy_control( private_data, port_param, PSCR_ForceMDIX );
	}
	case PORT_AUTO_MDI_X: {
		return marvell_phy_control( private_data, port_param, PSCR_AutoMDI );
	}
	default:
		return -1;
	}
}

static int marvell_ppu_control( void *private_param, int mask_param, int state_param ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	unsigned int ReadVal;
	db_printf("%s@%d\n", __FUNCTION__, __LINE__ );

	ReadVal = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_GlobalControl_REG );
	ReadVal &= ~mask_param;
	ReadVal |= state_param;
	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_GlobalControl_REG, ReadVal );
	return 0;
}


#if defined(PHYOPT_PACKETTAGGING)
/** tagging functions ********************************************************/
#if 0
static void print_marvellTag( int debug_level, MarvellTag_t *tag ) {
	if ( debug_level == 0 ) {
		return;
	}
	switch ( debug_level ) {
	case BIT(NETDEBUG_TAG_INSERT):  printk("NETDEBUG_TAG_INSERT\n"); break;
	case BIT(NETDEBUG_TAG_REMOVE):  printk("NETDEBUG_TAG_REMOVE\n"); break;
	default:
		break;
	}
	printk("Operation  : ");
	switch ( tag->egress.Operation ) {
	case OPERATION_FORWARD:         printk("Forward\n");   break;
	case OPERATION_INGRESSFROMCPU:  printk("Ingress\n");   break;
	case OPERATION_EGRESSTOCPU:     printk("Egress\n");    break;
	default: printk("BAD OPCODE\n");                       break;
	}

	switch ( tag->egress.Operation ) {
	case OPERATION_FORWARD: {
			printk("Tagged     : %02X\n", tag->egress.Src_Tagged );
			printk("PRI        : %02X\n", tag->egress.PRI );
			printk("VID        : %03X\n", tag->egress.VID );
			break;
		}
	case OPERATION_EGRESSTOCPU: {
			char code;
			code = (tag->egress.Code3_1 << 1) | tag->egress.Code0;
			printk("Src_Tagged : %02X\n", tag->egress.Src_Tagged );
			printk("SrcDevId   : %02X\n", tag->egress.Src_Dev );
			printk("SrcPort    : %02X\n", tag->egress.Src_Port );
			printk("Code       : %02X\n", code );
			printk("PRI        : %02X\n", tag->egress.PRI );
			printk("VID        : %03X\n", tag->egress.VID );
			break;
		}
	case OPERATION_INGRESSFROMCPU: {
			printk("Trg_Tagged : %02X\n", tag->ingress.Trg_Tagged );
			printk("TrgDevId   : %02X\n", tag->ingress.Trg_Dev );
			printk("TrgPort    : %02X\n", tag->ingress.Trg_Port );
			printk("Prio       : %02X\n", tag->ingress.Prio );
			printk("PRI        : %02X\n", tag->ingress.PRI );
			printk("VID        : %03X\n", tag->ingress.VID );
			break;
		}
	default:
		break;
	}
}
#endif
/*
 * adds dummy tag if ingress tagging is ON.
 * converts VLAN tags to ingress tags
 */
int marvell_tag_insert( void *private_param, struct sk_buff **skb ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;

	bpdu_header_TYPE *eth;
	struct vlan_ethhdr *vlan;
	MarvellTag_t *tag;

	if ( !BITS_SET( private_data->flags, PHYOPT_PHYINIT|PHYOPT_PACKETTAGGING )) {
		return 0;
	}

	db_printf("%s@%d\n", __FUNCTION__, __LINE__ );

	eth = (bpdu_header_TYPE *)(*skb)->data;
	if ( eth->ethhdr.length_proto == ntohs( 0x8100 )) {
		vlan = (struct vlan_ethhdr *)(*skb)->data;
		tag = (MarvellTag_t *)((*skb)->data + 2*ETH_ALEN); // find position of tag

		// if packet is tagged with VLAN header, convert to Marvell tag
		tag->u16[0] = 0; // clear unused/overlapping fields
		tag->ingress.Operation = OPERATION_INGRESSFROMCPU;
//		tag->ingress.Trg_Dev = (vlan->h_vlan_TCI >> 8) & MASK(4,0);
		tag->ingress.Trg_Dev = private_data->switch_smida;
		tag->ingress.Trg_Port = (vlan->h_vlan_TCI >> 0) & MASK(4,0);
		// no net change in length
	} else {
		// add tag to new packet
		*skb = skb_pad( *skb, sizeof( MarvellTag_t ));	// pad for space
		skb_put( *skb, sizeof( MarvellTag_t ));

		// reload pointers
		vlan = (struct vlan_ethhdr *)(*skb)->data;
		tag = (MarvellTag_t *)((*skb)->data + 2*ETH_ALEN); // find position of tag

		memmove((char *)tag + sizeof( MarvellTag_t ), (char *)tag, (*skb)->len - 2*ETH_ALEN ); // move data down to make room for tag
		memset((unsigned char *)tag, 0, sizeof( *tag )); // clear tag
		tag->ingress.Operation = OPERATION_FORWARD;
	}

//	print_marvellTrailerTag(( net_debug & BIT(NETDEBUG_TAG_INSERT)), tag );
	return 0;
}

// detect and move vlan headers to the end, make egress trailers look like VLAN
int marvell_tag_remove( void *private_param, struct sk_buff *skb ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;

	vlan_packet_header_TYPE *packet;
	MarvellTag_t *tag;

	if ( !BITS_SET( private_data->flags, PHYOPT_PHYINIT|PHYOPT_PACKETTAGGING )) {
		return 0;
	}

	db_printf("%s@%d\n", __FUNCTION__, __LINE__ );

	packet = (vlan_packet_header_TYPE *)skb->data;
	tag = (MarvellTag_t *)(skb->data + 2*ETH_ALEN);

//	print_marvellTrailerTag(( net_debug & BIT(NETDEBUG_TAG_REMOVE)), tag );

	tag->u16[0] = htons( 0x8100 ); // this makes the egress tag look like a VLAN tag

#ifdef PHYOPT_VLAN
	while ( packet->h_proto == ntohs( 0x8100 )) { // if this packet has a VLAN tag, move that to the rear
		db_printf( KERN_INFO __FUNCTION__": VLAN packet detected!\n");

		if (( packet->DSAP != BPDU_DSAP_SSAP )||
		    ( packet->SSAP != BPDU_DSAP_SSAP )||
		    ( packet->protocol_id != BPDU_PROTOCOL_ID )||
		    ( packet->protocol_vid != BPDU_PROTOCOL_VERSION )||
		    ( packet->bpdu_type != BPDU_TYPE )) {
			memcpy( skb->tail, &packet->h_proto, VLAN_HLEN ); // copy the VLAN header to the end
			memmove( &packet->h_proto, &packet->h_vlan_encapsulated_proto, skb->len - 2*ETH_ALEN );	// move everything up
		} else {
			break;
		}
	}
#endif	

	return 0;
}


#endif // PHYOPT_PACKETTAGGING
#ifdef PHYOPT_ATU_ACCESS
/** ATU access functions ******************************************************/
static void marvell_atuWrite( void *private_param, unsigned short atu_op, atuEntry_t *entry_param ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	unsigned short reg_data;

	do {						    // wait for ATU ready
		reg_data = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUOperation_REG );
	} while ( BITS_SET( reg_data, SAOR_ATUBusy ));

	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB01_REG, htons(entry_param->regs.ATUMACAddrB01));
	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB23_REG, htons(entry_param->regs.ATUMACAddrB23));
	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMacAddrB45_REG, htons(entry_param->regs.ATUMACAddrB45));
	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUData_REG, entry_param->regs.ATUData );
	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUOperation_REG, SAOR_ATUBusy|atu_op );
}

static void marvell_atuRead( void *private_param, atuEntry_t *entry_param ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	unsigned short reg_data;

	do {						    // wait for ATU ready
		reg_data = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUOperation_REG );
	} while ( BITS_SET( reg_data, SAOR_ATUBusy ));

	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB01_REG, entry_param->regs.ATUMACAddrB01 );
	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB23_REG, entry_param->regs.ATUMACAddrB23 );
	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMacAddrB45_REG, entry_param->regs.ATUMACAddrB45 );
	marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUOperation_REG,  SAOR_ATUBusy|SAOR_ATUOp_GetNext );

	do {						    // wait for ATU ready
		reg_data = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUOperation_REG );
	} while ( BITS_SET( reg_data, SAOR_ATUBusy ));

	entry_param->regs.ATUData = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUData_REG );
	entry_param->regs.ATUMACAddrB01 = ntohs( marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB01_REG ));
	entry_param->regs.ATUMACAddrB23 = ntohs( marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB23_REG ));
	entry_param->regs.ATUMACAddrB45 = ntohs( marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMacAddrB45_REG ));
}

static int marvell_procDump_atu( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)data_param;
	unsigned short reg_data;
	int len = 0, port_idx;
	atuEntry_t entry;
	off_t begin=0;

	if ( offset_param == 0 ) {
		len += sprintf( buf+len, "\nMarvell 10/100/1000 Switch: ATU Table\n");

		do {						    // wait for ATU ready
			reg_data = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUOperation_REG );
		} while ( BITS_SET( reg_data, SAOR_ATUBusy ));

		// set start MAC at beginning of list
		marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB01_REG, 0 );
		marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB23_REG, 0 );
		marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMacAddrB45_REG, 0 );

	}
	while ( 1 ) {
		marvell_write( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUOperation_REG, (SAOR_ATUBusy|SAOR_ATUOp_GetNext));
		do {						// wait for ATU ready
			reg_data = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUOperation_REG );
		} while ( BITS_SET( reg_data, SAOR_ATUBusy ));

		entry.regs.ATUOperation = reg_data;
		entry.regs.ATUData = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUData_REG );
		entry.regs.ATUMACAddrB01 = ntohs( marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB01_REG ));
		entry.regs.ATUMACAddrB23 = ntohs( marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMACAddrB23_REG ));
		entry.regs.ATUMACAddrB45 = ntohs( marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUMacAddrB45_REG ));

		if (( entry.regs.ATUMACAddrB01 == 0xffff )&&( entry.regs.ATUMACAddrB23 == 0xffff )&&( entry.regs.ATUMACAddrB45 == 0xffff )) {
			break;
		}
		len += sprintf( buf+len, "  ESA: %02X:%02X:%02X:%02X:%02X:%02X",
				entry.data.esa[0], entry.data.esa[1], entry.data.esa[2],
				entry.data.esa[3], entry.data.esa[4], entry.data.esa[5] );

		reg_data = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_ATUControl_REG );
		len += sprintf( buf+len, ", Expires in %04ds", (int)(((reg_data & SACR_AgeTime) >> 4)*15));

		reg_data = marvell_read( private_data, SMIDA_SwitchGlobal_REG, SGR_VTUVID_REG );
		if ( reg_data & BIT(12)) {
			len += sprintf( buf+len, ", VID:%02X", (int)(reg_data & MASK(11,0)));
		}
		len += sprintf( buf+len, ", PRI:%02X", (u16)SAOR_EntryPri_Get( entry.regs.ATUOperation ));
		len += sprintf( buf+len, ", ES:%02X", (u16)SADR_EntryState_Get( entry.regs.ATUData ));

		len += sprintf( buf+len, ", Port MAP:[" );
		for ( port_idx = 0; port_idx < NUM_SWITCHPORTS; port_idx++ ) {
			if ( port_idx != 0 ) {
				len += sprintf( buf+len, "|");
			}
			if ( SADR_PortVec_Get( entry.regs.ATUData ) & BIT(port_idx)) {
				len += sprintf( buf+len, "%2d", port_idx );
			} else {
				len += sprintf( buf+len, "  ");
			}
		}
		len += sprintf( buf+len, "]\n");
	        if (len+begin > offset_param+count)
			goto done;
		if (len+begin < offset_param) {
			begin += len;
			len = 0;
		}
	}
	*eof = 1;
done:
        if (offset_param >= len+begin)
		return 0;
        *start = buf + (offset_param-begin);
		return ((count < begin+len-offset_param) ? count : begin+len-offset_param);
}


#endif
/** Core functions ************************************************************/
// gets the ports in use and allocates the structures needed
// TODO: look at the platform port mask, determine if the serdes ports are being used,
//	and adjust the proc entries accordingly
int marvellswitch_platform_probe(struct platform_device *pdev_param) {
//	struct device *dev = pdev_param->dev;
	struct marvellswitch_platform_data *mpd = pdev_param->dev.platform_data;
	struct marvell_private_data_t *p;
	int switch_idx, port_idx, count, port;

	db_printf("%s@%d\n", __func__, __LINE__);
	if ( mpd == NULL ) {
		printk( KERN_ERR "%s@%d: platform data structure is NULL\n", __func__, __LINE__ );
		return -1;
	}

	db_printf("%s@%d: platform has %d switch(es)\n", __func__, __LINE__, mpd->switch_count);
	for ( switch_idx = 0; switch_idx < mpd->switch_count; switch_idx++ ) {
		p = (struct marvell_private_data_t *)kmalloc( sizeof( struct marvell_private_data_t ), GFP_KERNEL );
		memset( p, 0, sizeof( struct marvell_private_data_t ));
		if ( p_marvell_private == NULL ) {
			p_marvell_private = p;
			INIT_LIST_HEAD(&p->list);
		} else {
			list_add( &p->list, &p_marvell_private->list );
		}
		p->port_count = count = mpd->switches[switch_idx].port_count;

		p->statDump_entries = kmalloc( sizeof( marvell_statDump_data_t )*(count), GFP_KERNEL );
		p->procDump_entries = kmalloc( sizeof( marvell_procDump_data_t )*(count*2+2), GFP_KERNEL );
		memset( p->statDump_entries, 0, sizeof( marvell_statDump_data_t )*(count));
		memset( p->procDump_entries, 0, sizeof( marvell_procDump_data_t )*(count*2+2));

		p->procDump_entries[0].priv		= p;
		sprintf( p->procDump_entries[0].name, "global" );
		p->procDump_entries[0].phy_start	= SMIDA_SwitchGlobal_REG;
		p->procDump_entries[0].phy_count	= 1;
		p->procDump_entries[0].reg_start	= SGR_GlobalStatus_REG;
		p->procDump_entries[0].reg_count	= NUM_GLOBAL_REGS;

		p->procDump_entries[1].priv		= p;
		sprintf( p->procDump_entries[1].name, "global2" );
		p->procDump_entries[1].phy_start 	= SMIDA_SwitchGlobal2_REG;
		p->procDump_entries[1].phy_count 	= 1;
		p->procDump_entries[1].reg_start 	= 0;
		p->procDump_entries[1].reg_count 	= NUM_GLOBAL2_REGS;

		db_printf("%s@%d: platform switch %d has %d configured ports\n", __func__, __LINE__,
				switch_idx, mpd->switches[switch_idx].port_count );

		for ( port_idx = 0; port_idx < count; port_idx++  ) {
			p->port_mask |= mpd->switches[switch_idx].ports[port_idx].port_mask;
			port = ilog2(mpd->switches[switch_idx].ports[port_idx].port_mask); 

			db_printf("%s@%d: switch %d, port #%d, phyiscal port %d, description \'%s\'\n",
				__func__, __LINE__, switch_idx, port_idx, port, 
				mpd->switches[switch_idx].ports[port_idx].description);

			p->procDump_entries[2+port_idx*2].priv		= p;
			sprintf( p->procDump_entries[2+port_idx*2].name, "phy%d", port );
			sprintf( p->procDump_entries[2+port_idx*2].description, "%s",
				mpd->switches[switch_idx].ports[port_idx].description );
			p->procDump_entries[2+port_idx*2].phy_start	= SMIDA_PHY0_REG + port;
			p->procDump_entries[2+port_idx*2].phy_count	= 1;
			p->procDump_entries[2+port_idx*2].reg_start	= SIR_PHYControl_REG;
			p->procDump_entries[2+port_idx*2].reg_count	= NUM_PHY_REGS;
			p->procDump_entries[2+port_idx*2].ppu_disable = true;

			p->procDump_entries[3+port_idx*2].priv		= p;
			sprintf( p->procDump_entries[3+port_idx*2].name, "port%d", port );
			sprintf( p->procDump_entries[3+port_idx*2].description, "%s",
				mpd->switches[switch_idx].ports[port_idx].description );
			p->procDump_entries[3+port_idx*2].phy_start	= SMIDA_SwitchPort0_REG + port;
			p->procDump_entries[3+port_idx*2].phy_count	= 1;
			p->procDump_entries[3+port_idx*2].reg_start	= SPR_PortStatus_REG;
			p->procDump_entries[3+port_idx*2].reg_count	= NUM_SWITCHPORT_REGS;

			p->statDump_entries[port_idx].priv		= p;
			sprintf( p->statDump_entries[port_idx].name, "port%d", port );
			sprintf( p->statDump_entries[port_idx].description, "%s",
				mpd->switches[switch_idx].ports[port_idx].description );
			p->statDump_entries[port_idx].port_num		= port;
		} // for each port on this switch
	} // for each switch
	return 0;
}

static bool initd = false;
int marvell_m88exxxx_probe(struct phy_device *phydev) {
	struct marvell_private_data_t *p = p_marvell_private; 
	unsigned short idx;
	char str[32];

	db_printf("%s@%d: dev.p %p, mii_bus %p\n", __func__, __LINE__, phydev->priv, phydev->bus );

	if ( initd == true ) {
		printk("%s(%s): multiple init!\n", __func__, phydev->bus->name );
		return 0;
	}
	if ( p_marvell_private == NULL ) {
		printk("%s(%s): no platform init!\n", __func__, phydev->bus->name );
		return 0;
	}

	// TODO: search the list of platform driver instances (if multiple)
	//	to make sure we are using the correct one

	p->mii_bus = phydev->bus;
	p->switch_smida = 0;
	phydev->priv = p;

	if ( marvell_find( p ) < 0 ) {
		db_printf("%s@%d: Marvell switch not found!\n", __FUNCTION__, __LINE__ );
//		kfree( phydev->priv );
		phydev->priv = NULL;
		return 0;
	}


	marvell_ppu_control( p, SGCR_PPU_ENABLE, 0 );

#if 0 // various unused initialization
	marvell_protected_write( private_data, SMIDA_SwitchGlobal_REG, SGR_GlobalControl2_REG, SWITCH_DEVID );

	for ( port_idx = 0; port_idx < NUM_SWITCHPORTS; port_idx++ ) {
		temp = marvell_read( private_data, SMIDA_SwitchPort0_REG + port_idx, SPR_PortControl_REG );
		temp |= SPCR_EgressMode_Untagged;
		marvell_protected_write( private_data, SMIDA_SwitchPort0_REG + port_idx, SPR_PortControl_REG, temp );
	}
	for ( port_idx = 0; port_idx < NUM_SWITCHPORTS; port_idx++ ) {
		marvell_port_control( private_data, port_idx, DEFAULT_PORTSTATE );
	}
#endif

	marvell_ppu_control( p, SGCR_PPU_ENABLE, SGCR_PPU_ENABLE );

	/* create proc directory */
	sprintf( str,"driver/switch%02X", p->switch_smida );
	p->proc_dir = proc_mkdir( str, NULL );
	if ( p->proc_dir == NULL ) {
		return -ENOMEM;	    // we're screwed
	}

	for ( idx = 0; idx < 2*p->port_count + 2; idx++ ) {
		struct proc_dir_entry *res;
		sprintf( str,"regs_%s", p->procDump_entries[idx].name );
		res = create_proc_entry( str, 0, p->proc_dir );
		if (res) {
			res->read_proc	= marvell_procDump_regs;
			res->write_proc	= marvell_procWrite_page;
			res->data	= &p->procDump_entries[idx];
		}
	}

	for ( idx = 0; idx < p->port_count; idx++ ) {
		sprintf( str,"stats_%s", p->statDump_entries[idx].name );
		create_proc_read_entry( str,
			0,				/* default mode */
			p->proc_dir,			/* parent dir */
			marvell_procDump_counters,
			&p->statDump_entries[idx]);	/* client data */
	}

#ifdef PHYOPT_ATU_ACCESS
	sprintf( str,"routing" );
	create_proc_read_entry( str,
				0,			/* default mode */
				p->proc_dir,		/* parent dir */
				marvell_procDump_atu,
				p );			/* client data */
#endif
#ifdef PHYOPT_TEMPERATURE_SENSOR
	sprintf( str,"temperature" );
	create_proc_read_entry( str,
				0,
				p->proc_dir,
				marvell_procDump_temperature,
				p );
#endif

	p->flags |= PHYOPT_DEFAULT_INIT;

	marvell_set_config( p, NULL );

	initd = true;
	return 0;
}

int marvell_m88e6161_remove(struct platform_device *private_param) {
#ifdef PHYOPT_PACKETTAGGING
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	if ( BITS_SET( private_data->flags, PHYOPT_PACKETTAGGING )) {
		unsigned short temp;
		temp = marvell_read( private_data, SMIDA_SwitchPort0_REG + private_data->management_port, SPR_PortControl_REG );
		temp &= ~SPCR_TaggingMask;
		temp |= SPCR_EgressMode_Untagged;
		marvell_write( private_data, SMIDA_SwitchPort0_REG + private_data->management_port, SPR_PortControl_REG, temp );

		private_data->flags &= ~PHYOPT_PACKETTAGGING;
		printk( KERN_INFO "%s@%d: Packet tagging DISABLED\n", __FUNCTION__, __LINE__ );
	}
#endif
	return 0;
}



/* This is intended to get the status of the port that the MII is connected to. 
 *  since this port is on a switch it will always be up, 100 Mbit, and full-dulpex 
 */
int marvell_m88e6352_status( struct phy_device *phydev ) {

	phydev->state 	= PHY_RUNNING;
	phydev->link 	= 1;
	phydev->duplex 	= DUPLEX_FULL;
	phydev->speed 	= SPEED_100;
	phydev->pause 	= phydev->asym_pause = 0;

	return 0;
}





int marvell_m88e6352_config_aneg(struct phy_device *phydev) {

	phydev->link 		= 1;
	phydev->drv->flags 	= IFF_UP;
	phydev->speed 		= SPEED_100;
	phydev->duplex 		= DUPLEX_FULL;

	return 0;
}



int marvell_m88e6352_config_init( struct phy_device *phydev ) {
    unsigned short temp;

	// Set the address of the port to 16
	phydev->addr = 0x16;

    // Set the Physical Control register ForceSpd field
    // to b01 = 100 or 200 Mbps
    // This drops the clock down to 25 MHz

	temp = phy_read(phydev, 0x01);

    temp &= 0xFFFC;
    temp |= 0x0001;

    temp = phy_write(phydev, 0x01, temp);

	return 0;
}





int marvell_set_config( void *private_param, struct ifmap *map ) {

#ifdef PHYOPT_PACKETTAGGING
    struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
    unsigned short temp;
	temp = marvell_read( private_data, SMIDA_SwitchPort0_REG + private_data->management_port, SPR_PortControl_REG );
	temp &= ~SPCR_TaggingMask;

	if ( BITS_SET( private_data->flags, PHYOPT_PACKETTAGGING )) {
		temp |= SPCR_InterswitchPort;
		printk("%s@%d: packet tagging ON\n", __FUNCTION__, __LINE__ );
	} else {
		temp |= SPCR_EgressMode_Untagged;
		printk("%s@%d: packet tagging OFF\n", __FUNCTION__, __LINE__ );
	}
	marvell_write( private_data, SMIDA_SwitchPort0_REG + private_data->management_port, SPR_PortControl_REG, temp );
#endif

	return 0;
}



int marvell_phy_ioctl( void *private_param, int cmd, struct ifreq *rq ) {
	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)private_param;
	eth_swcmd_t *swcmd = (eth_swcmd_t *)rq->ifr_data;

	if ( cmd == SIOCSETHSWCMD ) {
		switch ( swcmd->cmd_id ) {
		case SET_PORT_MODE: {
			return marvell_set_portMode( private_param, swcmd->port_id, swcmd->cmd.port_mode );
		}
#ifdef CONFIG_BYPASS_CONTROL
		case SET_BYPASS_MODE: {
			unsigned short reg_val;
			unsigned int retval = 0;
			marvell_ppu_control( private_data, SGCR_PPU_ENABLE, 0 );	
			if (swcmd->cmd.byp_op == BYPASS_DISABLED) {
				reg_val = marvell_read( private_data, CONFIG_BYPASS_PHY_ADD, CONFIG_BYPASS_REG_ADD );
				reg_val |= (CONFIG_BYPASS_REG_MASK & 0xFFFF);
				marvell_protected_write( private_data, CONFIG_BYPASS_PHY_ADD, CONFIG_BYPASS_REG_ADD, reg_val ); 
			} else if (swcmd->cmd.byp_op == BYPASS_ENABLED) {
				reg_val = marvell_read( private_data, CONFIG_BYPASS_PHY_ADD, CONFIG_BYPASS_REG_ADD );
				reg_val &= ~(CONFIG_BYPASS_REG_MASK & 0xAAAA);
				marvell_protected_write( private_data, CONFIG_BYPASS_PHY_ADD, CONFIG_BYPASS_REG_ADD, reg_val ); 
			} else {
				retval = -EINVAL;
			}
			marvell_ppu_control( private_data, SGCR_PPU_ENABLE, SGCR_PPU_ENABLE );	
			return retval;
		}
#endif		
		case GET_SWITCH_ID: {
			unsigned char type;
			unsigned int retval =0;
			printk(KERN_ERR "switch_identifier: 0x%X\n", private_data->switch_identifier);
			switch ( private_data->switch_identifier ) {
			case SWITCHID_6161: {
				type = SWITCHID_6161;
				if ( copy_to_user((char *)(&swcmd->cmd.sw_type), &type, sizeof(switch_type_t))) {
					retval = -EFAULT;
				}
			}
			case SWITCHID_6123: {
				type = SWITCHID_6123;
				if ( copy_to_user((char *)(&swcmd->cmd.sw_type), &type, sizeof(switch_type_t))) {
					retval = -EFAULT;
				}
			}
			case SWITCHID_6352: {
				type = SWITCHID_6352;
				if ( copy_to_user((char *)(&swcmd->cmd.sw_type), &type, sizeof(switch_type_t))) {
					retval = -EFAULT;
				}
			}
			default:
				retval = -EFAULT;
			}
			return retval;
		}
#ifdef PHYOPT_VLAN
		case SET_VLAN_ROUTE: {
			unsigned short vlanPortVec;
			vlanPortVec = marvell_read( private_data, SMIDA_SwitchPort0_REG + swcmd->port_id, SPR_PortBasedVLANMap_REG );
			if ( swcmd->cmd.vlan_route.op == VLAN_OP_SET ) {
				vlanPortVec |= SPVM_PortVec( swcmd->cmd.vlan_route.dest_port_id );
			} else if ( swcmd->cmd.vlan_route.op == VLAN_OP_CLEAR ) {
				vlanPortVec &= ~SPVM_PortVec( swcmd->cmd.vlan_route.dest_port_id );
			} else if ( swcmd->cmd.vlan_route.op == VLAN_OP_SET_ALL ) {
				vlanPortVec = SPVM_VLANTable_MASK ^ SPVM_PortVec( swcmd->port_id );
			} else if ( swcmd->cmd.vlan_route.op == VLAN_OP_CLEAR_ALL ) {
				vlanPortVec = 0;
			} else {
				return -1;
			}
			marvell_protected_write( private_data, SMIDA_SwitchPort0_REG + swcmd->port_id, SPR_PortBasedVLANMap_REG, vlanPortVec );
			return 0;
		}
		case SET_VLAN_ID: {
			marvell_protected_write( private_data, SMIDA_SwitchPort0_REG + swcmd->port_id,
				SPR_DefaultVLAN_Pri_REG, (( swcmd->cmd.vlan_id[1] << 8 ) | swcmd->cmd.vlan_id[0] ));
			return 0;
		}
#endif
#ifdef PHYOPT_PACKETTAGGING
		case SET_TAGGING: {
			unsigned short temp;
			temp = marvell_read( private_data, SMIDA_SwitchPort0_REG + swcmd->port_id, SPR_PortControl_REG );
			temp &= ~SPCR_TaggingMask;
			if ( swcmd->cmd.tag_opt ) {
				printk("%s@%d: packet tagging ON\n", __FUNCTION__, __LINE__ );
				private_data->flags |= PHYOPT_PACKETTAGGING;
				private_data->management_port = swcmd->port_id;	// this must be the management port
				if ( swcmd->switch_id == 0 ) { // only switches attached to CPU (switch_id == 0) use this tagging mode
					temp |= SPCR_InterswitchPort;
				} else {
					temp |= SPCR_EgressMode_Tagged;
				}
			} else {
				printk("%s@%d: packet tagging OFF\n", __FUNCTION__, __LINE__ );
				private_data->flags &= ~PHYOPT_PACKETTAGGING;
				temp |= SPCR_EgressMode_Untagged;
			}
			marvell_protected_write( private_data, SMIDA_SwitchPort0_REG + swcmd->port_id,
				SPR_PortControl_REG, temp );
			return 0;
		}
#endif
#ifdef PHYOPT_ATU_ACCESS
		case SET_ATU_ENTRY: {
			atuEntry_t entry;
			if ( swcmd->cmd.atu.op >= NUM_ATU_OPS ) {
				return -1;
			}

			if ( swcmd->cmd.atu.op == ATU_FLUSH ) {
				marvell_atuWrite( private_param, SAOR_ATUOp_FlushUnlocked, &entry );
				return 0;
			} else if ( swcmd->cmd.atu.op == ATU_FLUSH_ALL ) {
				marvell_atuWrite( private_param, SAOR_ATUOp_FlushAll, &entry );
				return 0;
			}

			memcpy( entry.data.esa, swcmd->cmd.atu.MAC, ETH_ALEN );
			if ( swcmd->cmd.atu.op == ATU_READ ) {
				marvell_atuRead( private_param, &entry );
				//memcpy( swcmd->cmd.atu.MAC, entry.data.esa, ETH_ALEN );
				swcmd->cmd.atu.priority = SAOR_EntryPri_Get( entry.regs.ATUOperation );
				swcmd->cmd.atu.port_vector = SADR_PortVec_Get( entry.regs.ATUData );
				return 0;
			} else if ( swcmd->cmd.atu.op == ATU_DELETE ) {
				entry.regs.ATUData = SADR_EntryState_Set( 0 );
				marvell_atuWrite( private_param, SAOR_ATUOp_LoadEntry, &entry );
				return 0;
			}

			entry.regs.ATUData = 0;
			entry.regs.ATUOperation = 0;
			entry.regs.ATUOperation |= SAOR_EntryPri_Set( swcmd->cmd.atu.priority );
			entry.regs.ATUData |= SADR_PortVec_Set( swcmd->cmd.atu.port_vector );

			if ( BITS_CLEAR( entry.data.esa[0], BIT(0))) { // unicast
				entry.regs.ATUData |= SADR_EntryState_Set( SADR_EntryState_UnicastStatic );
			} else if ( swcmd->cmd.atu.op == ATU_ADD_STATIC ) {
				entry.regs.ATUData |= SADR_EntryState_Set( SADR_EntryState_MulticastStatic );
			} else if ( swcmd->cmd.atu.op == ATU_ADD_MANAGEMENT ) {
				entry.regs.ATUData |= SADR_EntryState_Set( SADR_EntryState_MulticastManagement );
			}
			marvell_atuWrite( private_param, SAOR_ATUOp_LoadEntry, &entry ); // only option
			return 0;
		}
#endif
		case SET_IGMPSNOOPING: {
			unsigned short temp;
			temp = marvell_read( private_data, SMIDA_SwitchPort0_REG + swcmd->port_id, SPR_PortControl_REG );
			if ( swcmd->cmd.igmpSnooping_opt) {
				printk("%s@%d: IGMP snooping ON\n", __FUNCTION__, __LINE__ );
				temp |= SPCR_IgmpMldSnooping;
			} else {
				printk("%s@%d: IGMP snooping OFF\n", __FUNCTION__, __LINE__ );
				temp &= ~SPCR_IgmpMldSnooping;
			}
			marvell_protected_write( private_data, SMIDA_SwitchPort0_REG + swcmd->port_id, SPR_PortControl_REG, temp );
			return 0;
		}
	        default:
			printk("%s@%d: unsupported IOCTL, %08X\n", __func__, __LINE__, cmd);
			return -EOPNOTSUPP;
		}
	} else {
		return -EOPNOTSUPP;
	}
}

static int marvellsw_ioctl(struct inode *inode, struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param)
{
	int retval = 0;
	struct ifreq *ifr = (struct ifreq *)ioctl_param;
	struct mii_ioctl_data *mii_data = if_mii(ifr);
//	struct marvell_private_data_t *private_data = (struct marvell_private_data_t *)inode->i_private;
	struct marvell_private_data_t *p = p_marvell_private;
	pr_debug("%s@%d\n", __func__, __LINE__ );
	switch ( ioctl_num ) {
	case SIOCGMIIREG: // get MII register
		mii_data->val_out = marvell_read( p, mii_data->phy_id, mii_data->reg_num );
		break;
	case SIOCSMIIREG:
		marvell_write( p, mii_data->phy_id, mii_data->reg_num, mii_data->val_in );
		break;
	default:
		return marvell_phy_ioctl( p, ioctl_num, ifr);
	}
	return retval;
}

static int marvellsw_open(struct inode *inode, struct file *filp)
{
	pr_debug("%s@%d\n", __func__, __LINE__ );
	return 0;
}

static int marvellsw_release (struct inode *inode, struct file *filp)
{
	pr_debug("%s@%d\n", __func__, __LINE__ );
	return 0;
}

static const struct file_operations marvellsw_fops = {
	.ioctl = marvellsw_ioctl,
//	.read = marvellsw_read,
//	.write = marvellsw_write,
	.open = marvellsw_open,
	.release = marvellsw_release,
};

static struct miscdevice marvellsw_miscdev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "marvellsw",
	.fops = &marvellsw_fops,
};

static struct platform_driver marvell_m88e6161_swicth = {
    .probe = marvellswitch_platform_probe,
//  .remove = marvell_m88e6161_remove,
    .driver = {
        .name = "marvell-m88e6161-switch",
    },
};

static int __init marvell_m88e6161_init(void)
{
	int ret = 0;

	db_printf( "%s@%d\n", __func__, __LINE__ );
	ret = platform_driver_register(&marvell_m88e6161_swicth);

	ret = phy_driver_register(&marvell_m88e6161_driver);

	ret = phy_driver_register(&marvell_m88e6352_driver);

	ret = misc_register(&marvellsw_miscdev);

	return ret;
}
module_init(marvell_m88e6161_init);

static void __exit marvell_m88e6161_exit(void)
{
	db_printf( "%s@%d\n", __func__, __LINE__ );
	platform_driver_unregister(&marvell_m88e6161_swicth);
	phy_driver_unregister(&marvell_m88e6161_driver);
	phy_driver_unregister(&marvell_m88e6352_driver);
}
module_exit(marvell_m88e6161_exit);


