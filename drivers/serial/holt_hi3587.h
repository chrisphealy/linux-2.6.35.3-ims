#ifndef __HI3587_H
#define __HI3587_H

#ifndef BIT
#define BIT(x)	(1<<(x))
#endif

// commands
#define HI3587_CMD_S_MRESET		0x1
#define HI3587_CMD_S_TXRESET		0x11
#define HI3587_CMD_S_TXENABLE		0x12

#define HI3587_CMD_S_ACLKDIV		0x7	// 8-bit divisor: 1,2,4,8,10 only
#define HI3587_CMD_G_ACLKDIV		0xC	// 8-bit

#define HI3587_CMD_G_STATUS		0xA	// 8-bit
#define HI3587_CMD_G_CTRL		0xB	// 16-bit
#define HI3587_CMD_S_CTRL		0x10	// 16-bit

#define HI3587_CMD_S_TXFIFO		0xE	// up to 32 x 32b

// status register
#define SR3_TXFIFO_EMPTY		BIT(3)
#define SR4_TXFIFO_HALF			BIT(4)
#define SR5_TXFIFO_FULL			BIT(5)

// control registers
#define CR1_CLKSEL_ACLK			0
#define CR1_CLKSEL_ADIV			BIT(1)
#define CR3_TX32nd_isDATA 		0
#define CR3_TX32nd_isPARITY		BIT(3)
#define CR9_TXPARITY_ODD		0
#define CR9_TXPARITY_EVEN 		BIT(9)
#define CR10_TXCLKDIV_10		0
#define CR10_TXCLKDIV_80		BIT(10)
#define CR11_LABEL_RVRS			0
#define CR11_LABEL_FWD			BIT(11)
#define CR12_DRIVER_EN			0
#define CR12_DRIVER_DIS			BIT(12)
#define CR13_TXENMODE_CMD		0
#define CR13_TXENMODE_FIFO		BIT(13)
#define CR14_TFLAG_onFIFOEMPTY		0
#define CR14_TFLAG_onFIFOFULL		BIT(14)

#define HI3587_CR_INIT 	(	CR1_CLKSEL_ACLK		\
			|	CR10_TXCLKDIV_10	\
			|	CR11_LABEL_FWD		\
			|	CR12_DRIVER_EN		\
			|	CR13_TXENMODE_FIFO	\
			|	CR14_TFLAG_onFIFOEMPTY 	)


// NOTE: the order of this enum matches the order of the commands below.
// this is intended to be an idx of the command descriptor array.
// structures are defined in holt_arinc.c
// the first 4 fields should always be the same from chip to chip.
enum {
	HI3587_CMD_S_MRESET_IDX = 0,
	HI3587_CMD_G_STATUS_IDX,
	HI3587_CMD_G_CTRL_IDX,
	HI3587_CMD_S_CTRL_IDX,
	HI3587_CMD_S_TXRESET_IDX,
	HI3587_CMD_S_TXENABLE_IDX,
	HI3587_CMD_G_ACLKDIV_IDX,
	HI3587_CMD_S_ACLKDIV_IDX,
	HI3587_CMD_S_TXFIFO_IDX,
};

struct holt_arinc_cmd_desc hi3587_commands[] = {
	{
		.name = "Master Reset",
		.cmd_id = HI3587_CMD_S_MRESET,
	},
	{
		.name = "Get Status",
		.cmd_id = HI3587_CMD_G_STATUS,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Get Control",
		.cmd_id = HI3587_CMD_G_CTRL,
		.field_sz = 1,
		.word_sz = 16,
	},
	{
		.name = "Set Control",
		.cmd_id = HI3587_CMD_S_CTRL,
		.field_sz = 1,
		.word_sz = 16,
	},
	{
		.name = "Transmit Reset",
		.cmd_id = HI3587_CMD_S_TXRESET,
	},
	{
		.name = "Transmit Enable",
		.cmd_id = HI3587_CMD_S_TXENABLE,
	},
	{
		.name = "Get ACLK Divider",
		.cmd_id = HI3587_CMD_G_ACLKDIV,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Set ACLK Divider",
		.cmd_id = HI3587_CMD_S_ACLKDIV,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Load Transmit FIFO",
		.cmd_id = HI3587_CMD_S_TXFIFO,
		.field_sz = 32,
		.word_sz = 32,
	},
};

static struct chip_features hi3587_features = {
	.name = "hi3587",
	.cr_init = HI3587_CR_INIT,
	.use_fifo_tx = true,
	.num_txers = 1,
	.cmd_list = hi3587_commands,
	.ioctl_cmdIdx = {
		HI3587_CMD_S_MRESET_IDX,
		HI3587_CMD_G_STATUS_IDX,
		HI3587_CMD_G_CTRL_IDX,
		HI3587_CMD_S_CTRL_IDX,
	},
};

#endif
