#ifndef __HI359X_H
#define __HI359X_H

#ifndef BIT
#define BIT(x)	(1<<(x))
#endif

// cmd format: CR15:CR8 channel number (n), CR7:CR0 command (x)
#define HI359X_CMD_S_LABEL	0x1	// 128b field
#define HI359X_CMD_G_LABEL	0x2	// 128b field

#define HI359X_CMD_G_RXFIFO	0x3	// 32b field

#define HI359X_CMD_S_CTRL	0x4	// 16b field
#define HI359X_CMD_G_CTRL	0x5	// 16b field
#define HI359X_CMD_G_STATUS	0x6	// 16b field
#define HI359X_CMD_S_MRESET	0x7	// no field

#define HI359X_CMD_S_TXFIFO_HS	0x8	// 32b field
#define HI359X_CMD_S_TXFIFO_LS	0x9	// 32b field

// control register, accessed with commands n4, n5
#define CR0_RX_DATA_RATE_HS	0
#define CR0_RX_DATA_RATE_LS	BIT(0)
#define CR1_FLAGonRCVNOTEMPTY	0
#define CR1_FLAGonRCVFULL	BIT(1)
#define CR2_LABELRECOG_ENABLE	BIT(2)
#define CR3_RX_RESET		BIT(3)
#define CR4_RX_PARITY_ENABLE	BIT(4)
#define CR5_LOOPBACK_ENABLE	0
#define CR5_LOOPBACK_NORMAL	BIT(5)
#define CR6_RX_DECODE_EN	BIT(6) // bits 10 & 9 must match CR7 & CR8
#define CR7_ARINCb10		BIT(7)
#define CR8_ARINCb9		BIT(8)
#define CR9_LABELBITORDER_RVS	0
#define CR9_LABELBITORDER_FWD	BIT(9)

#define HI359X_CR_INIT	(	CR0_RX_DATA_RATE_HS	\
			|	CR1_FLAGonRCVNOTEMPTY	\
			|	CR3_RX_RESET		\
			|	CR5_LOOPBACK_NORMAL	\
			|	CR9_LABELBITORDER_FWD	)


// status registers, accessed with commands n6
#define SR0_RX1FIFO_EMPTY	BIT(0)
#define SR1_RX2FIFO_EMPTY	BIT(1)
#define SR2_RX3FIFO_EMPTY	BIT(2)
#define SR3_RX4FIFO_EMPTY	BIT(3)
#define SR4_RX5FIFO_EMPTY	BIT(4)
#define SR5_RX5FIFO_EMPTY	BIT(5)
#define SR6_RX6FIFO_EMPTY	BIT(6)
#define SR7_RX8FIFO_EMPTY	BIT(7)
#define SR8_RX1FIFO_FULL	BIT(8)
#define SR9_RX2FIFO_FULL	BIT(9)
#define SR10_RX3FIFO_FULL	BIT(10)
#define SR11_RX4FIFO_FULL	BIT(11)
#define SR12_RX5FIFO_FULL	BIT(12)
#define SR13_RX6FIFO_FULL	BIT(13)
#define SR14_RX7FIFO_FULL	BIT(14)
#define SR15_RX8FIFO_FULL	BIT(15)

// NOTE: the order of this enum matches the order of the commands below.
// this is intended to be an idx of the command descriptor array.
// structures are defined in holt_arinc.c
// the first 4 fields should always be the same from chip to chip.
enum {
	HI359X_CMD_S_MRESET_IDX = 0,
	HI359X_CMD_G_STATUS_IDX,
	HI359X_CMD_G_CTRL_IDX,
	HI359X_CMD_S_CTRL_IDX,
	HI359X_CMD_G_LABEL_IDX,
	HI359X_CMD_S_LABEL_IDX,
	HI359X_CMD_G_RXFIFO_IDX,
	HI359X_CMD_S_TXFIFO_HS_IDX,
	HI359X_CMD_S_TXFIFO_LS_IDX,
};

struct holt_arinc_cmd_desc hi359x_commands[] = {
        {
                .name = "Master Reset",
                .cmd_id = HI359X_CMD_S_MRESET,
        },
	{
		.name = "Get Status",
		.cmd_id = HI359X_CMD_G_STATUS,
		.field_sz = 1,
		.word_sz = 16,
	},
	{
		.name = "Get Control",
		.cmd_id = HI359X_CMD_G_CTRL,
		.field_sz = 1,
		.word_sz = 16,
	},
	{
		.name = "Set Control",
		.cmd_id = HI359X_CMD_S_CTRL,
		.field_sz = 1,
		.word_sz = 16,
	},
	{
		.name = "Get Labels",
		.cmd_id = HI359X_CMD_G_LABEL,
		.field_sz = 16,
		.word_sz = 8,
	},
	{
		.name = "Set Labels",
		.cmd_id = HI359X_CMD_S_LABEL,
		.field_sz = 16,
		.word_sz = 8,
	},
	{
		.name = "Read RX FIFO",
		.cmd_id = HI359X_CMD_G_RXFIFO,
		.field_sz = 4,
		.word_sz = 32,
	},
	{
		.name = "Load HS Transmit FIFO",
		.cmd_id = HI359X_CMD_S_TXFIFO_HS,
		.field_sz = 1,
		.word_sz = 32,
	},
	{
		.name = "Load LS Transmit FIFO",
		.cmd_id = HI359X_CMD_S_TXFIFO_LS,
		.field_sz = 1,
		.word_sz = 32,
	},
};

static struct chip_features hi3597_features = {
	.name = "hi3597",
	.cr_init = HI359X_CR_INIT,
	.baseminor_tx = 0,
	.num_txers = 1,
	.use_fifo_rx = true,
	.baseminor_rx = 1,
	.num_rxers = 8,
	.cmd_list = hi359x_commands,
	.ioctl_cmdIdx = {
		HI359X_CMD_S_MRESET_IDX,
		HI3587_CMD_G_STATUS_IDX,
		HI359X_CMD_G_CTRL_IDX,
		HI359X_CMD_S_CTRL_IDX,
		HI359X_CMD_G_LABEL_IDX,
		HI359X_CMD_S_LABEL_IDX,
	}
};

#endif
