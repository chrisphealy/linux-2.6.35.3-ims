//#define DEBUG
//#define FIFO_DEBUG
//#define POLL_DEBUG
//#define SPI_DEBUG
#define USE_WORDSIZE
#define USE_BUILTIN_SPI
#define USE_GPIO_FOR_CHIPSELECT
//#define USE_MEMS_CHECKER

#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/jiffies.h>
#include <linux/param.h>
#include <linux/sched.h>
#include <linux/time.h>
#include <linux/wait.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/timer.h>
#include <linux/poll.h>
#include <linux/platform_device.h>
#include <linux/list.h>

#include <linux/io.h>
#include <linux/clk.h>
#include <linux/spi/spi_bitbang.h>
#include <linux/platform_device.h>
#include <linux/fsl_devices.h>
#include <linux/spi/holt_arinc_platform.h>

#include <linux/arinc.h>

#ifndef MAX
#define  MAX( x, y ) (((x) > (y)) ? (x) : (y))
#endif
#ifndef MIN
#define  MIN( x, y ) (((x) < (y)) ? (x) : (y))
#endif
#ifdef FIFO_DEBUG
#define f_debug printk
#else
#define f_debug(...)
#endif
#ifdef POLL_DEBUG
#define p_debug printk
#else
#define p_debug(...)
#endif
#ifdef SPI_DEBUG
#define s_debug printk
#else
#define s_debug(...)
#endif

/*****************************************************************************/
// memory tracking
#ifdef USE_MEMS_CHECKER
int mems_cnt = 0;

// statically allocate this
struct memory_resource {
    void *base;
    size_t size;
} mems[16];

void mems_bug( void *ptr, size_t size )
{
    printk("%s: %p[%d]\n", __func__, ptr, size );
    BUG();
}
#endif

void *mems_alloc( size_t size )
{
    void *ptr = kzalloc( size, GFP_KERNEL);
#ifdef USE_MEMS_CHECKER
    mems[mems_cnt].base = ptr;
    mems[mems_cnt].size = size;
    mems_cnt++;
#endif
    return ptr;
}

#ifdef USE_MEMS_CHECKER
void mems_check( void *ptr, size_t size )
{
    int idx;
    for ( idx = 0; idx < mems_cnt; idx++ ) {
        if (( ptr >= mems[idx].base )&&( ptr + size <= mems[idx].base + mems[idx].size )) {
            break;
        }
    }
    if ( idx >= mems_cnt ) {
        mems_bug( ptr, size );
    }
}
#else
#define mems_check(...)
#endif

/** mxc spi definitions ******************************************************/
#ifdef USE_BUILTIN_SPI

#define MXC_eCSPI_RXDATA            0x00
#define MXC_eCSPI_TXDATA            0x04
#define MXC_eCSPI_CONTROLREG            0x08
#define MXC_eCSPI_CONFIGREG         0x0C
#define MXC_eCSPI_INTREG            0x10
#define MXC_eCSPI_DMAREG            0x14
#define MXC_eCSPI_STATREG           0x18
#define MXC_eCSPI_PERIODREG         0x1C
#define MXC_eCSPI_TESTREG           0x20
#define MXC_eCSPI_MSGDATA           0x24

#define MXC_eCSPI_TXFIFO_SIZE           64  // 64 words up to 32 bits

#define MXC_eCSPI_CONTROLREG_BURSTLEN(x)    ((x)<<20)   // number of bits to send in a burst +1 (word size)
#define MXC_eCSPI_CONTROLREG_CHANNELSELECT_MASK (0x3<<18)
#define MXC_eCSPI_CONTROLREG_CHANNELSELECT(x)   (((x)&0x3)<<18)
#define MXC_eCSPI_CONTROLREG_DRCTRL(x)      (((x)&0x3)<<16)
#define MXC_eCSPI_CONTROLREG_PREDIV(x)      (((x)&0xf)<<12)
#define MXC_eCSPI_CONTROLREG_POSTDIV(x)     (((x)&0xf)<<8)
#define MXC_eCSPI_CONTROLREG_CHANNELMODE(x) (((x)&0xf)<<4)
#define MXC_eCSPI_CONTROLREG_SMC        BIT(3)
#define MXC_eCSPI_CONTROLREG_XCH        BIT(2)
#define MXC_eCSPI_CONTROLREG_HW         BIT(1)
#define MXC_eCSPI_CONTROLREG_EN         BIT(0)

#define MXC_eCSPI_CONFIGREG_HTLENGTH(x)     (((x)&0x1f)<<24)
#define MXC_eCSPI_CONFIGREG_SCLK_CTL(x)     (((x)&0xf)<<20)
#define MXC_eCSPI_CONFIGREG_DATA_CTL(x)     (((x)&0xf)<<16)
#define MXC_eCSPI_CONFIGREG_SSB_POL(x)      (((x)&0xf)<<12)
#define MXC_eCSPI_CONFIGREG_SSB_CTL(x)      (((x)&0xf)<<8)
#define MXC_eCSPI_CONFIGREG_SCLK_POL(x)     (((x)&0xf)<<4)
#define MXC_eCSPI_CONFIGREG_SCLK_PHA(x)     (((x)&0xf)<<0)

#define MXC_eCSPI_STATREG_TC            BIT(7)  // transfer complete
#define MXC_eCSPI_STATREG_RO            BIT(6)  // receive overflow
#define MXC_eCSPI_STATREG_RF            BIT(5)  // receive fifo full
#define MXC_eCSPI_STATREG_RH            BIT(4)  // receive fifo half full
#define MXC_eCSPI_STATREG_RR            BIT(3)  // receive fifo read
#define MXC_eCSPI_STATREG_TF            BIT(2)  // tx fifo full
#define MXC_eCSPI_STATREG_TH            BIT(1)  // tx fifo half full
#define MXC_eCSPI_STATREG_TE            BIT(0)  // tx fifo empty

#define MXC_eCSPI_PREIODREG_CSD_CTRL(x)     (((x)&0x1f)<<16)
#define MXC_eCSPI_PERIODREG_CSRC        BIT(15) // clock source: 0 SiPI, 1 32khz
#define MXC_eCSPI_PREIODREG_SAMPLE(x)       ((x))   // number of clocks to wait

#define MXC_eCSPI_TESTREG_LBC           BIT(31)
#define MXC_eCSPI_TESTREG_CL(x)         (((x)>>28)&0xf)
#define MXC_eCSPI_TESTREG_RXCNT(x)      (((x)>>8)&0x7f)
#define MXC_eCSPI_TESTREG_TXCNT(x)      (((x)>>0)&0x7f)


/*!
 * This structure is a way for the low level driver to define their own
 * \b spi_master structure. This structure includes the core \b spi_master
 * structure that is provided by Linux SPI Framework/driver as an
 * element and has other elements that are specifically required by this
 * low-level driver.
 */
struct mxc_spi {
    struct device dev;
    struct completion xfer_done;    /* Completion flags used in data transfers */
    struct resource *res;       /* Resource structure, which will maintain base addresses and IRQs */
    void *base;         /* Base address of CSPI, used in readl and writel */
    int irq;            /* CSPI IRQ number */
    struct clk *clk;        /* CSPI Clock id */
    unsigned long spi_ipg_clk;  /* CSPI input clock SCLK */

    struct mutex        lock;   // spi lock

    int use_count;
    struct list_head slave_head;
};
struct mxc_spi *mxc_spi_dev = NULL;


/** mxc spi direct access ****************************************************/
static int mxc_spi_setup(struct spi_device *spi, struct spi_transfer *t)
{
    int ret = 0;
    int max_speed_hz, bits_per_word;
    unsigned int prediv, postdiv;   // clock control
    unsigned int conreg = 0;
//  struct mxc_spi *master_drv_data = spi_master_get_devdata(spi->master);
    struct mxc_spi *master_drv_data = mxc_spi_dev;

//  clk_enable(master_drv_data->clk);

    if ( t->bits_per_word ) {
        bits_per_word = t->bits_per_word;
    } else {
        bits_per_word = spi->bits_per_word;
    }
    conreg |= MXC_eCSPI_CONTROLREG_BURSTLEN( bits_per_word - 1 );

    max_speed_hz = spi->max_speed_hz;
    if ( t->speed_hz ) {
        max_speed_hz = MIN( max_speed_hz, t->speed_hz );
    }

    conreg |= MXC_eCSPI_CONTROLREG_CHANNELSELECT( spi->chip_select );

    // DRCTRL is Don't care (0)
    for ( postdiv = 0; postdiv < 16; postdiv++ ) {
        prediv = master_drv_data->spi_ipg_clk / (max_speed_hz << postdiv);
        if ( prediv <= 16 )
            break;
    }
    conreg |= MXC_eCSPI_CONTROLREG_PREDIV(prediv);
    conreg |= MXC_eCSPI_CONTROLREG_POSTDIV(postdiv);

    conreg |= MXC_eCSPI_CONTROLREG_CHANNELMODE( BIT(spi->chip_select));// sets channel to master
    // SMC: may want to set this to burst data when written to fifo
//  conreg |= MXC_eCSPI_CONTROLREG_SMC;         // enable auto-send
//  conreg |= MXC_eCSPI_CONTROLREG_HW;          // enable "HW trigger" mode

    // XCH: start spi transfer
    conreg |= MXC_eCSPI_CONTROLREG_EN; // enable

    __raw_writel(     0, master_drv_data->base + MXC_eCSPI_CONTROLREG);
    __raw_writel(conreg, master_drv_data->base + MXC_eCSPI_CONTROLREG);

    // setup config register
    conreg = 0;
//  conreg |= MXC_eCSPI_CONFIGREG_HTLENGTH(x);
//  conreg |= MXC_eCSPI_CONFIGREG_SCLK_CTL(x); // set inactive state 0:hi,1:lo
//  conreg |= MXC_eCSPI_CONFIGREG_DATA_CTL(x); // set inactive state 0:hi,1:lo
    if (spi->mode & SPI_CS_HIGH) {
        conreg |= MXC_eCSPI_CONFIGREG_SSB_POL( BIT(spi->chip_select));
    }
    conreg |= MXC_eCSPI_CONFIGREG_SSB_CTL( BIT(spi->chip_select)); // set single or multiple burst, if SMC is 0
    if (spi->mode & SPI_CPOL) {
        conreg |= MXC_eCSPI_CONFIGREG_SCLK_POL( BIT(spi->chip_select)); // clock polarity, 0:active hi
    }
    if (spi->mode & SPI_CPHA) {
        conreg |= MXC_eCSPI_CONFIGREG_SCLK_PHA( BIT(spi->chip_select)); // 0:Phase 0, 1:Phase 1
    }

    __raw_writel(conreg, master_drv_data->base + MXC_eCSPI_CONFIGREG);

    __raw_writel( MXC_eCSPI_PERIODREG_CSRC, master_drv_data->base + MXC_eCSPI_PERIODREG );

//  clk_disable(master_drv_data->clk);
    return ret;
}

// the eCSPI treats each burst of data (BURSTLEN) as an atomic transfer.
// obviously, this is a problem for multi-word transfers.
// instead, manipulate the chipselect as a GPIO.
static void mxc_spi_chipselect(struct spi_device *spi, bool active )
{
#ifdef USE_GPIO_FOR_CHIPSELECT
    int gpio = (int)spi->controller_data;
    active ^= (( spi->mode & SPI_CS_HIGH ) == SPI_CS_HIGH);
    gpio_set_value( gpio, !active );
//  printk("%s(%x): %d <- %d\n", __func__, spi->mode, gpio, !active );
#else
//  struct mxc_spi *master_drv_data = spi_master_get_devdata(spi->master);
    struct mxc_spi *master_drv_data = mxc_spi_dev; // spi_master_get_devdata(spi->master);
    int conreg = __raw_readl( master_drv_data->base + MXC_eCSPI_CONFIGREG );

    conreg &= ~MXC_eCSPI_CONTROLREG_CHANNELSELECT_MASK;
    conreg |= MXC_eCSPI_CONTROLREG_CHANNELSELECT( spi->chip_select );

    __raw_writel(conreg, master_drv_data->base + MXC_eCSPI_CONFIGREG);
#endif
}

#define NSEC_DELAY  100
#define USEC_DELAY  4
static int mxc_spi_do_transfer(struct spi_device *spi, struct spi_message *m)
{
    int ret = 0, i;
    int do_setup = 1;
    bool cs_change = 1;
    u32 count, data, total_count;
    volatile u32 conreg, status;
    //struct mxc_spi *master_drv_data = spi_master_get_devdata(spi->master);
    struct mxc_spi *master_drv_data = mxc_spi_dev; // FIXME later
    struct spi_transfer *t;
    int bits_per_word = 8; // spi->bits_per_word;
    int timeout_us;


    clk_enable(master_drv_data->clk);

    list_for_each_entry (t, &m->transfers, transfer_list) {

        // transfer over-riding settings
        if ( t->speed_hz ) {
            do_setup = 1;
        }
        if ( t->bits_per_word ) {
            do_setup = 1;
            bits_per_word = t->bits_per_word;
        }
        if (do_setup != 0) {
            do_setup = 0;
            ret = mxc_spi_setup(spi, t);
            if (ret < 0)
                break;
        }

        if ( cs_change ) {
            mxc_spi_chipselect( spi, BITBANG_CS_ACTIVE );
            ndelay(NSEC_DELAY);
        }
        cs_change = t->cs_change;

        for ( total_count = 0; total_count < t->len; total_count += count ) {
            count = MIN( t->len - total_count, MXC_eCSPI_TXFIFO_SIZE);

            // even if there is no data to transmit, the same clock is used for transmit
            // and receive. loading the transmit fifo tells the controller how much data
            // is expected to be tranfered
            data = 0;
            for (i = 0; i < count; i++) {
                if ( t->tx_buf ) {
                    if ( bits_per_word <= 8 ) {
                        data = ((u8 *)t->tx_buf)[total_count+i];
                    } else if ( bits_per_word <= 16 ) {
                        data = ((u16 *)t->tx_buf)[total_count+i];
                    } else { // bits > 16
                        data = ((u32 *)t->tx_buf)[total_count+i];
                    }
                }
                __raw_writel( data, master_drv_data->base + MXC_eCSPI_TXDATA );
            }

            // clear status
            status = __raw_readl( master_drv_data->base + MXC_eCSPI_STATREG );
            __raw_writel( status, master_drv_data->base + MXC_eCSPI_STATREG );

            // start transfer
            conreg = __raw_readl(master_drv_data->base + MXC_eCSPI_CONTROLREG);
            conreg |= MXC_eCSPI_CONTROLREG_XCH;
            __raw_writel( conreg, master_drv_data->base + MXC_eCSPI_CONTROLREG);

            // set timeout to something reasonable
            timeout_us = ((bits_per_word * count)<<20)/spi->max_speed_hz;
            timeout_us <<= 4;

            while ( timeout_us > 0 ) {
// it looks like any of these 3 methods could be used to determine the completion of the transfer
#if 0
                conreg = __raw_readl(master_drv_data->base + MXC_eCSPI_CONTROLREG);
                if (( conreg & MXC_eCSPI_CONTROLREG_XCH ) == 0 ) {
                    s_debug("%s: XCH clear\n", __func__ );
                    break;
                }
#endif
#if 1
                status = __raw_readl( master_drv_data->base + MXC_eCSPI_STATREG );
                if ( status & MXC_eCSPI_STATREG_TC ) {
                    s_debug("%s: TC set\n", __func__ );
                    break;
                }
#endif
#if 0
                // this method fails, showing rcv count == 0
                status = __raw_readl( master_drv_data->base + MXC_eCSPI_TESTREG );
                if ( MXC_eCSPI_TESTREG_RXCNT( status ) == count ) {
                    s_debug("%s: rx count reached\n", __func__ );
                    break;
                }
#endif
                udelay( USEC_DELAY );
                timeout_us -= USEC_DELAY;
            }
            if ( timeout_us < 0 )
                printk(KERN_ERR"%s: MXC eCSPI transfer timeout!\n", __func__ );

            for (i = 0; i < count; i++) {
                data = __raw_readl(master_drv_data->base + MXC_eCSPI_RXDATA);
                if ( t->rx_buf ) {
                    if ( bits_per_word <= 8 ) {
                        ((u8 *)t->rx_buf)[total_count+i] = data;
                    } else if ( bits_per_word <= 16 ) {
                        ((u16 *)t->rx_buf)[total_count+i] = data;
                    } else { // bits > 16
                        ((u32 *)t->rx_buf)[total_count+i] = data;
                    }
                }
            }
        }

        if (t->delay_usecs) {
            udelay(t->delay_usecs);
        }

        if (!cs_change)
            continue;

        ndelay(NSEC_DELAY);
        mxc_spi_chipselect(spi, BITBANG_CS_INACTIVE);
        ndelay(NSEC_DELAY);
    }

    mxc_spi_chipselect(spi, BITBANG_CS_INACTIVE); // make sure the SS is inactive when we're done.
    clk_disable(master_drv_data->clk);
    return ret;
}

static int mxc_spi_init(struct platform_device *pdev)
{
    int ret = 0;
    struct spi_master *master;
    struct mxc_spi *master_drv_data = mxc_spi_dev;
    struct arinc_spi_platform_info *platform = (struct arinc_spi_platform_info *)pdev->dev.platform_data;
    struct platform_device *cspi_dev = platform->cspi_info;
    char clock_desc[32];

    if ( master_drv_data == NULL ) {
        master_drv_data = mems_alloc(sizeof(struct mxc_spi));
        if (!master_drv_data) {
            dev_err(&cspi_dev->dev, "can't alloc for mxc_spi_dev\n");
            return -ENOMEM;
        }

        master = mems_alloc(sizeof(struct spi_master));
        if ( master == NULL ) {
            dev_err(&cspi_dev->dev, "can't alloc for spi_master\n");
            return -ENOMEM;
        }
        INIT_LIST_HEAD( &master_drv_data->slave_head );

        platform_set_drvdata( cspi_dev, master );
        spi_master_set_devdata( master, master_drv_data );

        master->bus_num = cspi_dev->id + 1;
        master->num_chipselect = 4; // mxc_platform_info->maxchipselect;
        master->mode_bits = SPI_CPOL | SPI_CPHA | SPI_CS_HIGH;

        init_completion(&master_drv_data->xfer_done);
        mutex_init( &master_drv_data->lock );

        master_drv_data->res = platform_get_resource(cspi_dev, IORESOURCE_MEM, 0);
        if (!master_drv_data->res) {
            dev_err(&cspi_dev->dev, "can't get platform resource for CSPI%d\n",
                master->bus_num);
            ret = -ENOMEM;
            goto err;
        }

        if (!request_mem_region(master_drv_data->res->start,
                    master_drv_data->res->end -
                    master_drv_data->res->start + 1, cspi_dev->name)) {
            dev_err(&cspi_dev->dev, "request_mem_region failed for CSPI%d\n",
                master->bus_num);
            ret = -ENOMEM;
            goto err;
        }

        master_drv_data->base = ioremap(master_drv_data->res->start,
            master_drv_data->res->end - master_drv_data->res->start + 1);
        if (!master_drv_data->base) {
            dev_err(&cspi_dev->dev, "invalid base address for CSPI%d\n",
                master->bus_num);
            ret = -EINVAL;
            goto err1;
        }

        master_drv_data->irq = platform_get_irq(cspi_dev, 0);
        if (master_drv_data->irq < 0) {
            dev_err(&cspi_dev->dev, "can't get IRQ for CSPI%d\n",
                master->bus_num);
            ret = -EINVAL;
            goto err1;
        }

// TODO: not ready for interrupts yet
#if 0
        ret = request_irq(master_drv_data->irq, mxc_spi_isr,
                  0, "CSPI_IRQ", master_drv_data);
        if (ret != 0) {
            dev_err(&cspi_dev->dev, "request_irq failed for CSPI%d\n",
                mxc_spi_dev->bus_num);
            goto err1;
        }
#endif

        sprintf( clock_desc, "%s.%d", cspi_dev->name, cspi_dev->id );
        master_drv_data->clk = clk_get_sys( clock_desc, NULL );
        if ( master_drv_data->clk < 0 ) {
            dev_err(&cspi_dev->dev, "clk_get failed for CSPI%d\n",
                cspi_dev->id+1);
            goto err1;
        }
        clk_enable(master_drv_data->clk);
        master_drv_data->spi_ipg_clk = clk_get_rate(master_drv_data->clk);

        udelay(1);
        __raw_writel( MXC_eCSPI_CONTROLREG_EN, master_drv_data->base + MXC_eCSPI_CONFIGREG );
        __raw_writel( MXC_eCSPI_PERIODREG_CSRC, master_drv_data->base + MXC_eCSPI_PERIODREG );
        __raw_writel( 0, MXC_eCSPI_INTREG + master_drv_data->base + MXC_eCSPI_CONFIGREG );

        printk(KERN_INFO "ARINC CSPI: %s-%d probed\n", cspi_dev->name, cspi_dev->id);

        clk_disable(master_drv_data->clk);

        mxc_spi_dev = master_drv_data;
    }
    master_drv_data->use_count++;
    return ret;

      err1:
    iounmap(master_drv_data->base);
    release_mem_region(cspi_dev->resource[0].start,
               cspi_dev->resource[0].end - cspi_dev->resource[0].start + 1);
      err:
    kfree(master);
    kfree(mxc_spi_dev);
    platform_set_drvdata(cspi_dev, NULL);
    return ret;
}

static int mxc_spi_remove(struct platform_device *pdev)
{
    if (mxc_spi_dev) {
        mxc_spi_dev->use_count--;
        if ( mxc_spi_dev->use_count == 0 ) {
            struct arinc_spi_platform_info *platform = (struct arinc_spi_platform_info *)pdev->dev.platform_data;
            struct platform_device *cspi_dev = platform->cspi_info;
            struct spi_master *master = platform_get_drvdata(cspi_dev);
//          struct mxc_spi *master_drv_data = spi_master_get_devdata(master);
            struct mxc_spi *master_drv_data = mxc_spi_dev;

            /* Disable the CSPI module */
            clk_enable(master_drv_data->clk);
            __raw_writel( 0, master_drv_data->base + MXC_eCSPI_CONFIGREG );
            clk_disable(master_drv_data->clk);

            /* Unregister for SPI Interrupt */
//          free_irq(master_drv_data->irq, master_drv_data);

            iounmap(master_drv_data->base);
            release_mem_region( master_drv_data->res->start,
                        master_drv_data->res->end -
                        master_drv_data->res->start + 1);


            printk(KERN_INFO "ARINC CSPI: %s-%d removed\n", cspi_dev->name, cspi_dev->id);
            kfree(master);
            kfree(mxc_spi_dev);
            platform_set_drvdata(cspi_dev, NULL);
        }
    }
    return 0;
}

#endif // USE_BUILTIN_SPI
/*****************************************************************************/
/** fifo handling ************************************************************/
#define FIFO_SIZE   (1<<(f->order)) // power of 2
#define FIFO_SZ_MASK    (FIFO_SIZE-1)
typedef struct fifo {
    int     head;
    int     tail;
    int     order;
    u32     *buffer;
} fifo_t;

static void print_fifo_info( fifo_t *f )
{
    f_debug("fifo %p: head %08x, tail %08X, size %08X mask %08X\n", f, f->head, f->tail, FIFO_SIZE, FIFO_SZ_MASK );
}

// returns the number of elements in the fifo
static inline int f_depth( fifo_t *f )
{
#if 0 // def FIFO_DEBUG
    int depth = (((f->head + FIFO_SIZE) - f->tail )&FIFO_SZ_MASK);
    printk("%s %p: %d\n", __func__, f, depth );
    return depth;
#else
    mems_check( f, 0 );
    return (((f->head + FIFO_SIZE) - f->tail )&FIFO_SZ_MASK);
#endif
}

// returns true if the fifo is full.
static bool f_full( fifo_t *f )
{
#ifdef FIFO_DEBUG
    bool full = (((FIFO_SIZE-1) - f_depth(f)) <= 0);
    printk("%s %p: %d\n", __func__, f, full );
    return full;
#else
    return ((FIFO_SIZE-1) - f_depth(f)) <= 0;
#endif
}

// returns true if the fifo is empty.
static bool f_empty( fifo_t *f )
{
#ifdef FIFO_DEBUG
    bool empty = (f_depth(f) == 0);
    printk("%s %p: %d\n", __func__, f, empty );
    return empty;
#else
    return (f_depth(f) == 0);
#endif
}

// adds an element to the fifo, if not full
static void f_in( fifo_t *f, u32 data )
{
    mems_check( f, 0 );
    print_fifo_info(f);
    f_debug("%s: fifo %p <- %08X\n", __func__, f, data );
    if ( !f_full( f )) {
        mems_check( &f->buffer[f->head], 4 );
        f->buffer[f->head] = data;
        f->head = (f->head + 1) & FIFO_SZ_MASK;
    }
    print_fifo_info(f);
}

// returns an element from the fifo, if not empty
static u32 f_out( fifo_t *f )
{
    u32 data = 0;
    mems_check( f, 0 );
    print_fifo_info(f);
    if ( !f_empty( f )) {
        mems_check( &f->buffer[f->tail], 4 );
        data = f->buffer[f->tail];
        f_debug("%s: fifo %p -> %08X\n", __func__, f, data );
        f->tail = (f->tail + 1) & FIFO_SZ_MASK;
    } else {
        printk( KERN_INFO "%s %p: fifo underrun\n", __func__, f );
    }
    print_fifo_info(f);
    return data;
}

static void f_zero( fifo_t *f )
{
    f_debug("%s %p\n", __func__, f );
    f->head = f->tail = 0;
}

/** structs and enums ********************************************************/
// this is an index in to the chip_fearture and driver_ids data structures
enum arinc_type {
    hi3587 = 0,
    hi3597,
    hi3593tx,
    hi3593r1,
    hi3593r2,

    arinc_type_max,
};

// try to keep these minimum number of commands in the same order for ease of implementation
enum arinc_cmds {
    CMD_S_MRESET_IDX = 0,
    CMD_G_STATUS_IDX,
    CMD_G_CTRL_IDX,
    CMD_S_CTRL_IDX,
};

struct holt_arinc_cmd_desc {
    char name[32];      // description of command function
    int cmd_id;     // the command id recognized by the chip
    int field_sz;       // the size of the data field for this command, in words
    int word_sz;        // the size of the data words for this command, in bits
};

struct chip_features {
    char name[32];

    int cr_init;            // control register initialization value
    bool use_fifo_tx;
    int baseminor_tx;       // starting minor number for tx inodes
    int num_txers;          // number of arinc transmit channels
    bool use_fifo_rx;
    int baseminor_rx;       // starting minor number for rx inodes
    int num_rxers;          // number of arinc receive channels

    struct holt_arinc_cmd_desc *cmd_list;   // list of supported commands

    // mapping of ioctl commands to the chip command list, defined above
    int ioctl_cmdIdx[ARINC_NUMBER_IOCTL_CMDS];
};

// among other things, these files include arrays of the above structures
#include "holt_hi3587.h"
#include "holt_hi359x.h"
#include "holt_hi3593.h"

#define TX_LOCKOUT_NS	2560000	// ideal lockout time for low speed, assuming 32/100k/8
#define TX_LOCKOUT_NS_DEFAULT (TX_LOCKOUT_NS + (TX_LOCKOUT_NS>>5))// defines the default tx lockout time for low speed. this value is div by 8 for high speed.

static struct class *arinc_class;
struct holt_arinc_dev {
    struct list_head    device_list;

    int major;  // every chip will have it's own major, dynamically assigned.

    // char device structs
    struct cdev     cdev;
    dev_t           devt;

    // the usual spi stuff...
    struct spi_device   spi;
//  struct mutex        lock;   // spi lock

    // irq used for this device.
    int         rx_irq;
    int         tx_irq;

    // these are the features for the chip associated with this device
    enum arinc_type     type;
    struct chip_features    *chip;

    // driver stuff
    struct semaphore    sem_rx; // read lock
    struct semaphore    sem_tx; // write lock
    wait_queue_head_t   queue_rx;   // read wait queue
    wait_queue_head_t   queue_tx;   // write wait queue
    struct work_struct  work_rx;    // read thread
    struct work_struct  work_tx;    // transmit thread
    struct timer_list   timeout_tx;

    // special cases
    struct timespec	ts_txlast;	// simple transmit timestamp. prevents overwriting the tx register
    bool 		high_speed;	// HS indicator. indicates which transmit register is being used, where such a thing is possible.
    u32			tx_lockout_ns;	// tx lockout, used in conjuntion with above, is the minimum time that must transpire between tx reg writes.

    // some devices have multiple tx commands, depending on speed
    // this is the one to use, may be none..., some chips have rx, some don't
    struct holt_arinc_cmd_desc *cmd_tx;
    struct holt_arinc_cmd_desc *cmd_rx;

    // data fifos
    fifo_t *fifo_rx;
    fifo_t *fifo_tx;
};

/** supported driver and feature identification ******************************/
static struct chip_features *holt_features[] = {
    [hi3587] = &hi3587_features,
    [hi3597] = &hi3597_features,
    [hi3593tx] = &hi3593tx_features,
    [hi3593r1] = &hi3593r1_features,
    [hi3593r2] = &hi3593r2_features,
};

//static struct spi_device_id holt_arinc_driver_ids[] = {
static struct platform_device_id holt_arinc_driver_ids[] = {
    {
        .name           = "hi3587-arinc",
        .driver_data    = hi3587,
    }, {
        .name           = "hi3597-arinc",
        .driver_data    = hi3597,
    }, {
        .name           = "hi3593tx-arinc",
        .driver_data    = hi3593tx,
    }, {
        .name           = "hi3593r1-arinc",
        .driver_data    = hi3593r1,
    }, {
        .name           = "hi3593r2-arinc",
        .driver_data    = hi3593r2,
    }, {
    }
};
//MODULE_DEVICE_TABLE(spi, holt_arinc_driver_ids);
//MODULE_DEVICE_TABLE(platform, holt_arinc_driver_ids);

#define TIMEOUT_IN_JIFFIES  (10*HZ)

/** function prototypes ******************************************************/
//static int holt_arinc_probe(struct spi_device *spi);
//static int __devexit holt_arinc_remove(struct spi_device *spi);
static int holt_arinc_probe(struct platform_device *);
static int __devexit holt_arinc_remove(struct platform_device *);

/** spi access functions *****************************************************/
static int arinc_bus_read( struct holt_arinc_dev *hdev, u8 cmd, void *buf, size_t word_sz, size_t count )
{
    int ret = 0;
    struct spi_message m;
    struct spi_transfer t[2];

    if (unlikely(!count))
        return count;

    s_debug( "%s: cmd 0x%02X, data word_sz %d, count %d\n", __func__,
        cmd, word_sz, count );

    spi_message_init(&m);
    memset(t, 0, sizeof(t));

    t[0].tx_buf = (char *)&cmd;
    t[0].len = 1;
    t[0].bits_per_word = 8;
    spi_message_add_tail(&t[0], &m);

    t[1].rx_buf = buf;
#ifdef USE_WORDSIZE
    t[1].len = count;
    t[1].bits_per_word = word_sz;
#else
    t[1].len = count * word_sz/8; // words to bytes
#endif
    spi_message_add_tail(&t[1], &m);

    mutex_lock( &mxc_spi_dev->lock );
    mxc_spi_do_transfer( &hdev->spi, &m );
    mutex_unlock( &mxc_spi_dev->lock );

    if ( ret < 0 ) {
        dev_err( &hdev->spi.dev, "read %zu bytes: err. %d\n",
            count * word_sz/8, ret );
    }

    s_debug( "\tread %zu bytes.\n",
        count * word_sz/8 );

#ifndef USE_WORDSIZE
    switch ( word_sz ) {
    default:
    case 8:
        break;
    case 16:
        *(u16 *)buf = __be16_to_cpu( *(u16 *)buf );
        break;
    case 32:
        *(u32 *)buf = __be32_to_cpu( *(u32 *)buf );
        break;
    }
#endif

    return ret ? : count;
}

static int arinc_bus_write( struct holt_arinc_dev *hdev, u8 cmd, void *buf, size_t word_sz, size_t count )
{
    int ret = 0;
    struct spi_message m;
    struct spi_transfer t[2];

    switch ( word_sz ) {
        default:
        case 8:
        case 16:
            break;
        case 32:
            pr_debug("Write 0x%08X\n", *(u32 *)buf);
            break;
    }

    spi_message_init(&m);
    memset(t, 0, sizeof(t));

    t[0].tx_buf = (char *)&cmd;
    t[0].len = 1;
    t[0].bits_per_word = 8;
    spi_message_add_tail(&t[0], &m);

    if ( buf && count ) {
#ifndef USE_WORDSIZE
        switch ( word_sz ) {
        default:
        case 8:
            break;
        case 16:
            *(u16 *)buf = __cpu_to_be16( *(u16 *)buf );
            break;
        case 32:
            *(u32 *)buf = __cpu_to_be32( *(u32 *)buf );
            break;
        }
#endif

        t[1].tx_buf = buf;
        t[1].len = count * word_sz/8; // words to bytes
#ifdef USE_WORDSIZE
        t[1].len = count;
        t[1].bits_per_word = word_sz;
#else
        t[1].len = count * word_sz/8; // words to bytes
#endif
        spi_message_add_tail(&t[1], &m);
    }

    mutex_lock( &mxc_spi_dev->lock );
    mxc_spi_do_transfer( &hdev->spi, &m );
    mutex_unlock( &mxc_spi_dev->lock );

    return ret;
}

static int arinc_read_status( struct holt_arinc_dev *hdev, void *status )
{
    int ret = 0;
	u16 cmd_idx = hdev->chip->ioctl_cmdIdx[CMD_G_STATUS_IDX];
	u16 cmd_id = hdev->chip->cmd_list[cmd_idx].cmd_id;
	u16 word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;
	u16 field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;

    ret = arinc_bus_read( hdev, cmd_id, status, word_sz, field_sz );

    switch ( word_sz ) {
    case 8:
        pr_debug("%s@%d(%s): %02X\n", __func__, __LINE__, hdev->chip->name, *(u8 *)status );
        break;
    default:
    case 16:
        pr_debug("%s@%d(%s): %04X\n", __func__, __LINE__, hdev->chip->name, *(u16 *)status );
        break;
    }
    return ret;
}

/** interrupt handling *******************************************************/
static void holt_rx_worker(struct work_struct *work)
{
    int ret = 0, idx;
    u8 status[2], cmd;
    u32 data;
    bool repeat = false;
    struct holt_arinc_dev *hdev = container_of(work, struct holt_arinc_dev, work_rx);

    pr_debug("%s@%d\n", __func__, __LINE__ );

    do {
        // read status
        ret = arinc_read_status( hdev, status );
        if ( ret < 0 ) {
            dev_err( &hdev->spi.dev, "%s@%d: err %d\n", __func__, __LINE__, ret );
            break;
        }

        repeat = false; // won't repeat unless at least one channel has data
        // check every channel for not empty
        for ( idx = 0; idx < hdev->chip->num_rxers; idx++ ) {
            if (( status[0] & BIT(idx)) == 0 ) {
                // found one that's not empty, run again until it is
                repeat = true;

                switch ( hdev->type ) {
                case hi3597:
                    cmd = ((idx+1) << 4)|hdev->cmd_rx->cmd_id;
                    break;
                case hi3593r1:
                case hi3593r2:
                default:
                    cmd = hdev->cmd_rx->cmd_id;
                    break;
                }

                ret = arinc_bus_read( hdev,
                    cmd,
                    &data,
                    hdev->cmd_rx->word_sz,
                    1 );

                pr_debug("%s@%d - rx channel %d: %08X\n", __func__, __LINE__, idx+1, data );

                if ( ret < 0 ) {
                    dev_err( &hdev->spi.dev, "%s@%d: err %d\n", __func__, __LINE__, ret );
                    break;
                }

                /*
                 * FIXME: there is no protection against fifo overrun. the sw fifo should
                 * be many times larger than the hw fifo
                 */
                f_in( &hdev->fifo_rx[idx], data );

                // wake up anyone waiting on data
                pr_debug("%s waking up rx'ers\n", __func__ );
                wake_up_interruptible( &hdev->queue_rx );
            }
        }
    } while ( repeat );
//  enable_irq( hdev->rx_irq );
}

// threaded irq handler
static irqreturn_t holt_irq(int irq, void *dev_id)
{
    struct holt_arinc_dev *hdev = (struct holt_arinc_dev *)dev_id;

//  disable_irq_nosync( irq );

    /*
     * it doesn't really matter what the status is.
     * if there is any interrupt from the device, schedule the appropriate worker!
     */

    switch ( hdev->type ) {
    case hi3587:
    {
#ifdef DEBUG
        u8 status[2];
        arinc_read_status( hdev, status );
        if ( status[0] & SR3_TXFIFO_EMPTY ) {
            pr_debug("%s: transmit fifo empty\n", hdev->chip->name );
        }
        if ( status[0] & SR4_TXFIFO_HALF ) {
            pr_debug("%s: transmit fifo half empty\n", hdev->chip->name );
        }
        if ( status[0] & SR5_TXFIFO_FULL ) {
            pr_debug("%s: transmit fifo full \n", hdev->chip->name );
        }
#endif
        if ( schedule_work( &hdev->work_tx ) == 0 )
            pr_debug("%s: TX worker already scheduled\n", __func__ );
    
        break;
	}
    case hi3597: {
#ifdef DEBUG
        int idx;
        u8 status[2];
        arinc_read_status( hdev, status );
        // this section just prints debug info.
        for ( idx = 0; idx < 8; idx++ ) {
            if ( status[0] & BIT(idx)) {
                pr_debug("%s: receive fifo %d empty\n", hdev->chip->name, idx+1 );
            } else {
                pr_debug("%s: receive fifo %d not empty\n", hdev->chip->name, idx+1 );
            }
        }
        // check for fifo full
        for ( idx = 0; idx < 8; idx++ ) {
            if ( status[1] & BIT(idx)) {
                pr_debug("%s: receive fifo %d full\n", hdev->chip->name, idx+1 );
            }
        }
#endif
        if ( schedule_work( &hdev->work_rx ) == 0 )
            pr_debug("%s: RX worker already scheduled\n", __func__ );
        break;
    }
    case hi3593tx: {
#ifdef DEBUG
        u8 status[2];
        arinc_read_status( hdev, status );
        if ( status[0] & TS2_TFFULL ) {
            pr_debug("%s: Tx FIFO Full!\n", hdev->chip->name );
        }
        if ( status[0] & TS1_TFHALF ) {
            pr_debug("%s: Tx FIFO half-full\n", hdev->chip->name );
        }
        if ( status[0] & TS0_TFEMPTY ) {
            pr_debug("%s: Tx FIFO empty\n", hdev->chip->name );
        }
#endif
        if ( schedule_work( &hdev->work_tx ) == 0 )
            pr_debug("%s: TX worker already scheduled\n", __func__ );
        break;
    }
    case hi3593r1:
    case hi3593r2: {
#ifdef DEBUG
        u8 status[2];
        arinc_read_status( hdev, status );
        if ( status[0] & RS5_PL3 ) {
            pr_debug("%s: Priority Label RX - Mailbox 3\n", hdev->chip->name );
        }
        if ( status[0] & RS4_PL2 ) {
            pr_debug("%s: Priority Label RX - Mailbox 2\n", hdev->chip->name );
        }
        if ( status[0] & RS3_PL1 ) {
            pr_debug("%s: Priority Label RX - Mailbox 1\n", hdev->chip->name );
        }
        if ( status[0] & RS2_FFFULL ) {
            pr_debug("%s: Rx FIFO full\n", hdev->chip->name );
        }
        if ( status[0] & RS1_FFHALF ) {
            pr_debug("%s: Rx FIFO half-full\n", hdev->chip->name );
        }
        if ( status[0] & RS0_FFEMPTY ) {
            pr_debug("%s: Rx FIFO empty\n", hdev->chip->name );
        } else {
            pr_debug("%s: Rx FIFO NOT empty!\n", hdev->chip->name );
        }
#endif
        if ( schedule_work( &hdev->work_rx ) == 0 )
            pr_debug("%s: RX worker already scheduled\n", __func__ );
        break;
    }
    default:
        break;
    }
    return IRQ_HANDLED;
}

/** transmit worker **********************************************************/
static void timeout_tx(unsigned long arg)
{
    struct holt_arinc_dev *hdev = (struct holt_arinc_dev *)arg;
    dev_err( &hdev->spi.dev, "ARINC transmit timeout\n" );
}

// only applies to devices with transmit status
static void holt_tx_worker(struct work_struct *work)
{
    int ret = 0, workToDo = 1;
    int wordsToWrite = 1;
    int wordsWritten = 0;
    const int TX_FIFO_SIZE = 32;
    u32 data;
    struct holt_arinc_dev *hdev;
    u8 status[1];

    hdev = container_of(work, struct holt_arinc_dev, work_tx);

    while (workToDo--) {
        // read status
        ret = arinc_read_status( hdev, status );
        if ( ret < 0 ) {
            dev_err( &hdev->spi.dev, "%s: err %d\n", __func__, ret );
            return;
        }

        switch ( hdev->type ) {
        case hi3587:
            // i don't know the exact circumstances under which these interrupts occur, e.g,
            // if half-full status occurs exactly at half, so this is my best guess.
            if ( status[0] & SR5_TXFIFO_FULL ) {
                // just exit. when a half-full interupt occurs, this routine will be rescheduled
                pr_debug("%s: exiting, device fifo full, fifo_tx %p\n", __func__, hdev->fifo_tx );
                wordsToWrite = 0;
                return;
            } else if ( status[0] & SR3_TXFIFO_EMPTY ) {
                // disable tx timeout, tx fifo is empty
                del_timer_sync( &hdev->timeout_tx );
                wordsToWrite = TX_FIFO_SIZE;
            } else if ( status[0] & SR4_TXFIFO_HALF ) {
                // an interrupt occurred and tx fifo is not full, so maybe tx is still working...
                // reset timeout
                mod_timer( &hdev->timeout_tx, jiffies + TIMEOUT_IN_JIFFIES );
                wordsToWrite = 1;
            }
            else {
                // an interrupt occurred and tx fifo is not full, so maybe tx is still working...
                // reset timeout
                mod_timer( &hdev->timeout_tx, jiffies + TIMEOUT_IN_JIFFIES );
                // It is not half full so we have room for 16
                wordsToWrite = TX_FIFO_SIZE / 2;
            }
            break;
        case hi3593tx:
            // TODO: read 3593 interrupts lines to reduce CPU/bus usage. they indicate
            //  both TX_EMPTY AND TX_FULL!

            // i don't know the exact circumstances under which these interrupts occur, e.g,
            // if half-full status occurs exactly at half, so this is my best guess.
            if ( status[0] & TS2_TFFULL ) {
                // just exit. when a half-full interupt occurs, this routine will be rescheduled
                pr_debug("%s: exiting, device fifo full, fifo_tx %p\n", __func__, hdev->fifo_tx );
                wordsToWrite = 0;
                return;
            } else if ( status[0] & TS0_TFEMPTY ) {
                // disable tx timeout, tx fifo is empty
                del_timer_sync( &hdev->timeout_tx );
                wordsToWrite = TX_FIFO_SIZE;
            } else if ( status[0] & TS1_TFHALF ) {
                // an interrupt occurred and tx fifo is not full, but it is at least half full.
                // reset timeout
                mod_timer( &hdev->timeout_tx, jiffies + TIMEOUT_IN_JIFFIES );
                wordsToWrite = 1;
            }
            else {
                mod_timer( &hdev->timeout_tx, jiffies + TIMEOUT_IN_JIFFIES );
                // It is not half full so we have room for 16
                wordsToWrite = TX_FIFO_SIZE / 2;
            }
            break;
        default:
            break;
        }
        while (wordsWritten < wordsToWrite) {
            if ( f_empty( hdev->fifo_tx )) {
                pr_debug("%s: exiting, fifo_tx %p empty\n", __func__, hdev->fifo_tx );
                break;
            }

            pr_debug("%s: transmitting from fifo_tx %p, depth %d\n", __func__, hdev->fifo_tx, f_depth( hdev->fifo_tx ));
            data = f_out( hdev->fifo_tx );
            ret = arinc_bus_write( hdev,
                                   hdev->cmd_tx->cmd_id,
                                   &data,
                                   hdev->cmd_tx->word_sz,  // u32
                                   1 );
            wordsWritten++;
        }
        // added data to the fifo, reset timer
        mod_timer( &hdev->timeout_tx, jiffies + TIMEOUT_IN_JIFFIES );
    }

    // wake up anyone waiting on write
    pr_debug("%s waking up tx'ers\n", __func__ );
    wake_up_interruptible( &hdev->queue_tx );

    // enable tx empty interrupt
//  enable_irq( hdev->tx_irq );
}

/** char driver fops *********************************************************/
// read from buffer
static ssize_t holt_arinc_read(struct file *filp, char __user *to, size_t count, loff_t *offset)
{
    int read, channel;
    u32 data;
    struct inode *inode = filp->f_dentry->d_inode;
    struct holt_arinc_dev *hdev = (struct holt_arinc_dev *)filp->private_data;

    pr_debug("%s@%d: count %d bytes\n", __func__, __LINE__, count );

    if (( hdev->cmd_rx == NULL )||( hdev->chip->use_fifo_rx == false ))
        return -ENODEV;

    channel = iminor( inode ) - hdev->chip->baseminor_rx;

    if ( down_interruptible( &hdev->sem_rx ))
        return -ERESTARTSYS;

    for ( read = 0; read < count; read += sizeof(data)) {
        while ( f_empty( &hdev->fifo_rx[channel] )) {
            up( &hdev->sem_rx );
            if ( filp->f_flags & O_NONBLOCK ) {
                return read ? read: -EAGAIN;
            } else {
                pr_debug("%s waiting on channel %d\n", __func__, channel );
                if ( wait_event_interruptible( hdev->queue_rx, !f_empty( &hdev->fifo_rx[channel] ))) {
                    return -ERESTARTSYS;
                }
                pr_debug("%s woke up\n", __func__ );
            }
            if ( down_interruptible( &hdev->sem_rx ))
                return -ERESTARTSYS;
        }

        data = f_out( &hdev->fifo_rx[channel] );
        copy_to_user( to + read, &data, sizeof(data));
    }

    up( &hdev->sem_rx );
    return read;
}

// write to device.
// for chips that have a simple transmitter, hi3597, there is no fifo, status, etc. so just write 1 word and exit
static ssize_t holt_arinc_simple_write(struct file *filp, const char __user *from, size_t count, loff_t *offset)
{
    int ret = 0;
    struct holt_arinc_dev *hdev = (struct holt_arinc_dev *)filp->private_data;
    struct timespec ts_delta;
    u32 min_latency_ns;
    u32 data;

    pr_debug("%s@%d\n", __func__, __LINE__ );

    if ( hdev->cmd_tx == NULL )
        return -ENODEV;

    if ( down_interruptible( &hdev->sem_tx ))
        return -ERESTARTSYS;

    copy_from_user( &data, from, 1*sizeof(u32));

    // hi3597 transmits data backwards!
    if ( hdev->type == hi3597 ) {
        u32 temp, idx, old = data;
        for ( temp = 0, idx = 0; idx < 32; idx++ ) {
            temp <<= 1;
            temp |= data & BIT(0);
            data >>= 1;
        }
        pr_debug("%s: data %08X -> %08X\n", __func__, old, temp );
        data = temp;
    }

    // make sure the correct minimum amount of time has gone by since the last
    // transfer, taking in to account the transmit speed (HS vs LS)
    // the formula is this: 32bits / bitrate, converted to ns
    //min_latency_ns = 2560000;
    min_latency_ns = hdev->tx_lockout_ns;
    if ( hdev->high_speed ) {
        //min_latency_ns = 320000;
		min_latency_ns >>= 3;
    }
    ts_delta = timespec_sub( current_kernel_time(), hdev->ts_txlast );
    if ( ts_delta.tv_nsec < min_latency_ns ) {
        pr_debug("%s@%d: waited %lu nsec\n", __func__, __LINE__,
            min_latency_ns - ts_delta.tv_nsec );
        ndelay( min_latency_ns - ts_delta.tv_nsec ); // min delay is 1uS
    }

    ret = arinc_bus_write( hdev,
        hdev->cmd_tx->cmd_id,
        &data,
        hdev->cmd_tx->word_sz,
        hdev->cmd_tx->field_sz );

    hdev->ts_txlast = current_kernel_time();

    up( &hdev->sem_tx );
    return ret ? : 1*sizeof(u32);
}

// write to device
static ssize_t holt_arinc_write(struct file *filp, const char __user *from, size_t count, loff_t *offset)
{
    int ret = 0, wrote, channel;
    struct inode *inode = filp->f_dentry->d_inode;
    struct holt_arinc_dev *hdev = (struct holt_arinc_dev *)filp->private_data;
    u32 data;

    pr_debug("%s@%d\n", __func__, __LINE__ );

    // if the device does not use a fifo, call the simple write routine instead
    if ( hdev->chip->use_fifo_tx == false )
        return holt_arinc_simple_write( filp, from, count, offset );

    if ( hdev->cmd_tx == NULL )
        return -ENODEV;

    channel = iminor( inode ) - hdev->chip->baseminor_tx;

    if ( down_interruptible( &hdev->sem_tx ))
        return -ERESTARTSYS;

    for ( wrote = 0; wrote < count; wrote += 1*sizeof(u32)) {
        while ( f_full( &hdev->fifo_tx[channel] )) {
            up( &hdev->sem_tx );
            if ( filp->f_flags & O_NONBLOCK ) {
                return -EAGAIN;
            } else {
                pr_debug("%s waiting on channel %d\n", __func__, channel );
                if ( wait_event_interruptible( hdev->queue_tx, !f_full( &hdev->fifo_tx[channel] ))) {
                    return -ERESTARTSYS;
                }
                pr_debug("%s woke up\n", __func__ );
            }
            if ( down_interruptible( &hdev->sem_tx ))
                return -ERESTARTSYS;
        }
        copy_from_user( &data, from + wrote, 1*sizeof(u32));
        f_in( &hdev->fifo_tx[channel], data );
    }
    up( &hdev->sem_tx );

    // if not tx'ing, start
    schedule_work( &hdev->work_tx );
    return ret ? : wrote;
}

// Function to setup HI-3593 label filter table entries corresponding to requested labels
// Implemented as per HI-3593 data-sheet section: LABEL RECOGNITION
// The 256-bit label look-up table specifies which incoming to label to stored in 
// receive FIFO by setting "1" in the look-up. Bit-1 corresponds to label 0XFF,
// Bit-2 corresponds to label 0XFE...and so on
// Note: HI-3593 can filter upto 256 labels 
static void setup_label_lookup(u8 *label_lookup_buf, unsigned long ioctl_param)
{
    int table_idx = 0, lookup_table_size = 32;
    int label_lookup_idx, label_lookup_bitpos;
    int label_idx = 1, num_labels = 0;
    unsigned char *accepted_labels = (unsigned char *) ioctl_param;

    /* First byte has accepted label count */
    num_labels = accepted_labels[0];

    /* Initialize label filter table map to zeros */
    while (table_idx < lookup_table_size) {
    	label_lookup_buf[table_idx++] = 0;
    }

    /* Enable label filter table look-up with accepted label bits */
    while (label_idx <= num_labels) {
    	label_lookup_idx = 31 - (accepted_labels[label_idx]/8);
    	label_lookup_bitpos = accepted_labels[label_idx]%8;
    	label_lookup_buf[label_lookup_idx] |= (1 << label_lookup_bitpos);
    	label_idx++;
    }
}

// generic ioctl, only commands that are common between the chips are supported here
static int holt_arinc_ioctl(struct inode *inode, struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param)
{
    int ret = 0, channel, idx;
    struct holt_arinc_dev *hdev = (struct holt_arinc_dev *)filp->private_data;
    int cmd_idx, command, field_sz, word_sz;
    u8 buf[16];
    u8 label_lookup_buf[32];

    pr_debug("%s@%d\n", __func__, __LINE__ );

    channel = iminor( inode );

    switch ( ioctl_num ) {
    // reset
    case ARINCIOC_S_MRESET:
        pr_debug("%s@%d: ARINCIOC_S_MRESET\n", __func__, __LINE__ );
        cmd_idx = hdev->chip->ioctl_cmdIdx[ARINC_S_MRESET];
        command = hdev->chip->cmd_list[cmd_idx].cmd_id;

        ret = arinc_bus_write( hdev, command, NULL, 0, 0 );
        if ( hdev->chip->use_fifo_rx ) {
            for ( idx = 0; idx < hdev->chip->num_rxers; idx++ ) {
                f_zero( &hdev->fifo_rx[idx] );
            }
        }
        if ( hdev->chip->use_fifo_tx ) {
            for ( idx = 0; idx < hdev->chip->num_txers; idx++ ) {
                f_zero( &hdev->fifo_tx[idx] );
            }
        }
        break;

    // control register access
    case ARINCIOC_G_CTRL: {
        pr_debug("%s@%d: ARINCIOC_G_CTRL\n", __func__, __LINE__ );
        cmd_idx = hdev->chip->ioctl_cmdIdx[ARINC_G_CTRL];
        command = (channel << 4)|hdev->chip->cmd_list[cmd_idx].cmd_id;
        field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;
        word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;

        ret = arinc_bus_read( hdev, command, buf, word_sz, field_sz );
        switch( word_sz ) {
        case 8:
            pr_debug("\t u8:%02X\n", *(u8 *)buf );
            break;
        default:
        case 16:
            pr_debug("\tu16:%04X\n", *(u16 *)buf );
            break;
        case 32:
            pr_debug("\tu32:%08X\n", *(u32 *)buf );
            break;
        }
        copy_to_user((void *)ioctl_param, buf, (field_sz*word_sz)/8);
        break;
    }
    case ARINCIOC_S_CTRL: {
        pr_debug("%s@%d: ARINCIOC_S_CTRL\n", __func__, __LINE__ );
        cmd_idx = hdev->chip->ioctl_cmdIdx[ARINC_S_CTRL];
        command = (channel << 4)|hdev->chip->cmd_list[cmd_idx].cmd_id;
        field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;
        word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;

        copy_from_user( buf, (void *)ioctl_param, (field_sz*word_sz)/8 );
        switch( word_sz ) {
        case 8:
            pr_debug("\t u8:%02X\n", *(u8 *)buf );
            break;
        default:
        case 16:
            pr_debug("\tu16:%04X\n", *(u16 *)buf );
            break;
        case 32:
            pr_debug("\tu32:%08X\n", *(u32 *)buf );
            break;
        }
        ret = arinc_bus_write( hdev, command, buf, word_sz, field_sz );
        break;
    }
    // label register acceses
    case ARINCIOC_G_LABEL:
        pr_debug("%s@%d: ARINCIOC_G_LABEL\n", __func__, __LINE__ );
        cmd_idx = hdev->chip->ioctl_cmdIdx[ARINC_G_LABEL];
        if ( cmd_idx ) {
            int idx, len = 0;
            char label_str[64];
            switch ( hdev->type ) {
            case hi3597:
                command = (channel << 4)|hdev->chip->cmd_list[cmd_idx].cmd_id;
                field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;
                word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;
                ret = arinc_bus_read( hdev, command, buf, word_sz, field_sz );
                copy_to_user((void *)ioctl_param, buf, (field_sz*word_sz)/8);
                for ( idx = 0; idx < 16; idx++ ) {
                    len += sprintf(label_str+len,"%02X ", buf[idx]);
                }
                pr_debug("\t%s\n", label_str);
                break;
            case hi3593r1:
            case hi3593r2:
            default:
                command = hdev->chip->cmd_list[cmd_idx].cmd_id;
                field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;
                word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;
                ret = arinc_bus_read( hdev, command, label_lookup_buf, word_sz, field_sz );
                copy_to_user((void *)ioctl_param, label_lookup_buf, (field_sz*word_sz)/8);
                for ( idx = 0; idx < 32; idx++ ) {
                    len += sprintf(label_str+len,"%02X ", label_lookup_buf[idx]);
                }
                pr_debug("\t%s\n", label_str);
                break;
            }
        } else {
            ret = -EINVAL;
        }
        break;

    case ARINCIOC_S_LABEL:
        pr_debug("%s@%d: ARINCIOC_S_LABEL\n", __func__, __LINE__ );
        cmd_idx = hdev->chip->ioctl_cmdIdx[ARINC_S_LABEL];
        if ( cmd_idx ) {
            int idx, len = 0;
            char label_str[128];
            switch ( hdev->type ) {
            case hi3597:
                command = (channel << 4)|hdev->chip->cmd_list[cmd_idx].cmd_id;
                field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;
                word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;
                copy_from_user( buf, (void *)ioctl_param, (field_sz*word_sz)/8 );
                for ( idx = 0; idx < 16; idx++ ) {
                    len += sprintf(label_str+len,"%02X ", buf[idx]);
                }
                pr_debug("\t%s\n", label_str);
                ret = arinc_bus_write( hdev, command, buf, word_sz, field_sz );
                break;
            case hi3593r1:
            case hi3593r2:
            default:
                command = hdev->chip->cmd_list[cmd_idx].cmd_id;
                field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;
                word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;
                //Setup label look-up table
                setup_label_lookup(label_lookup_buf, ioctl_param);
                for ( idx = 0; idx < 32; idx++ ) {
                    len += sprintf(label_str+len,"%02X ", label_lookup_buf[idx]);
                }
                pr_debug("\t%s\n", label_str);
                ret = arinc_bus_write( hdev, command, label_lookup_buf, word_sz, field_sz );
                break;
            }
        } else {
            ret = -EINVAL;
        }
        break;

	case ARINCIOC_G_TX_HISPEED: {
		bool high_speed = false;
		pr_debug("%s@%d: ARINCIOC_G_TX_HISPEED:\n", __func__, __LINE__ );
		switch ( hdev->type ) {
		case hi3597:
			high_speed = ( hdev->cmd_tx->cmd_id == hi359x_commands[HI359X_CMD_S_TXFIFO_HS_IDX].cmd_id );
			pr_debug("%s@%d: hdev->cmd_tx->cmd_id %08X(%s/%s)\n", __func__, __LINE__, hdev->cmd_tx->cmd_id,
				( hdev->cmd_tx->cmd_id == hi359x_commands[HI359X_CMD_S_TXFIFO_HS_IDX].cmd_id )?"HS":"LS",
				hdev->high_speed?"HS":"LS");
			copy_to_user((void *)ioctl_param, &high_speed, sizeof( bool ));
			break;
		case hi3593tx:
		default:
			printk("%s@%d: ARINCIOC_G_TX_HISPEED: unsupported device, ret -EINVAL\n", __func__, __LINE__ );
			ret = -EINVAL;
			break;
		}
		break;
	}
	case ARINCIOC_S_TX_HISPEED: {
		bool high_speed = false;
		copy_from_user( &high_speed, (void *)ioctl_param, sizeof( bool ));
		pr_debug("%s@%d: ARINCIOC_S_TX_HISPEED: set %s\n", __func__, __LINE__, high_speed?"HS":"LS" );
		switch ( hdev->type ) {
		case hi3597:
			if ( true == high_speed ) {
				hdev->cmd_tx = &holt_features[hdev->type]->cmd_list[HI359X_CMD_S_TXFIFO_HS_IDX];
			} else {
				hdev->cmd_tx = &holt_features[hdev->type]->cmd_list[HI359X_CMD_S_TXFIFO_LS_IDX];
			}
			hdev->high_speed = high_speed;
			break;
		case hi3593tx:
		default:
			printk("%s@%d: ARINCIOC_S_TX_HISPEED: unsupported device, ret -EINVAL\n", __func__, __LINE__ );
			ret = -EINVAL;
			break;
		}
		break;
	}
	case ARINCIOC_G_TX_LOCKOUT_US: {
		u32 tx_lockout_us = hdev->tx_lockout_ns / 1000;
		printk("%s@%d: ARINCIOC_G_TX_LOCKOUT_US: %d uS\n", __func__, __LINE__, tx_lockout_us );
		switch ( hdev->type ) {
		case hi3597:
			copy_to_user((void *)ioctl_param, &tx_lockout_us, sizeof( u32 ));
			break;
		case hi3593tx:
		default:
			printk("%s@%d: ARINCIOC_G_TX_LOCKOUT_US: unsupported device, ret -EINVAL\n", __func__, __LINE__ );
			ret = -EINVAL;
			break;
		}
		break;
	}
	case ARINCIOC_S_TX_LOCKOUT_US: {
		u32 tx_lockout_us = 0; 
		copy_from_user( &tx_lockout_us, (void *)ioctl_param, sizeof( u32 ));
		pr_debug("%s@%d: ARINCIOC_S_TX_LOCKOUT_US: %d uS\n", __func__, __LINE__, tx_lockout_us );
		switch ( hdev->type ) {
		case hi3597:
			hdev->tx_lockout_ns = tx_lockout_us * 1000;
			break;
		case hi3593tx:
		default:
			printk("%s@%d: ARINCIOC_S_TX_HISPEED: unsupported device, ret -EINVAL\n", __func__, __LINE__ );
			ret = -EINVAL;
			break;
		}
		break;
	}

    default:
	printk("%s@%d: UNKNOWN ioctl val %d(%08X)\n", __func__, __LINE__, ioctl_num, ioctl_num );
        ret = -EINVAL;
        break;
    }
    return ret;
}

static unsigned int holt_arinc_poll(struct file *filp, struct poll_table_struct *wait)
{
    struct holt_arinc_dev *hdev = (struct holt_arinc_dev *)filp->private_data;
    struct inode *inode = filp->f_dentry->d_inode;
    unsigned int mask = 0;
    unsigned int channel;

    p_debug("%s: enter\n", __func__ );
    if ( filp->f_mode & FMODE_READ ) {
        channel = iminor( inode ) - hdev->chip->baseminor_rx;
        p_debug("%s: waiting on RX channel %d\n", __func__, channel+1 );
        poll_wait(filp, &hdev->queue_rx, wait);
        p_debug("%s: woke up on channel %d\n", __func__, channel+1 );
        if ( !f_empty( &hdev->fifo_rx[channel] )) {
            mask |= POLLIN | POLLRDNORM;
            p_debug("%s: exit with mask %08X\n", __func__, mask );
        }
    } else if ( filp->f_mode & FMODE_WRITE ) {
        channel = iminor( inode ) - hdev->chip->baseminor_tx;
        p_debug("->%s: waiting on TX channel %d\n", __func__, channel+1 );
        poll_wait(filp, &hdev->queue_tx, wait);
        p_debug("<-%s: woke up on TX channel %d\n", __func__, channel+1 );
        if ( hdev->chip->use_fifo_tx ) {
            if ( f_depth( &hdev->fifo_tx[channel] ) < ARINC_TX_FIFO_SIZE/2 ) {
                mask |= POLLOUT | POLLWRNORM;
                p_debug("--%s: exit with mask %08X\n", __func__, mask );
            }
        } else {
            // for tx devices without a fifo, always be ready (hi3597)
            mask |= POLLOUT | POLLWRNORM;
        }
    }
    return mask;
}

static int holt_arinc_open(struct inode *inode, struct file *filp)
{
    int ret = 0;
    struct holt_arinc_dev *hdev;
    pr_debug("%s@%d\n", __func__, __LINE__ );

    hdev = container_of(inode->i_cdev, struct holt_arinc_dev, cdev);
    filp->private_data = hdev;

    return ret;
}

static int holt_arinc_close(struct inode *inode, struct file *filp)
{
    int ret = 0, channel;
    struct holt_arinc_dev *hdev = (struct holt_arinc_dev *)filp->private_data;
    u16 data;
	u16 cmd_idx = hdev->chip->ioctl_cmdIdx[CMD_S_CTRL_IDX];
	u16 cmd_id = hdev->chip->cmd_list[cmd_idx].cmd_id;
	u16 word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;
	u16 field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;


    if ( filp->f_mode & FMODE_READ ) {
        channel = iminor( inode ) - hdev->chip->baseminor_rx;

        pr_debug("%s@%d: resetting %s RX channel %d\n", __func__, __LINE__, hdev->chip->name, channel+1 );

        // reinit the port
        data = hdev->chip->cr_init;
        switch ( hdev->type ) {
            case hi3597:
                // channel is 0 referenced for sw fifo index, 1 referenced for 3597 fifo
                cmd_id |= ((channel+1)<<4);
                break;
            case hi3593r1:
            case hi3593r2:
            default:
                break;
        }

        ret = arinc_bus_write( hdev, cmd_id, &data, word_sz, field_sz );
        if ( ret < 0 ) {
            printk("%s@%d: arinc_bus_write returned %d\n", __func__, __LINE__, ret );
            return -ENODEV;
        }
        // zero the fifo
        if ( hdev->chip->num_rxers && hdev->chip->use_fifo_rx ) {
            f_zero( &hdev->fifo_rx[channel] );
        }
    } else if ( filp->f_mode & FMODE_WRITE ) {
    }

    return ret;
}

static const struct file_operations holt_arinc_fops = {
    .ioctl = holt_arinc_ioctl,
    .poll = holt_arinc_poll,
    .read = holt_arinc_read,
    .write = holt_arinc_write,
    .open = holt_arinc_open,
    .release = holt_arinc_close,
};

/** platform interface *******************************************************/
static struct platform_driver holt_arinc_driver = {
        .driver = {
        .name = "holt_arinc",
        .owner = THIS_MODULE,
    },
        .probe = holt_arinc_probe,
        .remove = holt_arinc_remove,
//  .id_table = holt_arinc_driver_ids,
};

static int holt_setup_cdev( struct holt_arinc_dev *hdev, int baseminor, int count )
{
    int err, devno = MKDEV( hdev->major, baseminor );

    cdev_init( &hdev->cdev, &holt_arinc_fops );
    hdev->cdev.owner = THIS_MODULE;
//  kobject_set_name(&cdev->kobj, "%s", name);
    err = cdev_add( &hdev->cdev, devno, count );
    /* Fail gracefully if need be */
    if (err)
        printk(KERN_NOTICE "Error %d adding %d", err, baseminor);
    return err;
}

static int holt_device_init( struct holt_arinc_dev *hdev )
{
    int ret = 0, idx;

//  clk_enable(mxc_spi_dev->clk);
    // check to see if the device is there (initialize it)
    {
        u16 data; // use separate data storage, init everytime, in case there is some endianness changes...
		u16 cmd_idx = hdev->chip->ioctl_cmdIdx[CMD_S_CTRL_IDX];
		u16 cmd_id = hdev->chip->cmd_list[cmd_idx].cmd_id;
		u16 word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;
		u16 field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;

        switch ( hdev->type ) {
        case hi3597:
            for ( idx = 2; idx <= 8; idx++ ) {
                data = hdev->chip->cr_init;
                ret = arinc_bus_write( hdev,
					cmd_id | (idx<<4),
                    &data,
					word_sz,
					field_sz );
                if ( ret < 0 ) {
                    printk("%s@%d: arinc_bus_write returned %d\n", __func__, __LINE__, ret );
                    return -ENODEV;
                }
            }
            break;
        case hi3587:
        case hi3593tx:
        case hi3593r1:
        case hi3593r2:
        default:
            data = hdev->chip->cr_init;
            ret = arinc_bus_write( hdev,
				cmd_id,
                &data,
				word_sz,
				field_sz );
            if ( ret < 0 ) {
                printk("%s@%d: arinc_bus_write returned %d\n", __func__, __LINE__, ret );
                return -ENODEV;
            }
            break;
        }

		cmd_idx = hdev->chip->ioctl_cmdIdx[CMD_G_CTRL_IDX];
		cmd_id = hdev->chip->cmd_list[cmd_idx].cmd_id;
		word_sz = hdev->chip->cmd_list[cmd_idx].word_sz;
		field_sz = hdev->chip->cmd_list[cmd_idx].field_sz;

        switch ( hdev->type ) {
        case hi3597:
			cmd_id |= (8<<4);
            break;
        case hi3587:
        case hi3593tx:
        case hi3593r1:
        case hi3593r2:
        default:
            break;
        }
        ret = arinc_bus_read( hdev,
			cmd_id,
            &data,
			word_sz,
			field_sz );

        if ( ret < 0 ) {
            printk("%s@%d: arinc_bus_read returned %d\n", __func__, __LINE__, ret );
            return -ENODEV;
        }

		pr_debug("%s: wrote init %04x, read %04x\n", __func__, hdev->chip->cr_init, data );
        if ( hdev->chip->cr_init != data ) {
            return -ENODEV;
        }
    }

    // we know the device is there, do a master reset
    ret = arinc_bus_write( hdev,
        hdev->chip->cmd_list[CMD_S_MRESET_IDX].cmd_id,
        NULL, 0, 0 );

    return ret;
}

static int holt_setup_device_structure( struct holt_arinc_dev *hdev )
{
    int ret = 0, idx;

    // init rest of device struct
    init_MUTEX( &hdev->sem_rx );
    init_MUTEX( &hdev->sem_tx );
    init_waitqueue_head( &hdev->queue_rx );
    init_waitqueue_head( &hdev->queue_tx );

    // set tx and rx cmds
    switch ( hdev->type ) {
    case hi3597:
        hdev->cmd_rx = &holt_features[hdev->type]->cmd_list[HI359X_CMD_G_RXFIFO_IDX];
        hdev->cmd_tx = &holt_features[hdev->type]->cmd_list[HI359X_CMD_S_TXFIFO_HS_IDX];
        hdev->high_speed = true;
    	hdev->ts_txlast = current_kernel_time();
        hdev->tx_lockout_ns = TX_LOCKOUT_NS_DEFAULT; // default tx lockout for ideal clock
        break;
    case hi3587:
        hdev->cmd_tx = &holt_features[hdev->type]->cmd_list[HI3587_CMD_S_TXFIFO_IDX];
        break;
    case hi3593tx:
        hdev->cmd_tx = &holt_features[hi3593tx]->cmd_list[HI3593_CMD_S_TXFIFO_IDX];
        break;
    case hi3593r1:
        hdev->cmd_rx = &holt_features[hi3593r1]->cmd_list[HI3593_CMD_G_RX1FIFO_IDX];
        break;
    case hi3593r2:
        hdev->cmd_rx = &holt_features[hi3593r2]->cmd_list[HI3593_CMD_G_RX2FIFO_IDX];
        break;
    default:
        break;
    }

    // setup fifos
    if ( hdev->chip->num_rxers && hdev->chip->use_fifo_rx ) {
        hdev->fifo_rx = (fifo_t *)mems_alloc( hdev->chip->num_rxers * sizeof(fifo_t));
        if ( !hdev->fifo_rx ) {
            return -ENOMEM;
        }
        hdev->fifo_rx[0].buffer = (u32 *)mems_alloc( hdev->chip->num_rxers * sizeof(u32) * ARINC_RX_FIFO_SIZE );
        if ( !hdev->fifo_rx[0].buffer ) {
            return -ENOMEM;
        }
        for ( idx = 0; idx < hdev->chip->num_rxers; idx++ ) {
            hdev->fifo_rx[idx].order = ARINC_RX_FIFO_ORDER;
            hdev->fifo_rx[idx].buffer = hdev->fifo_rx[0].buffer + ( idx * ARINC_RX_FIFO_SIZE);
        }
    }
    if ( hdev->chip->num_txers && hdev->chip->use_fifo_tx ) {
        hdev->fifo_tx = (fifo_t *)mems_alloc( hdev->chip->num_txers * sizeof(fifo_t));
        if ( !hdev->fifo_tx ) {
            return -ENOMEM;
        }
        hdev->fifo_tx[0].buffer = (u32 *)mems_alloc( hdev->chip->num_txers * sizeof(u32) * ARINC_TX_FIFO_SIZE );
        if ( !hdev->fifo_tx[0].buffer ) {
            return -ENOMEM;
        }
        for ( idx = 0; idx < hdev->chip->num_txers; idx++ ) {
            hdev->fifo_tx[idx].order = ARINC_TX_FIFO_ORDER;
            hdev->fifo_tx[idx].buffer = hdev->fifo_tx[0].buffer + ( idx * ARINC_TX_FIFO_SIZE);
        }
    }

    if ( hdev->chip->num_txers ) {
        // setup timer struct here, so that we can just use mod_timer to start/update
        init_timer( &hdev->timeout_tx );
        hdev->timeout_tx.data = (unsigned int)hdev;
        hdev->timeout_tx.function = timeout_tx;
    }
    return ret;
}

static int holt_register_interrupt( struct holt_arinc_dev *hdev, int irq )
{
//  ret = request_irq( irq, holt_irq,
    int ret = request_threaded_irq( irq, NULL, holt_irq,
            IRQF_TRIGGER_RISING, hdev->chip->name, hdev );
    if (ret) {
        printk( KERN_ERR "%s: Failed to register interrupt %d\n",
            hdev->chip->name, irq );
    }
    return ret;
}

static int holt_setup_interrupts( struct holt_arinc_dev *hdev, struct spi_board_info *chip_info )
{
    int *intrIO;
    int irq, idx;
    switch ( hdev->type ) {
    case hi3597:
        if (( hdev->tx_irq = gpio_to_irq((int)chip_info->platform_data))) {
            return holt_register_interrupt( hdev, hdev->tx_irq );
        } else {
            dev_err( hdev->spi.dev.parent, "Interrupt not defined!\n");
            return -1;
        }
        break;
    case hi3587:
        if (( hdev->rx_irq = gpio_to_irq((int)chip_info->platform_data))) {
            return holt_register_interrupt( hdev, hdev->rx_irq );
        } else {
            dev_err( hdev->spi.dev.parent, "Interrupt not defined!\n");
            return -1;
        }
        break;
    // FIXME: need to fix this for separate function handling in 3593
    case hi3593tx:
        intrIO = (int *)chip_info->platform_data;
        if(intrIO[HI_INTRIO_TXEMPTY] != 0) {
            if ( ( hdev->tx_irq = gpio_to_irq(intrIO[HI_INTRIO_TXEMPTY])) ) {
                return holt_register_interrupt( hdev, hdev->tx_irq );
            } else {
                dev_err( hdev->spi.dev.parent, "Interrupt not defined!\n");
                return -1;
            }
        }
        break;
    case hi3593r1:
    case hi3593r2:
        intrIO = (int *)chip_info->platform_data;
        for ( idx = HI_INTRIO_RxINT; idx < HI_INTRIO_MAX; idx++ ) {
            if ( intrIO[idx] ) { // interrupt is assigned
                if (( irq = gpio_to_irq(intrIO[idx]))) { // good interrupt
                    if ( holt_register_interrupt( hdev, irq ) == 0 ) {
                        switch ( idx ) {
                        // FIXME: need to define more interrupt assignment holders...
                        case HI_INTRIO_RxINT:
                            hdev->rx_irq = irq;
                            break;
                        case HI_INTRIO_RxFLAG:  // R1FLAG pin
                        case HI_INTRIO_MBx_ALL: // combined mailbox 1 interrupts
                        case HI_INTRIO_MBx_1:
                        case HI_INTRIO_MBx_2:
                        case HI_INTRIO_MBx_3:
                        default:
                            break;
                        }
                    }
                }
            }
        }
        break;
    default:
        break;
    }
    return 0;
}


static int holt_arinc_probe(struct platform_device *pdev)
{
    int ret = 0, chip_idx;
    struct holt_arinc_dev *hdev;
    const struct platform_device_id *pdid;
    struct arinc_spi_platform_info *platform = (struct arinc_spi_platform_info *)pdev->dev.platform_data;
    struct spi_board_info *chip_info = (struct spi_board_info *)platform->chip_info;

    pr_debug("%s@%d\n", __func__, __LINE__ );

    ret = mxc_spi_init( pdev );
    if ( ret < 0 )
        return ret;

    for ( chip_idx = 0; strlen( chip_info[chip_idx].modalias ); chip_idx++ ) {
        struct spi_device *spi;
        // init minimum device struct
        hdev = mems_alloc(sizeof(*hdev));
        if (!hdev) {
            return -ENOMEM;
        }
        INIT_LIST_HEAD( &hdev->device_list);
        list_add( &hdev->device_list, &mxc_spi_dev->slave_head );

        spi = &hdev->spi;
        // TODO: init spi_device struct
        spi->dev.parent = &pdev->dev;
        if ( chip_info[chip_idx].platform_data == NULL ) {
            printk(KERN_ERR"%s: platform data not defined\n!", __func__ ); // kaboom!
        }
        spi->dev.platform_data = (void *) chip_info[chip_idx].platform_data;

        spi->max_speed_hz = chip_info[chip_idx].max_speed_hz;
        spi->chip_select = chip_info[chip_idx].chip_select;
        spi->mode = chip_info[chip_idx].mode;
        spi->bits_per_word = 8;
        spi->irq = chip_info[chip_idx].irq;
        spi->controller_state = NULL;
        spi->controller_data = chip_info[chip_idx].controller_data;
        strlcpy( spi->modalias, chip_info[chip_idx].modalias, sizeof(spi->modalias));


        for ( pdid = &holt_arinc_driver_ids[0] ; pdid->name != NULL; pdid++ ) {
            if ( strcmp( pdid->name, chip_info[chip_idx].modalias ) == 0 ) {
                hdev->type = (enum arinc_type)pdid->driver_data;
                break;
            }
        }
        if ( pdid->name == NULL ) {
            printk(KERN_ERR"Unsupported chip %s\n", chip_info[chip_idx].modalias );
            continue;
        }

        hdev->chip = holt_features[hdev->type];

        ret = holt_device_init( hdev );
        if ( ret < 0 ) {
            printk(KERN_ERR"Failed to initialize device: %s\n", chip_info[chip_idx].modalias );
            continue;
        }
        ret = holt_setup_device_structure( hdev );
        if ( ret < 0 ) {
            printk(KERN_ERR"Failed to initialize structures: %s\n", chip_info[chip_idx].modalias );
            continue;
        }

        // setup inodes
        // allocate major number and reserve minor numbers
        hdev->devt = MKDEV( hdev->major, 0 );
        ret = alloc_chrdev_region( &hdev->devt, MIN( hdev->chip->baseminor_tx, hdev->chip->baseminor_rx ),
            hdev->chip->num_rxers + hdev->chip->num_txers,
            hdev->chip->name );
        hdev->major = MAJOR( hdev->devt );

        // setup sysfs/dev node entries and transmit timers
        if ( hdev->chip->num_txers ) {
            int dev_idx;
            if ( holt_setup_cdev( hdev, hdev->chip->baseminor_tx, hdev->chip->num_txers )) {
                ret = -EBUSY;
                continue;
            }
            for ( dev_idx = hdev->chip->baseminor_tx; dev_idx < hdev->chip->baseminor_tx + hdev->chip->num_txers; dev_idx++ ) {
                device_create( arinc_class, &pdev->dev, MKDEV( hdev->major, dev_idx ), hdev,
                    "%s_%d.%d", hdev->chip->name, chip_info[chip_idx].chip_select, dev_idx );
            }
            INIT_WORK( &hdev->work_tx, holt_tx_worker );
        }
        if ( hdev->chip->num_rxers ) {
            int dev_idx;
            if ( holt_setup_cdev( hdev, hdev->chip->baseminor_rx, hdev->chip->num_rxers )) {
                ret = -EBUSY;
                continue;
            }
            for ( dev_idx = hdev->chip->baseminor_rx; dev_idx < hdev->chip->baseminor_rx + hdev->chip->num_rxers; dev_idx++ ) {
                device_create( arinc_class, &pdev->dev, MKDEV( hdev->major, dev_idx ), hdev,
                    "%s_%d.%d", hdev->chip->name, chip_info[chip_idx].chip_select, dev_idx );
            }
            INIT_WORK( &hdev->work_rx, holt_rx_worker );
        }

        // setup interrupt handling
        ret = holt_setup_interrupts( hdev, &chip_info[chip_idx] );
        if ( ret < 0 ) {
            printk(KERN_ERR"Failed to setup interrupts: %s\n", chip_info[chip_idx].modalias );
            continue;
        }


// TODO:
//      find a way to track hdev's
// FIXME
//      dev_set_drvdata( &spi->dev, hdev );
        printk("%s_%d registered on major %d\n", hdev->chip->name, chip_info[chip_idx].chip_select,
            hdev->major );
    } // for each device
    return 0;

//fail_dev:
    unregister_chrdev_region( hdev->devt, hdev->chip->num_txers + hdev->chip->num_rxers );
    cdev_del( &hdev->cdev );
//fail_mem:
    if ( hdev->fifo_rx ) {
        if ( hdev->fifo_rx[0].buffer )
            kfree( hdev->fifo_rx[0].buffer );
        kfree( hdev->fifo_rx );
    }
    if ( hdev->fifo_tx ) {
        if ( hdev->fifo_tx[0].buffer )
            kfree( hdev->fifo_tx[0].buffer );
        kfree( hdev->fifo_tx );
    }
    kfree(hdev);
    return ret;
}

static int __devexit holt_arinc_remove(struct platform_device *pdev)
{
    int ret = 0, idx;
    struct holt_arinc_dev *hdev;
    pr_debug("%s@%d\n", __func__, __LINE__ );

// FIXME
//  hdev = dev_get_drvdata( &spi->dev );

    list_for_each_entry( hdev, &mxc_spi_dev->slave_head, device_list ) {
        free_irq( hdev->rx_irq, hdev );
        if ( hdev->chip->num_rxers ) {
            cancel_work_sync( &hdev->work_rx );
        }
        free_irq( hdev->tx_irq, hdev );
        if ( hdev->chip->num_txers ) {
            del_timer( &hdev->timeout_tx );
            cancel_work_sync( &hdev->work_tx );
        }
        for ( idx = hdev->chip->baseminor_tx; idx < hdev->chip->baseminor_tx + hdev->chip->num_txers; idx++ ) {
            device_destroy( arinc_class, MKDEV( hdev->major, idx ));
        }
        for ( idx = hdev->chip->baseminor_rx; idx < hdev->chip->baseminor_rx + hdev->chip->num_rxers; idx++ ) {
            device_destroy( arinc_class, MKDEV( hdev->major, idx ));
        }

        unregister_chrdev_region( hdev->devt, hdev->chip->num_txers + hdev->chip->num_rxers );
        cdev_del( &hdev->cdev );

        if ( hdev->fifo_rx ) {
            if ( hdev->fifo_rx[0].buffer )
                kfree( hdev->fifo_rx[0].buffer );
            kfree( hdev->fifo_rx );
        }
        if ( hdev->fifo_tx ) {
            if ( hdev->fifo_tx[0].buffer )
                kfree( hdev->fifo_tx[0].buffer );
            kfree( hdev->fifo_tx );
        }

// FIXME
//      dev_set_drvdata( &spi->dev, NULL );
        kfree( hdev );
    }

#ifdef USE_BUILTIN_SPI
    {
        mxc_spi_remove( pdev );
    }
#endif
    return ret;
}

/** module interface *********************************************************/
static int __init holt_arinc_register(void)
{
    pr_debug( "%s@%d\n", __func__, __LINE__ );
    arinc_class = class_create(THIS_MODULE, "arinc");
    return platform_driver_register( &holt_arinc_driver );
}
module_init(holt_arinc_register);

static void __exit holt_arinc_unregister(void)
{
    pr_debug( "%s@%d\n", __func__, __LINE__ );
    platform_driver_unregister( &holt_arinc_driver );
}
module_exit(holt_arinc_unregister);
