/*---------------------------------------------------------------------------
 *
 * niufpga.c
 *     Copyright (c) 2012 Guenter Roeck <linux@roeck-us.net>
 *
 *---------------------------------------------------------------------------
 */
 
#include <linux/module.h>
#include <linux/init.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/mutex.h>
#include <linux/ioport.h>
#include <linux/platform_device.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <asm/atomic.h>
#include <linux/interrupt.h>
#include <linux/irqreturn.h>
#include <linux/io.h>
#include <asm/gpio.h>

#define DRVNAME "niufpga"

#define NIUFPGA_MEM_START	0xb0000000	/* Should be something real	*/
#define NIUFPGA_MEM_SIZE	0x300
#define NIUFPGA_INTR		106		/* GPIO4_10 [3*32 + 10] (Pin CSI2_D13)	*/

#define NIUFPGA_INT_STATUS_REG		0x002A // Address offset from NIUFPGA_MEM_START for Interrupt Status Register
#define NIUFPGA_INT_STATUS_RTC_REG	0x002C // Address offset from NIUFPGA_MEM_START for Interrupt Status RTC Register

//#define NIUFPGA_INT_STS_PT	  0x0001
//#define NIUFPGA_INT_STS_LAC	  0x007e
//#define NIUFPGA_INT_STS_CTD	  0x0f00
#define NIUFPGA_INT_REPORT        0x1f01  // INT_STATUS register interrupt bit mask

struct niufpga_device_attribute {
	struct device_attribute dev_attr;
	u32 index;
};

#define to_niufpga_dev_attr(_dev_attr) \
	container_of(_dev_attr, struct niufpga_device_attribute, dev_attr)

#define NIUFPGA_ATTR(_name, _mode, _show, _store, _index)	\
	{ .dev_attr = __ATTR(_name, _mode, _show, _store),	\
	  .index = _index }

#define NIUFPGA_DEVICE_ATTR(_name, _mode, _show, _store, _index)\
struct niufpga_device_attribute niufpga_dev_attr_##_name	\
	= NIUFPGA_ATTR(_name, _mode, _show, _store, _index)

struct niufpga_bin_attribute {
	struct bin_attribute bin_attr;
	u32 start;
};

#define to_niufpga_bin_attr(_bin_attr) \
	container_of(_bin_attr, struct niufpga_bin_attribute, bin_attr)

#define NIUFPGA_B_ATTR(_name, _mode, _read, _write, _start, _size)	\
	{ .bin_attr = {							\
		.attr = {						\
			.name = __stringify(_name),			\
			.mode = _mode,					\
		},							\
		.read = _read,						\
		.write = _write,					\
		.size = _size,						\
	  },								\
	  .start = _start }

#define NIUFPGA_BIN_ATTR(_name, _mode, _show, _store, _start, _size)	\
struct niufpga_bin_attribute niufpga_bin_attr_##_name			\
	= NIUFPGA_B_ATTR(_name, _mode, _show, _store, _start, _size)

struct niufpga_data {
	struct device *dev;
	void __iomem *mem;
	struct mutex lock;
	u16 int_sts;
};

static ssize_t show_reg(struct device *dev,
			struct device_attribute *da, char *buf)
{
	struct niufpga_device_attribute *attr = to_niufpga_dev_attr(da);
	struct niufpga_data *data = dev_get_drvdata(dev);
	int reg = attr->index;

	return snprintf(buf, PAGE_SIZE, "0x%x\n", *((u16 *)(data->mem + reg)));
}

static ssize_t store_reg(struct device *dev,
			 struct device_attribute *da,
			 const char *buf, size_t count)
{
	struct niufpga_device_attribute *attr = to_niufpga_dev_attr(da);
	struct niufpga_data *data = dev_get_drvdata(dev);
	int reg = attr->index;
	unsigned long val;
	int err;

	err = strict_strtoul(buf, 0, &val);
	if (unlikely(err < 0))
		return err;
	if (val > 0xffff)
		return -EINVAL;

	*((u16 *)(data->mem + reg)) = val;

	return count;
}

static ssize_t show_int(struct device *dev,
			struct device_attribute *da, char *buf)
{
	struct niufpga_device_attribute *attr = to_niufpga_dev_attr(da);
	struct niufpga_data *data = dev_get_drvdata(dev);
	u16 mask = attr->index;
	u16 intr;
	u8  int_report;

	mutex_lock(&data->lock );

	// The int register does not match the output STATUS_REGISTER
	// Need to remap the register
	int_report = (data->int_sts >> 8);
	int_report |= ((data->int_sts & 0x01) << 7);

	intr = data->int_sts;
	data->int_sts &= ~intr;

	mutex_unlock(&data->lock);

	return snprintf(buf, PAGE_SIZE, "0x%x\n", int_report);
}


// FPGA Registers
static NIUFPGA_DEVICE_ATTR(reg_pinmux1,         S_IRUGO | S_IWUSR, show_reg, store_reg, 0x00);
static NIUFPGA_DEVICE_ATTR(reg_pinmux2,         S_IRUGO | S_IWUSR, show_reg, store_reg, 0x02);
static NIUFPGA_DEVICE_ATTR(reg_latch2,          S_IRUGO | S_IWUSR, show_reg, store_reg, 0x04);
static NIUFPGA_DEVICE_ATTR(reg_pinmux4,         S_IRUGO | S_IWUSR, show_reg, store_reg, 0x06);
static NIUFPGA_DEVICE_ATTR(reg_latch1,          S_IRUGO | S_IWUSR, show_reg, store_reg, 0x10);
static NIUFPGA_DEVICE_ATTR(reg_out1,            S_IRUGO | S_IWUSR, show_reg, store_reg, 0x12);
static NIUFPGA_DEVICE_ATTR(reg_in1,             S_IRUGO | S_IWUSR, show_reg, store_reg, 0x14);
static NIUFPGA_DEVICE_ATTR(reg_in2,             S_IRUGO | S_IWUSR, show_reg, store_reg, 0x16);
static NIUFPGA_DEVICE_ATTR(reg_perr_reset1,     S_IRUGO | S_IWUSR, show_reg, store_reg, 0x18);
static NIUFPGA_DEVICE_ATTR(reg_perr_reset2,     S_IRUGO | S_IWUSR, show_reg, store_reg, 0x1a);
static NIUFPGA_DEVICE_ATTR(reg_perr_ovfl1,      S_IRUGO | S_IWUSR, show_reg, store_reg, 0x1c);
static NIUFPGA_DEVICE_ATTR(reg_perr_ovfl2,      S_IRUGO | S_IWUSR, show_reg, store_reg, 0x1e);
static NIUFPGA_DEVICE_ATTR(reg_config1,         S_IRUGO | S_IWUSR, show_reg, store_reg, 0x20);
static NIUFPGA_DEVICE_ATTR(reg_config2,         S_IRUGO | S_IWUSR, show_reg, store_reg, 0x22);
static NIUFPGA_DEVICE_ATTR(reg_int_enable,      S_IRUGO | S_IWUSR, show_reg, store_reg, 0x28);
static NIUFPGA_DEVICE_ATTR(reg_int_status,      S_IRUGO | S_IWUSR, show_reg, store_reg, NIUFPGA_INT_STATUS_REG);
static NIUFPGA_DEVICE_ATTR(reg_scratchpad,      S_IRUGO | S_IWUSR, show_reg, store_reg, 0x30);
static NIUFPGA_DEVICE_ATTR(reg_image_id,        S_IRUGO | S_IWUSR, show_reg, store_reg, 0x32);
static NIUFPGA_DEVICE_ATTR(reg_image_revision,  S_IRUGO | S_IWUSR, show_reg, store_reg, 0x34);
static NIUFPGA_DEVICE_ATTR(reg_llock_reset1,    S_IRUGO | S_IWUSR, show_reg, store_reg, 0x36);
static NIUFPGA_DEVICE_ATTR(reg_llock_ovfl1,     S_IRUGO | S_IWUSR, show_reg, store_reg, 0x38);
static NIUFPGA_DEVICE_ATTR(reg_max_psud_r,      S_IRUGO | S_IWUSR, show_reg, store_reg, 0x8e);
static NIUFPGA_DEVICE_ATTR(reg_max_psud_c,      S_IRUGO | S_IWUSR, show_reg, store_reg, 0x90);
static NIUFPGA_DEVICE_ATTR(reg_max_psud_l,      S_IRUGO | S_IWUSR, show_reg, store_reg, 0x92);
static NIUFPGA_DEVICE_ATTR(reg_psud_icount_r,   S_IRUGO | S_IWUSR, show_reg, store_reg, 0x94);
static NIUFPGA_DEVICE_ATTR(reg_psud_icount_c,   S_IRUGO | S_IWUSR, show_reg, store_reg, 0x96);
static NIUFPGA_DEVICE_ATTR(reg_psud_icount_l,   S_IRUGO | S_IWUSR, show_reg, store_reg, 0x98);
static NIUFPGA_DEVICE_ATTR(reg_psud_isymbol,    S_IRUGO | S_IWUSR, show_reg, store_reg, 0x9A);

// Interrupt Sysfs Dev node
static NIUFPGA_DEVICE_ATTR(int_report,       S_IRUGO,           show_int, NULL,      NIUFPGA_INT_REPORT);

static struct attribute *niufpga_attrs[] = {
    // FPGA Registers
	&niufpga_dev_attr_reg_pinmux1.dev_attr.attr,
	&niufpga_dev_attr_reg_pinmux2.dev_attr.attr,
    &niufpga_dev_attr_reg_latch2.dev_attr.attr,
	&niufpga_dev_attr_reg_pinmux4.dev_attr.attr,
	&niufpga_dev_attr_reg_latch1.dev_attr.attr,
	&niufpga_dev_attr_reg_out1.dev_attr.attr,
	&niufpga_dev_attr_reg_in1.dev_attr.attr,
	&niufpga_dev_attr_reg_in2.dev_attr.attr,
	&niufpga_dev_attr_reg_perr_reset1.dev_attr.attr,
	&niufpga_dev_attr_reg_perr_reset2.dev_attr.attr,
	&niufpga_dev_attr_reg_perr_ovfl1.dev_attr.attr,
	&niufpga_dev_attr_reg_perr_ovfl2.dev_attr.attr,
	&niufpga_dev_attr_reg_config1.dev_attr.attr,
	&niufpga_dev_attr_reg_config2.dev_attr.attr,
	&niufpga_dev_attr_reg_int_enable.dev_attr.attr,
	&niufpga_dev_attr_reg_int_status.dev_attr.attr,
	&niufpga_dev_attr_reg_scratchpad.dev_attr.attr,
	&niufpga_dev_attr_reg_image_id.dev_attr.attr,
	&niufpga_dev_attr_reg_image_revision.dev_attr.attr,
	&niufpga_dev_attr_reg_llock_reset1.dev_attr.attr,
	&niufpga_dev_attr_reg_llock_ovfl1.dev_attr.attr,
    &niufpga_dev_attr_reg_max_psud_r.dev_attr.attr,
    &niufpga_dev_attr_reg_max_psud_c.dev_attr.attr,
    &niufpga_dev_attr_reg_max_psud_l.dev_attr.attr,
    &niufpga_dev_attr_reg_psud_icount_r.dev_attr.attr,
    &niufpga_dev_attr_reg_psud_icount_c.dev_attr.attr,
    &niufpga_dev_attr_reg_psud_icount_l.dev_attr.attr,
    &niufpga_dev_attr_reg_psud_isymbol.dev_attr.attr,

    // Interrupt
    &niufpga_dev_attr_int_report.dev_attr.attr,
        
	NULL,
};

static const struct attribute_group niufpga_attr_group = {
	.attrs = niufpga_attrs,
};

static ssize_t niufpga_xfer(struct niufpga_data *data,
			    u16 *to, u16 *from, size_t count)
{
	ssize_t retval = 0;

	if (unlikely(!count))
		return count;

	/* Transfer data to/from fpga, protecting against concurrent updates */
	mutex_lock(&data->lock);
	while (count) {
		*(to++) = *(from++);
		count -= sizeof(u16);
		retval += sizeof(u16);
	}
	mutex_unlock(&data->lock);

	return retval;
}

static ssize_t bin_read(struct file *filp, struct kobject *kobj,
			struct bin_attribute *ba, char *buf,
			loff_t off, size_t count)
{
	struct niufpga_data *data
	  = dev_get_drvdata(container_of(kobj, struct device, kobj));
	struct niufpga_bin_attribute *attr = to_niufpga_bin_attr(ba);

	if ((off & 1) || (count & 1))
		return -EINVAL;

	return niufpga_xfer(data,
			    (u16 *)buf, (u16 *)(data->mem + attr->start + off),
			    count);
}

static ssize_t bin_write(struct file *filp, struct kobject *kobj,
			 struct bin_attribute *ba, char *buf,
			 loff_t off, size_t count)
{
	struct niufpga_data *data
	  = dev_get_drvdata(container_of(kobj, struct device, kobj));
	struct niufpga_bin_attribute *attr = to_niufpga_bin_attr(ba);

	if ((off & 1) || (count & 1))
		return -EINVAL;

	return niufpga_xfer(data,
			    (u16 *)(data->mem + attr->start + off), (u16 *)buf,
			    count);
}

static NIUFPGA_BIN_ATTR(block_perr_cnt,     S_IRUGO | S_IWUSR, bin_read, bin_write, 0x0080,    17 * 2 );
static NIUFPGA_BIN_ATTR(block_llock_cnt,    S_IRUGO | S_IWUSR, bin_read, bin_write, 0x00b0,     6 * 2 );
static NIUFPGA_BIN_ATTR(block_psud_state,   S_IRUGO | S_IWUSR, bin_read, bin_write, 0x0100,    96 * 2 );
static NIUFPGA_BIN_ATTR(raw,                S_IRUGO | S_IWUSR, bin_read, bin_write, 0x0000,  0x300    );

static struct bin_attribute *niufpga_bin_attrs[] = {
	&niufpga_bin_attr_block_perr_cnt.bin_attr,
	&niufpga_bin_attr_block_llock_cnt.bin_attr,
        &niufpga_bin_attr_block_psud_state.bin_attr,
	&niufpga_bin_attr_raw.bin_attr,
};

static irqreturn_t niufpga_irq_handler(int irq, void *priv)
{
	struct niufpga_data *data = priv;
	u16 int_sts;
	u16 *irq_reg = data->mem + NIUFPGA_INT_STATUS_REG;

	// Read the interrupt register
	int_sts = *irq_reg;

	// Write back the same data to clear the interrupt register    
	*irq_reg = int_sts;

    if (int_sts & NIUFPGA_INT_REPORT) {
		data->int_sts |= int_sts;
		sysfs_notify(&data->dev->kobj, NULL, "int_report"); 
    }

	return IRQ_HANDLED;
}

static int __devinit niufpga_probe(struct platform_device *pdev)
{
	int i, ret;
    int irq_gpio;
    int result;
	struct niufpga_data *data;
	struct resource *res, *irq;

	data = devm_kzalloc(&pdev->dev, sizeof(*data), GFP_KERNEL);
	if (unlikely(!data))
		return -ENOMEM;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (unlikely(!res))
		return -ENODEV;

	irq = platform_get_resource(pdev, IORESOURCE_IRQ, 0);
	if (unlikely(!irq))
		return -ENODEV;

	if (res->start) {
		if (!devm_request_mem_region(&pdev->dev, res->start,
					     resource_size(res), DRVNAME))
			return -EBUSY;
		data->mem = devm_ioremap(&pdev->dev,
					 res->start, resource_size(res));
		if (unlikely(!data->mem))
			return -ENOMEM;
	} else {
		/* This is for testing only */
		data->mem = devm_kzalloc(&pdev->dev, NIUFPGA_MEM_SIZE, GFP_KERNEL);
		if (unlikely(!data->mem))
			return -ENOMEM;
	}

	if (irq->start) {
		if (devm_request_irq(&pdev->dev, irq->start,
				     niufpga_irq_handler, 0, DRVNAME, data)) {
			dev_err(&pdev->dev, "Failed to request irq %d\n",
				(int)irq->start);
			return -EBUSY;
		}
	}

	platform_set_drvdata(pdev, data);
	mutex_init(&data->lock);
	data->dev = &pdev->dev;

	ret = sysfs_create_group(&pdev->dev.kobj, &niufpga_attr_group);
	if (unlikely(ret)) {
		dev_err(&pdev->dev, "sysfs create failed\n");
		return ret;
	}

	for (i = 0; i < ARRAY_SIZE(niufpga_bin_attrs); i++) {
		ret = sysfs_create_bin_file(&pdev->dev.kobj,
					    niufpga_bin_attrs[i]);
		if (unlikely(ret))
			goto err;
	}

    irq_gpio = gpio_to_irq(NIUFPGA_INTR);
	if(irq_gpio < 0){
		printk("failed to map GPIO %d to interrupt: %d\n", NIUFPGA_INTR, irq_gpio);
		gpio_free(NIUFPGA_INTR);
		return irq_gpio;
	}
	printk("GPIO %d mapped to IRQ %d\n", NIUFPGA_INTR, irq_gpio);

	result = request_threaded_irq(irq_gpio, NULL, niufpga_irq_handler, IRQF_DISABLED | IRQF_TRIGGER_RISING, "niufpga_int", data);

    switch(result) {
	case -EBUSY:
		printk("irq %d busy\n", irq_gpio);
		gpio_free(NIUFPGA_INTR);
		return -EBUSY;
	case -EINVAL:
		printk("bad irq number or handler\n");
		gpio_free(NIUFPGA_INTR);
		return -EINVAL;
	default:
		printk("irq %d, gpio %d obtained\n", irq_gpio, NIUFPGA_INTR);
		break;
	}




	return 0;

err:
	for (i = 0; i < ARRAY_SIZE(niufpga_bin_attrs); i++)
		sysfs_remove_bin_file(&pdev->dev.kobj, niufpga_bin_attrs[i]);
	sysfs_remove_group(&pdev->dev.kobj, &niufpga_attr_group);
	return ret;
}

static int __devexit niufpga_remove(struct platform_device *pdev)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(niufpga_bin_attrs); i++)
		sysfs_remove_bin_file(&pdev->dev.kobj, niufpga_bin_attrs[i]);

	sysfs_remove_group(&pdev->dev.kobj, &niufpga_attr_group);

	platform_set_drvdata(pdev, NULL);

	return 0;
}

struct resource niufpga_resources[] = {
	[0] = {
		.start  = NIUFPGA_MEM_START,
		.end    = NIUFPGA_MEM_START + NIUFPGA_MEM_SIZE - 1,
		.flags  = IORESOURCE_MEM,
	},
	[1] = {
		.start  = NIUFPGA_INTR,
		.end    = NIUFPGA_INTR,
		.flags  = IORESOURCE_IRQ,
	},

};

static void dummy_release(struct device *dev)
{
}

static struct platform_device niufpga_device = {
	.name = DRVNAME,
	.id = 0,
	.resource = niufpga_resources,
	.dev = {
		.release = dummy_release,
	},
	.num_resources  = ARRAY_SIZE(niufpga_resources),
};

static struct platform_driver __refdata niufpga_driver = {
	.driver = {
		.name = DRVNAME,
		.owner = THIS_MODULE,
	},
	.probe = niufpga_probe,
	.remove = __devexit_p(niufpga_remove),
};

static int __init niufpga_init(void)
{
	int ret;

	ret = platform_device_register(&niufpga_device);
	if (ret)
		return ret;

	ret = platform_driver_register(&niufpga_driver);
	if (ret)
		platform_device_unregister(&niufpga_device);

	return ret;
}
module_init(niufpga_init);

static void __exit niufpga_exit(void)
{
	platform_driver_unregister(&niufpga_driver);
	platform_device_unregister(&niufpga_device);
}
module_exit(niufpga_exit);

MODULE_AUTHOR("Guenter Roeck <linux@roeck-us.net>");
MODULE_LICENSE("GPL");
