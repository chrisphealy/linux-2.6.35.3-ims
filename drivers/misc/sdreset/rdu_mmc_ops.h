/*
 * rdu_mmc_ops.h - MMC device interface definition
 */

#ifndef RDU_MMC_OPS_H
#define RDU_MMC_OPS_H

#include <linux/device.h>

int rdu_mmc_ops_init(struct class *cl);
void rdu_mmc_ops_exit(void);


#endif
