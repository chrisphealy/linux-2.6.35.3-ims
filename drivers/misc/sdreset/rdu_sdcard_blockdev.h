/*
 * rdu_sdcard_blockdev.h - Block device interface definition
 */

#ifndef RDU_SDCARD_BLOCKDEV_H
#define RDU_SDCARD_BLOCKDEV_H

#include <linux/device.h>

int rdu_sdcard_blockdev_init(struct class *cl);
void rdu_sdcard_blockdev_exit(void);

/* Allow access to the passthrough blockdev's pre-powerdown logic */
int sdcard_passthrough_blockdev_pre_powerdown(struct device *ldm_cdev);


#endif
