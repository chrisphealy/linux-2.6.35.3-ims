/*
 * rdu_mmc_ops.c - MMC device interface implementation
 */

#include <linux/err.h>
#include <linux/mmc/card.h>
#include <linux/mmc/host.h>
#include <linux/slab.h>

#include "spc_class_mgr.h"
#include "sdreset_driver.h"
#include "spc_class_pm.h"
#include "rdu_mmc_ops.h"


struct sp_dev_type_mmc_priv {
	int ready_for_poweroff;
};

static struct class_interface class_intf;

static const char *const mmc_bus_name = "mmc";

/* Safe the device in anticipation of a power loss */
static int mmc_pre_poweroff(struct device *ldm_cdev)
{
	struct device *realdev = ldm_cdev->parent;
	struct sp_dev_type_mmc_priv *priv = dev_get_drvdata(ldm_cdev);
	struct mmc_card *card = container_of(realdev, struct mmc_card, dev);
	int rv = 0;

	if (WARN_ON(!realdev || !card || !card->host))
		return -ENODATA;

	device_lock(ldm_cdev);

	if (priv->ready_for_poweroff) {
		device_unlock(ldm_cdev);
		return rv;
	}

	/* Suspend the block device */
	rv = device_prepare_suspend(realdev);
	if (rv) {
		device_unlock(ldm_cdev);
		return rv;
	}

	rv = device_suspend(realdev);
	if (rv) {
		device_unlock(ldm_cdev);
		return rv;
	}

	/*
	 * Call stack for mmc_suspend_host
	 *	host->bus_ops->suspend(host)
	 *	 == mmc_sd_suspend || mmc_suspend || mmc_sdio_suspend
	 */
	rv = mmc_suspend_host(card->host);
	if (rv)
		printk(KERN_ERR "%s: mmc_suspend_host returned %d\n",
			__func__, rv);

	priv->ready_for_poweroff = 0xffff;

	/* TODO (major) - free the mmc device interrupt lines */

	device_unlock(ldm_cdev);
	return rv;
}

/* Recover the device from a power loss */
static int mmc_post_poweron(struct device *ldm_cdev)
{
	struct device *dev = ldm_cdev->parent;
	struct sp_dev_type_mmc_priv *priv = dev_get_drvdata(ldm_cdev);
	struct mmc_card *card = container_of(dev, struct mmc_card, dev);
	int rv = 0;

	if (WARN_ON(!card || !card->host))
		return -ENODATA;

	device_lock(ldm_cdev);

	if (!priv->ready_for_poweroff) {
		device_unlock(ldm_cdev);
		return rv;
	}

	rv = mmc_resume_host(card->host);
	if (rv)
		printk(KERN_ERR "%s: mmc_resume_host returned %d\n",
			__func__, rv);

	rv = device_resume(dev);
	if (rv) {
		device_unlock(ldm_cdev);
		return rv;
	}

	priv->ready_for_poweroff = 0x0;

	/* TODO (major) - allocate the mmc device interrupt lines */

	device_unlock(ldm_cdev);
	return rv;
}

/* Release the device type */
static void mmc_type_release(struct device *dev)
{
	void *drv_data = dev_get_drvdata(dev);
	dev_set_drvdata(dev, NULL);
	kfree(drv_data);
}

/* Define operations for relevant MMC devices */
struct sp_dev_type sp_dev_type_mmc = {
	.pre_poweroff = mmc_pre_poweroff,
	.post_poweron = mmc_post_poweron,
	.ldm_type = {
		.name = "RDU MMC Device",
		.release = mmc_type_release
	}
};

/* Check the device to determine if the device is relevant to our handler */
static int is_relevant_mmc_dev(struct device *const dev)
{
	if (!dev->bus || !dev->bus->name)
		return 0;

	if (strncmp(mmc_bus_name, dev->bus->name, strlen(mmc_bus_name)))
		return 0;

	return 1;
}

/* Handle new MMC devices that are added to the class */
static int mmc_add_dev(struct device *dev, struct class_interface *intf)
{
	struct sp_dev_type_mmc_priv *priv;

	/* Obtain the device */
	device_lock(dev);

	/* Check to see if the device is relevant */
	if (!is_relevant_mmc_dev(dev->parent)) {
		device_unlock(dev);
		return 0;
	}

	/* Check to see if the device is already claimed */
	if (dev->type) {
		device_unlock(dev);
		return 0;
	}

	/* Allocate the private data for the device */
	priv = kzalloc(sizeof(*priv), GFP_KERNEL);
	if (!priv) {
		device_unlock(dev);
		return 0;
	}

	/* Set the device type to MMC */
	dev->type = &sp_dev_type_mmc.ldm_type;

	/* Set the device MMC private data */
	dev_set_drvdata(dev, priv);

	/* Release the device */
	device_unlock(dev);

	/* Report that the operation was successful */
	return 0;
}

/* Submodule initialization */
int rdu_mmc_ops_init(struct class *cl)
{
	/* Initialize the class interface */
	class_intf.add_dev = mmc_add_dev;
	class_intf.class = cl;

	/* Register the class interface */
	return class_interface_register(&class_intf);
}

/* Submodule exit */
void rdu_mmc_ops_exit(void)
{
	/* Unregister the class interface */
	class_interface_unregister(&class_intf);
}
