/*
 * sdreset_driver.c - kernel module interface implementation
 *
 * This RDU-specific driver attempts to workaround the "CRD_PWR" hardware bug by
 * dropping the entire VGEN2 power rail.  "The CRD_PWR hardware bug" refers to
 * an issue wherein the RDU fails to utilize the SMSC 2660's CRD_PWR lines for
 * powering SD cards.  As a result, the 2660 cannot reset SD cards when such is
 * necessary.  The state of all affected devices must be saved before and
 * restored after the power rail drop occurs.  That blackout affects:
 *  - The emmc1/mmcblk0 device
 *  - The RDU mezzanine (and SD Cards)
 */

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/regulator/consumer.h>

#include "sdreset_driver.h"
#include "rdu_mmc_ops.h"
#include "rdu_sdcard_ops.h"
#include "rdu_sdcard_blockdev.h"
#include "spc_class_mgr.h"


struct blacklist_entry {
	int exact_match;
	char * match_string;
};

/* The delays between and during a reset event */
static const unsigned long vgen2_reset_settle_delay = 10000;
static const unsigned long vgen2_reset_back_to_back_delay = (10 * 1000000000UL);
static const unsigned long vgen2_maximum_delay = MAX_UDELAY_MS * 1000;

/* The device class that maintains a list of devices affected by VGEN2 kicks */
static struct class *vgen2_devices;

/* The time mutex and the previous reset time */
static DEFINE_MUTEX(time_mutex);
static struct timespec last_reset_time;

/* List of devices that are not of interest */
static const struct blacklist_entry vgen2_blacklist_strings[] = {
	{0, "1-1"}, /* Ignore front panel USB and eMMC2 */
	{0, "2-1"}, /* Ignore RJU USB */
	{0, NULL},
};

/* List of devices that are of interest */
static const char * const vgen2_devices_devs[] = {
	":0:0:0", /* SCSI bus address for SD card 1 device */
	":0:0:1", /* SCSI bus address for SD card 2 device */
	"mmc0:0001", /* MMC bus address for for MMC device */
	NULL,
};

/* Prototype for the exit function*/
static void sdreset_exit(void);

/* Reset function for the class */
static void reset_vgen2_devices(void)
{
	struct timespec expire_time;
	struct timespec start_time;
	unsigned long power_delay;
	unsigned long power_delay_step;

	/* Obtain the time mutex */
	mutex_lock(&time_mutex);

	/* Get the previous reset time */
	expire_time = last_reset_time;

	/* Get the current reset time */
	getnstimeofday(&start_time);

	/* Get the next reset time */
	timespec_add_ns(&expire_time, vgen2_reset_back_to_back_delay);

	/* Determine if the time delay has been satisfied to allow a new reset */
	if (timespec_compare(&expire_time, &start_time) >= 0) {
		mutex_unlock(&time_mutex);
		return;
	}

	/* Set the previous reset time */
	last_reset_time = start_time;

	/* Release the time mutex */
	mutex_unlock(&time_mutex);

	/* Invoke pre-poweroff function for devices in the class */
	if (sp_class_pre_poweroff_all_devs(vgen2_devices))
		printk(KERN_ERR "Error calling pre_poweroff for devices\n");

	/* Delay to allow all SCSI commands to complete. */
	udelay(vgen2_maximum_delay);

	/* Disable the power to the VGEN2 rail */
	if (regulator_force_disable(regulator_get(NULL, "VGEN2")))
		printk(KERN_ERR "Failed to disable the VGEN2 regulator\n");

	/* Delay long enough to satisfy the reset requirements for the SD cards */
	power_delay = vgen2_reset_settle_delay;
	while (power_delay) {
		power_delay_step = min(power_delay, vgen2_maximum_delay);
		udelay(power_delay_step);
		power_delay -= power_delay_step;
	}

	/* Enable the power to the VGEN2 rail */
	if (regulator_enable(regulator_get(NULL, "VGEN2")))
		printk(KERN_ERR "Failed to enable the VGEN2 regulator\n");

	/* Invoke post-poweron function for devices in the class */
	if (sp_class_post_poweron_all_devs(vgen2_devices))
		printk(KERN_ERR "Error calling post_poweron for devices\n");
}

/* Check if a device is not of interest */
static int is_blacklisted(struct device *testdev)
{
	struct device *curdev;
	size_t i;

	/* Loop through forbidden device names for each device and its parents */
	for (curdev = testdev; curdev; curdev = curdev->parent)
		for (i = 0; vgen2_blacklist_strings[i].match_string; i++)
			if (vgen2_blacklist_strings[i].exact_match) {
				if (!strcmp(dev_name(curdev), vgen2_blacklist_strings[i].match_string))
					return 1;
			} else {
				if (strstr(dev_name(curdev), vgen2_blacklist_strings[i].match_string))
					return 1;
			}

	/* Report that the operation was successful */
	return 0;
}

/* Check for the correct device */
static int device_match(struct device *a, void *b)
{
	/* Determine if the device has already been processed */
	return ((a == b) || (a->parent == b));
}

/* Add the device to the class if it is relevant */
static int add_vgen2_dev(struct device *dev, void *data)
{
	int is_relevant = 0;
	size_t i;
	struct device *ldm_cdev;

	/* Obtain the device */
	get_device(dev);

	/* Check if the device is associated with a bus of interest */
	if (!dev->bus) {
		put_device(dev);
		return 0;
	}

	/* Check if the device is on the ignore list */
	if (is_blacklisted(dev)) {
		put_device(dev);
		return 0;
	}

	/* Check if the device is on the interest list */
	for (i = 0; vgen2_devices_devs[i]; ++i) {
		if (strstr(dev_name(dev), vgen2_devices_devs[i])) {
			is_relevant = 1;
			break;
		}
	}
	if (!is_relevant) {
		put_device(dev);
		return 0;
	}

	/* Determine if the device exists */
	if (class_find_device(vgen2_devices, NULL, dev, device_match)) {
		put_device(dev);
		put_device(dev);
		return -EEXIST;
	}

	/* Add the device to the class */
	ldm_cdev = device_create(vgen2_devices, dev, 0, NULL, "sp_dev:%s",
		dev_name(dev));
	if (IS_ERR_OR_NULL(ldm_cdev)) {
		put_device(dev);
		return -ENOMEM;
	}

	dev_info(dev, "added device %s to class %s\n", dev_name(ldm_cdev),
		vgen2_devices->name);

	/* Release the device */
	put_device(dev);

	/* Report that the operation was successful */
	return 0;
}

/* Remove a device from the class if it is relevant */
static int remove_vgen2_dev(struct device *dev, void *data)
{
	return sp_dev_unregister(dev, data);
}

/* Handle bus notifications */
static int bus_notifier_call(struct notifier_block *self, unsigned long action,
	void *vdev)
{
	struct device *changed = vdev;
	int rv;

	/* Handle the bus event */
	switch (action) {
		/* Add a device to the class */
		case BUS_NOTIFY_ADD_DEVICE:
			rv = add_vgen2_dev(changed, NULL);
			break;

		/* Remove a device from the class */
		case BUS_NOTIFY_DEL_DEVICE:
			rv = remove_vgen2_dev(changed, vgen2_devices);
			break;

		/* Ignore uninteresting events */
		default:
			rv = 0;
			break;
	}

	/* Report that status of the operation */
	return rv;
}

/* Request bus notification processing */
static struct notifier_block bus_notifier = {
	.notifier_call = bus_notifier_call
};

/* Parse each device and its children */
static int check_next_device(struct device *dev, void *data)
{
	int rv;

	/* Process all children of this device */
	rv = device_for_each_child(dev, data, check_next_device);
	if (rv)
		return rv;

	/* Add a device to the class */
	rv =  add_vgen2_dev(dev, data);
	if ((rv) && (rv != -EEXIST))
		return rv;

	/* Report that the operation was successful */
	return 0;
}

/* Kernel module initialization code */
static int sdreset_init(void)
{
	int rv;

	/* Create the device class */
	vgen2_devices = create_sp_class("vgen2_affected", reset_vgen2_devices);
	if (IS_ERR_OR_NULL(vgen2_devices)) {
		rv = PTR_ERR(vgen2_devices);
		vgen2_devices = NULL;
		return rv;
	}

	/* Search for relevant devices on the SCSI bus */
	rv = bus_for_each_dev(&scsi_bus_type, NULL, NULL, check_next_device);
	if (rv) {
		sdreset_exit();
		return rv;
	}

	/* Search for relevant devices on the MMC bus */
	rv = bus_for_each_dev(&mmc_bus_type, NULL, NULL, check_next_device);
	if (rv) {
		sdreset_exit();
		return rv;
	}

	/* Register for SCSI bus notifications */
	rv = bus_register_notifier(&scsi_bus_type, &bus_notifier);
	if (rv < 0) {
		sdreset_exit();
		return rv;
	}

	/* Register for MMC bus notifications */
	rv = bus_register_notifier(&mmc_bus_type, &bus_notifier);
	if (rv < 0) {
		sdreset_exit();
		return rv;
	}

	/* Initialize MMC device support */
	rv = rdu_mmc_ops_init(vgen2_devices);
	if (rv) {
		sdreset_exit();
		return rv;
	}

	/* Initialize SD card support */
	rv = rdu_sdcard_ops_init(vgen2_devices);
	if (rv) {
		sdreset_exit();
		return rv;
	}

	/* Initialize SD card block device support */
	rv = rdu_sdcard_blockdev_init(vgen2_devices);
	if (rv) {
		sdreset_exit();
		return rv;
	}

	/* Report that the operation was successful */
	return 0;
}

/* Kernel module exit code */
static void sdreset_exit(void)
{
	/* Exit MMC device support */
	rdu_mmc_ops_exit();

	/* Exit SD card support */
	rdu_sdcard_ops_exit();

	/* Exit SD card block device support */
	rdu_sdcard_blockdev_exit();

	/* Unregister for SCSI bus notifications */
	bus_unregister_notifier(&scsi_bus_type, &bus_notifier);

	/* Unregister for MMC bus notifications */
	bus_unregister_notifier(&mmc_bus_type, &bus_notifier);

	/* Destroy the device class */
	sp_class_unregister(vgen2_devices);
}

module_init(sdreset_init);
module_exit(sdreset_exit);

MODULE_LICENSE("GPL");
