/*
 * spc_class_mgr.c - Switchable power manager implementation
 */

#include <linux/device.h>
#include <linux/slab.h>
#include <linux/genhd.h>
#include <scsi/scsi_device.h>
#include <scsi/scsi_host.h>

#include <scsi/sdreset.h>

#include "spc_class_mgr.h"


/* List structure to track unregistered devices */
struct unregistered_device {
	struct list_head list;
	struct device *ldm_cdev;
};

/* Work task for handling SysFS induced SCSI resets */
struct work_struct_arg {
	struct work_struct ws;
	struct sp_class *spc;
};

static ssize_t sdreset_show(struct class *class, struct class_attribute *attr,
	char *buf)
{
	return snprintf(buf, PAGE_SIZE, "%d\n", to_sp_class(class)->reset_occurred);
}

static ssize_t sdreset_store(struct class *class, struct class_attribute *attr,
	const char *buf, size_t count)
{
	char * bufend;
	int value;

	value = simple_strtol(buf, &bufend, 10);
	if (buf == bufend)
		return -EFAULT;

	switch (value) {
		case 0:
			to_sp_class(class)->reset_occurred = 0;
			break;

		default:
			return -EFAULT;
	}

	return count;
}

static struct class_attribute class_attribs[] = {
	__ATTR(sdreset,  S_IWUSR | S_IRUGO, sdreset_show, sdreset_store),
	__ATTR_NULL,
};

static ssize_t inject_scsi_error(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	struct priv_shost_data *priv;
	char * bufend;
	int value;

	priv = to_scsi_device(dev->parent)->host->shost_data;

	value = simple_strtol(buf, &bufend, 10);
	if (buf == bufend)
		return -EFAULT;

	switch (value) {
		case 0:
			dev_info(dev->parent, "injecting failures disabled\n");
			priv->inject_usb_failures = NULL;
			break;

		case 1:
			dev_info(dev->parent, "injecting failures enabled\n");
			priv->inject_usb_failures = dev->parent;
			break;

		default:
			return -EFAULT;
	}

	return count;
}

static struct device_attribute device_attribs[] = {
		__ATTR(inject_scsi_error,  S_IWUSR, NULL, inject_scsi_error),
		__ATTR_NULL,
};

static void reset_work_cb(struct work_struct *w)
{
	struct work_struct_arg *arg = container_of(w, struct work_struct_arg, ws);

	/* Reset the class of devices */
	arg->spc->safe_power_reset();

	/* Notify the SysFS attribute that an SDReset event has occurred */
	arg->spc->reset_occurred = 1;

	/* Free the work structure when complete */
	kfree(arg);
}

static DECLARE_DELAYED_WORK(reset_work, reset_work_cb);

/* The power off handler for the device */
static int sp_dev_pre_poweroff(struct device *ldm_cdev, void *v)
{
	struct sp_dev_type *spdt;
	int *first_error = v;
	int rv;

	/* Exit if the device type is not defined */
	if (WARN_ON(!ldm_cdev->type))
		return -ENODATA;

	/* Obtain pointer to the SPD device */
	spdt = to_sp_dev_type(ldm_cdev->type);

	/* Exit if the function is not defined */
	if (!spdt->pre_poweroff)
		return 0;

	dev_dbg(ldm_cdev, "powering down\n");

	/* Prepare the device for power off */
	rv = spdt->pre_poweroff(ldm_cdev);

	/* Only report the first error code */
	if (rv && !*first_error)
		*first_error = rv;

	/* Report that the operation was successful */
	return 0;
}

/* The power on handler for the device */
static int sp_dev_post_poweron(struct device *ldm_cdev, void *v)
{
	struct sp_dev_type *spdt;
	int *first_error = v;
	int rv;

	/* Exit if the device type is not defined */
	if (WARN_ON(!ldm_cdev->type))
		return -ENODATA;

	/* Obtain pointer to the SPD device */
	spdt = to_sp_dev_type(ldm_cdev->type);

	/* Exit if the function is not defined */
	if (!spdt->post_poweron)
		return 0;

	dev_dbg(ldm_cdev, "powering up\n");

	/* Prepare the device for power on */
	rv = spdt->post_poweron(ldm_cdev);

	/* Only report the first error code */
	if (rv && !*first_error)
		*first_error = rv;

	/* Report that the operation was successful */
	return 0;
}

/* The power off handler for the class */
int sp_class_pre_poweroff_all_devs(struct class *c)
{
	struct sp_class *spc = to_sp_class(c);
	int first_error = 0;
	int rv;

	mutex_lock(&spc->sp_mutex);
	if (spc->in_reset) {
		mutex_unlock(&spc->sp_mutex);
		return 0;
	}
	spc->in_reset = 1;
	spc->in_power_op = 1;
	mutex_unlock(&spc->sp_mutex);

	printk(KERN_INFO "%s: Suspending all devices\n", c->name);

	/* Call the power off handler for each device */
	rv = class_for_each_device(c, NULL, &first_error, sp_dev_pre_poweroff);

	/* Report the status of the operation */
	return rv ?: first_error;
}

/* The power on handler for the class */
int sp_class_post_poweron_all_devs(struct class *c)
{
	struct sp_class *spc = to_sp_class(c);
	int first_error = 0;
	int rv;
	struct list_head *tmp, *pos;

	mutex_lock(&spc->sp_mutex);
	if (!spc->in_reset) {
		mutex_unlock(&spc->sp_mutex);
		return 0;
	}
	spc->in_reset = 0;
	mutex_unlock(&spc->sp_mutex);

	printk(KERN_INFO "%s: Resuming all devices\n", c->name);

	/* Call the power on handler for each device */
	rv = class_for_each_device(c, NULL, &first_error, sp_dev_post_poweron);

	mutex_lock(&spc->sp_mutex);
	BUG_ON(spc->in_reset);
	/* Since we're done iterating over the class, it's safe to directly
	 * unregister devices */
	spc->in_power_op = 0;
	mutex_unlock(&spc->sp_mutex);

	/* Cleanup any devices that unregistered while in_power_op was 1 */
	list_for_each_safe(pos, tmp, &spc->reset_unregs) {
		struct unregistered_device *d =
			container_of(pos, struct unregistered_device, list);
		put_device(d->ldm_cdev);
		device_unregister(d->ldm_cdev);
		list_del(pos);
		kfree(d);
	}

	/* Report the status of the operation */
	return rv ?: first_error;
}

/* Asynchronous reset of class devices */
static void async_reset(struct class *cl)
{
	struct sp_class *spc = to_sp_class(cl);
	struct work_struct_arg *arg;

	/* Allocate the work structure data */
	arg = kzalloc(sizeof(*arg), GFP_KERNEL);
	if (!arg)
		return;

	/* Associate the work structure data */
	INIT_WORK(&arg->ws, reset_work_cb);

	/* Initialize the work structure data */
	arg->spc = spc;

	/* Schedule the work task */
	schedule_work(&arg->ws);
}

/*###########################################################################
 * Unregistration helpers
 *##########################################################################*/
static void __sp_dev_unregister(struct device *dev, void *vcl)
{
	struct sp_class *spc = vcl;
	int defer;
	/*
	 * If the bus notification system has more than one thread, that second
	 * device can try to add the newly powered up device to the class BEFORE the
	 * first thread has removed the old device.
	 *
	 * Hold a device refcount across the power operation.  Don't "put" that
	 * device refcount until after post_poweron returns.  Even if the device
	 * gets unregistered by the bus thread(s), it wont see its refcount drop to
	 * 0 and thus won't be released.
	 *
	 * If a device is unregistered by a buss thread while power is off, put the
	 * device on a small list rather than unregisting it.  Then, in
	 * post_poweron, scan the list for devices that have been unregistered.
	 * Invoke their post_poweron, then actually invoke device_unregister.
	 *
	 * Also, types need a type_release callback because class_interface's
	 * "remove_device" callback is invoked at unregistration time, not
	 * at release time. An unregistered device need not have a refcount
	 * of 0 (case in point, post_poweron can be invoked on devices that
	 * are unregistered).
	 */
	dev_dbg(dev, "Unregistering\n");

	mutex_lock(&spc->sp_mutex);
	defer = spc->in_power_op;
	mutex_unlock(&spc->sp_mutex);

	if (!defer)
		device_unregister(dev);
	else {
		struct unregistered_device *r = kzalloc(sizeof(*r), GFP_KERNEL);
		if (r) {
			r->ldm_cdev = get_device(dev);
			list_add_tail(&r->list, &spc->reset_unregs);
		} else {
			device_unregister(dev);
		}
	}
}


/* Remove an SP device */
static int __sp_dev_unregister_if_relevant(struct device *ldm_cdev, void *vcl)
{
	if (ldm_cdev->class == vcl)
		__sp_dev_unregister(ldm_cdev, vcl);

	return 0;
}

/* Unregister each device if it is relevant */
int sp_dev_unregister(struct device *actual_dev, struct class *cl)
{
	return device_for_each_child(actual_dev, cl, __sp_dev_unregister_if_relevant);
}

/* Unregister an SP class */
void sp_class_unregister(struct class *cl)
{
	struct sp_class *spc = to_sp_class(cl);
	struct list_head *pos, *tmp;

	list_for_each_safe(pos, tmp, &spc->reset_unregs) {
		struct unregistered_device *d =
			container_of(pos, struct unregistered_device, list);

		put_device(d->ldm_cdev);
		device_unregister(d->ldm_cdev);

		list_del(pos);
		kfree(d);
	}

	/* Unregister the devices if they are associated */
	class_for_each_device(cl, NULL, cl, __sp_dev_unregister_if_relevant);

	/* Unregister the class */
	class_unregister(cl);
}

/* When the class' refcount drops to 0, this is invoked by the LDM */
static void release_sp_class(struct class *c)
{
	struct sp_class *spc = to_sp_class(c);

	mutex_destroy(&spc->sp_mutex);
	kfree(c->name);
	kfree(spc);
}

/*
 * Create a Switchable Power Class (SP) instance.
 *
 * \name  Name of the class to create. This will appear in sysfs.
 * \reset  Power operation reset function for the class.
 *
 * \return An instance of class which is created or -ERRNO on error.
 */
struct class *create_sp_class(const char *const name, void (*reset)(void))
{
	struct sp_class *spc;
	int rv;

	if (!name)
		return ERR_PTR(-ENODATA);

	spc = kzalloc(sizeof(*spc), GFP_KERNEL);
	if (!spc)
		return ERR_PTR(-ENOMEM);

	spc->class.name = kstrdup(name, GFP_KERNEL);
	if (!spc->class.name) {
		kfree(spc);
		return ERR_PTR(-ENOMEM);
	}

	/*
	 * struct class' device_release() callback is not used.  Rather, struct
	 * device's release() callback is used.  This is because of the
	 * prioritization specified
	 * in drivers/base/core.c.
	 */
	spc->class.owner = THIS_MODULE;
	spc->class.class_release = release_sp_class;
	spc->class.class_attrs = class_attribs;
	spc->class.dev_attrs = device_attribs;

	INIT_LIST_HEAD(&spc->reset_unregs);
	spc->reset_occurred = 0;
	spc->safe_power_reset = reset;
	spc->async_safe_power_reset = async_reset;
	mutex_init(&spc->sp_mutex);

	rv = class_register(&spc->class);
	if (rv) {
		kfree(spc);
		return ERR_PTR(rv);
	}

	return &spc->class;
}
