/*
 * rdu_sdcard_ops.c - SD card device interface implementation
 */

#include <linux/slab.h>
#include <scsi/scsi_eh.h>
#include <scsi/scsi_device.h>
#include <scsi/scsi_host.h>

#include <scsi/sdreset.h>

#include "sdreset_driver.h"
#include "spc_class_mgr.h"
#include "rdu_sdcard_ops.h"
#include "rdu_sdcard_blockdev.h"


static struct class_interface class_intf;

/* Name of USB mass storage host template: drivers/usb/storage/scsiglue.c */
static const char * const _usbmass_scsi_host_template_name = "usb-storage";

/* Private data for this type */
struct sp_dev_type_sdcard_priv {
	int reset_status;
};

/* For devices of this type, this is invoked at release time */
static void sdcard_type_release(struct device *ldm_cdev)
{
	void *drv_data = dev_get_drvdata(ldm_cdev);
	kfree(drv_data);
	dev_set_drvdata(ldm_cdev, NULL);
}

/*
 * Set the scsi_host_template for this parent's Scsi_Host.  If set_ops is null
 * or an error is encountered, defaults will be restored.
 */
static int _set_scsihost_template(struct device *ldm_cdev,
	int (*set_ops)(struct Scsi_Host *h))
{
	struct Scsi_Host *host = (to_scsi_device(ldm_cdev->parent))->host;
	int rv = 0;
	unsigned long f;
	struct device *put_dev = NULL;

	scsi_block_requests(host);
	spin_lock_irqsave(host->host_lock, f);

	/*
	 * If shost_data has data, but not our data, there is a problem:
	 *	 the scsi lld is using the shost_data pointer.
	 */
	if (!verify_sentinal(host->shost_data)) {
		rv = -EADDRINUSE;
		goto out;
	}

	/*
	 * Should default host_template be saved? Yes if:
	 *	- save is needed (shost_data is null) AND
	 *	- there is something to do later (set_ops is non-null)
	 */
	if (NULL == host->shost_data && set_ops) {
		/* Hijack shost_data for storage of original hostt */
		struct scsi_host_template *new_hostt;
		struct priv_shost_data *new_shost_data;

		new_hostt = kzalloc(sizeof(*new_hostt), GFP_KERNEL);
		if (!new_hostt) {
			rv = -ENOMEM;
			goto out;
		}

		new_shost_data =
			kzalloc(sizeof(*new_shost_data), GFP_KERNEL);
		if (!new_shost_data) {
			rv = -ENOMEM;
			kfree(new_hostt);
			goto out;
		}
		new_shost_data->sentinal = SDRESET_SCSI_PRIV_SENTINAL;

		/* Save the old hostt pointer */
		new_shost_data->old_hostt = host->hostt;

		new_shost_data->ldm_cdev = get_device(ldm_cdev);

		/* Setup the default values in the new
		 * scsi_host_template struct */
		memcpy(new_hostt, host->hostt, sizeof(*new_hostt));

		/* Set the hostt pointer to the new scsi_host_template struct */
		host->hostt = new_hostt;

		/* Save data in shost_data */
		host->shost_data = (void *) new_shost_data;

		dev_dbg(ldm_cdev, "defaults saved\n");
	}

	/* Set new operations */
	if (set_ops) {
		rv = set_ops(host);
		dev_dbg(ldm_cdev, "set_ops() invoked, returned %d\n", rv);
	}

	/* Restore defaults as requested or needed */
	if (NULL != host->shost_data && (!set_ops || rv )) {
		struct scsi_host_template *nondefault_hostt;
		struct priv_shost_data *priv = host->shost_data;

		/* This driver expects to see its data in shost_data */
		if (!verify_sentinal(priv)) {
			rv = -EADDRINUSE;
			goto out;
		}

		/* Reference will be cleaned up later, when unlocked */
		put_dev = priv->ldm_cdev;

		/* Save a pointer to free()-able areas */
		nondefault_hostt = host->hostt;

		/* Set the hostt pointer to the default
		 * scsi_host_template struct */
		host->hostt = priv->old_hostt;

		/* free the nondefault scsi_host_template struct */
		kfree(nondefault_hostt);

		/* free the priv_shost_data struct */
		kfree(priv);
		host->shost_data = NULL;

		dev_dbg(ldm_cdev, "defaults restored\n");
	}

out:
	/* Sanity check cleanup actions */
	if (WARN_ON(!set_ops && host->shost_data))
			rv = rv ?: -EINVAL;

	spin_unlock_irqrestore(host->host_lock, f);
	scsi_unblock_requests(host);

	/* Do this after unlock */
	put_device(put_dev);

	return rv;
}

/* Pass-through version of USB bulk-only mass storage class reset request" */
static int eh_device_reset_passthrough(struct scsi_cmnd *cmd)
{
	struct priv_shost_data *priv;

	if (!cmd || !cmd->device || !cmd->device->host)
		return -ENODATA;

	priv = (struct priv_shost_data *) cmd->device->host->shost_data;

	if (!priv || !priv->old_hostt)
		return -ENODATA;

	if (priv->old_hostt->eh_device_reset_handler)
		return priv->old_hostt->eh_device_reset_handler(cmd);

	return SUCCESS;
}

/* Rail-dropping version of SCSI bus reset operation reset request" */
static int eh_bus_reset_kick_vgen2(struct scsi_cmnd *cmd)
{
	struct priv_shost_data *priv;
	struct sp_class *spc;

	if (!cmd || !cmd->device || !cmd->device->host)
		return -ENODATA;

	priv = (struct priv_shost_data *) cmd->device->host->shost_data;
	spc = to_sp_class(priv->ldm_cdev->class);

	spc->async_safe_power_reset(priv->ldm_cdev->class);

	return SUCCESS;
}

static int set_reset_handlers_kick_vgen2(struct Scsi_Host *host)
{
	host->hostt->eh_device_reset_handler = &eh_device_reset_passthrough;
	host->hostt->eh_bus_reset_handler = &eh_bus_reset_kick_vgen2;

	return 0;
}

/*
 * Set the scsi_host_template for this parent's Scsi_Host.
 */
static int set_scsihost_template(struct device *dev, HijackState state)
{
	int rv;

	if (!scsi_is_sdev_device(dev->parent))
		return -ENODATA;

	switch (state) {
		case HJ_NOT_HIJACKED:
			rv = _set_scsihost_template(dev, NULL);
			break;

		case HJ_VGEN2_DROP:
			rv = _set_scsihost_template(dev, set_reset_handlers_kick_vgen2);
			break;

		default:
			rv = -EFAULT;
			break;
	}

	return rv;
}

/*
 * Define operations for affected rdu sdcard devices. If a device passes
 * "is_relevant_sdcard_dev," its child device (the ldm_cdev) will have its type
 *  set to this static struct.
 */
struct sp_dev_type sp_dev_type_sdcard = {
	.pre_poweroff = sdcard_passthrough_blockdev_pre_powerdown,
	.post_poweron = NULL,
	.ldm_type = {
		.name = "RDU SdCard dev type",
		.release = sdcard_type_release,
	}
};

/* Relevance test for the class interface's add function. */
static int is_relevant_sdcard_dev(struct device * const dev)
{
	struct scsi_device const *sdev = NULL;
	struct Scsi_Host const *shost = NULL;

	/*
	 * The bus type is guaranteed to match by scsi_sysfs.c
	 * when scsi_is_sdev_device(dev) returns "yes"
	 */
	if (!dev || !scsi_is_sdev_device(dev))
		goto no_match;

	sdev = to_scsi_device(dev);
	shost = sdev->host;

	if (TYPE_DISK != sdev->type)
		goto no_match;

	if (!shost || !shost->hostt)
		goto no_match;

	/* If it's not using the usb_mass_storage host template,
	 * it cannot use these dev attrs */
	if (strncmp(_usbmass_scsi_host_template_name,
				shost->hostt->name,
				strlen(_usbmass_scsi_host_template_name)))
		goto no_match;

	return 1;

no_match:
	return 0;
}

/* Class interface device add handler */
static int sdcard_add_dev(struct device *dev, struct class_interface *intf)
{
	struct sp_dev_type_sdcard_priv *type_priv;
	int rv;

	device_lock(dev);

	if (!is_relevant_sdcard_dev(dev->parent)) {
		device_unlock(dev);
		return 0;
	}

	/* Relevant, but type set, another class interface found device relevant */
	if (WARN_ON(dev->type)) {
		device_unlock(dev);
		return 0;
	}

	type_priv = kzalloc(sizeof(*type_priv), GFP_KERNEL);
	if (!type_priv) {
		device_unlock(dev);
		return 0;
	}

	dev->type = &sp_dev_type_sdcard.ldm_type;
	dev_set_drvdata(dev, type_priv);

	rv = set_scsihost_template(dev, HJ_VGEN2_DROP);
	if (WARN_ON(rv)) {
		printk(KERN_ERR "%s: rv is %d\n", __func__, rv);
		device_unlock(dev);
		return 0;
	}

	device_unlock(dev);

	return 0;
}

/* Class interface device remove handler */
static void sdcard_remove_dev(struct device *dev, struct class_interface *intf)
{
	int rv = 0;

	device_lock(dev);

	if (dev->type != &sp_dev_type_sdcard.ldm_type) {
		device_unlock(dev);
		return;
	}

	rv = set_scsihost_template(dev, HJ_NOT_HIJACKED);
	if (rv)
		dev_emerg(dev, "scsi_host_template failure, reboot ASAP\n");

	device_unlock(dev);
}

/* Submodule initialization */
int rdu_sdcard_ops_init(struct class *cl)
{
	/* Initialize the class interface */
	class_intf.add_dev = sdcard_add_dev;
	class_intf.remove_dev = sdcard_remove_dev;
	class_intf.class = cl;

	/* Register the class interface */
	return class_interface_register(&class_intf);
}

/* Submodule exit */
void rdu_sdcard_ops_exit(void)
{
	/* Unregister the class interface */
	class_interface_unregister(&class_intf);
}
