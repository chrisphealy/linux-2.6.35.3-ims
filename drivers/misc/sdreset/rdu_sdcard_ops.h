/*
 * rdu_sdcard_ops.h - SD card device interface definition
 */

#ifndef RDU_SDCARD_OPS_H
#define RDU_SDCARD_OPS_H

#include <linux/device.h>

#include "spc_class_mgr.h"

int rdu_sdcard_ops_init(struct class *cl);
void rdu_sdcard_ops_exit(void);

/* sp_dev_types found in class_vgen2_affected */
extern struct sp_dev_type sp_dev_type_sdcard;


#endif
