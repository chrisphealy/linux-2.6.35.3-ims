/*
 * sdreset_driver.h - Container for devices to be reset definition
 */

#ifndef SDRESET_DRIVER_H
#define SDRESET_DRIVER_H

#include <linux/device.h>

extern struct bus_type scsi_bus_type;
extern struct bus_type mmc_bus_type;


#endif
