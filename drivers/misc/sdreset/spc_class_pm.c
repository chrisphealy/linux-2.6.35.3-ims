/*
 * spc_device_pm.c - Device power management implementation
 */

#include <linux/pm.h>
#include <linux/device.h>

#include "spc_class_pm.h"


/**
 * Reimplementation of drivers/base/power/main.c's
 *	 static int __device_suspend(struct device *dev,
 *					pm_message_t state, bool async)
 */
int device_suspend(struct device *dev)
{
	int error = 0;
	int hit = 0;
	pm_message_t t;
	t.event = PM_EVENT_SUSPEND;

	if (!dev)
		goto End;

	device_lock(dev);

	if (dev->class) {
		if (dev->class->pm) {
			error = dev->class->pm->suspend(dev);
			dev_dbg(dev, "has dev->class->pm->suspend");
			++hit;
		} else if (dev->class->suspend) {
			error = dev->class->suspend(dev, t);
			dev_dbg(dev, "has dev->class->suspend");
			++hit;
		}
		if (error)
			goto End;
	}

	if (dev->type) {
		if (dev->type->pm) {
			error = dev->type->pm->suspend(dev);
			dev_dbg(dev, "has dev->type->pm->suspend");
			++hit;
		}
		if (error)
			goto End;
	}

	if (dev->bus) {
		if (dev->bus->pm) {
			error = dev->bus->pm->suspend(dev);
			dev_dbg(dev, "has dev->bus->pm->suspend");
			++hit;
		} else if (dev->bus->suspend) {
			error = dev->bus->suspend(dev, t);
			dev_dbg(dev, "has dev->bus->suspend");
			++hit;
		}
		if (error)
			goto End;
	}

	if (!error)
		dev->power.status = DPM_OFF;

	dev_dbg(dev, "called %d fps\n", hit);

End:
	if (error)
		printk(KERN_ERR "%s: error is %d\n", __func__, error);

	if (dev)
		device_unlock(dev);

	return error;
}


/**
 * Reimplementation of drivers/base/power/main.c's
 *		static int device_resume(struct device *dev,
 *		pm_message_t state, bool async)
 */
int device_resume(struct device *dev)
{
	int error = 0;

	if (!dev)
		goto End;

	device_lock(dev);

	if (dev->class) {
		if (dev->class->pm) {
			error = dev->class->pm->resume(dev);
			dev_dbg(dev, "has dev->class->pm->resume");
		} else if (dev->class->resume) {
			error = dev->class->resume(dev);
			dev_dbg(dev, "has dev->class->resume");
		}
		if (error)
			goto End;
	}

	if (dev->type) {
		if (dev->type->pm) {
			error = dev->type->pm->resume(dev);
			dev_dbg(dev, "has dev->type->pm->resume");
		}
		if (error)
			goto End;
	}

	if (dev->bus) {
		if (dev->bus->pm) {
			error = dev->bus->pm->resume(dev);
			dev_dbg(dev, "has dev->bus->pm->resume");
		} else if (dev->bus->resume) {
			error = dev->bus->resume(dev);
			dev_dbg(dev, "has dev->bus->resume");
		}
	}

	if (!error)
		dev->power.status = DPM_ON;

End:
	if (error)
		printk(KERN_ERR "%s: error is %d\n", __func__, error);

	if (dev)
		device_unlock(dev);

	return error;
}


/**
 * Reimplementation of drivers/base/power/main.c's
 *	static int device_prepare(struct device *dev, pm_message_t state)
 */
int device_prepare_suspend(struct device *dev)
{
	int error = 0;

	if (!dev)
		goto End;

	device_lock(dev);

	if (dev->bus && dev->bus->pm && dev->bus->pm->prepare) {
		error = dev->bus->pm->prepare(dev);
		dev_dbg(dev, "has dev->bus->pm->prepare");

		if (error)
			goto End;
	}

	if (dev->type && dev->type->pm && dev->type->pm->prepare) {
		error = dev->type->pm->prepare(dev);
		dev_dbg(dev, "has dev->type->pm->prepare");
		if (error)
			goto End;
	}

	if (dev->class && dev->class->pm && dev->class->pm->prepare) {
		error = dev->class->pm->prepare(dev);
		dev_dbg(dev, "has dev->class->pm->prepare");
	}
End:
	if (dev)
		device_unlock(dev);

	return error;
}
