/*
 * spi-liufpga.c - LIU board FPGA driver implementation
 */

#include <linux/cdev.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/fs.h>
#include <linux/delay.h>
#include <linux/spi/spi.h>
#include <linux/spi/liufpga.h>
#include <mach/gpio.h>


/* FPGA data type widths */
#define U8_BITS						8
#define FPGA_BITS_U8				(sizeof(u8) * U8_BITS)  /* Byte width */
#define FPGA_BITS_U16				(sizeof(u16) * U8_BITS) /* Short width */
#define FPGA_BITS_U32				(sizeof(u32) * U8_BITS) /* Long width */

/* FPGA device responses */
#define FPGA_ACK					0x01		/* Command processing success */
#define FPGA_NACK_COMMAND			0x11		/* Command not recognized */
#define FPGA_NACK_DATA				0x12		/* Command data bad format */
#define FPGA_NACK_BUSY				0x13		/* Command device busy abort*/

#define FPGA_CMD_MAX_COUNT			1			/* Maximum command transfer */
#define FPGA_ACK_MAX_COUNT			1			/* Maximum ACK transfer */
#define FPGA_CHECKSUM_MAX_COUNT		1			/* Maximum checksum transfer */

#define FPGA_IMAGE_ID				0xCA42		/* FPGA revision identifier */

#define FPGA_SPI_RESET_DELAY		100			/* FPGA reset delay in us */

/* FPGA read status command format */
struct FpgaCommandStatus {
	u32		fpgaCount    : 16;					/* FPGA resister count */
	u32		fpgaAddress  : 12;					/* FPGA resister index */
	u32		fpgaCommand  :  4;					/* FPGA command */
} __attribute__ ((packed));

/* FPGA Way register command format */
struct FpgaCommandWay {
	u32		fpgaValue    : 16;					/* FPGA command data */
	u32		fpgaWord     :  4;					/* FPGA resister index */
	u32		fpgaSeuId    :  5;					/* FPGA SEU index */
	u32		fpgaWay      :  2;					/* FPGA way index */
	u32		fpgaLac      :  1;					/* FPGA LAC index */
	u32		fpgaCommand  :  4;					/* FPGA command */
} __attribute__ ((packed));

/* FPGA General Purpose register command format */
struct FpgaCommandGP {
	u32		fpgaValue    : 16;					/* FPGA command data */
	u32		fpgaAddress  : 12;					/* FPGA resister index */
	u32		fpgaCommand  :  4;					/* FPGA command */
} __attribute__ ((packed));

/* FPGA command format */
union FpgaCommandGroup {
	struct FpgaCommandStatus		commandStatus;
	struct FpgaCommandWay			commandWay;
	struct FpgaCommandGP			commandGp;
	u32								commandValue;
};

/* FPGA state data structure */
struct LiuFpgaState {
	struct spi_device *				spiDevice;

	dev_t							fpgaCharDevt;
	struct class *					fpgaCharClass;
	struct device *					fpgaCharDevice;
	struct cdev						fpgaCharCdev;

	struct mutex					fpgaIoMutex;
};

/* Handle the IRQ for the FPGA */
static irqreturn_t LiuFpgaIrqHandler(int irq, void * dev_id)
{
	return IRQ_HANDLED;
}

/* Send a command to the FPGA */
static int LiuFpgaSendCommand(struct LiuFpgaState * fpgaStatus, u32 fpgaData)
{
	u32 * transferBuffer;

	/* Validate the FPGA status */
	if (fpgaStatus == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA state pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	dev_dbg(&fpgaStatus->spiDevice->dev, "%s - enter\n", __func__);

	/* Allocate the transfer buffer */
	transferBuffer = kzalloc(sizeof (fpgaData), GFP_KERNEL);

	/* Copy the data to the transfer buffer */
	memcpy(transferBuffer, &fpgaData, sizeof (fpgaData));

	/* Set the write operation word width based upon the number of bytes to be transferred.
	 * The following write transfer bit widths are valid for the FPGA:
	 *     32 bit width - used to send the value of a command word to the FPGA.
	 */
	fpgaStatus->spiDevice->bits_per_word = FPGA_BITS_U32;
	spi_setup(fpgaStatus->spiDevice);

	/* Perform the write operation and report any errors */
	if (spi_write(fpgaStatus->spiDevice, (u8 *) transferBuffer, FPGA_CMD_MAX_COUNT) < 0) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - SPI write failure\n", __func__);
		kfree(transferBuffer);
		return -EIO;
	}

	/* Set the read operation word width based upon the number of bytes to be transferred.
	 * The following read transfer bit widths are valid for the FPGA:
	 *      8 bit width - used to receive the ACK/NACK response from the FPGA
	 *     16 bit width - used to read the value of a register entry from the Way and General
	 *                    Purpose memory regions, which are 16 bits wide.
	 *     32 bit width - used to read an array of 1 to 121 register entries from the Status
	 *                    memory region, which are 32 bits wide.  The previous bounds check
	 *                    ensures that the array is of a valid length, where this comparison
	 *                    sets the bit width of the transfer.
	 */
	fpgaStatus->spiDevice->bits_per_word = FPGA_BITS_U8;
	spi_setup(fpgaStatus->spiDevice);

	/* Perform the read operation and report any errors */
	if (spi_read(fpgaStatus->spiDevice, (u8 *) transferBuffer, FPGA_ACK_MAX_COUNT) < 0) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - SPI read failure\n", __func__);
		kfree(transferBuffer);
		return -EIO;
	}

	/* Validate the FPGA reply */
	if (*((u8 *) transferBuffer) == FPGA_NACK_COMMAND) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - FPGA command invalid\n", __func__);
		kfree(transferBuffer);
		return -EPERM;
	} else if (*((u8 *) transferBuffer) == FPGA_NACK_DATA) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - FPGA address invalid\n", __func__);
		kfree(transferBuffer);
		return -EFAULT;
	} else if (*((u8 *) transferBuffer) == FPGA_NACK_BUSY) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - FPGA device busy\n", __func__);
		kfree(transferBuffer);
		return -EBUSY;
	} else if (*((u8 *) transferBuffer) != FPGA_ACK) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - FPGA response unknown %02X\n", __func__, *((u8 *) transferBuffer));
		kfree(transferBuffer);
		return -EIO;
	}

	/* Release the transfer buffer */
	kfree(transferBuffer);

	/* Report the results of the send operation */
	return 0;
}

/* Receive data from the FPGA */
static int LiuFpgaReceiveData(struct LiuFpgaState * fpgaStatus, void * fpgaData, u8 fpgaWidth, u8 fpgaCount)
{
	u32 * transferBuffer;

	/* Validate the FPGA status */
	if (fpgaStatus == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA state pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA data */
	if (fpgaData == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA data pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA width */
	if ((fpgaWidth != FPGA_BITS_U32) && (fpgaWidth != FPGA_BITS_U16)) {
		printk(KERN_DEBUG "%s: %s - FPGA word width invalid\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA count */
	if (fpgaCount == 0) {
		printk(KERN_DEBUG "%s: %s - FPGA data count zero\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	dev_dbg(&fpgaStatus->spiDevice->dev, "%s - enter\n", __func__);

	/* Allocate the transfer buffer */
	transferBuffer = kzalloc(fpgaCount * (fpgaWidth / 8), GFP_KERNEL);

	/* Set the read operation word width based upon the number of bytes to be transferred.
	 * The following read transfer bit widths are valid for the FPGA:
	 *      8 bit width - used to receive the ACK/NACK response from the FPGA
	 *     16 bit width - used to read the value of a register entry from the Way and General
	 *                    Purpose memory regions, which are 16 bits wide.
	 *     32 bit width - used to read an array of 1 to 121 register entries from the Status
	 *                    memory region, which are 32 bits wide.  The previous bounds check
	 *                    ensures that the array is of a valid length, where this comparison
	 *                    sets the bit width of the transfer.
	 */
	fpgaStatus->spiDevice->bits_per_word = fpgaWidth;
	spi_setup(fpgaStatus->spiDevice);

	/* Perform the read operation and report any errors */
	if (spi_read(fpgaStatus->spiDevice, (u8 *) transferBuffer, fpgaCount) < 0) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - SPI read failure\n", __func__);
		kfree(transferBuffer);
		return -EIO;
	}

	/* Copy the data from the transfer buffer */
	memcpy(fpgaData, transferBuffer, fpgaCount * (fpgaWidth / 8));

	/* Release the transfer buffer */
	kfree(transferBuffer);

	/* Report the results of the receive operation */
	return 0;
}

/* Invoke a Status memory command */
static int LiuFpgaCommandStatus(struct LiuFpgaState * fpgaStatus, u8 fpgaCommand, unsigned long fpgaData)
{
	union FpgaCommandGroup		statusCommand;
	struct FpgaIoctlDataStatus	statusData;
	u8							statusIndex;
	u32							statusChecksum;
	u32							statusChecksumRead;

	/* Validate the FPGA status */
	if (fpgaStatus == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA state pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA command */
	if ((fpgaCommand != READ_STATUS_REGS) && (fpgaCommand != CANCEL_STATUS_READ)) {
		printk(KERN_DEBUG "%s: %s - FPGA memory command invalid\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA data */
	if ((fpgaCommand == READ_STATUS_REGS) && (fpgaData == 0)) {
		printk(KERN_DEBUG "%s: %s - FPGA data pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	dev_dbg(&fpgaStatus->spiDevice->dev, "%s - enter\n", __func__);

	if (fpgaCommand == READ_STATUS_REGS) {
		/* Copy the data from user space */
		if (copy_from_user(&statusData, (struct FpgaIoctlDataStatus *) fpgaData, sizeof(struct FpgaIoctlDataStatus))) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - error in copy from user space\n", __func__);
			return -EACCES;
		}

		/* Validate the operation address */
		if (statusData.fpgaAddress >= FPGA_STATUS_MAX_ADDRESS) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - invalid Status address %u\n", __func__, statusData.fpgaAddress);
			return -EINVAL;
		}

		/* Validate the operation length */
		if (statusData.fpgaCount > FPGA_STATUS_MAX_COUNT) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - invalid Status length %u\n", __func__, statusData.fpgaAddress);
			return -EINVAL;
		}
	}

	/* Initialize the FPGA command */
	statusCommand.commandStatus.fpgaCommand = fpgaCommand;
	if (fpgaCommand == READ_STATUS_REGS) {
		statusCommand.commandStatus.fpgaAddress = statusData.fpgaAddress;
		statusCommand.commandStatus.fpgaCount = statusData.fpgaCount;
	} else {
		statusCommand.commandStatus.fpgaAddress = 0;
		statusCommand.commandStatus.fpgaCount = 0;
	}

	/* Send the command to the FPGA */
	if(LiuFpgaSendCommand(fpgaStatus, statusCommand.commandValue)) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - error in command transmit\n", __func__);
		return -EIO;
	}

	if (fpgaCommand == READ_STATUS_REGS) {
		/* Initialize the calculated checksum */
		statusChecksum = 0;

		/* Read the data from the FPGA and calculate the checksum */
		for (statusIndex = 0; statusIndex < statusData.fpgaCount; statusIndex++) {
			if (LiuFpgaReceiveData(fpgaStatus, &statusData.fpgaValue[statusIndex], FPGA_BITS_U32, 1) < 0) {
				dev_err(&fpgaStatus->spiDevice->dev, "%s - SPI read failure\n", __func__);
				return -EIO;
			}

			statusChecksum += statusData.fpgaValue[statusIndex];
		}

		/* Read the checksum from the FPGA */
		if (LiuFpgaReceiveData(fpgaStatus, &statusChecksumRead, FPGA_BITS_U32, FPGA_CHECKSUM_MAX_COUNT) < 0) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - SPI read failure\n", __func__);
			return -EIO;
		}

		/* Verify the calculated checksum against the checksum from the FPGA*/
		if (statusChecksum != statusChecksumRead) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - checksum invalid\n", __func__);
			return -EIO;
		}

		/* Copy the data to user space */
		if (copy_to_user((struct FpgaIoctlDataStatus *) fpgaData, &statusData, sizeof(struct FpgaIoctlDataStatus))) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - error in copy to user space\n", __func__);
			return -EACCES;
		}
	}

	return 0;
}

/* Invoke a Way memory command */
static int LiuFpgaCommandWay(struct LiuFpgaState * fpgaStatus, u8 fpgaCommand, unsigned long fpgaData)
{
	union FpgaCommandGroup		wayCommand;
	struct FpgaIoctlDataWay		wayData;

	/* Validate the FPGA status */
	if (fpgaStatus == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA state pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA command */
	if ((fpgaCommand != READ_WAY_REG) && (fpgaCommand != WRITE_WAY_REG) &&
			(fpgaCommand != SET_WAY_REG) && (fpgaCommand != CLEAR_WAY_REG)) {
		printk(KERN_DEBUG "%s: %s - FPGA memory command invalid\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA data */
	if (fpgaData == 0) {
		printk(KERN_DEBUG "%s: %s - FPGA data pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	dev_dbg(&fpgaStatus->spiDevice->dev, "%s - enter\n", __func__);

	/* Copy the data from user space */
	if (copy_from_user(&wayData, (struct FpgaIoctlDataWay *) fpgaData, sizeof(struct FpgaIoctlDataWay))) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - error in copy from user space\n", __func__);
		return -EACCES;
	}

	/* Validate the way address LAC component */
	if (wayData.fpgaLac >= FPGA_WAY_MAX_LAC) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - invalid way address LAC field %u\n", __func__, wayData.fpgaLac);
		return -EINVAL;
	}

	/* Validate the way address Way component */
	if (wayData.fpgaWay >= FPGA_WAY_MAX_WAY) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - invalid way address Way field %u\n", __func__, wayData.fpgaWay);
		return -EINVAL;
	}

	/* Validate the way address SEU ID component */
	if (wayData.fpgaSeuId >= FPGA_WAY_MAX_SEU_ID) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - invalid way address SEU ID field %u\n", __func__, wayData.fpgaSeuId);
		return -EINVAL;
	}

	/* Initialize the FPGA command */
	wayCommand.commandWay.fpgaCommand = fpgaCommand;
	wayCommand.commandWay.fpgaLac = wayData.fpgaLac;
	wayCommand.commandWay.fpgaWay = wayData.fpgaWay;
	wayCommand.commandWay.fpgaSeuId = wayData.fpgaSeuId;
	wayCommand.commandWay.fpgaWord = wayData.fpgaWordId;
	if (fpgaCommand == READ_WAY_REG) {
		wayCommand.commandGp.fpgaValue = 0;
	} else {
		wayCommand.commandGp.fpgaValue = wayData.fpgaValue[0];
	}

	/* Send the command to the FPGA */
	if(LiuFpgaSendCommand(fpgaStatus, wayCommand.commandValue)) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - error in command transmit\n", __func__);
		return -EIO;
	}

	if (fpgaCommand == READ_WAY_REG) {
		/* Read the data from the FPGA */
		if (LiuFpgaReceiveData(fpgaStatus, wayData.fpgaValue, FPGA_BITS_U16, FPGA_WAY_MAX_COUNT) < 0) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - SPI read failure\n", __func__);
			return -EIO;
		}

		/* Copy the data to user space */
		if (copy_to_user((struct FpgaIoctlDataWay *) fpgaData, &wayData, sizeof(struct FpgaIoctlDataWay))) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - error in copy to user space\n", __func__);
			return -EACCES;
		}
	}

	return 0;
}

/* Invoke a General Purpose memory command */
static int LiuFpgaCommandGp(struct LiuFpgaState * fpgaStatus, u8 fpgaCommand, unsigned long fpgaData)
{
	union FpgaCommandGroup				gpCommand;
	struct FpgaIoctlDataGeneralPurpose	gpData;

	/* Validate the FPGA status */
	if (fpgaStatus == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA state pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA command */
	if ((fpgaCommand != READ_GP_REG) && (fpgaCommand != WRITE_GP_REG)) {
		printk(KERN_DEBUG "%s: %s - FPGA memory command invalid\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the FPGA data */
	if (fpgaData == 0) {
		printk(KERN_DEBUG "%s: %s - FPGA data pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	dev_dbg(&fpgaStatus->spiDevice->dev, "%s - enter\n", __func__);

	/* Copy the data from user space */
	if (copy_from_user(&gpData, (struct FpgaIoctlDataGeneralPurpose *) fpgaData, sizeof(struct FpgaIoctlDataGeneralPurpose))) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - error in copy from user space\n", __func__);
		return -EACCES;
	}

	/* Validate the operation address */
	if (gpData.fpgaAddress >= FPGA_GP_MAX_ADDRESS) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - invalid general purpose address %u\n", __func__, gpData.fpgaAddress);
		return -EINVAL;
	}

	/* Initialize the FPGA command */
	gpCommand.commandGp.fpgaCommand = fpgaCommand;
	gpCommand.commandGp.fpgaAddress = gpData.fpgaAddress;
	if (fpgaCommand == READ_GP_REG) {
		gpCommand.commandGp.fpgaValue = 0;
	} else {
		gpCommand.commandGp.fpgaValue = gpData.fpgaValue[0];
	}

	/* Send the command to the FPGA */
	if(LiuFpgaSendCommand(fpgaStatus, gpCommand.commandValue)) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - error in command transmit\n", __func__);
		return -EIO;
	}

	if (fpgaCommand == READ_GP_REG) {
		/* Read the data from the FPGA */
		if (LiuFpgaReceiveData(fpgaStatus, gpData.fpgaValue, FPGA_BITS_U16, FPGA_GP_MAX_COUNT) < 0) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - SPI read failure\n", __func__);
			return -EIO;
		}

		/* Copy the data to user space */
		if (copy_to_user((struct FpgaIoctlDataGeneralPurpose *) fpgaData, &gpData, sizeof(struct FpgaIoctlDataGeneralPurpose))) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - error in copy to user space\n", __func__);
			return -EACCES;
		}
	}

	return 0;
}

/* Reset the FPGA SPI state machine */
static void LiuFpgaCommandReset(struct FpgaLiuPlatformData * fpgaData)
{
	gpio_direction_output(fpgaData->fpgaSpiGpio, 0);
	udelay(FPGA_SPI_RESET_DELAY);
	gpio_direction_output(fpgaData->fpgaSpiGpio, 1);
}

/* Process the current IOCTL request */
static long LiuFpgaIoctl(struct file * filp, unsigned int cmd, unsigned long arg)
{
	struct LiuFpgaState *		fpgaStatus;
	struct FpgaLiuPlatformData *	fpgaData;
	int				status;

	/* Validate the command file */
	if (filp == NULL) {
		printk(KERN_DEBUG "%s: %s - filp parameter NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the command value */
	if (cmd == 0) {
		printk(KERN_DEBUG "%s: %s - cmd parameter invalid = %u\n", FPGA_DEVNAME, __func__, cmd);
		return -EINVAL;
	}

	/* Validate the FPGA status buffer */
	if (filp->private_data == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA status pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Get the FPGA status buffer */
	fpgaStatus = (struct LiuFpgaState *) filp->private_data;

	/* Validate the FPGA status buffer */
	if (fpgaStatus->spiDevice == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA spi device pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	fpgaData = (struct FpgaLiuPlatformData *) fpgaStatus->spiDevice->dev.platform_data;

	dev_dbg(&fpgaStatus->spiDevice->dev, "%s - enter\n", __func__);

	/* Handle the specified IOCTL */
	switch (cmd) {
		/* Read data from the status registers */
		case FPGA_READ_STATUS:
			/* Perform the operation guarded by the mutex */
			mutex_lock(&fpgaStatus->fpgaIoMutex);
			LiuFpgaCommandReset(fpgaData);
			status = LiuFpgaCommandStatus(fpgaStatus, READ_STATUS_REGS, arg);
			mutex_unlock(&fpgaStatus->fpgaIoMutex);

			break;

		/* Cancel a status register read operation */
		case FPGA_CANCEL_STATUS:
			/* Perform the operation guarded by the mutex */
			mutex_lock(&fpgaStatus->fpgaIoMutex);
			LiuFpgaCommandReset(fpgaData);
			status = LiuFpgaCommandStatus(fpgaStatus, CANCEL_STATUS_READ, arg);
			mutex_unlock(&fpgaStatus->fpgaIoMutex);

			break;

		/* Read data from the way registers */
		case FPGA_READ_WAY:
			/* Perform the operation guarded by the mutex */
			mutex_lock(&fpgaStatus->fpgaIoMutex);
			LiuFpgaCommandReset(fpgaData);
			status = LiuFpgaCommandWay(fpgaStatus, READ_WAY_REG, arg);
			mutex_unlock(&fpgaStatus->fpgaIoMutex);

			break;

		/* Write data to the way registers */
		case FPGA_WRITE_WAY:
			/* Perform the operation guarded by the mutex */
			mutex_lock(&fpgaStatus->fpgaIoMutex);
			LiuFpgaCommandReset(fpgaData);
			status = LiuFpgaCommandWay(fpgaStatus, WRITE_WAY_REG, arg);
			mutex_unlock(&fpgaStatus->fpgaIoMutex);

			break;

		/* Set data bits in the way registers */
		case FPGA_SET_WAY_BITS:
			/* Perform the operation guarded by the mutex */
			mutex_lock(&fpgaStatus->fpgaIoMutex);
			LiuFpgaCommandReset(fpgaData);
			status = LiuFpgaCommandWay(fpgaStatus, SET_WAY_REG, arg);
			mutex_unlock(&fpgaStatus->fpgaIoMutex);

			break;

		/* Clear data bits in the way registers */
		case FPGA_CLEAR_WAY_BITS:
			/* Perform the operation guarded by the mutex */
			mutex_lock(&fpgaStatus->fpgaIoMutex);
			LiuFpgaCommandReset(fpgaData);
			status = LiuFpgaCommandWay(fpgaStatus, CLEAR_WAY_REG, arg);
			mutex_unlock(&fpgaStatus->fpgaIoMutex);

			break;

		/* Read data from the General Purpose registers */
		case FPGA_READ_GP:
			/* Perform the operation guarded by the mutex */
			mutex_lock(&fpgaStatus->fpgaIoMutex);
			LiuFpgaCommandReset(fpgaData);
			status = LiuFpgaCommandGp(fpgaStatus, READ_GP_REG, arg);
			mutex_unlock(&fpgaStatus->fpgaIoMutex);

			break;

		/* Write data to the General Purpose registers */
		case FPGA_WRITE_GP:
			/* Perform the operation guarded by the mutex */
			mutex_lock(&fpgaStatus->fpgaIoMutex);
			LiuFpgaCommandReset(fpgaData);
			status = LiuFpgaCommandGp(fpgaStatus, WRITE_GP_REG, arg);
			mutex_unlock(&fpgaStatus->fpgaIoMutex);

			break;

		/* Unrecognized command requested */
		default:
			dev_err(&fpgaStatus->spiDevice->dev, "%s - illegal IOCTL specified %08X\n", __func__, cmd);
			return -EINVAL;
	}

	if (status) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - error in FPGA command\n", __func__);
		return -EIO;
	}

	/* Report the results of the operation */
	return 0;
}

/* Opening the device as a file */
static int LiuFpgaOpen(struct inode * inode, struct file * filp)
{
	struct LiuFpgaState *	fpgaStatus;

	/* Validate the operation inode */
	if (inode == NULL) {
		printk(KERN_DEBUG "%s: %s - inode parameter NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Validate the operation file */
	if (filp == NULL) {
		printk(KERN_DEBUG "%s: %s - filp parameter NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	/* Get the FPGA state buffer*/
	fpgaStatus = (struct LiuFpgaState *) container_of(inode->i_cdev, struct LiuFpgaState, fpgaCharCdev);

	/* Validate the FPGA status */
	if (fpgaStatus == NULL) {
		printk(KERN_DEBUG "%s: %s - FPGA state pointer NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	dev_dbg(&fpgaStatus->spiDevice->dev, "%s - enter\n", __func__);

	/* Save the FPGA state data */
	filp->private_data = (void *) fpgaStatus;

	/* Report that the operation completed successfully */
	return 0;
}

/* FPGA character driver operations descriptor */
static struct file_operations liuFpgaOps = {
	.unlocked_ioctl	= LiuFpgaIoctl,
	.open			= LiuFpgaOpen,
};

/* Probe an instance of a device */
static int LiuFpgaProbe(struct spi_device * spi)
{
	struct LiuFpgaState *				fpgaStatus;
	union FpgaCommandGroup				gpCommand;
	u16									gpValue;
	struct FpgaLiuPlatformData *		fpgaData;

	/* Validate the SPI device descriptor */
	if (spi == NULL) {
		printk(KERN_DEBUG "%s: %s - spi parameter NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	dev_dbg(&spi->dev, "%s - enter\n", __func__);

	/* Allocate the FPGA status structure */
	fpgaStatus = kzalloc(sizeof(struct LiuFpgaState), GFP_KERNEL);
	if (fpgaStatus == NULL) {
		dev_err(&spi->dev, "%s - failed to allocate FPGA status buffer\n", __func__);
		return -ENOMEM;
	}

	/* Set the SPI bus device */
	fpgaStatus->spiDevice = spi;

	/* Allocate the major device ID */
	if (alloc_chrdev_region(&fpgaStatus->fpgaCharDevt, 0, 1, FPGA_DEVNAME) < 0) {
		dev_err(&spi->dev, "%s - character device number allocation failed\n", __func__);
		kfree(fpgaStatus);
		return -ENODEV;
	}

	/* Create the driver class */
	fpgaStatus->fpgaCharClass = class_create(THIS_MODULE, FPGA_DEVNAME);
	if (fpgaStatus->fpgaCharClass == NULL) {
		dev_err(&spi->dev, "%s - character driver class creation failed\n", __func__);
		unregister_chrdev_region(fpgaStatus->fpgaCharDevt, 1);
		kfree(fpgaStatus);
		return -ENODEV;
	}

	/* Create the driver device */
	fpgaStatus->fpgaCharDevice = device_create(fpgaStatus->fpgaCharClass, NULL, fpgaStatus->fpgaCharDevt, spi, FPGA_DEVNAME);
	if(IS_ERR(fpgaStatus->fpgaCharDevice)) {
		dev_err(&spi->dev, "%s - character driver device creation failed\n", __func__);
		class_destroy(fpgaStatus->fpgaCharClass);
		unregister_chrdev_region(fpgaStatus->fpgaCharDevt, 1);
		kfree(fpgaStatus);
		return -ENODEV;
	}

	/* Initialize the driver character device */
	cdev_init(&fpgaStatus->fpgaCharCdev, &liuFpgaOps);

	/* Add the driver character device */
	if (cdev_add(&fpgaStatus->fpgaCharCdev, fpgaStatus->fpgaCharDevt, 1) == -1)
	{
		dev_err(&spi->dev, "%s - character driver character device creation failed\n", __func__);
		device_destroy(fpgaStatus->fpgaCharClass, fpgaStatus->fpgaCharDevt);
		class_destroy(fpgaStatus->fpgaCharClass);
		unregister_chrdev_region(fpgaStatus->fpgaCharDevt, 1);
		kfree(fpgaStatus);
		return -ENODEV;
	}

	/* Initialize the driver IO guard */
	mutex_init(&fpgaStatus->fpgaIoMutex);

	/* Allocate the FPGA SPI reset GPIO */
	fpgaData = (struct FpgaLiuPlatformData *) fpgaStatus->spiDevice->dev.platform_data;
	gpio_request(fpgaData->fpgaSpiGpio, "");

	/* Reset the FPGA's SPI state machine before first read */
	LiuFpgaCommandReset(fpgaData);

	/* Initialize hardware here */
	gpCommand.commandGp.fpgaCommand = READ_GP_REG;
	gpCommand.commandGp.fpgaAddress = 0x20;
	gpCommand.commandGp.fpgaValue = 0;
	if(LiuFpgaSendCommand(fpgaStatus, gpCommand.commandValue)) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - FPGA not responding in probe\n", __func__);
		mutex_destroy(&fpgaStatus->fpgaIoMutex);
		cdev_del(&fpgaStatus->fpgaCharCdev);
		device_destroy(fpgaStatus->fpgaCharClass, fpgaStatus->fpgaCharDevt);
		class_destroy(fpgaStatus->fpgaCharClass);
		unregister_chrdev_region(fpgaStatus->fpgaCharDevt, 1);
		kfree(fpgaStatus);
		return -ENODEV;
	}

	if (LiuFpgaReceiveData(fpgaStatus, &gpValue, FPGA_BITS_U16, FPGA_GP_MAX_COUNT) < 0) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - FPGA not responding in probe\n", __func__);
		mutex_destroy(&fpgaStatus->fpgaIoMutex);
		cdev_del(&fpgaStatus->fpgaCharCdev);
		device_destroy(fpgaStatus->fpgaCharClass, fpgaStatus->fpgaCharDevt);
		class_destroy(fpgaStatus->fpgaCharClass);
		unregister_chrdev_region(fpgaStatus->fpgaCharDevt, 1);
		kfree(fpgaStatus);
		return -ENODEV;
	}

	/* Verify FPGA ID */
	if (gpValue != FPGA_IMAGE_ID) {
		dev_err(&fpgaStatus->spiDevice->dev, "%s - FPGA ID incorrect\n", __func__);
		mutex_destroy(&fpgaStatus->fpgaIoMutex);
		cdev_del(&fpgaStatus->fpgaCharCdev);
		device_destroy(fpgaStatus->fpgaCharClass, fpgaStatus->fpgaCharDevt);
		class_destroy(fpgaStatus->fpgaCharClass);
		unregister_chrdev_region(fpgaStatus->fpgaCharDevt, 1);
		kfree(fpgaStatus);
		return -ENODEV;
	}

	/* Check if a valid IRQ was specified */
	if (fpgaStatus->spiDevice->irq) {
		/* Allocate the FPGA IRQ */
		if (request_irq(fpgaStatus->spiDevice->irq, LiuFpgaIrqHandler, 0, FPGA_DEVNAME, (void *) fpgaStatus) < 0) {
			dev_err(&fpgaStatus->spiDevice->dev, "%s - failed to allocate IRQ %d\n", __func__, fpgaStatus->spiDevice->irq);
			mutex_destroy(&fpgaStatus->fpgaIoMutex);
			cdev_del(&fpgaStatus->fpgaCharCdev);
			device_destroy(fpgaStatus->fpgaCharClass, fpgaStatus->fpgaCharDevt);
			class_destroy(fpgaStatus->fpgaCharClass);
			unregister_chrdev_region(fpgaStatus->fpgaCharDevt, 1);
			kfree(fpgaStatus);
			return -EINVAL;
		}
	}

	/* Set the FPGA character driver state data */
	spi_set_drvdata(spi, fpgaStatus);

	dev_err(&fpgaStatus->spiDevice->dev, "%s - Loaded Successfully\n", __func__);

	return 0;
}

/* Remove an instance of a device */
static int LiuFpgaRemove(struct spi_device * spi)
{
	struct LiuFpgaState *			fpgaStatus;
	struct FpgaLiuPlatformData *	fpgaData;

	/* Validate the SPI device descriptor */
	if (spi == NULL) {
		printk(KERN_DEBUG "%s: %s - spi parameter NULL\n", FPGA_DEVNAME, __func__);
		return -EINVAL;
	}

	dev_dbg(&spi->dev, "%s - enter\n", __func__);

	/* Get the FPGA state buffer*/
	fpgaStatus = spi_get_drvdata(spi);

	/* Free the FPGA SPI reset GPIO */
	fpgaData = (struct FpgaLiuPlatformData *) fpgaStatus->spiDevice->dev.platform_data;
	gpio_free(fpgaData->fpgaSpiGpio);

	/* Free the FPGA IRQ */
	if (fpgaStatus->spiDevice->irq) {
		free_irq(fpgaStatus->spiDevice->irq, (void *) fpgaStatus);
	}

	/* Un-initialize hardware here */

	/* Destroy the driver IO guard */
	mutex_destroy(&fpgaStatus->fpgaIoMutex);

	/* Remove the driver character device */
	cdev_del(&fpgaStatus->fpgaCharCdev);

	/* Destroy the driver device */
	device_destroy(fpgaStatus->fpgaCharClass, fpgaStatus->fpgaCharDevt);

	/* Destroy the driver class */
	class_destroy(fpgaStatus->fpgaCharClass);

	/* Free the major device ID */
	unregister_chrdev_region(fpgaStatus->fpgaCharDevt, 1);

	/* Free the FPGA status structure */
	kfree(fpgaStatus);

	return 0;
}

/* SPI protocol driver descriptor */
static struct spi_driver liuFpgaDriver = {
	.driver = {
		.name		= FPGA_DEVNAME,
		.bus		= &spi_bus_type,
		.owner		= THIS_MODULE,
	},
	.probe		= LiuFpgaProbe,
	.remove		= LiuFpgaRemove,
};

/* SPI protocol driver init operations */
static int __init LiuFpgaInit(void)
{
	return spi_register_driver(&liuFpgaDriver);
}

/* SPI protocol driver exit operations */
static void __exit LiuFpgaExit(void)
{
	spi_unregister_driver(&liuFpgaDriver);
}

/* SPI protocol device driver registration */
module_init(LiuFpgaInit);
module_exit(LiuFpgaExit);

/* SPI protocol device driver details */
MODULE_DESCRIPTION("LIU FPGA SPI driver");
MODULE_AUTHOR("Marc Sandusky <msandusky@imsco-us.com>");
MODULE_ALIAS("spi:liufpga");
MODULE_LICENSE("GPL v2");
