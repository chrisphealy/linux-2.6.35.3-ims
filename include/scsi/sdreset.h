/*
 * sdreset.h - SCSI interface definition
 */

#ifndef SDRESET_H_
#define SDRESET_H_

#ifdef CONFIG_SDRESET

#define SDRESET_SCSI_PRIV_SENTINAL 0x1337

typedef enum _HijackState {
	HJ_NOT_HIJACKED,
	HJ_VGEN2_DROP,
} HijackState;

struct priv_shost_data {
	unsigned long sentinal; /* Must be the first struct entry */
	struct scsi_host_template *old_hostt;
	struct device *ldm_cdev;
	struct device *inject_usb_failures;
};

static inline int verify_sentinal(void *data)
{
	struct priv_shost_data *p = data;

	/* There is no data; OK to use the pointer */
	if (!p)
		return 1;

	/* There is data, but it's not our data (bad) */
	if (WARN_ON(p->sentinal != SDRESET_SCSI_PRIV_SENTINAL))
		return 0;

	/* Sentinal is matched, it's our data */
	return 1;
}
#endif

#endif /* SDRESET_H_ */
