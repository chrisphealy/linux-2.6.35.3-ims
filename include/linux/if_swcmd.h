/* Generic Bridge ioctrl defines */

#ifndef _IF_ETHSWCMD_H_
#define _IF_ETHSWCMD_H_

#include <linux/sockios.h>

/*******************************************************************************/
#define SIOCSETHSWCMD       (SIOCDEVPRIVATE + 15) /* Max 16 SIOCDEVPRIVATE */

enum {
	SET_VLAN_ROUTE,
	SET_VLAN_ID,
	SET_PORT_MODE,
	SET_TAGGING,
	SET_ATU_ENTRY,
	SET_TRUNK_ENTRY,
	GET_PORT_CNTS,
	SET_BYPASS_MODE,
	GET_SWITCH_ID,
	GET_PORT_MODE,
	SET_IGMPSNOOPING,

	NUM_ETHSW_CMDS
};
typedef unsigned char cmd_id_t;

typedef unsigned char swport_t;
typedef unsigned char swdev_t;

typedef union PortCounters_t {
	struct {
		unsigned int GoodOctetes_Rec_low;
		unsigned int GoodOctetes_Rec_high;
		unsigned int BadOctets_Rec;
		unsigned int MacTransError;
		unsigned int GoodFramesRec;
		unsigned int BadFramesRec;
		unsigned int BroadcastFramesRec;
		unsigned int MulticastFramesRec;
		unsigned int Frames64Octets;
		unsigned int Frames_65_to_127_Octets;
		unsigned int Frames_128_to_255_Octets;
		unsigned int Frames_256_to_511_Octets;
		unsigned int Frames_512_to_1023_Octets;
		unsigned int Frames_1024_to_Max_Octets;
		unsigned int GoodOctetsSent;
		unsigned int GoodFramesSent;
		unsigned int ExcessiveCollision;
		unsigned int MulticastFramesSent;
		unsigned int BroadcastFramesSent;
		unsigned int UnrecongMACControlRev;
		unsigned int FCsent;
		unsigned int GoodFCRec;
		unsigned int DropEvent;
		unsigned int Undersize;
		unsigned int Fragments;
		unsigned int Oversize;
		unsigned int Jabber;
		unsigned int MacRecError;
		unsigned int BadCRC;
		unsigned int Collisions;
		unsigned int LateCollisions;
	} counters;
	unsigned int raw[32];
} PortCounters_t;

enum {
	VLAN_OP_SET,		/* Setup a VLAN Route*/
	VLAN_OP_CLEAR,		/* Clear a VLAN Route*/
	VLAN_OP_SET_ALL,	/* Unobstruct port*/
	VLAN_OP_CLEAR_ALL,	/* Isolate port*/

	NUM_VLAN_OPS
};
typedef unsigned char   vlan_op_t;

enum {
	PORT_DISABLE,	    /* */
	PORT_BLOCK,
	PORT_LISTEN,
	PORT_LEARNING,
	PORT_FORWARD,	    /* normal port operation*/
	PORT_FORCE_MDI,	    
	PORT_FORCE_MDIX,
	PORT_AUTO_MDI_X,
 
	NUM_PORT_MODES
};
typedef unsigned char port_mode_t;

enum {
	ATU_DELETE,
	ATU_ADD_STATIC,
	ATU_ADD_MANAGEMENT,
	ATU_READ,
	ATU_FLUSH,
	ATU_FLUSH_ALL,

	NUM_ATU_OPS
};
typedef unsigned char atu_op_t;

enum {
	TRUNK_OP_SET,
    TRUNK_OP_CLEAR,
    NUM_TRUNK_OPS
};
typedef unsigned char trunk_op_t;

enum {
	TAGGING_OFF,
	TAGGING_ON
};
typedef unsigned  char tag_opt_t;

enum
{
    SNOOPING_OFF,
    SNOOPING_ON
};
typedef unsigned  char igmpSnooping_opt_t;

enum {
	BYPASS_DISABLED,
	BYPASS_ENABLED
};
typedef unsigned char bypass_opt_t;

enum {
	MARVELL_6123,
	MARVELL_6161,
	MARVELL_6352,
	NUM_SWITCH_TYPES
};


typedef unsigned char switch_type_t;


typedef struct {
	cmd_id_t cmd_id;				/* PHY Command */
	swport_t port_id;				/* source port / port to act on*/
	swdev_t switch_id;				/* source switch / switch to act on*/

	union {
		struct {
			vlan_op_t op;			/* route opcode */
			swport_t dest_port_id;		/* destination port */
			swdev_t dest_switch_id;		/* destination switch*/
		} vlan_route;

		unsigned char vlan_id[2];		/* port VLAN tag ID*/

		port_mode_t port_mode;			/* switch port state */
		tag_opt_t tag_opt;			/* set tagging option */
		igmpSnooping_opt_t igmpSnooping_opt;
		bypass_opt_t byp_op;			/* Set Bypass Relay mode */
		switch_type_t sw_type;			/* Valid after GET_SWITCH_ID command. */

		struct {
			unsigned char MAC[6];		/* MAC to add to ATU table */
			atu_op_t op;				/* type of ATU entry */
			unsigned char priority;		/* priority of ATU entry. range: 0-7*/
			unsigned int port_vector;
		} atu;
		struct {
			trunk_op_t op;					// type of Trunk op
			unsigned int trunk_group_id;	// Trunk Id or Number, set to 0 to clear port from Trunk.
        } trunk_group;
	} cmd;

	PortCounters_t port_counters[24];		// If we get a bigger switch this will have to increase.
} eth_swcmd_t;

/*******************************************************************************/
/*******************************************************************************/

#define SIOCSVLANCMD           (SIOCDEVPRIVATE + 14) /* Max 16 SIOCDEVPRIVATE */

typedef enum {
	/* Port based VLAN commands */
	VLAN_PORT_BASED_SET,		/* Setup a port based VLAN Route
					   Arguments: 	ingress_port
					       		egress_port 
					 */
	VLAN_PORT_BASED_CLEAR,		/* Clear a port based VLAN Route
					   Arguments:	ingress_port
							egress_port
					 */
	VLAN_PORT_BASED_SET_ALL,	/* Unobstruct port  based VLAN Route
					   Arguments:	ingress_port													
					 */
	VLAN_PORT_BASED_CLEAR_ALL,	/* Isolate port  based VLAN Route
					   Arguments:	ingress_port													
					 */
	VLAN_PORT_BASED_DEFAULT_VID,	/* Set default port based VLAN Route
					   Arguments:	vlan_id
							ingress_port
							ingress_mode 
					 */

	/* VLAN routing commands */
	VLAN_VTU_LOAD,			/* Add a VLAN VTU entry
					   Arguments:	vlan_id
					 */
	VLAN_VTU_PURGE,			/* Remove a VLAN VTU entry
					   Arguments:	vlan_id
					 */
	VLAN_VTU_ADD_PORT,		/* Add a port to VLAN VTU entry
					   Arguments:	vlan_id
							egress_port
							egress_mode 
					 */
	VLAN_VTU_DELETE_PORT,		/* Delete a port to VLAN VTU entry
					   Arguments:	vlan_id
							egress_port
					 */
	VLAN_VTU_FLUSH_ALL,		/* Delete ALL VTU entries
					   Arguments:	None 
					 */
	VLAN_VTU_GET_FIRST,     
	VLAN_VTU_GET_NEXT,      
	VLAN_VTU_GET_VIO,       
	VLAN_VTU_CLEAR_VIO,     

	NUM_VLAN_CMDS
} vlan_cmd_t;

typedef enum {
	VLAN_INGRESS_DISABLE,  /* Disable 802.1Q Vlan mode for this port (Port Based Vlan) */
	VLAN_INGRESS_FALLBACK, 
	VLAN_INGRESS_CHECK,
	VLAN_INGRESS_SECURE,

	NUM_VLAN_INGRESS_MODES
} vlan_ingress_mode_t;

typedef enum {
	VLAN_EGRESS_UNMODIFIED,	   /* port is a member and frames egress unmodifed */
	VLAN_EGRESS_UNTAGGED,	   /* port is a member and frames egress untagged */
	VLAN_EGRESS_TAGGED,	   /* port is a member and frames egress tagged */
	VLAN_EGRESS_NOT_MEMBER,	   /* port is NOT a member and frames are dropped on ingress and not allow to egress */

	NUM_VLAN_EGRESS_MODES
} vlan_egress_mode_t;


typedef struct {
	vlan_cmd_t          op;			/* VLAN switch command */
	int                 vlan_id;
	swport_t            ingress_port_id;
	vlan_ingress_mode_t ingress_mode;
	swport_t            egress_port_id;
	vlan_egress_mode_t  egress_mode;
} sw_vlan_ioctl_args_t;

/*******************************************************************************/
#endif
