/*
 * liufpga.h - LIU board FPGA driver header
 */
#ifndef _FPGA_H_
#define _FPGA_H_

#include <linux/ioctl.h>
#include <linux/types.h>


/*---------------------------------------------------------------------------*/
/* Common definitions                                                        */
/*---------------------------------------------------------------------------*/

/* FPGA device name */
#define FPGA_DEVNAME				"liufpga"	/* Name for device node */

/* FPGA device commands */
#define READ_STATUS_REGS			0x01		/* Read status registers */
#define CANCEL_STATUS_READ			0x0F		/* Cancel status command */
#define READ_WAY_REG				0x02		/* Read way register */
#define WRITE_WAY_REG				0x03		/* Write way register */
#define SET_WAY_REG					0x04		/* Set bits way register*/
#define CLEAR_WAY_REG				0x05		/* Clear bits way register */
#define READ_GP_REG					0x06		/* Read GP register */
#define WRITE_GP_REG				0x07		/* Write GP register */

/* FPGA status parameter limits */
#define FPGA_STATUS_MAX_ADDRESS		121			/* Maximum address entries */
#define FPGA_STATUS_MAX_COUNT		121			/* Maximum transfer count */

/* FPGA way parameter limits */
#define FPGA_WAY_MAX_LAC			2			/* Maximum LAC entries */
#define FPGA_WAY_MAX_WAY			4			/* Maximum WAY entries */
#define FPGA_WAY_MAX_SEU_ID			31			/* Maximum SEU entries */
#define FPGA_WAY_MAX_WORD_ID		16			/* Maximum Word entries */
#define FPGA_WAY_MAX_WORD_VALUE		65536		/* Maximum register value */
#define FPGA_WAY_MAX_COUNT			1			/* Maximum transfer count */

/* FPGA general purpose parameter limits */
#define FPGA_GP_MAX_ADDRESS			59			/* Maximum address entries */
#define FPGA_GP_MAX_WORD_VALUE		65536		/* Maximum register value */
#define FPGA_GP_MAX_COUNT			1			/* Maximum transfer count */

/* FPGA IOCTL definitions */
#define IOCTL_MAGIC_ID				0			/* IOCTL non-specific type */

/* FPGA read status device IOCTL */
#define FPGA_READ_STATUS			_IOWR(IOCTL_MAGIC_ID, READ_STATUS_REGS,\
										struct FpgaIoctlDataStatus)

/* FPGA cancel current status read device IOCTL */
#define FPGA_CANCEL_STATUS			_IO(IOCTL_MAGIC_ID, CANCEL_STATUS_READ)

/* FPGA read way device IOCTL */
#define FPGA_READ_WAY				_IOWR(IOCTL_MAGIC_ID, READ_WAY_REG,\
										struct FpgaIoctlDataWay)

/* FPGA write way device IOCTL */
#define FPGA_WRITE_WAY				_IOW(IOCTL_MAGIC_ID, WRITE_WAY_REG,\
										struct FpgaIoctlDataWay)

/* FPGA set way bits device IOCTL */
#define FPGA_SET_WAY_BITS			_IOW(IOCTL_MAGIC_ID, SET_WAY_REG,\
										struct FpgaIoctlDataWay)

/* FPGA clear way bits device IOCTL */
#define FPGA_CLEAR_WAY_BITS			_IOW(IOCTL_MAGIC_ID, CLEAR_WAY_REG,\
										struct FpgaIoctlDataWay)

/* FPGA read general purpose device IOCTL */
#define FPGA_READ_GP				_IOWR(IOCTL_MAGIC_ID, READ_GP_REG,\
										struct FpgaIoctlDataGeneralPurpose)

/* FPGA write general purpose device IOCTL */
#define FPGA_WRITE_GP				_IOW(IOCTL_MAGIC_ID, WRITE_GP_REG,\
										struct FpgaIoctlDataGeneralPurpose)

/*---------------------------------------------------------------------------*/
/* Common structures                                                         */
/*---------------------------------------------------------------------------*/

/* FPGA module platform data */
struct FpgaLiuPlatformData {
	u8		fpgaSpiGpio;						/* FPGA resister index */
} __attribute__ ((packed));

/* FPGA Status IOCTL data format */
struct FpgaIoctlDataStatus {
	u8		fpgaAddress;						/* FPGA resister index */
	u8		fpgaCount;							/* FPGA resister count */
	u32		fpgaValue[FPGA_STATUS_MAX_COUNT];	/* FPGA register value */
} __attribute__ ((packed));

/* FPGA Way IOCTL data format */
struct FpgaIoctlDataWay {
	u8		fpgaLac;							/* FPGA LAC index */
	u8		fpgaWay;							/* FPGA Way index */
	u8		fpgaSeuId;							/* FPGA SEU ID index*/
	u8		fpgaWordId;							/* FPGA Word index */
	u16		fpgaValue[FPGA_WAY_MAX_COUNT];		/* FPGA register value */
} __attribute__ ((packed));

/* FPGA General Purpose IOCTL data format */
struct FpgaIoctlDataGeneralPurpose {
	u8		fpgaAddress;						/* FPGA resister index */
	u16		fpgaValue[FPGA_GP_MAX_COUNT];		/* FPGA register value */
} __attribute__ ((packed));

#endif
