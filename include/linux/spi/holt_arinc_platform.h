#ifndef __HOLT_ARINC_PLATFORM_H
#define __HOLT_ARINC_PLATFORM_H

#include <linux/platform_device.h>

// the cspi_info platform_data struct is used to defined platform specific
// data to be passed to the driver. for the each device, we can differentiate
// the interpretation of this data depending on modalias.
//
// the 3587 and 3597 only use a single interrupt, so this field is used to
// describe the interrupt pin used for the particular part.
//
// the 3593 uses up to 12 interrupts. for this reason, we need a data structure
// to pass this information (immiediately below).
// some implementations may want to OR
// - some or all of these interrupts into a single combined interrupt
// -logical sets of interrupts in to combined single interrupts.
// for this reason, several optional interrupts assignments are defined in
// this structure. if they are defined, an appropriate ISR will be assigned
enum {
	// transmit interrupts
	HI_INTRIO_TXEMPTY = 0,
	HI_INTRIO_TXFULL,
};
enum {
	// receive interrupts
	HI_INTRIO_RxINT,	// R1INT pin
	HI_INTRIO_RxFLAG,	// R1FLAG pin
	HI_INTRIO_MBx_ALL,	// combined mailbox 1 interrupts
	HI_INTRIO_MBx_1,
	HI_INTRIO_MBx_2,
	HI_INTRIO_MBx_3,

	HI_INTRIO_MAX
};

typedef int intrio_assignments_t[HI_INTRIO_MAX];
//typedef struct intrio_assignments_t { int x[3]; } intrio_assignments_t;


struct arinc_spi_platform_info {
	struct platform_device *cspi_info;
	struct spi_board_info *chip_info;
};

#endif
