#ifndef __ARINC_H
#define __ARINC_H

enum {
	ARINC_S_MRESET = 0,
	ARINC_G_STATUS,

	ARINC_G_CTRL,
	ARINC_S_CTRL,

	ARINC_G_LABEL,
	ARINC_S_LABEL,

	ARINC_G_TX_HISPEED,
	ARINC_S_TX_HISPEED,

	ARINC_G_TX_LOCKOUT_US,
	ARINC_S_TX_LOCKOUT_US,

	ARINC_NUMBER_IOCTL_CMDS
};

struct arinc_label_field {
	unsigned char label[16];
};

#define ARINCIOC_S_MRESET	_IO('A', ARINC_S_MRESET )
#define ARINCIOC_G_CTRL		_IOR('A', ARINC_G_CTRL, short)	// pointer to u16
#define ARINCIOC_S_CTRL		_IOW('A', ARINC_S_CTRL, short)	// pointer to u16
#define ARINCIOC_G_LABEL	_IOR('A', ARINC_G_LABEL, unsigned char[8])	// pointer to struct arinc_label_field
#define ARINCIOC_S_LABEL	_IOW('A', ARINC_S_LABEL, unsigned char[8])	// pointer to struct arinc_label_field
#define ARINCIOC_G_TX_HISPEED	_IOR('A', ARINC_G_TX_HISPEED, int)
#define ARINCIOC_S_TX_HISPEED	_IOW('A', ARINC_S_TX_HISPEED, int)
#define ARINCIOC_G_TX_LOCKOUT_US _IOR('A', ARINC_G_TX_LOCKOUT_US, int)
#define ARINCIOC_S_TX_LOCKOUT_US _IOW('A', ARINC_S_TX_LOCKOUT_US, int)

#define ARINC_RX_FIFO_ORDER	10
#define ARINC_TX_FIFO_ORDER	10
#define ARINC_RX_FIFO_SIZE	(1<<(ARINC_RX_FIFO_ORDER))
#define ARINC_TX_FIFO_SIZE	(1<<(ARINC_TX_FIFO_ORDER))

#endif
