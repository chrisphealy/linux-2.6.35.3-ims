/*
 * net/dsa/mv88e6123_61_65.c - Marvell 88e6123/6161/6165 switch chip support
 * Copyright (c) 2008-2009 Marvell Semiconductor
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/if_bridge.h>
#include <linux/list.h>
#include <linux/netdevice.h>
#include <linux/phy.h>
#include <linux/platform_device.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/timer.h>
#include "dsa_priv.h"
#include "mv88e6xxx.h"

/*
 * Minimum delay of 10ms, adjusted  for non RT system timing
 */
#define RESET_DELAY		15

static void mv88e6123_61_65_hw_bridge_sync(struct dsa_switch *ds, int force);
static int mv88e6123_61_65_phy_read(struct dsa_switch *ds, int port,
				    int regnum);
static int mv88e6123_61_65_phy_write(struct dsa_switch *ds,
				     int port, int regnum, u16 val);

int mv88e6123_61_65_phy_page_read(struct dsa_switch *ds, int port,
				  int page, int regnum);
static int mv88e6123_61_65_phy_page_write(struct dsa_switch *ds, int port,
					  int page, int regnum, u16 val);

void map_forwarding_timeout_callback(struct dsa_switch *ds)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int ret;
	int i;
	printk(KERN_INFO "dsa: map_forwarding_timeout_callback called.\n");
	printk(KERN_INFO "dsa: ps->map_forwarding = 0\n");
	for (i = 0; i < MV88E6XXX_MAX_PORTS; i++) {
		ps->map_forwarding[i] = 0;
	}
	ret = del_timer( &ps->map_forwarding_timeout );
	if (ret)
		printk("dsa: The timer is still in use...\n");

	printk("dsa: Timer module uninstalling\n");

	return;
}


static char *mv88e6123_61_65_probe(struct mii_bus *bus, int sw_addr)
{
	int ret;

	ret = __mv88e6xxx_reg_read(bus, sw_addr, REG_PORT(0), 0x03);
	if (ret >= 0) {
		if (ret == 0x1212)
			return "Marvell 88E6123 (A1)";
		if (ret == 0x1213)
			return "Marvell 88E6123 (A2)";
		if ((ret & 0xfff0) == 0x1210)
			return "Marvell 88E6123";

		if (ret == 0x1612)
			return "Marvell 88E6161 (A1)";
		if (ret == 0x1613)
			return "Marvell 88E6161 (A2)";
		if ((ret & 0xfff0) == 0x1610)
			return "Marvell 88E6161";

		if (ret == 0x1652)
			return "Marvell 88E6165 (A1)";
		if ((ret & 0xfff0) == 0x1650)
			return "Marvell 88E6165";

		if (ret == 0x3522)
			return "Marvell 88E6352 (A1)";
		if ((ret & 0xfff0) == 0x3520)
			return "Marvell 88E6352";
	}

	return NULL;
}

static int mv88e6123_61_65_switch_reset(struct dsa_switch *ds)
{
	int i;
	int ret;

	/*
	 * Set all ports to the disabled state.
	 */
	for (i = 0; i < 8; i++) {
		ret = REG_READ(REG_PORT(i), 0x04);
		REG_WRITE(REG_PORT(i), 0x04, ret & 0xfffc);
	}

	/*
	 * Wait for transmit queues to drain.
	 */
	usleep_range(2000, 2500);

	/*
	 * Reset the switch.
	 */
	REG_WRITE(REG_GLOBAL, 0x04, 0xc400);

	/*
	 * Wait up to one second for reset to complete.
	 */
	for (i = 0; i < 1000; i++) {
		ret = REG_READ(REG_GLOBAL, 0x00);
		if ((ret & 0xc800) == 0xc800)
			break;

		usleep_range(1000, 1500);
	}
	if (i == 1000)
		return -ETIMEDOUT;

	return 0;
}

static int mv88e6123_61_65_setup_global(struct dsa_switch *ds)
{
	int ret;
	int i;

#ifndef CONFIG_NET_DSA_UNMANAGED
	/*
	 * Set the default address aging time to 5 minutes, and
	 * enable address learn messages to be sent to all message
	 * ports.
	 */
	REG_WRITE(REG_GLOBAL, 0x0a, 0x0148);

	/*
	 * Configure the priority mapping registers.
	 */
	ret = mv88e6xxx_config_prio(ds);
	if (ret < 0)
		return ret;

	/*
	 * Configure the upstream port, and configure the upstream
	 * port as the port to which ingress and egress monitor frames
	 * are to be sent.
	 */
	REG_WRITE(REG_GLOBAL, 0x1a, (dsa_upstream_port(ds) * 0x1110));

	/*
	 * Disable remote management for now, and set the switch's
	 * DSA device number.
	 */
	REG_WRITE(REG_GLOBAL, 0x1c, ds->index & 0x1f);

	/*
	 * Send all frames with destination addresses matching
	 * 01:80:c2:00:00:2x to the CPU port.
	 */
	REG_WRITE(REG_GLOBAL2, 0x02, 0xffff);

	/*
	 * Send all frames with destination addresses matching
	 * 01:80:c2:00:00:0x to the CPU port.
	 */
	REG_WRITE(REG_GLOBAL2, 0x03, 0xffff);

	/*
	 * Disable the loopback filter, disable flow control
	 * messages, disable flood broadcast override, disable
	 * removing of provider tags, disable ATU age violation
	 * interrupts, disable tag flow control, force flow
	 * control priority to the highest, and send all special
	 * multicast frames to the CPU at the highest priority.
	 */
	REG_WRITE(REG_GLOBAL2, 0x05, 0x00ff);

	/*
	 * Program the DSA routing table.
	 */
	for (i = 0; i < 32; i++) {
		int nexthop;

		nexthop = 0x1f;
		if (i != ds->index && i < ds->dst->pd->nr_chips)
			nexthop = ds->pd->rtable[i] & 0x1f;

		REG_WRITE(REG_GLOBAL2, 0x06, 0x8000 | (i << 8) | nexthop);
	}

	/*
	 * Clear all trunk masks.
	 */
	for (i = 0; i < 8; i++)
		REG_WRITE(REG_GLOBAL2, 0x07, 0x8000 | (i << 12) | 0xff);

	/*
	 * Clear all trunk mappings.
	 */
	for (i = 0; i < 16; i++)
		REG_WRITE(REG_GLOBAL2, 0x08, 0x8000 | (i << 11));

	/*
	 * Disable ingress rate limiting by resetting all ingress
	 * rate limit registers to their initial state.
	 */
	for (i = 0; i < 6; i++)
		REG_WRITE(REG_GLOBAL2, 0x09, 0x9000 | (i << 8));

	/*
	 * Initialise cross-chip port VLAN table to reset defaults.
	 */
	REG_WRITE(REG_GLOBAL2, 0x0b, 0x9000);

	/*
	 * Clear the priority override table.
	 */
	for (i = 0; i < 16; i++)
		REG_WRITE(REG_GLOBAL2, 0x0f, 0x8000 | (i << 8));

	/* @@@ initialise AVB (22/23) watchdog (27) sdet (29) registers */
#endif

	return 0;
}

static int mv88e6123_61_65_setup_port(struct dsa_switch *ds, int p)
{
	int addr = REG_PORT(p);
	u16 val;

#ifndef CONFIG_NET_DSA_UNMANAGED
	/*
	 * MAC Forcing register: don't force link, speed, duplex
	 * or flow control state to any particular values on physical
	 * ports, but force the CPU port and all DSA ports to 1000 Mb/s
	 * full duplex.
	 */
	//if ((dsa->cpu_port_mask | ds->dsa_port_mask) & (1 << p))
	//	REG_WRITE(addr, 0x01, 0x003e);
	//else
#ifdef CONFIG_BOARD_IMS_NIU
	if (!dsa_is_cpu_port(ds, p))
#endif
		REG_WRITE(addr, 0x01, 0x0003);

	/*
	 * Do not limit the period of time that this port can be
	 * paused for by the remote end or the period of time that
	 * this port can pause the remote end.
	 */
	REG_WRITE(addr, 0x02, 0x0000);

	/*
	 * Port Control: disable Drop-on-Unlock, disable Drop-on-Lock,
	 * disable Header mode, enable IGMP/MLD snooping, disable VLAN
	 * tunneling, determine priority by looking at 802.1p and IP
	 * priority fields (IP prio has precedence), and set STP state
	 * to Forwarding.
	 *
	 * If this is the CPU link, use DSA or EDSA tagging depending
	 * on which tagging mode was configured.
	 *
	 * If this is a link to another switch, use DSA tagging mode.
	 *
	 * If this is the upstream port for this switch, enable
	 * forwarding of unknown unicasts and multicasts.
	 */
	val = 0x0433;
	if (dsa_is_cpu_port(ds, p)) {
		if (ds->dst->tag_protocol == htons(ETH_P_EDSA))
			val |= 0x3300;
		else
			val |= 0x0100;
	}
	if (ds->dsa_port_mask & (1 << p))
		val |= 0x0100;
	if (p == dsa_upstream_port(ds))
		val |= 0x000c;
	REG_WRITE(addr, 0x04, val);

	/*
	 * Port Control 1: disable trunking.
	 */
	REG_WRITE(addr, 0x05, 0x0000);

	/*
	 * Port based VLAN map: give each port its own address
	 * database, allow the CPU port to talk to each of the 'real'
	 * ports, and allow each of the 'real' ports to only talk to
	 * the upstream port.
	 */
	val = (p & 0xf) << 12;
	if (dsa_is_cpu_port(ds, p))
		val |= ds->dsa_port_mask | ds->ext_port_mask;
	else
		val |= 1 << dsa_upstream_port(ds);
	REG_WRITE(addr, 0x06, val);

	/*
	 * Default VLAN ID and priority: don't set a default VLAN
	 * ID, and set the default packet priority to zero.
	 */
	REG_WRITE(addr, 0x07, 0x0000);

	/*
	 * Port Control 2: don't force a good FCS, set the maximum
	 * frame size to 10240 bytes, don't let the switch add or
	 * strip 802.1q tags, don't discard tagged or untagged frames
	 * on this port, do a destination address lookup on all
	 * received packets as usual, disable ARP mirroring and don't
	 * send a copy of all transmitted/received frames on this port
	 * to the CPU.
	 */
	REG_WRITE(addr, 0x08, 0x2080);

	/*
	 * Egress rate control: disable egress rate control.
	 */
	REG_WRITE(addr, 0x09, 0x0001);

	/*
	 * Egress rate control 2: disable egress rate control.
	 */
	REG_WRITE(addr, 0x0a, 0x0000);

	/*
	 * Port Association Vector: when learning source addresses
	 * of packets, add the address to the address database using
	 * a port bitmap that has only the bit for this port set and
	 * the other bits clear.
	 */
	REG_WRITE(addr, 0x0b, 1 << p);

	/*
	 * Port ATU control: disable limiting the number of address
	 * database entries that this port is allowed to use.
	 */
	REG_WRITE(addr, 0x0c, 0x0000);

	/*
	 * Priorit Override: disable DA, SA and VTU priority override.
	 */
	REG_WRITE(addr, 0x0d, 0x0000);

	/*
	 * Port Ethertype: use the Ethertype DSA Ethertype value.
	 */
	REG_WRITE(addr, 0x0f, ETH_P_EDSA);

	/*
	 * Tag Remap: use an identity 802.1p prio -> switch prio
	 * mapping.
	 */
	REG_WRITE(addr, 0x18, 0x3210);

	/*
	 * Tag Remap 2: use an identity 802.1p prio -> switch prio
	 * mapping.
	 */
	REG_WRITE(addr, 0x19, 0x7654);
#endif

	return 0;
}

/*
 * __atu_wait() - Wait for the ATU to complete it's operation
 * @dsa_switch:		switch structure
 *
 * The function __atu_wait waits for the ATU to complete it's
 * operation before returning or until a timeout of up to 50 ms
 * is reached.  ATU operations cannot be executed while the ATU
 * is already in the middle of an ATU operation.
 */
static int __atu_wait(struct dsa_switch *ds)
{
	int i;

	for (i = 0; i < 100; i++) {
		int ret;

		ret = REG_READ(REG_GLOBAL, 0x0b);
		if ((ret & 0x8000) == 0)
			return 0;

		usleep_range(300, 500);
	}

	return -ETIMEDOUT;
}

static int __atu_add_bpdu_mac_rdu(struct dsa_switch *ds, int fid)
{
	u8 *addr = ds->dst->master_netdev->dev_addr;

	REG_WRITE(REG_GLOBAL, 0x01, fid);
	REG_WRITE(REG_GLOBAL, 0x0c, 0x018e);	/* Set ATU Data Register */
	REG_WRITE(REG_GLOBAL, 0x0d, 0x0180);	/* First two octets of BPDU MAC address */
	REG_WRITE(REG_GLOBAL, 0x0e, 0xc200);	/* Second two octets of BPDU MAC address */
	REG_WRITE(REG_GLOBAL, 0x0f, 0x0000);	/* Third two octets of BPDU MAC address */
	REG_WRITE(REG_GLOBAL, 0x0b, 0xb000);	/* Load the BPDU mac address to the ATU */

	return __atu_wait(ds);
}

static int __atu_add_bpdu_mac_niu(struct dsa_switch *ds, int fid)
{
	u8 *addr = ds->dst->master_netdev->dev_addr;

	REG_WRITE(REG_GLOBAL, 0x01, fid);
	REG_WRITE(REG_GLOBAL, 0x0c, 0x003e);	/* Set ATU Data Register */
	REG_WRITE(REG_GLOBAL, 0x0d, 0x0180);	/* First two octets of BPDU MAC address */
	REG_WRITE(REG_GLOBAL, 0x0e, 0xc200);	/* Second two octets of BPDU MAC address */
	REG_WRITE(REG_GLOBAL, 0x0f, 0x0000);	/* Third two octets of BPDU MAC address */
	REG_WRITE(REG_GLOBAL, 0x0b, 0xb000);	/* Load the BPDU mac address to the ATU */

	return __atu_wait(ds);
}

static int __atu_add_local_mac(struct dsa_switch *ds, int fid)
{
	u8 *addr = ds->dst->master_netdev->dev_addr;

	REG_WRITE(REG_GLOBAL, 0x01, fid);
	REG_WRITE(REG_GLOBAL, 0x0c, (0x10 << dsa_upstream_port(ds)) | 0x8);
	REG_WRITE(REG_GLOBAL, 0x0d, (addr[0] << 8) | addr[1]);
	REG_WRITE(REG_GLOBAL, 0x0e, (addr[2] << 8) | addr[3]);
	REG_WRITE(REG_GLOBAL, 0x0f, (addr[4] << 8) | addr[5]);

	REG_WRITE(REG_GLOBAL, 0x0b, 0xb000);

	return __atu_wait(ds);
}

static int atu_add_local_mac(struct dsa_switch *ds)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int ret, i;

	mutex_lock(&ps->atu_mutex);
	/*
	 * Flush all ATU entries before setting the upstream port MAC.  This
	 * clears out any statically entered addresses that would cause problems
	 * in the event of an RDU swap.
	 */
	REG_WRITE(REG_GLOBAL, 0x0c, 0x10);
	REG_WRITE(REG_GLOBAL, 0x0b, 0x9000);

	ret = __atu_wait(ds);
	if (ret < 0) {
		mutex_unlock(&ps->atu_mutex);
		return ret;
	}

	/*
	 * Add the local MAC address to the ATU in the first 10 FIDs to
	 * cover all possible port/FID configurations
	 */
	/*
	 * Add a static entry to the ATU database with the MAC address of
	 * an STP BPDU for netleft and netright to support passing BPDUs
	 * through the switch when the port state is not in the forwarding
	 * state.
	 */
	for (i = 0; i < 10; i++) {
		ret = __atu_add_local_mac(ds, i);
		if (ret < 0) {
			mutex_unlock(&ps->atu_mutex);
			return ret;
		}
#if defined(CONFIG_BOARD_IMS_RDU)
		ret = __atu_add_bpdu_mac_rdu(ds, i);
		if (ret < 0) {
			mutex_unlock(&ps->atu_mutex);
			return ret;
		}
#endif
#if defined(CONFIG_BOARD_IMS_NIU)
		ret = __atu_add_bpdu_mac_niu(ds, i);
		if (ret < 0) {
			mutex_unlock(&ps->atu_mutex);
			return ret;
		}
#endif
	}

	mutex_unlock(&ps->atu_mutex);

	return ret;
}

static ssize_t atu_age_time_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = REG_READ(REG_GLOBAL, 0x0a);

	return sprintf(buf, "%d\n", 15*((ret >> 4) & 0xff));
}

static ssize_t atu_age_time_set(struct device *dev, struct device_attribute *attr,
			const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int ret;

	ret = sscanf(buf, "%d", &val);
	if (ret < 1)
		return -EINVAL;

	if (val < 15)
		val = 15;

	if (val > 600)
		val = 600;

	ret = REG_READ(REG_GLOBAL, 0x0a);
	REG_WRITE(REG_GLOBAL, 0x0a, (((val/15) & 0xff) * 0x10) | (ret & 0xf00f));

	printk(KERN_INFO "dsa: setting the ATU age time via sysfs entry to %d\n", val);

	return count;
}

static DEVICE_ATTR(atu_age_time, (S_IRUGO | S_IWUGO), atu_age_time_show, atu_age_time_set);

static ssize_t atu_reload(struct device *dev, struct device_attribute *attr,
			const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int i;
	int ret;

	ret = atu_add_local_mac(ds);
	
	return (ret < 0) ? ret : count;
}

static DEVICE_ATTR(atu_reload, S_IWUGO, NULL, atu_reload);

static int __atu_flush(struct dsa_switch *ds)
{
	REG_WRITE(REG_GLOBAL, 0x0b, 0xa000);

	return __atu_wait(ds);
}

static ssize_t atu_flush(struct device *dev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int ret;

	mutex_lock(&ps->atu_mutex);
	ret = __atu_flush(ds);
	mutex_unlock(&ps->atu_mutex);

	printk(KERN_INFO "dsa: flushing the ATU via sysfs entry\n");

	return (ret < 0) ? ret : count;
}

static DEVICE_ATTR(atu_flush, S_IWUGO, NULL, atu_flush);

static int __atu_dump_entry(struct dsa_switch *ds, int fid, int atu_op)
{
	int prio;
	int ret;
	int trunk;
	int portvec;
	int state;
	u8 addr[6];

	prio = (atu_op >> 8) & 0x07;

	ret = REG_READ(REG_GLOBAL, 0xc);
	trunk = (ret >> 15) & 0x01;
	portvec = (ret >> 4) & 0xff;
	state = ret & 0x0f;

	ret = REG_READ(REG_GLOBAL, 0xd);
	addr[0] = (ret >> 8) & 0xff;
	addr[1] = ret & 0xff;

	ret = REG_READ(REG_GLOBAL, 0xe);
	addr[2] = (ret >> 8) & 0xff;
	addr[3] = ret & 0xff;

	ret = REG_READ(REG_GLOBAL, 0xf);
	addr[4] = (ret >> 8) & 0xff;
	addr[5] = ret & 0xff;

	if (state == 0 && !memcmp(addr, "\xff\xff\xff\xff\xff\xff", 6))
		return 0;

	printk(KERN_INFO "  ESA: %pM, FID:0x%x, PRI:%x, ES:%x",
	      addr, fid, prio, state);

	if (!trunk) {
		int first;
		int i;

		printk(", Port MAP:[");

		first = 1;
		for (i = 0; i < 8; i++) {
			if (portvec & 1) {
				if (!first)
					printk(" | ");
				first = 0;

				printk("%d", i);
			}

			portvec >>= 1;
		}

		printk("]\n");
	} else {
		printk(", Trunk:%d\n", portvec);
	}

	return 1;
}

static int __atu_dump(struct dsa_switch *ds)
{
	int fid;

	printk(KERN_INFO "\n");
	printk(KERN_INFO "Marvell 10/100/1000 Switch: ATU Table\n");

	REG_WRITE(REG_GLOBAL, 0x0d, 0xffff);
	REG_WRITE(REG_GLOBAL, 0x0e, 0xffff);
	REG_WRITE(REG_GLOBAL, 0x0f, 0xffff);

	for (fid = 0; fid < 10; fid++) {
		int ret;

		REG_WRITE(REG_GLOBAL, 0x01, fid);

		do {
			REG_WRITE(REG_GLOBAL, 0x0b, 0xc000);

			ret = __atu_wait(ds);
			if (ret < 0)
				return ret;

			ret = __atu_dump_entry(ds, fid, ret);
			if (ret < 0)
				return ret;
		} while (ret);
	}

	printk(KERN_INFO "\n");

	return 0;
}

static ssize_t atu_dump(struct device *dev, struct device_attribute *attr,
			const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int ret;

	mutex_lock(&ps->atu_mutex);
	ret = __atu_dump(ds);
	mutex_unlock(&ps->atu_mutex);

	return (ret < 0) ? ret : count;
}

static DEVICE_ATTR(atu_dump, S_IWUGO, NULL, atu_dump);

static ssize_t bpdu_bypass_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = REG_READ(REG_GLOBAL2, 0x03);

	return sprintf(buf, "%d\n", 1 - (ret & 1));
}

static ssize_t bpdu_bypass_set(struct device *dev,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	ret = REG_READ(REG_GLOBAL2, 0x03);

	if (val == 1)
		REG_WRITE(REG_GLOBAL2, 0x03, ret & ~0x0001 );
	else if (val == 0)
		REG_WRITE(REG_GLOBAL2, 0x03, ret | 0x0001 );
	else
		return -EINVAL;

	return count;
}

static DEVICE_ATTR(bpdu_bypass, (S_IRUGO | S_IWUGO), bpdu_bypass_show, bpdu_bypass_set);

static ssize_t switch_setup_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = REG_READ(REG_GLOBAL2, 0x0b);

	return sprintf(buf, "%d\n", (ret & 0x0001));
}

static ssize_t switch_setup_set(struct device *dev,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	ret = REG_READ(REG_GLOBAL2, 0x0b);

	if (val == 0)
		REG_WRITE(REG_GLOBAL2, 0x0b, ret & ~0x0001 );
	else if (val == 1)
		REG_WRITE(REG_GLOBAL2, 0x0b, ret | 0x0001 );
	else
		return -EINVAL;

	return count;
}

static DEVICE_ATTR(switch_setup, (S_IRUGO | S_IWUGO), switch_setup_show, switch_setup_set);

static int avb_port;

static ssize_t avb_port_show(struct device *dev,
			     struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", avb_port);
}

static ssize_t avb_port_set(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	avb_port = val;

	return count;
}

static DEVICE_ATTR(avb_port, (S_IRUGO | S_IWUGO), avb_port_show, avb_port_set);


static int avb_block;

static ssize_t avb_block_show(struct device *dev,
			      struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", avb_block);
}

static ssize_t avb_block_set(struct device *dev,
			     struct device_attribute *attr,
			     const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	avb_block = val;

	return count;
}

static DEVICE_ATTR(avb_block, (S_IRUGO | S_IWUGO), avb_block_show, avb_block_set);


static int avb_addr;

static ssize_t avb_addr_show(struct device *dev,
			     struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", avb_addr);
}

static ssize_t avb_addr_set(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	avb_addr = val;

	return count;
}

static DEVICE_ATTR(avb_addr, (S_IRUGO | S_IWUGO), avb_addr_show, avb_addr_set);


static int avb_wait(struct dsa_switch *ds)
{
	int i;

	for (i = 0; i < 1000; i++) {
		int ret;

		ret = REG_READ(REG_GLOBAL2, 0x16);
		if ((ret & 0x8000) == 0x0000)
			return 0;

		usleep_range(1000, 2000);
	}

	return -ETIMEDOUT;
}

static ssize_t avb_data_show(struct device *dev,
			     struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int ret;

	mutex_lock(&ps->avb_mutex);

	ret = mv88e6xxx_reg_write(ds, REG_GLOBAL2, 0x16,
				  0xc000 | (avb_port << 8) |
				  (avb_block << 5) | avb_addr);
	if (ret < 0) {
		mutex_unlock(&ps->avb_mutex);
		return ret;
	}

	ret = avb_wait(ds);
	if (ret < 0) {
		mutex_unlock(&ps->avb_mutex);
		return ret;
	}

	ret = mv88e6xxx_reg_read(ds, REG_GLOBAL2, 0x17);
	if (ret < 0) {
		mutex_unlock(&ps->avb_mutex);
		return ret;
	}

	mutex_unlock(&ps->avb_mutex);

	return sprintf(buf, "%x\n", ret);
}

static ssize_t avb_data_set(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	mutex_lock(&ps->avb_mutex);

	ret = mv88e6xxx_reg_write(ds, REG_GLOBAL2, 0x17, val);
	if (ret < 0) {
		mutex_unlock(&ps->avb_mutex);
		return ret;
	}

	ret = mv88e6xxx_reg_write(ds, REG_GLOBAL2, 0x16,
				  0xb000 | (avb_port << 8) |
				  (avb_block << 5) | avb_addr);
	if (ret < 0) {
		mutex_unlock(&ps->avb_mutex);
		return ret;
	}

	ret = avb_wait(ds);
	if (ret < 0) {
		mutex_unlock(&ps->avb_mutex);
		return ret;
	}

	mutex_unlock(&ps->avb_mutex);

	return count;
}

static DEVICE_ATTR(avb_data, (S_IRUGO | S_IWUGO), avb_data_show, avb_data_set);


static int cable_length_read(struct dsa_switch *ds, int port, int pair)
{
	int ret;
	int i;

	ret = mv88e6123_61_65_phy_page_write(ds, port, 0x00ff, 0x10,
					     0x1118 | (pair & 3));
	if (ret < 0)
		return ret;

	for (i = 0; i < 100; i++) {
		ret = mv88e6123_61_65_phy_page_read(ds, port, 0x00ff, 0x10);
		if (ret < 0)
			return ret;

		if ((ret & 0x8000) == 0x8000)
			break;

		usleep_range(100, 200);
	}
	if (i == 100)
		return -ETIMEDOUT;

	return mv88e6123_61_65_phy_page_read(ds, port, 0x00ff, 0x12);
}

static ssize_t cable_length_show(struct device *dev,
				 struct device_attribute *attr,
				 int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int i;

	val = mv88e6xxx_reg_read(ds, REG_PORT(port), 0x00);
	if (val < 0)
		return val;

	/*
	 * Return zero if the link is not up at 1000 Mb/s.
	 */
	if ((val & 0x0b00) != 0x0a00)
		return sprintf(buf, "0\n");

	val = 0;
	for (i = 0; i < 10; i++) {
		val += cable_length_read(ds, port, 0);
		val += cable_length_read(ds, port, 1);
		val += cable_length_read(ds, port, 2);
		val += cable_length_read(ds, port, 3);
	}

	return sprintf(buf, "%d\n", val / 40);
}

static ssize_t cable_length_0_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	return cable_length_show(dev, attr, 0, buf);
}

static DEVICE_ATTR(cable_length_0, S_IRUGO, cable_length_0_show, NULL);

static ssize_t cable_length_1_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	return cable_length_show(dev, attr, 1, buf);
}

static DEVICE_ATTR(cable_length_1, S_IRUGO, cable_length_1_show, NULL);

static ssize_t cable_length_2_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	return cable_length_show(dev, attr, 2, buf);
}

static DEVICE_ATTR(cable_length_2, S_IRUGO, cable_length_2_show, NULL);

static ssize_t cable_length_3_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	return cable_length_show(dev, attr, 3, buf);
}

static DEVICE_ATTR(cable_length_3, S_IRUGO, cable_length_3_show, NULL);

static ssize_t cable_length_4_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	return cable_length_show(dev, attr, 4, buf);
}

static DEVICE_ATTR(cable_length_4, S_IRUGO, cable_length_4_show, NULL);

static ssize_t cable_length_5_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	return cable_length_show(dev, attr, 5, buf);
}

static DEVICE_ATTR(cable_length_5, S_IRUGO, cable_length_5_show, NULL);

static ssize_t cable_length_6_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	return cable_length_show(dev, attr, 6, buf);
}

static DEVICE_ATTR(cable_length_6, S_IRUGO, cable_length_6_show, NULL);


static ssize_t mdio_debug_show(struct device *dev,
			       struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);

	return sprintf(buf, "%d\n", ps->mdio_debug);
}

static ssize_t mdio_debug_set(struct device *dev,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int val;
	int ret;

	ret = sscanf(buf, "%d", &val);
	if (ret < 1)
		return -EINVAL;

	ps->mdio_debug = val;

	return count;
}

static DEVICE_ATTR(mdio_debug, (S_IRUGO | S_IWUGO), mdio_debug_show, mdio_debug_set);


static ssize_t mode_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
#ifdef CONFIG_NET_DSA_UNMANAGED
	return sprintf(buf, "unmanaged\n");
#else
	return sprintf(buf, "managed\n");
#endif
}

static DEVICE_ATTR(mode, S_IRUGO, mode_show, NULL);


static ssize_t monitor_dest_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = REG_READ(REG_GLOBAL, 0x1a);

	return sprintf(buf, "%d\n", (ret >> 12) & 0xf);
}

static ssize_t monitor_dest_set(struct device *dev,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int ret;

	ret = sscanf(buf, "%d", &val);
	if (ret < 1)
		return -EINVAL;

	ret = REG_READ(REG_GLOBAL, 0x1a);
	REG_WRITE(REG_GLOBAL, 0x1a, ((val & 0xf) * 0x1100) | (ret & 0x00ff));

	return count;
}

static DEVICE_ATTR(monitor_dest, (S_IRUGO | S_IWUGO), monitor_dest_show, monitor_dest_set);


static ssize_t monitor_src_show(struct device *dev,
				struct device_attribute *attr,
				int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = REG_READ(REG_PORT(port), 0x08);

	return sprintf(buf, "%d\n", ((ret & 0x0030) == 0x0030) ? 1 : 0);
}

static ssize_t monitor_src_set(struct device *dev,
			       struct device_attribute *attr, int port,
			       const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = REG_READ(REG_PORT(port), 0x08);

	if (!strncmp(buf, "0", 1))
		ret &= ~0x0030;
	else if (!strncmp(buf, "1", 1))
		ret |= 0x0030;

	REG_WRITE(REG_PORT(port), 0x08, ret);

	return count;
}


static ssize_t monitor_src_0_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	return monitor_src_show(dev, attr, 0, buf);
}

static ssize_t monitor_src_0_set(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	return monitor_src_set(dev, attr, 0, buf, count);
}

static DEVICE_ATTR(monitor_src_0, (S_IRUGO | S_IWUGO), monitor_src_0_show, monitor_src_0_set);


static ssize_t monitor_src_1_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	return monitor_src_show(dev, attr, 1, buf);
}

static ssize_t monitor_src_1_set(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	return monitor_src_set(dev, attr, 1, buf, count);
}

static DEVICE_ATTR(monitor_src_1, (S_IRUGO | S_IWUGO), monitor_src_1_show, monitor_src_1_set);


static ssize_t monitor_src_2_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	return monitor_src_show(dev, attr, 2, buf);
}

static ssize_t monitor_src_2_set(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	return monitor_src_set(dev, attr, 2, buf, count);
}

static DEVICE_ATTR(monitor_src_2, (S_IRUGO | S_IWUGO), monitor_src_2_show, monitor_src_2_set);


static ssize_t monitor_src_3_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	return monitor_src_show(dev, attr, 3, buf);
}

static ssize_t monitor_src_3_set(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	return monitor_src_set(dev, attr, 3, buf, count);
}

static DEVICE_ATTR(monitor_src_3, (S_IRUGO | S_IWUGO), monitor_src_3_show, monitor_src_3_set);


static ssize_t monitor_src_4_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	return monitor_src_show(dev, attr, 4, buf);
}

static ssize_t monitor_src_4_set(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	return monitor_src_set(dev, attr, 4, buf, count);
}

static DEVICE_ATTR(monitor_src_4, (S_IRUGO | S_IWUGO), monitor_src_4_show, monitor_src_4_set);

static ssize_t monitor_src_5_show(struct device *dev,
				  struct device_attribute *attr, char *buf)

{
	return monitor_src_show(dev, attr, 5, buf);
}

static ssize_t monitor_src_5_set(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	return monitor_src_set(dev, attr, 5, buf, count);
}

static DEVICE_ATTR(monitor_src_5, (S_IRUGO | S_IWUGO), monitor_src_5_show, monitor_src_5_set);

static ssize_t monitor_src_6_show(struct device *dev,
				  struct device_attribute *attr, char *buf)

{
	return monitor_src_show(dev, attr, 6, buf);
}

static ssize_t monitor_src_6_set(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	return monitor_src_set(dev, attr, 6, buf, count);
}

static DEVICE_ATTR(monitor_src_6, (S_IRUGO | S_IWUGO), monitor_src_6_show, monitor_src_6_set);


static int packet_generator_count;

static ssize_t packet_generator_count_show(struct device *dev,
			       struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", packet_generator_count);
}

static ssize_t packet_generator_count_set(struct device *dev,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%d", &val);
	if (ret < 1)
		return -EINVAL;

	packet_generator_count = val;

	return count;
}

static DEVICE_ATTR(packet_generator_count, (S_IRUGO | S_IWUGO), packet_generator_count_show, packet_generator_count_set);

static ssize_t packet_generator_show(struct device *dev,
			      struct device_attribute *attr,
			      int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = mv88e6123_61_65_phy_page_read(ds, port, 0x06, 0x10);
	if (ret < 0)
		return ret;

	return sprintf(buf, "%4x\n", ret);
}

static ssize_t packet_generator_set(struct device *dev,
			     struct device_attribute *attr, int port,
			     const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;
	int val;

	ret = sscanf(buf, "%d", &val);
	if (ret < 1)
		return -EINVAL;

	if (val == 0 || val == 1) {
		ret = mv88e6123_61_65_phy_page_write(ds, port, 0x06, 0x10,
			((packet_generator_count << 8) | (val << 3) | 0x06));
		if (ret < 0)
			return ret;
	}

	return count;
}


static ssize_t packet_generator_0_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return packet_generator_show(dev, attr, 0, buf);
}

static ssize_t packet_generator_0_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return packet_generator_set(dev, attr, 0, buf, count);
}

static DEVICE_ATTR(packet_generator_0, (S_IRUGO | S_IWUGO), packet_generator_0_show, packet_generator_0_set);


static ssize_t packet_generator_1_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return packet_generator_show(dev, attr, 1, buf);
}

static ssize_t packet_generator_1_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return packet_generator_set(dev, attr, 1, buf, count);
}

static DEVICE_ATTR(packet_generator_1, (S_IRUGO | S_IWUGO), packet_generator_1_show, packet_generator_1_set);


static ssize_t packet_generator_2_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return packet_generator_show(dev, attr, 2, buf);
}

static ssize_t packet_generator_2_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return packet_generator_set(dev, attr, 2, buf, count);
}

static DEVICE_ATTR(packet_generator_2, (S_IRUGO | S_IWUGO), packet_generator_2_show, packet_generator_2_set);


static ssize_t packet_generator_3_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return packet_generator_show(dev, attr, 3, buf);
}

static ssize_t packet_generator_3_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return packet_generator_set(dev, attr, 3, buf, count);
}

static DEVICE_ATTR(packet_generator_3, (S_IRUGO | S_IWUGO), packet_generator_3_show, packet_generator_3_set);


static ssize_t packet_generator_4_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return packet_generator_show(dev, attr, 4, buf);
}

static ssize_t packet_generator_4_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return packet_generator_set(dev, attr, 4, buf, count);
}

static DEVICE_ATTR(packet_generator_4, (S_IRUGO | S_IWUGO), packet_generator_4_show, packet_generator_4_set);

static ssize_t packet_generator_5_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return packet_generator_show(dev, attr, 5, buf);
}

static ssize_t packet_generator_5_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return packet_generator_set(dev, attr, 5, buf, count);
}

static DEVICE_ATTR(packet_generator_5, (S_IRUGO | S_IWUGO), packet_generator_5_show, packet_generator_5_set);

static ssize_t packet_generator_6_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return packet_generator_show(dev, attr, 6, buf);
}

static ssize_t packet_generator_6_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return packet_generator_set(dev, attr, 6, buf, count);
}

static DEVICE_ATTR(packet_generator_6, (S_IRUGO | S_IWUGO), packet_generator_6_show, packet_generator_6_set);

static int phy_device;

static ssize_t phy_device_show(struct device *dev,
			       struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", phy_device);
}

static ssize_t phy_device_set(struct device *dev,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	phy_device = val;

	return count;
}

static DEVICE_ATTR(phy_device, (S_IRUGO | S_IWUGO), phy_device_show, phy_device_set);


static int phy_page;

static ssize_t phy_page_show(struct device *dev,
			     struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", phy_page);
}

static ssize_t phy_page_set(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	phy_page = val;

	return count;
}

static DEVICE_ATTR(phy_page, (S_IRUGO | S_IWUGO), phy_page_show, phy_page_set);


static int phy_addr;

static ssize_t phy_addr_show(struct device *dev,
			     struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", phy_addr);
}

static ssize_t phy_addr_set(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	phy_addr = val;

	return count;
}

static DEVICE_ATTR(phy_addr, (S_IRUGO | S_IWUGO), phy_addr_show, phy_addr_set);


static ssize_t phy_data_show(struct device *dev,
			     struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;

	/*
	 * Perform a paged read if we're accessing a paged register.
	 */
	if (phy_addr >= 0x10 && phy_addr != 0x16) {
		val = mv88e6123_61_65_phy_page_read(ds, phy_device,
						    phy_page, phy_addr);
	} else {
		val = mv88e6123_61_65_phy_read(ds, phy_device, phy_addr);
	}

	if (val < 0)
		return val;

	return sprintf(buf, "%x\n", val);
}

static ssize_t phy_data_set(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	if (phy_addr == 0x16) {
		printk(KERN_WARNING "dsa phy_data_set: attempting direct write "
				 "to phy page register!  use the phy_page "
				 "sysfs attribute instead!\n");
		phy_page = val;
		return count;
	}

	/*
	 * Perform a paged write if we're accessing a paged register.
	 */
	if (phy_addr >= 0x10) {
		ret = mv88e6123_61_65_phy_page_write(ds, phy_device,
						    phy_page, phy_addr, val);
	} else {
		ret = mv88e6123_61_65_phy_write(ds, phy_device, phy_addr, val);
	}

	if (ret < 0)
		return ret;

	return count;
}

static DEVICE_ATTR(phy_data, (S_IRUGO | S_IWUGO), phy_data_show, phy_data_set);


static int reg_device;

static ssize_t reg_device_show(struct device *dev,
			       struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", reg_device);
}

static ssize_t reg_device_set(struct device *dev,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	reg_device = val;

	return count;
}

static DEVICE_ATTR(reg_device, (S_IRUGO | S_IWUGO), reg_device_show, reg_device_set);


static int reg_addr;

static ssize_t reg_addr_show(struct device *dev,
			     struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", reg_addr);
}

static ssize_t reg_addr_set(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	reg_addr = val;

	return count;
}

static DEVICE_ATTR(reg_addr, (S_IRUGO | S_IWUGO), reg_addr_show, reg_addr_set);


static ssize_t reg_data_show(struct device *dev,
			     struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;

	val = REG_READ(reg_device, reg_addr);

	return sprintf(buf, "%x\n", val);
}

static ssize_t reg_data_set(struct device *dev,
			    struct device_attribute *attr,
			    const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	REG_WRITE(reg_device, reg_addr, val);

	return count;
}

static DEVICE_ATTR(reg_data, (S_IRUGO | S_IWUGO), reg_data_show, reg_data_set);


static ssize_t reinit(struct device *dev, struct device_attribute *attr,
		      const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	//int ret;
	//int i;

	//ret = mv88e6123_61_65_setup_global(ds);
	//if (ret < 0)
	//	return ret;

	//for (i = 0; i < 7; i++) {
	//	ret = mv88e6123_61_65_setup_port(ds, i);
	//	if (ret < 0)
	//		return ret;
	//}

	mv88e6123_61_65_hw_bridge_sync(ds, 1);

	return count;
}

static DEVICE_ATTR(reinit, S_IWUGO, NULL, reinit);


static ssize_t revision_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = REG_READ(REG_PORT(0), 0x03);
	if (ret >= 0) {
		if (ret == 0x1212)
			return sprintf(buf, "Marvell 88E6123 (A1)\n");
		if (ret == 0x1213)
			return sprintf(buf, "Marvell 88E6123 (A2)\n");
		if ((ret & 0xfff0) == 0x1210)
			return sprintf(buf, "Marvell 88E6123\n");

		if (ret == 0x1612)
			return sprintf(buf, "Marvell 88E6161 (A1)\n");
		if (ret == 0x1613)
			return sprintf(buf, "Marvell 88E6161 (A2)\n");
		if ((ret & 0xfff0) == 0x1610)
			return sprintf(buf, "Marvell 88E6161\n");

		if (ret == 0x1652)
			return sprintf(buf, "Marvell 88E6165 (A1)\n");
		if ((ret & 0xfff0) == 0x1650)
			return sprintf(buf, "Marvell 88E6165\n");

		if (ret == 0x3522)
			return sprintf(buf, "Marvell 88E6352 (A1)\n");
		if ((ret & 0xfff0) == 0x3520)
			return sprintf(buf, "Marvell 88E6352\n");
	}

	return sprintf(buf, "Unknown\n");
}

static DEVICE_ATTR(revision, S_IRUGO, revision_show, NULL);

static int receive_errors[6];

static ssize_t rmon_reset(struct device *dev, struct device_attribute *attr,
			  const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;
	u32 ports;
	int i;

	ret = mv88e6xxx_reset_stats(ds);
	if (ret < 0)
		return ret;

	ports = ds->port_mask;
	for (i = 0; ports != 0; i++) {
		if (ports & 1) {
			int addr = REG_PORT(i);

			receive_errors[i] = 0;

			REG_WRITE(addr, 0x10, 0x0000);
			REG_WRITE(addr, 0x11, 0x0000);
			REG_WRITE(addr, 0x12, 0x0000);
			REG_WRITE(addr, 0x13, 0x0000);
		}

		ports >>= 1;
	}

	return count;
}

static DEVICE_ATTR(rmon_reset, S_IWUGO, NULL, rmon_reset);


static ssize_t stp_state_show(struct device *dev,
			      struct device_attribute *attr,
			      int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;
	char *state;

	ret = REG_READ(REG_PORT(port), 0x04);

	switch (ret & 3) {
	case 0:
		state = "disabled\n";
		break;
	case 1:
		state = "listening\n";
		break;
	case 2:
		state = "learning\n";
		break;
	case 3:
		state = "forwarding\n";
		break;
	}

	return sprintf(buf, "%s", state);
}

static ssize_t stp_state_set(struct device *dev,
			     struct device_attribute *attr, int port,
			     const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int state;
	int ret;

	if (!strncmp(buf, "disabled", 8))
		state = 0;
	else if (!strncmp(buf, "blocking", 8))
		state = 1;
	else if (!strncmp(buf, "listening", 9))
		state = 1;
	else if (!strncmp(buf, "learning", 8))
		state = 2;
	else if (!strncmp(buf, "forwarding", 10))
		state = 3;
	else
		return -EINVAL;

	ret = REG_READ(REG_PORT(port), 0x04);
	REG_WRITE(REG_PORT(port), 0x04, (ret & ~3) | state);

	printk(KERN_INFO "dsa: setting port %d STP state via sysfs to %s", port, buf);

	return (ret < 0) ? ret : count;
}


static ssize_t stp_state_0_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return stp_state_show(dev, attr, 0, buf);
}

static ssize_t stp_state_0_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return stp_state_set(dev, attr, 0, buf, count);
}

static DEVICE_ATTR(stp_state_0, (S_IRUGO | S_IWUGO), stp_state_0_show, stp_state_0_set);


static ssize_t stp_state_1_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return stp_state_show(dev, attr, 1, buf);
}

static ssize_t stp_state_1_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return stp_state_set(dev, attr, 1, buf, count);
}

static DEVICE_ATTR(stp_state_1, (S_IRUGO | S_IWUGO), stp_state_1_show, stp_state_1_set);


static ssize_t stp_state_2_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return stp_state_show(dev, attr, 2, buf);
}

static ssize_t stp_state_2_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return stp_state_set(dev, attr, 2, buf, count);
}

static DEVICE_ATTR(stp_state_2, (S_IRUGO | S_IWUGO), stp_state_2_show, stp_state_2_set);


static ssize_t stp_state_3_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return stp_state_show(dev, attr, 3, buf);
}

static ssize_t stp_state_3_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return stp_state_set(dev, attr, 3, buf, count);
}

static DEVICE_ATTR(stp_state_3, (S_IRUGO | S_IWUGO), stp_state_3_show, stp_state_3_set);


static ssize_t stp_state_4_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return stp_state_show(dev, attr, 4, buf);
}

static ssize_t stp_state_4_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return stp_state_set(dev, attr, 4, buf, count);
}

static DEVICE_ATTR(stp_state_4, (S_IRUGO | S_IWUGO), stp_state_4_show, stp_state_4_set);

static ssize_t stp_state_5_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return stp_state_show(dev, attr, 5, buf);
}

static ssize_t stp_state_5_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return stp_state_set(dev, attr, 5, buf, count);
}

static DEVICE_ATTR(stp_state_5, (S_IRUGO | S_IWUGO), stp_state_5_show, stp_state_5_set);

static ssize_t stp_state_6_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return stp_state_show(dev, attr, 6, buf);
}

static ssize_t stp_state_6_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return stp_state_set(dev, attr, 6, buf, count);
}

static DEVICE_ATTR(stp_state_6, (S_IRUGO | S_IWUGO), stp_state_6_show, stp_state_6_set);


static ssize_t master_state_show(struct device *dev,
			      struct device_attribute *attr,
			      int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	/*
	 * Are we advertising 1000baseT?
	 */
	ret = mv88e6123_61_65_phy_page_read(ds, port, 0x00, 0x09);
	if (ret < 0)
		return ret;

	if (ret & 0x0200) {
		/*
		 * Is the remote end advertising 1000baseT?
		 */
		ret = mv88e6123_61_65_phy_page_read(ds, port, 0x00, 0x0a);
		if (ret < 0)
			return ret;

		if (ret & 0x0800) {
			if (ret & 0x4000)
				return sprintf(buf, "master\n");
			else
				return sprintf(buf, "slave\n");
		}
	}

	return sprintf(buf, "invalid\n");
}

static ssize_t master_state_0_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return master_state_show(dev, attr, 0, buf);
}

static DEVICE_ATTR(master_state_0, S_IRUGO, master_state_0_show, NULL);

static ssize_t master_state_1_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return master_state_show(dev, attr, 1, buf);
}

static DEVICE_ATTR(master_state_1, S_IRUGO, master_state_1_show, NULL);

static ssize_t master_state_2_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return master_state_show(dev, attr, 2, buf);
}

static DEVICE_ATTR(master_state_2, S_IRUGO, master_state_2_show, NULL);

static ssize_t master_state_3_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return master_state_show(dev, attr, 3, buf);
}

static DEVICE_ATTR(master_state_3, S_IRUGO, master_state_3_show, NULL);

static ssize_t master_state_4_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return master_state_show(dev, attr, 4, buf);
}

static DEVICE_ATTR(master_state_4, S_IRUGO, master_state_4_show, NULL);

static ssize_t master_state_5_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return master_state_show(dev, attr, 5, buf);
}

static DEVICE_ATTR(master_state_5, S_IRUGO, master_state_5_show, NULL);

static ssize_t master_state_6_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return master_state_show(dev, attr, 6, buf);
}

static DEVICE_ATTR(master_state_6, S_IRUGO, master_state_6_show, NULL);

static ssize_t idle_error_count_show(struct device *dev,
			      struct device_attribute *attr,
			      int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = mv88e6123_61_65_phy_page_read(ds, port, 0x00, 0x0a);
	if (ret < 0)
		return ret;

	return sprintf(buf, "%d\n", (ret & 0xff));
}

static ssize_t idle_error_count_0_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return idle_error_count_show(dev, attr, 0, buf);
}

static DEVICE_ATTR(idle_error_count_0, S_IRUGO, idle_error_count_0_show, NULL);

static ssize_t idle_error_count_1_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return idle_error_count_show(dev, attr, 1, buf);
}

static DEVICE_ATTR(idle_error_count_1, S_IRUGO, idle_error_count_1_show, NULL);

static ssize_t idle_error_count_2_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return idle_error_count_show(dev, attr, 2, buf);
}

static DEVICE_ATTR(idle_error_count_2, S_IRUGO, idle_error_count_2_show, NULL);

static ssize_t idle_error_count_3_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return idle_error_count_show(dev, attr, 3, buf);
}

static DEVICE_ATTR(idle_error_count_3, S_IRUGO, idle_error_count_3_show, NULL);

static ssize_t idle_error_count_4_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return idle_error_count_show(dev, attr, 4, buf);
}

static DEVICE_ATTR(idle_error_count_4, S_IRUGO, idle_error_count_4_show, NULL);

static ssize_t idle_error_count_5_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return idle_error_count_show(dev, attr, 5, buf);
}

static DEVICE_ATTR(idle_error_count_5, S_IRUGO, idle_error_count_5_show, NULL);

static ssize_t idle_error_count_6_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return idle_error_count_show(dev, attr, 6, buf);
}

static DEVICE_ATTR(idle_error_count_6, S_IRUGO, idle_error_count_6_show, NULL);

static ssize_t receive_error_count_show(struct device *dev,
			      struct device_attribute *attr,
			      int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = mv88e6123_61_65_phy_page_read(ds, port, 0x00, 0x15);
	if (ret < 0)
		return ret;

	receive_errors[port] += ret;

	return sprintf(buf, "%d\n", receive_errors[port]);
}

static ssize_t receive_error_count_0_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return receive_error_count_show(dev, attr, 0, buf);
}

static DEVICE_ATTR(receive_error_count_0, S_IRUGO, receive_error_count_0_show, NULL);

static ssize_t receive_error_count_1_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return receive_error_count_show(dev, attr, 1, buf);
}

static DEVICE_ATTR(receive_error_count_1, S_IRUGO, receive_error_count_1_show, NULL);

static ssize_t receive_error_count_2_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return receive_error_count_show(dev, attr, 2, buf);
}

static DEVICE_ATTR(receive_error_count_2, S_IRUGO, receive_error_count_2_show, NULL);

static ssize_t receive_error_count_3_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return receive_error_count_show(dev, attr, 3, buf);
}

static DEVICE_ATTR(receive_error_count_3, S_IRUGO, receive_error_count_3_show, NULL);

static ssize_t receive_error_count_4_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return receive_error_count_show(dev, attr, 4, buf);
}

static DEVICE_ATTR(receive_error_count_4, S_IRUGO, receive_error_count_4_show, NULL);

static ssize_t receive_error_count_5_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return receive_error_count_show(dev, attr, 5, buf);
}

static DEVICE_ATTR(receive_error_count_5, S_IRUGO, receive_error_count_5_show, NULL);

static ssize_t receive_error_count_6_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return receive_error_count_show(dev, attr, 6, buf);
}

static DEVICE_ATTR(receive_error_count_6, S_IRUGO, receive_error_count_6_show, NULL);

static ssize_t pair_skew_show(struct device *dev,
			      struct device_attribute *attr,
			      int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;
	int skew;

	/*
	 * Check to see if pair skew register is valid by reading 21_5:6.
	 */
	ret = mv88e6123_61_65_phy_page_read(ds, port, 0x05, 0x15);
	if (ret < 0)
		return ret;

	if (ret & 0x40) {
		skew = mv88e6123_61_65_phy_page_read(ds, port, 0x05, 0x14);
		if (skew < 0)
			return skew;

		return sprintf(buf, "%04x\n", skew);
	} else {
		return sprintf(buf, "invalid\n");
	}
}

static ssize_t pair_skew_0_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return pair_skew_show(dev, attr, 0, buf);
}

static DEVICE_ATTR(pair_skew_0, S_IRUGO, pair_skew_0_show, NULL);

static ssize_t pair_skew_1_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return pair_skew_show(dev, attr, 1, buf);
}

static DEVICE_ATTR(pair_skew_1, S_IRUGO, pair_skew_1_show, NULL);

static ssize_t pair_skew_2_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return pair_skew_show(dev, attr, 2, buf);
}

static DEVICE_ATTR(pair_skew_2, S_IRUGO, pair_skew_2_show, NULL);

static ssize_t pair_skew_3_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return pair_skew_show(dev, attr, 3, buf);
}

static DEVICE_ATTR(pair_skew_3, S_IRUGO, pair_skew_3_show, NULL);

static ssize_t pair_skew_4_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return pair_skew_show(dev, attr, 4, buf);
}

static DEVICE_ATTR(pair_skew_4, S_IRUGO, pair_skew_4_show, NULL);

static ssize_t pair_skew_5_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return pair_skew_show(dev, attr, 5, buf);
}

static DEVICE_ATTR(pair_skew_5, S_IRUGO, pair_skew_5_show, NULL);

static ssize_t pair_skew_6_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return pair_skew_show(dev, attr, 6, buf);
}

static DEVICE_ATTR(pair_skew_6, S_IRUGO, pair_skew_6_show, NULL);

static ssize_t link_down_count_show(struct device *dev,
			      struct device_attribute *attr,
			      int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct net_device *ndev;
	struct dsa_slave_priv *p;

	if (port < 0 || port >= DSA_MAX_PORTS)
		return -EINVAL;

	ndev = ds->ports[port];
	p = netdev_priv(ndev);

	return sprintf(buf, "%d\n", p->link_down_count);
}

static ssize_t link_down_count_0_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return link_down_count_show(dev, attr, 0, buf);
}

static DEVICE_ATTR(link_down_count_0, S_IRUGO, link_down_count_0_show, NULL);

static ssize_t link_down_count_1_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return link_down_count_show(dev, attr, 1, buf);
}

static DEVICE_ATTR(link_down_count_1, S_IRUGO, link_down_count_1_show, NULL);

static ssize_t link_down_count_2_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return link_down_count_show(dev, attr, 2, buf);
}

static DEVICE_ATTR(link_down_count_2, S_IRUGO, link_down_count_2_show, NULL);

static ssize_t link_down_count_3_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return link_down_count_show(dev, attr, 3, buf);
}

static DEVICE_ATTR(link_down_count_3, S_IRUGO, link_down_count_3_show, NULL);

static ssize_t link_down_count_4_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return link_down_count_show(dev, attr, 4, buf);
}

static DEVICE_ATTR(link_down_count_4, S_IRUGO, link_down_count_4_show, NULL);

static ssize_t link_down_count_5_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return link_down_count_show(dev, attr, 5, buf);
}

static DEVICE_ATTR(link_down_count_5, S_IRUGO, link_down_count_5_show, NULL);

static ssize_t link_down_count_6_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return link_down_count_show(dev, attr, 6, buf);
}

static DEVICE_ATTR(link_down_count_6, S_IRUGO, link_down_count_6_show, NULL);


static int __perform_vct_test(struct dsa_switch *ds, int port)
{
	int ret;
	int i;
	int val;

	/*
	 * Set minimum measurement distance to zero.
	 */
	ret = mv88e6123_61_65_phy_page_write(ds, port, 0x05, 0x18, 0x0000);
	if (ret < 0)
		return ret;

	/*
	 * Set default cross pair threshold.
	 */
	ret = mv88e6123_61_65_phy_page_write(ds, port, 0x05, 0x19, 0x0104);
	if (ret < 0)
		return ret;

	/*
	 * Set default same pair threshold 0/1.
	 */
	ret = mv88e6123_61_65_phy_page_write(ds, port, 0x05, 0x1a, 0x0f12);
	if (ret < 0)
		return ret;

	/*
	 * Set default same pair threshold 2/3.
	 */
	ret = mv88e6123_61_65_phy_page_write(ds, port, 0x05, 0x1b, 0x0a0c);
	if (ret < 0)
		return ret;

	/*
	 * Do not wait 1.5s to break link before starting VCT test,
	 * transmit quarter pulses (32ns), at full amplitude (1000 mV),
	 * and set default same pair threshold 4.
	 */
	ret = mv88e6123_61_65_phy_page_write(ds, port, 0x05, 0x1c, 0x0c06);
	if (ret < 0)
		return ret;

	/*
	 * Start test by transmitting a pulse on all pairs
	 * simultaneously.
	 */
	ret = mv88e6123_61_65_phy_page_write(ds, port, 0x05, 0x17, 0x8600);
	if (ret < 0)
		return ret;

	/*
	 * Wait until test completes.
	 */
	for (i = 0; i < 2000; i++) {
		ret = mv88e6123_61_65_phy_page_read(ds, port, 0x05, 0x17);
		if (ret < 0)
			return ret;

		if ((ret & 0x4000) == 0x4000)
			break;

		usleep_range(1000, 2000);
	}
	if (i == 2000)
		return -ETIMEDOUT;

	/*
	 * Check for received reflections on each pair.
	 */
	val = 0;
	for (i = 0; i < 4; i++) {
		ret = mv88e6123_61_65_phy_page_read(ds, port, 0x05, 0x10 + i);
		if (ret < 0)
			return ret;

		/*
		 * 0x8000 means that no reflections were seen on this pair.
		 */
		if (ret == 0x8000) {
			val |= 1 << (4 * i);
			continue;
		}

		/*
		 * Impedance discontinuity if the reflected pulse
		 * is less than or equal to 312.5 mV.
		 */
		if (((ret >> 8) & 0x7f) <= 40) {
			val |= 4 << (4 * i);
			continue;
		}

		/*
		 * Positive reflection means an open fault.
		 */
		if (ret & 0x8000) {
			val |= 2 << (4 * i);
			continue;
		}

		/*
		 * Negative reflection means a short.
		 */
		val |= 3 << (4 * i);
	}

	return val;
}

static int phy_reset(struct dsa_switch *ds, int port)
{
	int ret;
	int i;

	/*
	 * Reset the PHY.  (This doesn't affect the PHY page register.)
	 */
	ret = mv88e6123_61_65_phy_read(ds, port, 0x00);
	if (ret < 0)
		return ret;

	ret = mv88e6123_61_65_phy_write(ds, port, 0x00, ret | 0x8000);
	if (ret < 0)
		return ret;


	/*
	 * Wait until PHY reset completes.
	 */
	for (i = 0; i < 100; i++) {
		ret = mv88e6123_61_65_phy_read(ds, port, 0x00);
		if (ret < 0)
		       return ret;

		if ((ret & 0x8000) == 0x0000)
			break;

		usleep_range(1000, 2000);
	}
	if (i == 100)
		return -ETIMEDOUT;

	return ret;
}

static ssize_t vct_show(struct device *dev,
			struct device_attribute *attr,
			int port, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int val;

	mutex_lock(&ps->vct_mutex);
	val = ps->vct_value[port];
	mutex_unlock(&ps->vct_mutex);

	return sprintf(buf, "%.4x\n", val);
}

static int perform_vct_test(struct dsa_switch *ds, int port)
{
	int reg4;
	int reg9;
	int ret;

	reg4 = mv88e6123_61_65_phy_read(ds, port, 0x04);
	if (reg4 < 0)
		return reg4;

	reg9 = mv88e6123_61_65_phy_read(ds, port, 0x09);
	if (reg9 < 0)
		return reg9;

	/*
	 * Disable advertising 10/100 Mb/s modes if any are enabled.
	 */
	if (reg4 & 0x01e0) {
		ret = mv88e6123_61_65_phy_write(ds, port, 0x04, reg4 & ~0x01e0);
		if (ret < 0)
			return ret;
	}

	/*
	 * Disable advertising 1000 Mb/s modes if any are enabled.
	 */
	if (reg9 & 0x0300) {
		ret = mv88e6123_61_65_phy_write(ds, port, 0x09, reg9 & ~0x0300);
		if (ret < 0)
			return ret;
	}

	/*
	 * Reset the PHY.
	 */
	ret = phy_reset(ds, port);
	if (ret < 0)
		return ret;

	/*
	 * Perform the VCT test.
	 */
	ret = __perform_vct_test(ds, port);

	/*
	 * Reenable advertisement bits.
	 */
	mv88e6123_61_65_phy_write(ds, port, 0x04, reg4);
	mv88e6123_61_65_phy_write(ds, port, 0x09, reg9);
	phy_reset(ds, port);

	return ret;
}

void set_vct_value(struct dsa_switch *ds, int port, int value)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int vct_prev = ps->vct_value[port];
	char buf[16] = {'\0'};

	ps->vct_value[port] = (value < 0) ? 0 : value;
	if (vct_prev != ps->vct_value[port]) {
		sprintf(buf, "vct_%d", port);
		sysfs_notify(&ps->device->kobj, NULL, buf);
	}
}

static void do_vct_tests_work(struct work_struct *ugly)
{
	struct mv88e6xxx_priv_state *ps;
	struct dsa_switch *ds;

	ps = container_of(ugly, struct mv88e6xxx_priv_state, vct_work);
	ds = ((struct dsa_switch *)ps) - 1;
	mutex_lock(&ps->vct_mutex);

	while (ps->vct_tests_scheduled) {
		int sched;
		int i;

		sched = ps->vct_tests_scheduled;

		mutex_unlock(&ps->vct_mutex);

		for (i = 0; i < MV88E6XXX_MAX_PORTS; i++) {
			int ret;

			if (!(sched & (1 << i)))
				continue;

			printk(KERN_INFO "dsa: starting vct test "
					 "on port %d\n", i);

			ret = perform_vct_test(ds, i);

			printk(KERN_INFO "dsa: vct test on port %d completed "
					 "with status %.4x\n", i, ret);

			set_vct_value(ds, i, ret);
		}

		mutex_lock(&ps->vct_mutex);

		ps->vct_tests_scheduled &= ~sched;
	}

	mutex_unlock(&ps->vct_mutex);
}

void schedule_vct_test(struct dsa_switch *ds, int port)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int oldval;

	mutex_lock(&ps->vct_mutex);
	oldval = ps->vct_tests_scheduled;
	ps->vct_tests_scheduled |= 1 << port;
	set_vct_value(ds, port, 0);
	mutex_unlock(&ps->vct_mutex);

	if (!oldval)
		queue_work(ps->vct_wq, &ps->vct_work);
}

static ssize_t vct_set(struct device *dev,
		       struct device_attribute *attr, int port,
		       const char *buf, size_t count)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int ret;

	ret = sscanf(buf, "%d", &val);
	if (ret < 1)
		return -EINVAL;

	/*
	 * If a 1 is written, schedule a VCT test if the link is
	 * currently down.
	 */
	if (val == 1) {
		ret = mv88e6xxx_reg_read(ds, REG_PORT(port), 0x00);
		if (ret < 0)
			return ret;

		if ((ret & 0x0800) == 0x0000)
			schedule_vct_test(ds, port);

		return count;
	}

	/*
	 * If a 2 is written, schedule a VCT test unconditionally.
	 */
	if (val == 2) {
		schedule_vct_test(ds, port);
		return count;
	}

	return count;
}

static ssize_t vct_0_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return vct_show(dev, attr, 0, buf);
}

static ssize_t vct_0_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return vct_set(dev, attr, 0, buf, count);
}

static DEVICE_ATTR(vct_0, (S_IRUGO | S_IWUGO), vct_0_show, vct_0_set);

static ssize_t vct_1_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return vct_show(dev, attr, 1, buf);
}

static ssize_t vct_1_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return vct_set(dev, attr, 1, buf, count);
}

static DEVICE_ATTR(vct_1, (S_IRUGO | S_IWUGO), vct_1_show, vct_1_set);

static ssize_t vct_2_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return vct_show(dev, attr, 2, buf);
}

static ssize_t vct_2_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return vct_set(dev, attr, 2, buf, count);
}

static DEVICE_ATTR(vct_2, (S_IRUGO | S_IWUGO), vct_2_show, vct_2_set);

static ssize_t vct_3_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return vct_show(dev, attr, 3, buf);
}

static ssize_t vct_3_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return vct_set(dev, attr, 3, buf, count);
}

static DEVICE_ATTR(vct_3, (S_IRUGO | S_IWUGO), vct_3_show, vct_3_set);

static ssize_t vct_4_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return vct_show(dev, attr, 4, buf);
}

static ssize_t vct_4_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return vct_set(dev, attr, 4, buf, count);
}

static DEVICE_ATTR(vct_4, (S_IRUGO | S_IWUGO), vct_4_show, vct_4_set);

static ssize_t vct_5_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return vct_show(dev, attr, 5, buf);
}

static ssize_t vct_5_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return vct_set(dev, attr, 5, buf, count);
}

static DEVICE_ATTR(vct_5, (S_IRUGO | S_IWUGO), vct_5_show, vct_5_set);

static ssize_t vct_6_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	return vct_show(dev, attr, 6, buf);
}

static ssize_t vct_6_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	return vct_set(dev, attr, 6, buf, count);
}

static DEVICE_ATTR(vct_6, (S_IRUGO | S_IWUGO), vct_6_show, vct_6_set);

#define SHUTDOWN_LINK_ON_MEMBER_VIOLATION_GET_SET(x) \
	static ssize_t shutdown_link_on_member_violation_show_##x(struct device* dev,\
			struct device_attribute *attr, char *buf) {\
		return shutdown_link_on_member_violation_show(dev, attr, x, buf);\
	}\
	static ssize_t shutdown_link_on_member_violation_set_##x(struct device* dev,\
			struct device_attribute *attr,\
			const char *buf, size_t count) {\
		return shutdown_link_on_member_violation_set(dev, attr, x, buf, count);\
	}\
	static DEVICE_ATTR(shutdown_link_on_member_violation_##x, (S_IRUGO | S_IWUGO), shutdown_link_on_member_violation_show_##x, shutdown_link_on_member_violation_set_##x)


static ssize_t shutdown_link_on_member_violation_show(struct device *dev,
				struct device_attribute *attr,
				int port, char *buf) {

	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct net_device *ndev;
	struct dsa_slave_priv *p;

	if (port < 0 || port >= DSA_MAX_PORTS)
		return -EINVAL;

	ndev = ds->ports[port];
	p = netdev_priv(ndev);

	return sprintf(buf, "%d\n", p->shutdown_link_on_member_violation);
}

static ssize_t shutdown_link_on_member_violation_set(struct device *dev,
				struct device_attribute *attr, int port,
				const char *buf, size_t count) {

	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	struct net_device *ndev;
	struct dsa_slave_priv *p;
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	ndev = ds->ports[port];
	p = netdev_priv(ndev);

	p->shutdown_link_on_member_violation = val;

	return count;

}

SHUTDOWN_LINK_ON_MEMBER_VIOLATION_GET_SET(0);
SHUTDOWN_LINK_ON_MEMBER_VIOLATION_GET_SET(1);
SHUTDOWN_LINK_ON_MEMBER_VIOLATION_GET_SET(2);
SHUTDOWN_LINK_ON_MEMBER_VIOLATION_GET_SET(3);
SHUTDOWN_LINK_ON_MEMBER_VIOLATION_GET_SET(4);
SHUTDOWN_LINK_ON_MEMBER_VIOLATION_GET_SET(5);
SHUTDOWN_LINK_ON_MEMBER_VIOLATION_GET_SET(6);

#define PHY_TX_DISABLE_GET_SET(x) \
	static ssize_t phy_tx_disable_show_##x(struct device* dev,\
			struct device_attribute *attr, char *buf) {\
		return phy_tx_disable_show(dev, attr, x, buf);\
	}\
	static ssize_t phy_tx_disable_set_##x(struct device* dev,\
			struct device_attribute *attr,\
			const char *buf, size_t count) {\
		return phy_tx_disable_set(dev, attr, x, buf, count);\
	}\
	static DEVICE_ATTR(phy_tx_disable_##x, (S_IRUGO | S_IWUGO), phy_tx_disable_show_##x, phy_tx_disable_set_##x)


static ssize_t phy_tx_disable_show(struct device *dev,
				struct device_attribute *attr,
				int port, char *buf) {

	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = mv88e6123_61_65_phy_page_read(ds, port, 0, 0x10);
	if (ret < 0)
		return ret;

	/*
	 * Show PHY transmitter on/off state.
	 */
	//ret &= 0x0008;

	return sprintf(buf, "%x\n", ((ret & 0x0008) == 0x0008) ? 1 : 0); 
}

static ssize_t phy_tx_disable_set(struct device *dev,
				struct device_attribute *attr, int port,
				const char *buf, size_t count) {

	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int val;
	int ret;

	ret = sscanf(buf, "%x", &val);
	if (ret < 1)
		return -EINVAL;

	ret = mv88e6123_61_65_phy_page_read(ds, port, 0, 0x10);
	if (ret < 0)
		return ret;

	/*
	 * Turn PHY transmitter on or off.
	 */
	if ( !!(ret & 0x0008) != val ) {
		ret = mv88e6123_61_65_phy_page_write(ds, port, 0, 0x10, ret ^ 0x0008);
		if (ret < 0)
			return ret;
		else
			printk(KERN_INFO "dsa: setting PHY port %d transmitter disable bit to %d via sysfs entry\n", port, val);
	}

	return count;
}

PHY_TX_DISABLE_GET_SET(0);
PHY_TX_DISABLE_GET_SET(1);
PHY_TX_DISABLE_GET_SET(2);
PHY_TX_DISABLE_GET_SET(3);
PHY_TX_DISABLE_GET_SET(4);
PHY_TX_DISABLE_GET_SET(5);
PHY_TX_DISABLE_GET_SET(6);

static ssize_t temp6_26_show(struct dsa_switch *ds, char *buf)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int ret;
	int val;

	mutex_lock(&ps->temp_mutex);

	/*
	 * Enable temperature sensor.
	 */
	ret = mv88e6123_61_65_phy_page_read(ds, 0, 0x06, 0x1a);
	if (ret < 0) {
		mutex_unlock(&ps->temp_mutex);
		return ret;
	}

	ret = mv88e6123_61_65_phy_page_write(ds, 0, 0x06, 0x1a, ret | (1 << 5));
	if (ret < 0) {
		mutex_unlock(&ps->temp_mutex);
		return ret;
	}

	/*
	 * Wait for temperature to stabilize.
	 */
	msleep(10);

	val = mv88e6123_61_65_phy_page_read(ds, 0, 0x06, 0x1a);
	if (val < 0) {
		mutex_unlock(&ps->temp_mutex);
		return val;
	}

	/*
	 * Disable temperature sensor.
	 */
	ret = mv88e6123_61_65_phy_page_write(ds, 0, 0x06, 0x1a,
					     ret & ~(1 << 5));
	if (ret < 0) {
		mutex_unlock(&ps->temp_mutex);
		return ret;
	}

	mutex_unlock(&ps->temp_mutex);

	return sprintf(buf, "%d\n", ((val & 0x1f) - 5) * 5000);
}

static ssize_t temp6_27_show(struct dsa_switch *ds, char *buf)
{
	int ret;

	ret = mv88e6123_61_65_phy_page_read(ds, 0, 0x06, 0x1b);
	if (ret < 0)
		return ret;

	return sprintf(buf, "%d\n", ((ret & 0xff) - 25) * 1000);
}

static ssize_t temperature_show(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dsa_switch_tree *dst = platform_get_drvdata(pdev);
	struct dsa_switch *ds = dst->ds[0];
	int ret;

	ret = mv88e6xxx_reg_read(ds, REG_PORT(0), 0x03);
	if (ret < 0)
		return ret;

	if ((ret & 0xfff0) == 0x3520) {
		/* Marvell 88E6352 */
		return temp6_27_show(ds, buf);
	} else {
		return temp6_26_show(ds, buf);
	}
}

static DEVICE_ATTR(temp1_input, S_IRUGO, temperature_show, NULL);

static ssize_t show_label(struct device *dev,
			  struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "switch_die_temp\n");
}

static DEVICE_ATTR(temp1_label, S_IRUGO, show_label, NULL);

static ssize_t show_name(struct device *dev, struct device_attribute *attr,
			 char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	return sprintf(buf, "%s\n", pdev->name);
}

static DEVICE_ATTR(name, S_IRUGO, show_name, NULL);

static struct attribute *mv88e6123_61_65_attributes[] = {
	&dev_attr_avb_port.attr,
	&dev_attr_avb_addr.attr,
	&dev_attr_avb_data.attr,
	&dev_attr_name.attr,
	&dev_attr_temp1_input.attr,
	&dev_attr_temp1_label.attr,
	&dev_attr_atu_age_time.attr,
	&dev_attr_atu_reload.attr,
	&dev_attr_atu_flush.attr,
	&dev_attr_atu_dump.attr,
	&dev_attr_bpdu_bypass.attr,
	&dev_attr_switch_setup.attr,
	&dev_attr_mdio_debug.attr,
	&dev_attr_mode.attr,
	&dev_attr_monitor_dest.attr,
	&dev_attr_packet_generator_count.attr,
	&dev_attr_phy_device.attr,
	&dev_attr_phy_page.attr,
	&dev_attr_phy_addr.attr,
	&dev_attr_phy_data.attr,
	&dev_attr_reg_device.attr,
	&dev_attr_reg_addr.attr,
	&dev_attr_reg_data.attr,
	&dev_attr_reinit.attr,
	&dev_attr_revision.attr,
	&dev_attr_rmon_reset.attr,
	NULL
};

static const struct attribute_group mv88e6123_61_65_group = {
	.attrs = mv88e6123_61_65_attributes,
};

static void mv88e6123_61_65_hw_bridge_sync_work(struct work_struct *ugly)
{
	struct mv88e6xxx_priv_state *ps;
	struct dsa_switch *ds;
	
	ps = container_of(ugly, struct mv88e6xxx_priv_state, hw_bridge_work);
	ds = ((struct dsa_switch *)ps) - 1;

	mv88e6123_61_65_hw_bridge_sync(ds, 0);
}

static int check_disable_energy_detect(struct dsa_switch *ds, int port)
{
	int ret;

	/*
	 * Is energy detect already disabled?
	 */
	ret = mv88e6123_61_65_phy_page_read(ds, port, 0x00, 0x10);
	if (ret < 0)
		return ret;

	if ((ret & 0x0300) == 0x0000)
		return 0;


	printk(KERN_INFO "dsa: disabling energy detect on port %d\n", port);

	/*
	 * Disable energy detect.
	 */
	ret = mv88e6123_61_65_phy_page_write(ds, port, 0x00, 0x10,
					     ret & ~0x0300);
	if (ret < 0)
		return ret;

	/*
	 * Reset the PHY.
	 */
	ret = phy_reset(ds, port);
	if (ret < 0)
		return ret;

	return 0;
}

static void mv88e6123_61_65_atu_flush_work(struct work_struct *ugly)
{
	struct mv88e6xxx_priv_state *ps;
	struct dsa_switch *ds;
	int ret;
	
	ps = container_of(ugly, struct mv88e6xxx_priv_state, atu_flush_work);
	ds = ((struct dsa_switch *)ps) - 1;

	mutex_lock(&ps->atu_mutex);
	ret = __atu_flush(ds);
	mutex_unlock(&ps->atu_mutex);
}

static void decode_global_status(int val)
{
	printk(KERN_NOTICE "dsa: global status %.4x:", val);
	if (val & 0x0800)
		printk(" InitReady");
	if (val & 0x0100)
		printk(" AVBInt");
	if (val & 0x0080)
		printk(" DeviceInt");
	if (val & 0x0040)
		printk(" StatsDone");
	if (val & 0x0020)
		printk(" VTUProb");
	if (val & 0x0010)
		printk(" VTUDone");
	if (val & 0x0004)
		printk(" ATUDone");
	if (val & 0x0001)
		printk(" EEInt");
	printk("\n");
}

static void decode_copper_phy_status(int val)
{
	if (val & 0x8000)
		printk(" CopperAutonegError");
	if (val & 0x4000)
		printk(" CopperSpeedChanged");
	if (val & 0x2000)
		printk(" CopperDuplexChanged");
	if (val & 0x1000)
		printk(" CopperPageReceived");
	if (val & 0x0800)
		printk(" CopperAutonegComplete");
	if (val & 0x0400)
		printk(" CopperLinkStatusChanged");
	if (val & 0x0200)
		printk(" CopperSymbolError");
	if (val & 0x0100)
		printk(" CopperFalseCarrier");
	if (val & 0x0040)
		printk(" MDICrossoverChanged");
	if (val & 0x0020)
		printk(" Downshift");
	if (val & 0x0010)
		printk(" EnergyDetectChanged");
	if (val & 0x0008)
		printk(" FLPExchangeCompleteButNoLink");
	if (val & 0x0004)
		printk(" DTEPowerDetectionStatusChanged");
	if (val & 0x0002)
		printk(" PolarityChanged");
	if (val & 0x0001)
		printk(" Jabber");
	printk("\n");
}

static int mv88e6123_61_65_check_interrupts(struct dsa_switch *ds)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int val;
	int phy;
	char buf[16] = {'\0'};

	val = mv88e6xxx_reg_read(ds, REG_GLOBAL, 0x00);
	if (!val)
		return 0;
	
	if ((val & 0x0001) | (val & 0x0004) | (val & 0x0010) | (val & 0x0020) | (val & 0x0100))
		decode_global_status(val);
	if (val & 0x0008) {
		int reg;
		int ret;
		int atu_data;
		u8 addr[6];

		printk(KERN_CRIT "dsa: atuprob interrupt:");

		reg = mv88e6xxx_reg_read(ds, REG_GLOBAL, 0x0b);

		mv88e6xxx_reg_write(ds, REG_GLOBAL, 0x0b, reg | 0xF000);

		ret = __atu_wait(ds);
		if (ret == 0) {
			reg = mv88e6xxx_reg_read(ds, REG_GLOBAL, 0x0b);

			if (reg & 0x0010)
				printk(" ATUFull Violation");
			if (reg & 0x0020)
				printk(" Miss Violation");
			if (reg & 0x0040) {
				atu_data = mv88e6xxx_reg_read(ds, REG_GLOBAL, 0x0c);
				reg = REG_READ(REG_GLOBAL, 0xd);
				addr[0] = (reg >> 8) & 0xff;
				addr[1] = reg & 0xff;
				reg = REG_READ(REG_GLOBAL, 0xe);
				addr[2] = (reg >> 8) & 0xff;
				addr[3] = reg & 0xff;
				reg = REG_READ(REG_GLOBAL, 0xf);
				addr[4] = (reg >> 8) & 0xff;
				addr[5] = reg & 0xff;

				printk(" Member Violation on Source Port ID: %x MAC: %pM Transmit Disable on PHY(s):", atu_data & 0x0F, addr);
				struct net_device *ndev;
				struct dsa_slave_priv *p;
				for (phy = 0; phy < 7; phy++) {
					ndev = ds->ports[phy];
					p = netdev_priv(ndev);
					if ((ds->phy_port_mask & (1 << phy))) {
						if (p->shutdown_link_on_member_violation == 1 && (phy == (atu_data & 0x0F))) {
							ret = mv88e6123_61_65_phy_page_read(ds, phy, 0x00, 0x10);
							ret = mv88e6123_61_65_phy_page_write(ds, phy, 0x00, 0x10, ret | 0x0008);
							printk(" %d", phy);
							sprintf(buf, "phy_tx_disable_%d", phy);
							sysfs_notify(&ps->device->kobj, NULL, buf);
						}
					}
				}
			}
			if (reg & 0x0090)		
				printk(" AgeOut Violation");
		}
		printk("\n");
	}

	if (val & 0x0080) {
		int val2;

//		printk(KERN_CRIT "dsa: Device interrupt\n");

		val2 = mv88e6xxx_reg_read(ds, REG_GLOBAL2, 0x00);

		if (val2 & 0x8000) {
			int val3;

			printk(KERN_CRIT "dsa: WatchDog interrupt:");

			val3 = mv88e6xxx_reg_read(ds, REG_GLOBAL2, 0x1b);

			if (val3 & 0x0080)
				printk(" EgressWDEvent");
			if (val3 & 0x0010)
				printk(" EgressWDHistory");
			if (val3 & 0x0002)
				printk(" WatchDogHistory");
			printk("\n");
		}

		if (val2 & 0x4000)
			printk(KERN_CRIT "dsa: JamLimit interrupt\n");

		val2 &= 0x07ff;
		if (val2) {

//			printk(KERN_CRIT "dsa: PHY interrupts %.4x\n", val2);

			for (phy = 0; phy < 11; phy++) {
				int val3;

				if (!(val2 & (1 << phy)))
					continue;

				val3 = mv88e6123_61_65_phy_page_read(ds, phy, 0x00, 0x13);	// Read PHY Copper Specific Status Register 2

				if (!val3)
					continue;

				printk(KERN_NOTICE "dsa: port %d status "
				                 "%.4x:", phy, val3);
				decode_copper_phy_status(val3);
			}

			//mv88e6xxx_reg_read(ds, REG_GLOBAL2, 0x00);  // After reading the PHY interrupts, Global 2 Interrupt Source Register must be read again to clear the interrupt.

#if !defined(CONFIG_BOARD_IMS_NIU)
			dsa_link_poll(ds->dst);
#endif
		}
	}

#if defined(CONFIG_BOARD_IMS_NIU)
	dsa_link_poll(ds->dst);
#endif

	return 1;
}

static irqreturn_t mv88e6123_61_65_irq(int irq, void *_ds)
{
	struct dsa_switch *ds = _ds;
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);

	ps->irq_count++;

	return mv88e6123_61_65_check_interrupts(ds) ? IRQ_HANDLED : IRQ_NONE;
}

static int mv88e6123_61_65_setup(struct dsa_switch *ds, struct device *dev)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int i;
	int ret;
	int val;

	ps->device = dev;
	mutex_init(&ps->smi_mutex);
	ps->smi_mutex_owner = NULL;
	ps->smi_mutex_depth = 0;
	mutex_init(&ps->stats_mutex);
	mutex_init(&ps->phy_indirect_mutex);
	mutex_init(&ps->atu_mutex);
	for (i = 0; i < ARRAY_SIZE(ps->phy_page_mutex); i++)
		mutex_init(&ps->phy_page_mutex[i]);
	for (i = 0; i < ARRAY_SIZE(ps->phy_page); i++)
		ps->phy_page[i] = -1;
	mutex_init(&ps->temp_mutex);

#ifdef CONFIG_NET_DSA_MDIO_DEBUG_DEFAULT_ON
	ps->mdio_debug = 1;
#else
	ps->mdio_debug = 0;
#endif

	spin_lock_init(&ps->hw_bridge_state);
	INIT_WORK(&ps->hw_bridge_work, mv88e6123_61_65_hw_bridge_sync_work);

	INIT_WORK(&ps->atu_flush_work, mv88e6123_61_65_atu_flush_work);

	/*
	 * Make sure the PHY Polling Unit is enabled as we might be
	 * booting up with an old Switch EEPROM config that has the 
	 * PHY Polling Unit disabled.  Without this code, we cannot
         * successfully talk to the PHY's and more specifically, we
         * cannot clear interrupt events resulting in an interrupt
         * storm.
	 */
	ret = REG_READ(REG_GLOBAL, 0x04);
	REG_WRITE(REG_GLOBAL, 0x04, (ret | 0x4000));

	ret = mv88e6xxx_reg_read(ds, REG_PORT(0), 0x03);
	if (ret < 0)
		return ret;

	if ((ret & 0xfff0) == 0x3520) {
		/* Marvell 88E6352 */
		ps->eeprom_length = 0x200;
	} else {
		ps->eeprom_length = 0;
	}

	for (i = 0; i < MV88E6XXX_MAX_PORTS; i++)
		ps->map_forwarding[i] = 0;	

#if defined(CONFIG_BOARD_IMS_RDU)

	/*
	 * Check to see if we can just keep the ports in the
	 * forwarding state during STP startup.  (If the processor
	 * has been started previously and the switch port is already
	 * in the forwarding state, we are good (for that port) to leave 
	 * it that way.)
	 */
	ret = REG_READ(REG_GLOBAL2, 0x0b);
	if (ret < 0)
		return ret;
	if ((ret & 0x0001) == 1) { /* Switch has already booted */
		printk(KERN_INFO "dsa: switch has already booted\n");
		for (i = 0; i < MV88E6XXX_MAX_PORTS; i++) {
			ret = mv88e6xxx_reg_read(ds, REG_PORT(i), 0x04);
			if ((ret & 0x0003) == 3) {
				ps->map_forwarding[i] = 1;
			}
		}
		printk(KERN_INFO "dsa: starting timer to disable map_forwarding\n");
		setup_timer( &ps->map_forwarding_timeout, map_forwarding_timeout_callback, ds );
		ret = mod_timer( &ps->map_forwarding_timeout, jiffies + msecs_to_jiffies(30000) );
  		if (ret) 
			printk("dsa: Error in mod_timer\n");
	} else {
		printk(KERN_INFO "dsa: switch is coming up fresh\n");	
	}

#endif

	mutex_init(&ps->avb_mutex);

	mutex_init(&ps->vct_mutex);
	ps->vct_wq = create_singlethread_workqueue("vct");
	INIT_WORK(&ps->vct_work, do_vct_tests_work);
	ps->vct_tests_scheduled = 0;
	for (i = 0; i < ARRAY_SIZE(ps->vct_value); i++)
		set_vct_value(ds, i, 0);

	ret = dsa_switch_register_irq_handler(ds, mv88e6123_61_65_irq, ds);
	if (ret == 0) {
		ps->irq_in_use = 1;
	}

//#ifndef CONFIG_NET_DSA_UNMANAGED
	//ret = mv88e6123_61_65_switch_reset(ds);
	//if (ret < 0)
	//	return ret;
//#endif

	/* @@@ initialise vtu and atu */

	//ret = mv88e6123_61_65_setup_global(ds);
	//if (ret < 0)
	//	return ret;

    if (ps->irq_in_use && !ps->irq_count) {
		int i;

		for (i = 0; i < 100; i++) {
			if (ps->irq_count)
				break;

			usleep_range(1000, 2000);
		}
	}

#if defined(CONFIG_BOARD_IMS_RDU) || defined(CONFIG_BOARD_IMS_NIU)

	dsa_switch_unregister_irq_handler(ds, ds);
	ps->irq_in_use = 0;

	dsa_switch_setup_status_poll(ds, mv88e6123_61_65_check_interrupts);

#endif

#if defined(CONFIG_BOARD_IMS_SCU2_ESB) || defined(CONFIG_BOARD_IMS_SCU2_MEZZ)

	if (!ps->irq_in_use || !ps->irq_count) {
		if (ps->irq_in_use) {
			printk(KERN_CRIT "dsa: interrupt line appears to be "
					 "broken, falling back to polling\n");
			dsa_switch_unregister_irq_handler(ds, ds);
			ps->irq_in_use = 0;
		}

		dsa_switch_setup_status_poll(ds,
			mv88e6123_61_65_check_interrupts);
	}

#endif

    ret = atu_add_local_mac(ds);
	if (ret < 0)
		return ret;


	for (i = 0; i < 7; i++) {
		//ret = mv88e6123_61_65_setup_port(ds, i);
		//if (ret < 0)
		//	return ret;

		ps->fid[i] = i;
		ps->stp_state[i] = 3;
	}

	for (i = 0; i < DSA_MAX_PORTS; i++) {
		if (!(ds->port_mask & (1 << i)))
			continue;

		ret = mv88e6123_61_65_phy_read(ds, i, 0x02);
		if (ret >= 0 && ret != 0xffff) {
			ds->phy_port_mask |= 1 << i;
			check_disable_energy_detect(ds, i);
		}
	}

	ret = sysfs_create_group(&dev->kobj, &mv88e6123_61_65_group);

	val = mv88e6xxx_reg_read(ds, REG_PORT(0), 0x03);
	if (val >= 0 && (val & 0xfff0) == 0x3520) {
		/* Marvell 88E6352 */
		ret |= device_create_file(dev, &dev_attr_avb_block);
	}

	if (ds->port_mask & 1) {
		ret |= device_create_file(dev, &dev_attr_monitor_src_0);
		ret |= device_create_file(dev, &dev_attr_stp_state_0);
	}
	if (ds->phy_port_mask & 1) {
		ret |= device_create_file(dev, &dev_attr_master_state_0);
		ret |= device_create_file(dev, &dev_attr_idle_error_count_0);
		ret |= device_create_file(dev, &dev_attr_receive_error_count_0);
		ret |= device_create_file(dev, &dev_attr_pair_skew_0);
		ret |= device_create_file(dev, &dev_attr_packet_generator_0);
		ret |= device_create_file(dev, &dev_attr_link_down_count_0);
		ret |= device_create_file(dev, &dev_attr_cable_length_0);
		ret |= device_create_file(dev, &dev_attr_vct_0);
		ret |= device_create_file(dev, &dev_attr_phy_tx_disable_0);
		ret |= device_create_file(dev, &dev_attr_shutdown_link_on_member_violation_0);
	}
	if (ds->port_mask & 2) {
		ret |= device_create_file(dev, &dev_attr_monitor_src_1);
		ret |= device_create_file(dev, &dev_attr_stp_state_1);
	}
	if (ds->phy_port_mask & 2) {
		ret |= device_create_file(dev, &dev_attr_master_state_1);
		ret |= device_create_file(dev, &dev_attr_idle_error_count_1);
		ret |= device_create_file(dev, &dev_attr_receive_error_count_1);
		ret |= device_create_file(dev, &dev_attr_pair_skew_1);
		ret |= device_create_file(dev, &dev_attr_packet_generator_1);
		ret |= device_create_file(dev, &dev_attr_link_down_count_1);
		ret |= device_create_file(dev, &dev_attr_cable_length_1);
		ret |= device_create_file(dev, &dev_attr_vct_1);
		ret |= device_create_file(dev, &dev_attr_phy_tx_disable_1);
		ret |= device_create_file(dev, &dev_attr_shutdown_link_on_member_violation_1);
	}
	if (ds->port_mask & 4) {
		ret |= device_create_file(dev, &dev_attr_monitor_src_2);
		ret |= device_create_file(dev, &dev_attr_stp_state_2);
	}
	if (ds->phy_port_mask & 4) {
		ret |= device_create_file(dev, &dev_attr_master_state_2);
		ret |= device_create_file(dev, &dev_attr_idle_error_count_2);
		ret |= device_create_file(dev, &dev_attr_receive_error_count_2);
		ret |= device_create_file(dev, &dev_attr_pair_skew_2);
		ret |= device_create_file(dev, &dev_attr_packet_generator_2);
		ret |= device_create_file(dev, &dev_attr_link_down_count_2);
		ret |= device_create_file(dev, &dev_attr_cable_length_2);
		ret |= device_create_file(dev, &dev_attr_vct_2);
		ret |= device_create_file(dev, &dev_attr_phy_tx_disable_2);
		ret |= device_create_file(dev, &dev_attr_shutdown_link_on_member_violation_2);
	}
	if (ds->port_mask & 8) {
		ret |= device_create_file(dev, &dev_attr_monitor_src_3);
		ret |= device_create_file(dev, &dev_attr_stp_state_3);
	}
	if (ds->phy_port_mask & 8) {
		ret |= device_create_file(dev, &dev_attr_master_state_3);
		ret |= device_create_file(dev, &dev_attr_idle_error_count_3);
		ret |= device_create_file(dev, &dev_attr_receive_error_count_3);
		ret |= device_create_file(dev, &dev_attr_pair_skew_3);
		ret |= device_create_file(dev, &dev_attr_packet_generator_3);
		ret |= device_create_file(dev, &dev_attr_link_down_count_3);
		ret |= device_create_file(dev, &dev_attr_cable_length_3);
		ret |= device_create_file(dev, &dev_attr_vct_3);
		ret |= device_create_file(dev, &dev_attr_phy_tx_disable_3);
		ret |= device_create_file(dev, &dev_attr_shutdown_link_on_member_violation_3);
	}
	if (ds->port_mask & 16) {
		ret |= device_create_file(dev, &dev_attr_monitor_src_4);
		ret |= device_create_file(dev, &dev_attr_stp_state_4);
	}
	if (ds->phy_port_mask & 16) {
		ret |= device_create_file(dev, &dev_attr_master_state_4);
		ret |= device_create_file(dev, &dev_attr_idle_error_count_4);
		ret |= device_create_file(dev, &dev_attr_receive_error_count_4);
		ret |= device_create_file(dev, &dev_attr_pair_skew_4);
		ret |= device_create_file(dev, &dev_attr_packet_generator_4);
		ret |= device_create_file(dev, &dev_attr_link_down_count_4);
		ret |= device_create_file(dev, &dev_attr_cable_length_4);
		ret |= device_create_file(dev, &dev_attr_vct_4);
		ret |= device_create_file(dev, &dev_attr_phy_tx_disable_4);
		ret |= device_create_file(dev, &dev_attr_shutdown_link_on_member_violation_4);
	}
	if (ds->port_mask & 32) {
		ret |= device_create_file(dev, &dev_attr_monitor_src_5);
		ret |= device_create_file(dev, &dev_attr_stp_state_5);
	}
	if (ds->phy_port_mask & 32) {
		ret |= device_create_file(dev, &dev_attr_master_state_5);
		ret |= device_create_file(dev, &dev_attr_idle_error_count_5);
		ret |= device_create_file(dev, &dev_attr_receive_error_count_5);
		ret |= device_create_file(dev, &dev_attr_pair_skew_5);
		ret |= device_create_file(dev, &dev_attr_packet_generator_5);
		ret |= device_create_file(dev, &dev_attr_link_down_count_5);
		ret |= device_create_file(dev, &dev_attr_cable_length_5);
		ret |= device_create_file(dev, &dev_attr_vct_5);
		ret |= device_create_file(dev, &dev_attr_phy_tx_disable_5);
		ret |= device_create_file(dev, &dev_attr_shutdown_link_on_member_violation_5);
	}
	if (ds->port_mask & 64) {
		ret |= device_create_file(dev, &dev_attr_monitor_src_6);
		ret |= device_create_file(dev, &dev_attr_stp_state_6);
	}
	if (ds->phy_port_mask & 64) {
		ret |= device_create_file(dev, &dev_attr_master_state_6);
		ret |= device_create_file(dev, &dev_attr_idle_error_count_6);
		ret |= device_create_file(dev, &dev_attr_receive_error_count_6);
		ret |= device_create_file(dev, &dev_attr_pair_skew_6);
		ret |= device_create_file(dev, &dev_attr_packet_generator_6);
		ret |= device_create_file(dev, &dev_attr_link_down_count_6);
		ret |= device_create_file(dev, &dev_attr_cable_length_6);
		ret |= device_create_file(dev, &dev_attr_vct_6);
		ret |= device_create_file(dev, &dev_attr_phy_tx_disable_6);
		ret |= device_create_file(dev, &dev_attr_shutdown_link_on_member_violation_6);
	}

	if (!ret) {
		struct device *hwmon_dev = hwmon_device_register(dev);
		if (IS_ERR(hwmon_dev))
		    	ret = PTR_ERR(hwmon_dev);
	}

	if (ret)
		dev_err(dev, "Error creating sysfs files.\n");

	return 0;
}

static int
__mv88e6123_61_65_phy_read(struct dsa_switch *ds, int addr, int regnum)
{
	return mv88e6xxx_phy_read_indirect(ds, addr, regnum);
}

static int
__mv88e6123_61_65_phy_write(struct dsa_switch *ds,
			    int addr, int regnum, u16 val)
{
	return mv88e6xxx_phy_write_indirect(ds, addr, regnum, val);
}

static int
__mv88e6123_61_65_phy_select_page(struct dsa_switch *ds, int port,
				  int addr, int page)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);

	if (page != ps->phy_page[port]) {
		int ret;

		ret = __mv88e6123_61_65_phy_write(ds, addr, 0x16, page);
		if (ret < 0) {
			ps->phy_page[port] = -1;
			return ret;
		}

		ps->phy_page[port] = page;
	}

	return 0;
}

static int mv88e6123_61_65_port_to_phy_addr(int port)
{
	if (port >= 0 && port <= 6)
		return port;
	return -1;
}

int mv88e6123_61_65_phy_page_read(struct dsa_switch *ds, int port,
				  int page, int regnum)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int addr;
	int ret;

	addr = mv88e6123_61_65_port_to_phy_addr(port);
	if (addr < 0)
		return -EINVAL;

	mutex_lock(&ps->phy_page_mutex[port]);

	ret = __mv88e6123_61_65_phy_select_page(ds, port, addr, page);
	if (ret == 0)
		ret = __mv88e6123_61_65_phy_read(ds, addr, regnum);

	mutex_unlock(&ps->phy_page_mutex[port]);

	return ret;
}

static int
mv88e6123_61_65_phy_page_write(struct dsa_switch *ds, int port,
			       int page, int regnum, u16 val)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int addr;
	int ret;

	addr = mv88e6123_61_65_port_to_phy_addr(port);
	if (addr < 0)
		return -EINVAL;

	mutex_lock(&ps->phy_page_mutex[port]);

	ret = __mv88e6123_61_65_phy_select_page(ds, port, addr, page);
	if (ret == 0)
		ret = __mv88e6123_61_65_phy_write(ds, addr, regnum, val);

	mutex_unlock(&ps->phy_page_mutex[port]);

	return ret;
}

static int
mv88e6123_61_65_phy_read(struct dsa_switch *ds, int port, int regnum)
{
	return mv88e6123_61_65_phy_page_read(ds, port, 0, regnum);
}

static int
mv88e6123_61_65_phy_write(struct dsa_switch *ds,
			  int port, int regnum, u16 val)
{
	return mv88e6123_61_65_phy_page_write(ds, port, 0, regnum, val);
}


static struct mv88e6xxx_hw_stat mv88e6123_61_65_hw_stats[] = {
	{ "in_good_octets", 8, 0x00, },
	{ "in_bad_octets", 4, 0x02, },
	{ "in_unicast", 4, 0x04, },
	{ "in_broadcasts", 4, 0x06, },
	{ "in_multicasts", 4, 0x07, },
	{ "in_pause", 4, 0x16, },
	{ "in_undersize", 4, 0x18, },
	{ "in_fragments", 4, 0x19, },
	{ "in_oversize", 4, 0x1a, },
	{ "in_jabber", 4, 0x1b, },
	{ "in_rx_error", 4, 0x1c, },
	{ "in_fcs_error", 4, 0x1d, },
	{ "out_octets", 8, 0x0e, },
	{ "out_unicast", 4, 0x10, },
	{ "out_broadcasts", 4, 0x13, },
	{ "out_multicasts", 4, 0x12, },
	{ "out_pause", 4, 0x15, },
	{ "excessive", 4, 0x11, },
	{ "collisions", 4, 0x1e, },
	{ "deferred", 4, 0x05, },
	{ "single", 4, 0x14, },
	{ "multiple", 4, 0x17, },
	{ "out_fcs_error", 4, 0x03, },
	{ "late", 4, 0x1f, },
	{ "hist_64bytes", 4, 0x08, },
	{ "hist_65_127bytes", 4, 0x09, },
	{ "hist_128_255bytes", 4, 0x0a, },
	{ "hist_256_511bytes", 4, 0x0b, },
	{ "hist_512_1023bytes", 4, 0x0c, },
	{ "hist_1024_max_bytes", 4, 0x0d, },
	{ "sw_in_discards", 4, 0x110, },
	{ "sw_in_filtered", 2, 0x112, },
	{ "sw_out_filtered", 2, 0x113, },
};

static void
mv88e6123_61_65_get_strings(struct dsa_switch *ds, int port, uint8_t *data)
{
	mv88e6xxx_get_strings(ds, ARRAY_SIZE(mv88e6123_61_65_hw_stats),
			      mv88e6123_61_65_hw_stats, port, data);
}

static void
mv88e6123_61_65_get_ethtool_stats(struct dsa_switch *ds,
				  int port, uint64_t *data)
{
	mv88e6xxx_get_ethtool_stats(ds, ARRAY_SIZE(mv88e6123_61_65_hw_stats),
				    mv88e6123_61_65_hw_stats, port, data);
}

static int mv88e6123_61_65_get_sset_count(struct dsa_switch *ds)
{
	return ARRAY_SIZE(mv88e6123_61_65_hw_stats);
}

static void mv88e6123_61_65_hw_bridge_sync(struct dsa_switch *ds, int force)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int should_flush_atu;
	int i;

	should_flush_atu = 0;
	
	spin_lock(&ps->hw_bridge_state);
	for (i = 0; i < MV88E6XXX_MAX_PORTS; i++) {
		if (force || ps->fid_dirty[i]) {
			int reg;
			int j;
	
			reg = ps->fid[i] << 12;
			if (ds->index == ds->dst->cpu_switch)
				reg |= 1 << ds->dst->cpu_port;
			ps->fid_dirty[i] = 0;
	
			for (j = 0; j < MV88E6XXX_MAX_PORTS; j++) {
				if (i != j && ps->bridge[i] != NULL &&
				    ps->bridge[i] == ps->bridge[j]) {
					reg |= 1 << j;
				}
			}
	
			spin_unlock(&ps->hw_bridge_state);
#if defined(CONFIG_BOARD_IMS_SCU2_ESB) || defined(CONFIG_BOARD_IMS_SCU2_MEZZ) || defined(CONFIG_BOARD_IMS_NIU)
			mv88e6xxx_reg_write(ds, REG_PORT(i), 6, reg);
#endif
			spin_lock(&ps->hw_bridge_state);
		}
	
		if (force || ps->stp_state_dirty[i]) {
			int old_state;
			int new_state;
			int reg;
	
			old_state = 0;
			new_state = ps->stp_state[i];
			ps->stp_state_dirty[i] = 0;
			spin_unlock(&ps->hw_bridge_state);
			reg = mv88e6xxx_reg_read(ds, REG_PORT(i), 4);
			if (reg >= 0) {
				old_state = reg & 0x0003;
				reg &= ~0x0003;
				reg |= new_state;
				mv88e6xxx_reg_write(ds, REG_PORT(i), 4, reg);
			}
			spin_lock(&ps->hw_bridge_state);

			/*
			 * Flush the ATU if we're moving a port from
			 * Learning or Forwarding state to Disabled or
			 * Blocking or Listening state.
			 */
			if (!!(old_state & 2) && !(new_state & 2))
				should_flush_atu = 1;
		}
	}
	spin_unlock(&ps->hw_bridge_state);

	if (should_flush_atu) {
		printk(KERN_INFO "dsa: flushing atu because of stp "
				 "port state change\n");

		mutex_lock(&ps->atu_mutex);
		__atu_flush(ds);
		mutex_unlock(&ps->atu_mutex);
	}
}

static void
set_stp_state(struct dsa_switch *ds, int port, int state)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int hw_state = 0;

	switch (state) {
		case BR_STATE_DISABLED:
			hw_state = 0;
			break;
		case BR_STATE_BLOCKING:
		case BR_STATE_LISTENING:
			hw_state = 1;
			break;
		case BR_STATE_LEARNING:
			hw_state = 2;
			break;
		case BR_STATE_FORWARDING:
			hw_state = 3;
			ps->map_forwarding[port] = 0;
			break;
		default:
			BUG();
	}

	if (ps->map_forwarding[port] == 1) {
		printk(KERN_INFO "dsa: port = %d force hw_state = 3 commanded state = %d\n", port, state);
		hw_state = 3;
	}

	if (ps->stp_state[port] != hw_state) {
		ps->stp_state_dirty[port] = 1;
		ps->stp_state[port] = hw_state;
	}
}

static void
mv88e6123_61_65_bridge_join(struct dsa_switch *ds, int port, void *bridge)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int fid;
	int i;

	spin_lock(&ps->hw_bridge_state);

	fid = 65536;
	for (i = 0; i < MV88E6XXX_MAX_PORTS; i++) {
		if (ps->bridge[i] == bridge) {
			if (ps->fid[i] < fid)
				fid = ps->fid[i];
			ps->fid_dirty[i] = 1;
		}
	}

	ps->bridge[port] = bridge;

	ps->fid_dirty[port] = 1;
	if (fid != 65536)
		ps->fid[port] = fid;

	set_stp_state(ds, port, BR_STATE_DISABLED);

	spin_unlock(&ps->hw_bridge_state);

	schedule_work(&ps->hw_bridge_work);
}

static void
mv88e6123_61_65_bridge_set_stp_state(struct dsa_switch *ds, int port, int state)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);

	spin_lock(&ps->hw_bridge_state);
	set_stp_state(ds, port, state);
	spin_unlock(&ps->hw_bridge_state);

	schedule_work(&ps->hw_bridge_work);
}

static void mv88e6123_61_65_bridge_leave(struct dsa_switch *ds, int port)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	u16 free_fids;
	int i;
	int fid;

	spin_lock(&ps->hw_bridge_state);

	free_fids = 0xffff;
	for (i = 0; i < MV88E6XXX_MAX_PORTS; i++) {
		if ((ds->cpu_port_mask | ds->ext_port_mask) & (1 << i)) {
			if (ps->bridge[i] == ps->bridge[port])
				ps->fid_dirty[i] = 1;
			if (i != port)
				free_fids &= ~(1 << ps->fid[i]);
		}
	}

	fid = ffs(free_fids) - 1;

	ps->fid[port] = fid;
	ps->bridge[port] = NULL;

	set_stp_state(ds, port, BR_STATE_FORWARDING);

	spin_unlock(&ps->hw_bridge_state);

	schedule_work(&ps->hw_bridge_work);

}

static void mv88e6123_61_65_bridge_flush(struct dsa_switch *ds, int port)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);

	schedule_work(&ps->atu_flush_work);
}

static int mv88e6123_61_65_get_eeprom_len(struct dsa_switch *ds)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);

	return ps->eeprom_length;
}

static int __mv88e6123_61_65_eeprom_wait(struct dsa_switch *ds, u16 mask)
{
	int i;

	for (i = 0; i < 1000; i++) {
		int ret;

		ret = mv88e6xxx_reg_read(ds, REG_GLOBAL2, 0x14);
		if (ret < 0)
			return ret;

		if ((ret & mask) == 0x0000)
			return 0;

		usleep_range(1000, 2000);
	}

	return -ETIMEDOUT;
}

static int mv88e6123_61_65_eeprom_loader_wait(struct dsa_switch *ds)
{
	return __mv88e6123_61_65_eeprom_wait(ds, 0x0800);
}

static int mv88e6123_61_65_eeprom_busy_wait(struct dsa_switch *ds)
{
	return __mv88e6123_61_65_eeprom_wait(ds, 0x8000);
}

static int mv88e6123_61_65_read_eeprom_word(struct dsa_switch *ds, int addr)
{
	int ret;

	mv88e6xxx_smi_lock(ds);

	ret = mv88e6xxx_reg_write(ds, REG_GLOBAL2, 0x14,
				  0xc000 | (addr & 0xff));
	if (ret < 0) {
		mv88e6xxx_smi_unlock(ds);
		return ret;
	}

	ret = mv88e6123_61_65_eeprom_busy_wait(ds);
	if (ret < 0) {
		mv88e6xxx_smi_unlock(ds);
		return ret;
	}

	ret = mv88e6xxx_reg_read(ds, REG_GLOBAL2, 0x15);

	mv88e6xxx_smi_unlock(ds);

	return ret;
}

static int mv88e6123_61_65_get_eeprom(struct dsa_switch *ds,
				      struct ethtool_eeprom *eeprom, u8 *data)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int offset;
	int len;
	int ret;

	if (!ps->eeprom_length)
		return -EOPNOTSUPP;

	offset = eeprom->offset;
	len = eeprom->len;
	eeprom->len = 0;

	eeprom->magic = 0xc3ec4951;

	ret = mv88e6123_61_65_eeprom_loader_wait(ds);
	if (ret < 0)
		return ret;

	if (offset & 1) {
		int word;

		word = mv88e6123_61_65_read_eeprom_word(ds, offset >> 1);
		if (word < 0)
			return word;

		*data++ = (word >> 8) & 0xff;

		offset++;
		len--;
		eeprom->len++;
	}

	while (len >= 2) {
		int word;

		word = mv88e6123_61_65_read_eeprom_word(ds, offset >> 1);
		if (word < 0)
			return word;

		*data++ = word & 0xff;
		*data++ = (word >> 8) & 0xff;

		offset += 2;
		len -= 2;
		eeprom->len += 2;
	}

	if (len) {
		int word;

		word = mv88e6123_61_65_read_eeprom_word(ds, offset >> 1);
		if (word < 0)
			return word;

		*data++ = word & 0xff;

		offset++;
		len--;
		eeprom->len++;
	}

	return 0;
}

static int mv88e6123_61_65_eeprom_is_readonly(struct dsa_switch *ds)
{
	int ret;

	ret = mv88e6xxx_reg_read(ds, REG_GLOBAL2, 0x14);
	if (ret < 0)
		return ret;

	if ((ret & 0x0400) == 0)
		return -EROFS;

	return 0;
}

static int
mv88e6123_61_65_write_eeprom_word(struct dsa_switch *ds, int addr, u16 data)
{
	int ret;

	mv88e6xxx_smi_lock(ds);

	ret = mv88e6xxx_reg_write(ds, REG_GLOBAL2, 0x15, data);
	if (ret < 0) {
		mv88e6xxx_smi_unlock(ds);
		return ret;
	}

	ret = mv88e6xxx_reg_write(ds, REG_GLOBAL2, 0x14,
				  0xb000 | (addr & 0xff));
	if (ret < 0) {
		mv88e6xxx_smi_unlock(ds);
		return ret;
	}

	ret = mv88e6123_61_65_eeprom_busy_wait(ds);

	mv88e6xxx_smi_unlock(ds);

	return 0;
}

static int mv88e6123_61_65_set_eeprom(struct dsa_switch *ds,
				      struct ethtool_eeprom *eeprom, u8 *data)
{
	struct mv88e6xxx_priv_state *ps = (void *)(ds + 1);
	int ret;
	int offset;
	int len;

	if (!ps->eeprom_length)
		return -EOPNOTSUPP;

	if (eeprom->magic != 0xc3ec4951)
		return -EINVAL;

	ret = mv88e6123_61_65_eeprom_is_readonly(ds);
	if (ret)
		return ret;

	offset = eeprom->offset;
	len = eeprom->len;
	eeprom->len = 0;

	ret = mv88e6123_61_65_eeprom_loader_wait(ds);
	if (ret < 0)
		return ret;

	if (offset & 1) {
		int word;

		word = mv88e6123_61_65_read_eeprom_word(ds, offset >> 1);
		if (word < 0)
			return word;

		word = (*data++ << 8) | (word & 0xff);

		ret = mv88e6123_61_65_write_eeprom_word(ds, offset >> 1, word);
		if (ret < 0)
			return ret;

		offset++;
		len--;
		eeprom->len++;
	}

	while (len >= 2) {
		int word;

		word = *data++;
		word |= *data++ << 8;

		ret = mv88e6123_61_65_write_eeprom_word(ds, offset >> 1, word);
		if (ret < 0)
			return ret;

		offset += 2;
		len -= 2;
		eeprom->len += 2;
	}

	if (len) {
		int word;

		word = mv88e6123_61_65_read_eeprom_word(ds, offset >> 1);
		if (word < 0)
			return word;

		word = (word & 0xff00) | *data++;

		ret = mv88e6123_61_65_write_eeprom_word(ds, offset >> 1, word);
		if (ret < 0)
			return ret;

		offset++;
		len--;
		eeprom->len++;
	}

	return 0;
}

static int mv88e6123_61_65_get_regs_len(struct dsa_switch *ds, int port)
{
	return 32 * sizeof(u32);
}

static void mv88e6123_61_65_get_regs(struct dsa_switch *ds, int port,
				     struct ethtool_regs *regs, void *_p)
{
	u32 *p = _p;
	int i;

	regs->version = 0;

	memset(p, 0, 32 * sizeof(u32));

	for (i = 0; i < 32; i++) {
		int ret;

		ret = mv88e6xxx_reg_read(ds, REG_PORT(port), i);
		if (ret >= 0)
			p[i] = ret;
		else
			p[i] = 0xffffffff;
	}
}

static int	mv88e6123_61_65_get_rstdly(void)
{
	return RESET_DELAY;
}

static struct dsa_switch_driver mv88e6123_61_65_switch_driver = {
	.tag_protocol		= cpu_to_be16(ETH_P_EDSA),
	.priv_size		= sizeof(struct mv88e6xxx_priv_state),
	.probe			= mv88e6123_61_65_probe,
	.setup			= mv88e6123_61_65_setup,
	.set_addr		= mv88e6xxx_set_addr_indirect,
	.phy_read		= mv88e6123_61_65_phy_read,
	.phy_write		= mv88e6123_61_65_phy_write,
	.poll_link		= mv88e6xxx_poll_link,
	.get_strings		= mv88e6123_61_65_get_strings,
	.get_ethtool_stats	= mv88e6123_61_65_get_ethtool_stats,
	.get_sset_count		= mv88e6123_61_65_get_sset_count,
	.bridge_join		= mv88e6123_61_65_bridge_join,
	.bridge_set_stp_state	= mv88e6123_61_65_bridge_set_stp_state,
	.bridge_leave		= mv88e6123_61_65_bridge_leave,
	.bridge_flush		= mv88e6123_61_65_bridge_flush,
	.get_eeprom_len		= mv88e6123_61_65_get_eeprom_len,
	.get_eeprom		= mv88e6123_61_65_get_eeprom,
	.set_eeprom		= mv88e6123_61_65_set_eeprom,
	.get_regs_len		= mv88e6123_61_65_get_regs_len,
	.get_regs		= mv88e6123_61_65_get_regs,
	.get_settings		= mv88e6xxx_get_settings,
	.get_reset_delay	= mv88e6123_61_65_get_rstdly,
};

static int __init mv88e6123_61_65_init(void)

{
	register_switch_driver(&mv88e6123_61_65_switch_driver);
	return 0;
}
module_init(mv88e6123_61_65_init);

static void __exit mv88e6123_61_65_cleanup(void)
{
	unregister_switch_driver(&mv88e6123_61_65_switch_driver);
}
module_exit(mv88e6123_61_65_cleanup);
