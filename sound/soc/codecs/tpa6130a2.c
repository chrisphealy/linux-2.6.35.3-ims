/*
 * ALSA SoC Texas Instruments TPA6130A2 headset stereo amplifier driver
 *
 * Copyright (C) Nokia Corporation
 *
 * Author: Peter Ujfalusi <peter.ujfalusi@nokia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <linux/module.h>
#include <linux/errno.h>
#include <linux/device.h>
#include <linux/i2c.h>
#include <linux/gpio.h>
#include <linux/regulator/consumer.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <sound/tpa6130a2-plat.h>
#include <sound/soc.h>
#include <sound/tlv.h>
#include <linux/device.h>
#include <linux/sysfs.h>

#include "tpa6130a2.h"

static struct i2c_client *tpa6130a2_client;

/* This struct is used to save the context */
struct tpa6130a2_data {
	struct mutex mutex;
	unsigned char regs[TPA6130A2_CACHEREGNUM];
	struct regulator *supply;
	int power_gpio;
	u8 power_state:1;
	enum tpa_model id;
	struct device *dev;

	int shutdown_in;
};

static int tpa6130a2_i2c_read(int reg)
{
	struct tpa6130a2_data *data;
	int val;
	int enabled = true;

	BUG_ON(tpa6130a2_client == NULL);
	data = i2c_get_clientdata(tpa6130a2_client);
	if ( data->shutdown_in ) {
		enabled = gpio_get_value( data->shutdown_in );
		pr_debug(KERN_DEBUG"%s: %s\n", __func__, enabled?"enabled":"disabled");
	}

	/* If powered off, return the cached value */
	if (enabled && data->power_state) {
		val = i2c_smbus_read_byte_data(tpa6130a2_client, reg);
		if (val < 0)
			dev_err(&tpa6130a2_client->dev, "Read failed\n");
		else
			data->regs[reg] = val;
	} else {
		val = data->regs[reg];
	}

	return val;
}

static int tpa6130a2_i2c_write(int reg, u8 value)
{
	struct tpa6130a2_data *data;
	int val = 0;
	int enabled = true;

	BUG_ON(tpa6130a2_client == NULL);
	data = i2c_get_clientdata(tpa6130a2_client);
	if ( data->shutdown_in ) {
		enabled = gpio_get_value( data->shutdown_in );
		pr_debug(KERN_DEBUG"%s: %s\n", __func__, enabled?"enabled":"disabled");
	}

	if (enabled && data->power_state) {
		val = i2c_smbus_write_byte_data(tpa6130a2_client, reg, value);
		if (val < 0) {
			dev_err(&tpa6130a2_client->dev, "Write failed\n");
			return val;
		}
	}

	/* Either powered on or off, we save the context */
	data->regs[reg] = value;

	return val;
}

static u8 tpa6130a2_read(int reg)
{
	struct tpa6130a2_data *data;

	BUG_ON(tpa6130a2_client == NULL);
	data = i2c_get_clientdata(tpa6130a2_client);

	return data->regs[reg];
}

static int tpa6130a2_initialize(void)
{
	struct tpa6130a2_data *data;
	int i, ret = 0;

	BUG_ON(tpa6130a2_client == NULL);
	data = i2c_get_clientdata(tpa6130a2_client);

	for (i = 1; i < TPA6130A2_REG_VERSION; i++) {
		ret = tpa6130a2_i2c_write(i, data->regs[i]);
		if (ret < 0)
			break;
	}

	return ret;
}

static int tpa6130a2_power(u8 power)
{
	struct	tpa6130a2_data *data;
	u8	val;
	int	ret = 0;

	BUG_ON(tpa6130a2_client == NULL);
	data = i2c_get_clientdata(tpa6130a2_client);

	mutex_lock(&data->mutex);
	if (power == data->power_state)
		goto exit;

	if (power) {
		ret = regulator_enable(data->supply);
		if (ret != 0) {
			dev_err(&tpa6130a2_client->dev,
				"Failed to enable supply: %d\n", ret);
			goto exit;
		}
		/* Power on */
		if (data->power_gpio >= 0)
			gpio_set_value(data->power_gpio, 1);

		data->power_state = 1;
		ret = tpa6130a2_initialize();
		if (ret < 0) {
			dev_err(&tpa6130a2_client->dev,
				"Failed to initialize chip\n");
			if (data->power_gpio >= 0)
				gpio_set_value(data->power_gpio, 0);
			regulator_disable(data->supply);
			data->power_state = 0;
			goto exit;
		}
	} else {
		/* set SWS */
		val = tpa6130a2_read(TPA6130A2_REG_CONTROL);
		val |= TPA6130A2_SWS;
		tpa6130a2_i2c_write(TPA6130A2_REG_CONTROL, val);

		/* Power off */
		if (data->power_gpio >= 0)
			gpio_set_value(data->power_gpio, 0);

		ret = regulator_disable(data->supply);
		if (ret != 0) {
			dev_err(&tpa6130a2_client->dev,
				"Failed to disable supply: %d\n", ret);
			goto exit;
		}

		data->power_state = 0;
	}

exit:
	mutex_unlock(&data->mutex);
	return ret;
}

static int tpa6130a2_get_volsw(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol)
{
	struct soc_mixer_control *mc =
		(struct soc_mixer_control *)kcontrol->private_value;
	struct tpa6130a2_data *data;
	unsigned int reg = mc->reg;
	unsigned int shift = mc->shift;
	int max = mc->max;
	unsigned int mask = (1 << fls(max)) - 1;
	unsigned int invert = mc->invert;

	BUG_ON(tpa6130a2_client == NULL);
	data = i2c_get_clientdata(tpa6130a2_client);

	mutex_lock(&data->mutex);

	ucontrol->value.integer.value[0] =
		(tpa6130a2_read(reg) >> shift) & mask;

	if (invert)
		ucontrol->value.integer.value[0] =
			max - ucontrol->value.integer.value[0];

	mutex_unlock(&data->mutex);
	return 0;
}

static int tpa6130a2_put_volsw(struct snd_kcontrol *kcontrol,
		struct snd_ctl_elem_value *ucontrol)
{
	struct soc_mixer_control *mc =
		(struct soc_mixer_control *)kcontrol->private_value;
	struct tpa6130a2_data *data;
	unsigned int reg = mc->reg;
	unsigned int shift = mc->shift;
	int max = mc->max;
	unsigned int mask = (1 << fls(max)) - 1;
	unsigned int invert = mc->invert;
	unsigned int val = (ucontrol->value.integer.value[0] & mask);
	unsigned int val_reg;

	BUG_ON(tpa6130a2_client == NULL);
	data = i2c_get_clientdata(tpa6130a2_client);

	if (invert)
		val = max - val;

	mutex_lock(&data->mutex);

	val_reg = tpa6130a2_read(reg);
	if (((val_reg >> shift) & mask) == val) {
		mutex_unlock(&data->mutex);
		return 0;
	}

	val_reg &= ~(mask << shift);
	val_reg |= val << shift;
	tpa6130a2_i2c_write(reg, val_reg);

	mutex_unlock(&data->mutex);

	return 1;
}

/*
 * TPA6130 volume. From -59.5 to 4 dB with increasing step size when going
 * down in gain.
 */
static const unsigned int tpa6130_tlv[] = {
#if 0 // "Too big dB_scale TLV size: 1200"
	TLV_DB_RANGE_HEAD(50),
	 0,  1, TLV_DB_SCALE_ITEM(-5950, 600, 0),
	 1,  2, TLV_DB_SCALE_ITEM(-5350, 350, 0),
	 2,  3, TLV_DB_SCALE_ITEM(-5000, 250, 0),
	 3,  4, TLV_DB_SCALE_ITEM(-4750, 200, 0),
	 4,  5, TLV_DB_SCALE_ITEM(-4550, 160, 0),
	 5,  6, TLV_DB_SCALE_ITEM(-4390, 160, 0),
	 6,  7, TLV_DB_SCALE_ITEM(-4140, 250, 0),
	 7,  8, TLV_DB_SCALE_ITEM(-3950, 300, 0),
	 8,  9, TLV_DB_SCALE_ITEM(-3650, 120, 0),
	 9, 10, TLV_DB_SCALE_ITEM(-3530, 200, 0),
	10, 11, TLV_DB_SCALE_ITEM(-3330, 160, 0),
	11, 12, TLV_DB_SCALE_ITEM(-3170, 130, 0),
	12, 13, TLV_DB_SCALE_ITEM(-3040, 180, 0),
	13, 14, TLV_DB_SCALE_ITEM(-2860, 150, 0),
	14, 15, TLV_DB_SCALE_ITEM(-2710,  80, 0),
	15, 16, TLV_DB_SCALE_ITEM(-2630, 160, 0),
	16, 17, TLV_DB_SCALE_ITEM(-2470, 100, 0),
	17, 18, TLV_DB_SCALE_ITEM(-2370, 120, 0),
	18, 19, TLV_DB_SCALE_ITEM(-2250,  80, 0),
	19, 20, TLV_DB_SCALE_ITEM(-2170, 120, 0),
	20, 21, TLV_DB_SCALE_ITEM(-2050,  90, 0),
	21, 22, TLV_DB_SCALE_ITEM(-1960,  80, 0),
	22, 23, TLV_DB_SCALE_ITEM(-1880, 100, 0),
	23, 25, TLV_DB_SCALE_ITEM(-1780,  80, 0),
	25, 26, TLV_DB_SCALE_ITEM(-1620, 100, 0),
	26, 27, TLV_DB_SCALE_ITEM(-1520,  70, 0),
	27, 28, TLV_DB_SCALE_ITEM(-1450,  80, 0),
	28, 32, TLV_DB_SCALE_ITEM(-1370,  70, 0),
	32, 35, TLV_DB_SCALE_ITEM(-1090,  60, 0),
	35, 36, TLV_DB_SCALE_ITEM(-900,  50, 0),
	36, 37, TLV_DB_SCALE_ITEM(-850,  70, 0),
	37, 38, TLV_DB_SCALE_ITEM(-780,  60, 0),
	38, 39, TLV_DB_SCALE_ITEM(-720,  50, 0),
	39, 40, TLV_DB_SCALE_ITEM(-670,  60, 0),
	40, 42, TLV_DB_SCALE_ITEM(-610,  50, 0),
	42, 43, TLV_DB_SCALE_ITEM(-510,  60, 0),
	43, 44, TLV_DB_SCALE_ITEM(-450,  40, 0),
	44, 45, TLV_DB_SCALE_ITEM(-410,  60, 0),
	45, 46, TLV_DB_SCALE_ITEM(-350,  40, 0),
	46, 48, TLV_DB_SCALE_ITEM(-310,  50, 0),
	48, 49, TLV_DB_SCALE_ITEM(-260,  40, 0),
	49, 50, TLV_DB_SCALE_ITEM(-170,  50, 0),
	50, 51, TLV_DB_SCALE_ITEM(-120,  40, 0),
	51, 52, TLV_DB_SCALE_ITEM(-80,  50, 0),
	52, 55, TLV_DB_SCALE_ITEM(-30,  40, 0),
	55, 56, TLV_DB_SCALE_ITEM( 90,  50, 0),
	56, 57, TLV_DB_SCALE_ITEM(140,  30, 0),
	57, 61, TLV_DB_SCALE_ITEM(170,  40, 0),
	61, 62, TLV_DB_SCALE_ITEM(330,  30, 0),
	62, 63, TLV_DB_SCALE_ITEM(360,  40, 0),
#else
	TLV_DB_RANGE_HEAD(7),
	 0,  1, TLV_DB_SCALE_ITEM(-5950, 600, 0),
	 1,  2, TLV_DB_SCALE_ITEM(-5350, 440, 0),
	 2, 10, TLV_DB_SCALE_ITEM(-4910, 200, 0),
	10, 18, TLV_DB_SCALE_ITEM(-3310, 135, 0),
	18, 22, TLV_DB_SCALE_ITEM(-2230,  90, 0),
	22, 32, TLV_DB_SCALE_ITEM(-1870,  80, 0),
	32, 63, TLV_DB_SCALE_ITEM(-1150,  50, 0),

#endif
};

static const struct snd_kcontrol_new tpa6130a2_controls[] = {
	SOC_SINGLE_EXT_TLV("TPA6130A2 Headphone Playback Volume",
		       TPA6130A2_REG_VOL_MUTE, 0, 0x3f, 0,
		       tpa6130a2_get_volsw, tpa6130a2_put_volsw,
		       tpa6130_tlv),
};

static const unsigned int tpa6140_tlv[] = {
	TLV_DB_RANGE_HEAD(3),
	0, 8, TLV_DB_SCALE_ITEM(-5900, 400, 0),
	9, 16, TLV_DB_SCALE_ITEM(-2500, 200, 0),
	17, 31, TLV_DB_SCALE_ITEM(-1000, 100, 0),
};

static const struct snd_kcontrol_new tpa6140a2_controls[] = {
	SOC_SINGLE_EXT_TLV("TPA6140A2 Headphone Playback Volume",
		       TPA6130A2_REG_VOL_MUTE, 1, 0x1f, 0,
		       tpa6130a2_get_volsw, tpa6130a2_put_volsw,
		       tpa6140_tlv),
};

/* this interrupt detects the state of the externally controlled
 * shutdown pin. when the amp is brought out of shutdown, the
 * shadowed register values must be restored. shutdown mode is low true,
 * so the detection state of the interrupt is IRQF_TRIGGER_RISING.
 */
static irqreturn_t tpa6130a2_shutdown_irq_handler(int irq, void *dev_id)
{
	struct tpa6130a2_data *data = (struct tpa6130a2_data *)dev_id;
	int _shutdown = gpio_get_value( data->shutdown_in );
	pr_debug(KERN_DEBUG"%s %s\n",__func__, _shutdown?"enabled":"disabled");

	/* 
	 * Cause a sysfs attribute change to alert the application
	 * layer that a state change has occured.
	 */
	sysfs_notify(&data->dev->kobj, NULL, "shutdown_in");
	if ( _shutdown ) {
		tpa6130a2_i2c_write(TPA6130A2_REG_CONTROL, data->regs[TPA6130A2_REG_CONTROL]);
		tpa6130a2_i2c_write(TPA6130A2_REG_VOL_MUTE, data->regs[TPA6130A2_REG_VOL_MUTE]);
	}

	return IRQ_HANDLED;
}

static ssize_t shutdown_in_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct tpa6130a2_data *data = platform_get_drvdata(pdev);
	int _shutdown = gpio_get_value( data->shutdown_in );


	return sprintf(buf, "%d\n", _shutdown);
}

static DEVICE_ATTR(shutdown_in, S_IRUGO, shutdown_in_show, NULL);

/*
 * Enable or disable channel (left or right)
 * The bit number for mute and amplifier are the same per channel:
 * bit 6: Right channel
 * bit 7: Left channel
 * in both registers.
 */
static void tpa6130a2_channel_enable(u8 channel, int enable)
{
	u8	val;

	if (enable) {
		/* Enable channel */
		/* Enable amplifier */
		val = tpa6130a2_read(TPA6130A2_REG_CONTROL);
		val |= channel;
		val &= ~TPA6130A2_SWS;
		tpa6130a2_i2c_write(TPA6130A2_REG_CONTROL, val);

		/* Unmute channel */
		val = tpa6130a2_read(TPA6130A2_REG_VOL_MUTE);
		val &= ~channel;
		tpa6130a2_i2c_write(TPA6130A2_REG_VOL_MUTE, val);
	} else {
		/* Disable channel */
		/* Mute channel */
		val = tpa6130a2_read(TPA6130A2_REG_VOL_MUTE);
		val |= channel;
		tpa6130a2_i2c_write(TPA6130A2_REG_VOL_MUTE, val);

		/* Disable amplifier */
		val = tpa6130a2_read(TPA6130A2_REG_CONTROL);
		val &= ~channel;
		tpa6130a2_i2c_write(TPA6130A2_REG_CONTROL, val);
	}
}

int tpa6130a2_stereo_enable(struct snd_soc_codec *codec, int enable)
{
	int ret = 0;

	if (tpa6130a2_client == NULL)
		return -ENODEV;

	if (enable) {
		ret = tpa6130a2_power(1);
		if (ret < 0)
			return ret;
		tpa6130a2_channel_enable(TPA6130A2_HP_EN_R | TPA6130A2_HP_EN_L,
					 1);
	} else {
		tpa6130a2_channel_enable(TPA6130A2_HP_EN_R | TPA6130A2_HP_EN_L,
					 0);
		ret = tpa6130a2_power(0);
	}

	return ret;
}
EXPORT_SYMBOL_GPL(tpa6130a2_stereo_enable);

int tpa6130a2_add_controls(struct snd_soc_codec *codec)
{
	struct	tpa6130a2_data *data;

	if (tpa6130a2_client == NULL)
		return -ENODEV;

	data = i2c_get_clientdata(tpa6130a2_client);

	if (data->id == TPA6140A2)
		return snd_soc_add_controls(codec, tpa6140a2_controls,
						ARRAY_SIZE(tpa6140a2_controls));
	else
		return snd_soc_add_controls(codec, tpa6130a2_controls,
						ARRAY_SIZE(tpa6130a2_controls));
}
EXPORT_SYMBOL_GPL(tpa6130a2_add_controls);

static struct attribute *tpa6130a2_bin_attrs[] = {
	&dev_attr_shutdown_in.attr,
	NULL
};

static const struct attribute_group tpa6130a2_group = {

	.attrs = tpa6130a2_bin_attrs,

};

static int __devinit tpa6130a2_probe(struct i2c_client *client,
				     const struct i2c_device_id *id)
{
	struct device *dev;
	struct tpa6130a2_data *data;
	struct tpa6130a2_platform_data *pdata;
	const char *regulator;
	int ret;

	dev = &client->dev;

	if (client->dev.platform_data == NULL) {
		dev_err(dev, "Platform data not set\n");
		dump_stack();
		return -ENODEV;
	}

	data = kzalloc(sizeof(*data), GFP_KERNEL);
	if (data == NULL) {
		dev_err(dev, "Can not allocate memory\n");
		return -ENOMEM;
	}

	tpa6130a2_client = client;

	i2c_set_clientdata(tpa6130a2_client, data);

	pdata = client->dev.platform_data;
	data->power_gpio = pdata->power_gpio;
	data->id = pdata->id;
	data->shutdown_in = pdata->shutdown_in;
	data->dev = dev;

	mutex_init(&data->mutex);

	/* Set default register values */
	data->regs[TPA6130A2_REG_CONTROL] =	TPA6130A2_SWS;
	data->regs[TPA6130A2_REG_VOL_MUTE] =	TPA6130A2_MUTE_R |
						TPA6130A2_MUTE_L;

	if (data->power_gpio >= 0) {
		ret = gpio_request(data->power_gpio, "tpa6130a2 enable");
		if (ret < 0) {
			dev_err(dev, "Failed to request power GPIO (%d)\n",
				data->power_gpio);
			goto err_gpio;
		}
		gpio_direction_output(data->power_gpio, 0);
	}

	switch (data->id) {
	default:
		dev_warn(dev, "Unknown TPA model (%d). Assuming 6130A2\n",
			 pdata->id);
	case TPA6130A2:
		regulator = "Vdd";
		break;
	case TPA6140A2:
		regulator = "AVdd";
		break;
	}

	data->supply = regulator_get(dev, regulator);
	if (IS_ERR(data->supply)) {
		ret = PTR_ERR(data->supply);
		dev_err(dev, "Failed to request supply: %d\n", ret);
		goto err_regulator;
	}

	ret = tpa6130a2_power(1);
	if (ret != 0)
		goto err_power;


	/* Read version */
	ret = tpa6130a2_i2c_read(TPA6130A2_REG_VERSION) &
				 TPA6130A2_VERSION_MASK;
	if ((ret != 1) && (ret != 2))
		dev_warn(dev, "UNTESTED version detected (%d)\n", ret);

	/* Disable the chip */
	ret = tpa6130a2_power(0);
	if (ret != 0)
		goto err_power;

	if ( data->shutdown_in ) {
		/* configure interrupt to detect enable (removal of shutdown signal) */
		ret = request_threaded_irq( gpio_to_irq( data->shutdown_in ), NULL, tpa6130a2_shutdown_irq_handler,
				IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, client->dev.driver->name, data);

		if (ret) {
			dev_err(&client->dev, "Failed to register interrupt\n");
			goto err_power;
		}
	}

	ret = sysfs_create_group(&dev->kobj, &tpa6130a2_group);
	if (ret) {
		dev_err(&client->dev, "Failed to create sysfs entries\n");
		goto err_power;
	}

	return 0;

err_power:
	regulator_put(data->supply);
err_regulator:
	if (data->power_gpio >= 0)
		gpio_free(data->power_gpio);
err_gpio:
	kfree(data);
	i2c_set_clientdata(tpa6130a2_client, NULL);
	tpa6130a2_client = NULL;

	return ret;
}

static int __devexit tpa6130a2_remove(struct i2c_client *client)
{
	struct tpa6130a2_data *data = i2c_get_clientdata(client);

	free_irq( gpio_to_irq( data->shutdown_in ), data);
	tpa6130a2_power(0);

	/* Remove sysfs entries */
	sysfs_remove_group(&data->dev->kobj, &tpa6130a2_group);

	if (data->power_gpio >= 0)
		gpio_free(data->power_gpio);

	regulator_put(data->supply);

	kfree(data);
	tpa6130a2_client = NULL;

	return 0;
}

static const struct i2c_device_id tpa6130a2_id[] = {
	{ "tpa6130a2", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, tpa6130a2_id);

static struct i2c_driver tpa6130a2_i2c_driver = {
	.driver = {
		.name = "tpa6130a2",
		.owner = THIS_MODULE,
	},
	.probe = tpa6130a2_probe,
	.remove = __devexit_p(tpa6130a2_remove),
	.id_table = tpa6130a2_id,
};

static int __init tpa6130a2_init(void)
{
	return i2c_add_driver(&tpa6130a2_i2c_driver);
}

static void __exit tpa6130a2_exit(void)
{
	i2c_del_driver(&tpa6130a2_i2c_driver);
}

MODULE_AUTHOR("Peter Ujfalusi");
MODULE_DESCRIPTION("TPA6130A2 Headphone amplifier driver");
MODULE_LICENSE("GPL");

module_init(tpa6130a2_init);
module_exit(tpa6130a2_exit);
