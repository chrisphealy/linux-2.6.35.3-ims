/*
 * Copyright 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/slab.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/fsl_devices.h>
#include <linux/spi/spi.h>
#include <linux/i2c.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/regulator/consumer.h>
#include <linux/pmic_external.h>
#include <linux/pmic_status.h>
#include <linux/ipu.h>
#include <linux/mxcfb.h>
#include <linux/pwm_backlight.h>
#include <mach/common.h>
#include <mach/hardware.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>
#include <asm/mach/keypad.h>
#include <asm/mach/flash.h>
#include <mach/gpio.h>
#include <mach/mmc.h>
#include <mach/mxc_dvfs.h>
#include <mach/mxc_edid.h>
#include <mach/iomux-RDU.h>
#include <mach/i2c.h>
#include <mach/mx51.h>
#include <mach/mxc_iim.h>
#include <linux/proc_fs.h>

#include <sound/tpa6130a2-plat.h>

#ifdef CONFIG_MARVELL_M88E6161
#include <net/marvellswitch_platform.h>
#endif
#if defined(CONFIG_TOUCHSCREEN_ATMEL_MXT) || defined(CONFIG_TOUCHSCREEN_ATMEL_MXT_MODULE)
#include <linux/i2c/atmel_mxt_ts.h>
#elif defined(CONFIG_TOUCHSCREEN_QT602240) || defined(CONFIG_TOUCHSCREEN_QT602240_MODULE)
#include <linux/i2c/qt602240_ts.h>
#endif

#include "devices.h"
#include "crm_regs.h"
#include "usb.h"

/*!
 * @file mach-mx51/mx51_babbage.c
 *
 * @brief This file contains the board specific initialization routines.
 *
 * @ingroup MSL_MX51
 */

#define RDU_SD3_CD			(0*32 + 0)	/* GPIO_1_0  */
#define RDU_SD3_WP			(0*32 + 1)	/* GPIO_1_1  */
#define RDU_I2C4_SDA			(0*32 + 2)      /* GPIO_1_2  */
#define RDU_WDOG_B			(0*32 + 4)	/* GPIO_1_4  */
#define RDU_SD2_WP			(0*32 + 5)	/* GPIO_1_5  */
#define RDU_SD2_CD			(0*32 + 6)	/* GPIO_1_6  */
#define RDU_ADV7180_PWRDWN		(0*32 + 7)	/* GPIO_1_7  */
#define RDU_INT_FROM_PMIC		(0*32 + 8)	/* GPIO_1_8  */
#define HP_AMP_SHUTDOWN_B		(0*32 + 9)	/* GPIO_1_9  */
#define RDU_USB_CLK_EN_B            	(0*32 + 19)	/* GPIO_1_19 */
#define RDU_BABBAGE_BOARD_TYPE          (0*32 + 22)	/* GPIO_1_22 */ 
#define RDU_RS485_2_TX_ENABLE		(0*32 + 23)	/* GPIO_1_23 */
#define RDU_SYS_ON_OFF_REQ		(0*32 + 24)	/* GPIO_1_24 */

#define RDU_CARD_RST_OUT		(1*32 + 10)	/* GPIO_2_10 */
#define RDU_DIAG_LED_GPIO		(1*32 + 11)	/* GPIO_2_11 */
#define RDU_RST_ENET_B			(1*32 + 14)	/* GPIO_2_14 */
#define RDU_ENET_IRQ_B			(1*32 + 15)	/* GPIO_2_15 */
#define RDU_CARD_ON_OUT			(1*32 + 16)	/* GPIO_2_16 */

#define RDU_26M_OSC_EN			(2*32 + 1)	/* GPIO_3_1  */
#define RDU_LVDS_PWR_DWN_B		(2*32 + 3)	/* GPIO_3_3  */
#define RDU_I2C4_SCL			(2*32 + 4)	/* GPIO_3_4  */
#define RDU_INT_TOUCH_B			(2*32 + 12)	/* GPIO_3_12 */
#define RDU_RST_TOUCH_B			(2*32 + 13)	/* GPIO_3_13 */
#define RDU_MDIO_1_8V			(2*32 + 25)	/* GPIO_3_25 */
#define RDU_MDC_1_8V			(2*32 + 26)	/* GPIO_3_26 */
#define RDU_DDS_SWITCH_INT_B		(2*32 + 28)	/* GPIO_3_28 */

#define RDU_SYSTEM_TYPE_0		(3*32 + 3)	/* GPIO_4_3  */
#define RDU_SYSTEM_TYPE_1		(3*32 + 4)	/* GPIO_4_4  */
#define RDU_SYSTEM_TYPE_2		(3*32 + 5)	/* GPIO_4_5  */
#define RDU_SYSTEM_TYPE_3		(3*32 + 6)	/* GPIO_4_6  */
#define RDU_RST_USB2_PHY		(3*32 + 7)	/* GPIO_4_7  */
#define RDU_RST_USB1_PHY		(3*32 + 8)	/* GPIO_4_8  */
#define RDU_RST_CAPTURE_B		(3*32 + 9)	/* GPIO_4_9  */
#define RDU_EEPROM2_DO			(3*32 + 11)	/* GPIO_4_11 */
#define RDU_EEPROM2_DI			(3*32 + 12)	/* GPIO_4_12 */
#define RDU_CAPTURE_IRQ_B		(3*32 + 13)	/* GPIO_4_13 */
#define RDU_EEPROM2_CS			(3*32 + 14)	/* GPIO_4_14 */
#define RDU_EEPROM2_CLK			(3*32 + 15)	/* GPIO_4_15 */
#define RDU_CSP1_SS0_GPIO		(3*32 + 24)	/* GPIO_4_24 */
#define RDU_AUDIO_CLK_EN_B		(3*32 + 26)	/* GPIO_4_26 */

// Defines the mmc bus bit width modes that the RDU supports.
// All 3 of the mmc ports are only wired for 4 bit operation,
// they can support 1-bit or 4-bit operation.
// mmcXBusWidth will be passed at the kernel command line
// and the following structures define the possible
// strings that can be passed in, and what bit mode they
// correlate to.
// For this version setting of mmc1 is supported and mmc2 & mmc3 are not!
struct mmc_bus_width {
	char *name;
	unsigned int bus_width;
};

enum {
	MMCBUSWIDTH_1BIT = 0,
	MMCBUSWIDTH_4BIT,
};

// Default mmc1 to 4-bit bus width
int mmc1_mode = MMCBUSWIDTH_4BIT;
static struct mmc_bus_width mmc_bus_widths[] = {
	{
	.name = "1-bit",
	.bus_width = 0, // No capabilities above 1-bit mode
	},
	{
	.name = "4-bit",
	.bus_width = MMC_CAP_4_BIT_DATA, // 4-bit bus
	},
};


extern int __init mx51_babbage_init_mc13892(void);
extern struct cpu_wp *(*get_cpu_wp)(int *wp);
extern void (*set_num_cpu_wp)(int num);
extern struct dvfs_wp *(*get_dvfs_core_wp)(int *wp);

static int num_cpu_wp;

static struct pad_desc mx51babbage_pads[] = {
	/* UART1 */
	MX51_PAD_UART1_RXD__UART1_RXD,
	MX51_PAD_UART1_TXD__UART1_TXD,
	MX51_PAD_UART1_RTS__UART1_RTS,
	MX51_PAD_UART1_CTS__UART1_CTS,

	/* UART 2 */
	MX51_PAD_UART2_RXD__UART2_RXD,
	MX51_PAD_UART2_TXD__UART2_TXD,

	/* UART3 */
	MX51_PAD_EIM_D25__UART3_RXD,
	MX51_PAD_EIM_D26__UART3_TXD,

	/* USB HOST1 */
	MX51_PAD_USBH1_STP__USBH1_STP,
	MX51_PAD_USBH1_CLK__USBH1_CLK,
	MX51_PAD_USBH1_DIR__USBH1_DIR,
	MX51_PAD_USBH1_NXT__USBH1_NXT,
	MX51_PAD_USBH1_DATA0__USBH1_DATA0,
	MX51_PAD_USBH1_DATA1__USBH1_DATA1,
	MX51_PAD_USBH1_DATA2__USBH1_DATA2,
	MX51_PAD_USBH1_DATA3__USBH1_DATA3,
	MX51_PAD_USBH1_DATA4__USBH1_DATA4,
	MX51_PAD_USBH1_DATA5__USBH1_DATA5,
	MX51_PAD_USBH1_DATA6__USBH1_DATA6,
	MX51_PAD_USBH1_DATA7__USBH1_DATA7,

	/* USB HOST2 */
	MX51_PAD_EIM_A24__USBH2_CLK,
	MX51_PAD_EIM_A25__USBH2_DIR,
	MX51_PAD_EIM_A26__USBH2_STP,
	MX51_PAD_EIM_A27__USBH2_NXT,
	MX51_PAD_EIM_D16__USBH2_DATA0,
	MX51_PAD_EIM_D17__USBH2_DATA1,
	MX51_PAD_EIM_D18__USBH2_DATA2,
	MX51_PAD_EIM_D19__USBH2_DATA3,
	MX51_PAD_EIM_D20__USBH2_DATA4,
	MX51_PAD_EIM_D21__USBH2_DATA5,
	MX51_PAD_EIM_D22__USBH2_DATA6,
	MX51_PAD_EIM_D23__USBH2_DATA7,

	MX51_PAD_GPIO_1_0__GPIO_1_0, 
	MX51_PAD_GPIO_1_1__GPIO_1_1,
	MX51_PAD_GPIO_1_4__GPIO_1_4,
	MX51_PAD_GPIO_1_5__GPIO_1_5,
	MX51_PAD_GPIO_1_6__GPIO_1_6,
	MX51_PAD_GPIO_1_8__GPIO_1_8,

	MX51_PAD_UART3_RXD__GPIO_1_22,
	MX51_PAD_UART3_TXD__GPIO_1_23,

	MX51_PAD_EIM_A16__GPIO_2_10,
	MX51_PAD_EIM_A17__GPIO_2_11,
	MX51_PAD_EIM_A20__GPIO_2_14,
	MX51_PAD_EIM_A21__GPIO_2_15,
	MX51_PAD_EIM_A22__GPIO_2_16,
	
	MX51_PAD_DI1_D0_CS__GPIO_3_3,
	MX51_PAD_NANDF_D0__GPIO_4_8,
	MX51_PAD_NANDF_D1__GPIO_4_7,
	MX51_PAD_NANDF_D2__GPIO_4_6,
	MX51_PAD_NANDF_D3__GPIO_4_5,
	MX51_PAD_NANDF_D4__GPIO_4_4,
	MX51_PAD_NANDF_D5__GPIO_4_3,
	MX51_PAD_NANDF_D12__GPIO_3_28,
	MX51_PAD_NANDF_D14__GPIO_3_26,
	MX51_PAD_NANDF_D15__GPIO_3_25,
	
	MX51_PAD_CSI2_D12__GPIO_4_9,
	MX51_PAD_CSI2_VSYNC__GPIO_4_13,
	MX51_PAD_CSPI1_RDY__GPIO_4_26,

	/* FEC (Fast Ethernet Controller) */
	//IMS_PENDING: Removed for debug
	/*
	MX51_PAD_EIM_EB2__FEC_MDIO,
	MX51_PAD_EIM_EB3__FEC_RDAT1,
	MX51_PAD_EIM_CS2__FEC_RDAT2,
	MX51_PAD_EIM_CS3__FEC_RDAT3,
	MX51_PAD_EIM_CS4__FEC_RX_ER,
	MX51_PAD_EIM_CS5__FEC_CRS,
	MX51_PAD_NANDF_RB2__FEC_COL,
	MX51_PAD_NANDF_RB3__FEC_RXCLK,
	MX51_PAD_NANDF_D8__FEC_TDATA0,
	MX51_PAD_NANDF_D9__FEC_RDATA0,
	MX51_PAD_NANDF_CS2__FEC_TX_ER,
	MX51_PAD_NANDF_CS3__FEC_MDC,
	MX51_PAD_NANDF_CS4__FEC_TDAT1,
	MX51_PAD_NANDF_CS5__FEC_TDAT2,
	MX51_PAD_NANDF_CS6__FEC_TDAT3,
	MX51_PAD_DISP2_DAT9__FEC_TX_EN,
	MX51_PAD_DISP2_DAT13__FEC_TX_CLK,
	*/
	//MX51_PAD_NANDF_D11__FEC_RX_DV,  //IMS_PENDING: Removed for debug

	MX51_PAD_KEY_COL5__I2C2_SDA,
	MX51_PAD_KEY_COL4__I2C2_SCL,

	/* SD1 */
	MX51_PAD_SD1_CMD__SD1_CMD,
	MX51_PAD_SD1_CLK__SD1_CLK,
	MX51_PAD_SD1_DATA0__SD1_DATA0,
	MX51_PAD_SD1_DATA1__SD1_DATA1,
	MX51_PAD_SD1_DATA2__SD1_DATA2,
	MX51_PAD_SD1_DATA3__SD1_DATA3,

	/* SD2 */
	MX51_PAD_SD2_CMD__SD2_CMD,
	MX51_PAD_SD2_CLK__SD2_CLK,
	MX51_PAD_SD2_DATA0__SD2_DATA0,
	MX51_PAD_SD2_DATA1__SD2_DATA1,
	MX51_PAD_SD2_DATA2__SD2_DATA2,
	MX51_PAD_SD2_DATA3__SD2_DATA3,
	
	/* SD3 */
	MX51_PAD_NANDF_RDY_INT__SD3_CMD,
	MX51_PAD_NANDF_CS7__SD3_CLK,
	MX51_PAD_NANDF_WE_B__SD3_DAT0,
	MX51_PAD_NANDF_RE_B__SD3_DAT1,
	MX51_PAD_NANDF_WP_B__SD3_DAT2,
	MX51_PAD_NANDF_RB0__SD3_DAT3,

	MX51_PAD_AUD3_BB_TXD__AUD3_BB_TXD,
	MX51_PAD_AUD3_BB_RXD__AUD3_BB_RXD,
	MX51_PAD_AUD3_BB_CK__AUD3_BB_CK,
	MX51_PAD_AUD3_BB_FS__AUD3_BB_FS,

	/* CSPI (to PMIC) */
	MX51_PAD_CSPI1_MOSI__CSPI1_MOSI,
	MX51_PAD_CSPI1_MISO__CSPI1_MISO,
	MX51_PAD_CSPI1_SCLK__CSPI1_SCLK,
	MX51_PAD_CSPI1_SS0__CSPI1_SS0,
	MX51_PAD_CSPI1_SS1__CSPI1_SS1,

	/* GPIO SPI */
	MX51_PAD_CSI2_D18__GPIO_4_11,   // EEPROM_DO
	MX51_PAD_CSI2_D19__GPIO_4_12,   // EEPROM_DI
	MX51_PAD_CSI2_HSYNC__GPIO_4_14, // EEPROM_CS
	MX51_PAD_CSI2_PIXCLK__GPIO_4_15,// EEPROM_CLK

	MX51_PAD_CSI1_D8__GPIO_3_12,
	MX51_PAD_CSI1_D9__GPIO_3_13,
	MX51_PAD_CSI1_D12__CSI1_D12,
	MX51_PAD_CSI1_D13__CSI1_D13,
	MX51_PAD_CSI1_D14__CSI1_D14,
	MX51_PAD_CSI1_D15__CSI1_D15,
	MX51_PAD_CSI1_D16__CSI1_D16,
	MX51_PAD_CSI1_D17__CSI1_D17,
	MX51_PAD_CSI1_D18__CSI1_D18,
	MX51_PAD_CSI1_D19__CSI1_D19,
	MX51_PAD_CSI1_VSYNC__CSI1_VSYNC,
	MX51_PAD_CSI1_HSYNC__CSI1_HSYNC,
	MX51_PAD_CSI1_PIXCLK__CSI1_PIXCLK,

	MX51_PAD_OWIRE_LINE__GPIO_1_24,
	MX51_PAD_DI1_PIN12__GPIO_3_1,
	MX51_PAD_DISP2_DAT6__GPIO_1_19,

	/* I2C4 (Bit bang I2C to Audio Codec) */
	MX51_PAD_GPIO_1_2__GPIO_1_2,
	MX51_PAD_DI1_D1_CS__GPIO_3_4, 

	/* HP_AMP_SHUTDOWN_B */
	MX51_PAD_GPIO_1_9__GPIO_1_9,
};

static struct dvfs_wp dvfs_core_setpoint[] = {
	{33, 8, 33, 10, 10, 0x08},
	{26, 0, 33, 20, 10, 0x08},
	{28, 8, 33, 20, 30, 0x08},
	{29, 0, 33, 20, 10, 0x08},
};

/* working point(wp): 0 - 800MHz; 1 - 166.25MHz; */
static struct cpu_wp cpu_wp_auto[] = {
	{
	 .pll_rate = 1000000000,
	 .cpu_rate = 1000000000,
	 .pdf = 0,
	 .mfi = 10,
	 .mfd = 11,
	 .mfn = 5,
	 .cpu_podf = 0,
	 .cpu_voltage = 1175000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 800000000,
	 .pdf = 0,
	 .mfi = 8,
	 .mfd = 2,
	 .mfn = 1,
	 .cpu_podf = 0,
	 .cpu_voltage = 1100000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 166250000,
	 .cpu_podf = 4,
	 .cpu_voltage = 850000,},
};

enum {
	VIDEOMODESIDX_TOSIHBA89 = 0,
	VIDEOMODESIDX_TOSIHBA7,
	VIDEOMODESIDX_NEC12,
	VIDEOMODESIDX_NEC15,
    VIDEOMODESIDX_NEC17,
    VIDEOMODESIDX_CHIMEI15,
};

int video_mode = -1;
static struct fb_videomode video_modes[] = {
	{
		"Toshiba89",             // Name 
		60,                      // Refresh rate
		1280,768,                // resolution  (X,Y)  
		12580,                   // Pixelclock in picoseconds 
		192, 64,                 // Margin (Left, Right)
		20, 3,                   // Margin (Upper, Lower) 
		128, 7,                  // Sync Length (Horizontal, Vertical) 
		0,                       // Sync
		FB_VMODE_NONINTERLACED,  // Video mode
		0,},                     // Flag
	{
		"Toshiba7",              // Name 
		60,                      // Refresh rate
		800,480,                 // resolution  (X,Y)  
		30120,                   // Pixelclock in picoseconds
		88, 40,                  // Margin (Left, Right)
		32, 11,                  // Margin (Upper, Lower) 
		128, 2,                  // Sync Length (Horizontal, Vertical) 
		0,                       // Sync
		FB_VMODE_NONINTERLACED,  // Video mode
		0,}, 

	{
		"NEC12",                 // Name 
		60,                      // Refresh rate
		1280,800,                // resolution  (X,Y)  
		14085,                   // Pixelclock in picoseconds
		150, 10,                 // Margin (Left, Right)
		20, 3,                   // Margin (Upper, Lower) 
		60, 10,                  // Sync Length (Horizontal, Vertical) 
		1,                       // Sync
		FB_VMODE_NONINTERLACED,  // Video mode
		0,}, 
	{
		"NEC15",                 // Name 
		60,                      // Refresh rate
		1280,768,                // resolution  (X,Y)  
		13872,                   // Pixelclock in picoseconds
		150, 10,                 // Margin (Left, Right)
		20, 3,                   // Margin (Upper, Lower) 
		60, 10,                  // Sync Length (Horizontal, Vertical) 
		1,                       // Sync
		FB_VMODE_NONINTERLACED,  // Video mode
		0,}, 
    {
		"NEC17",                 // Name 
		60,                      // Refresh rate
		1280,768,                // resolution  (X,Y)  
		14650,                   // Pixelclock in picoseconds
		0, 160,                  // Margin (Left, Right)
		0, 22,                   // Margin (Upper, Lower) 
		22, 22,                  // Sync Length (Horizontal, Vertical) 
		0,                       // Sync
		FB_VMODE_NONINTERLACED,  // Video mode
		0,}, 
    {
		"CHIMEI15",              // Name 
		60,                      // Refresh rate
		1280,800,                // resolution  (X,Y)  
		14084,                   // Pixelclock in picoseconds
		76, 51,                  // Margin (Left, Right)
		11, 2,                   // Margin (Upper, Lower) 
		33, 10,                  // Sync Length (Horizontal, Vertical) 
		0,                       // Sync
		FB_VMODE_NONINTERLACED,  // Video mode
		0,},
};

void platform_setDebugLed( int on )
{
	if ( on == true )
        	gpio_set_value(RDU_DIAG_LED_GPIO, 1);
	else
        	gpio_set_value(RDU_DIAG_LED_GPIO, 0);
}
EXPORT_SYMBOL(platform_setDebugLed);

struct cpu_wp *mx51_babbage_get_cpu_wp(int *wp)
{
	*wp = num_cpu_wp;
	return cpu_wp_auto;
}

void mx51_babbage_set_num_cpu_wp(int num)
{
	num_cpu_wp = num;
	return;
}


static struct dvfs_wp *mx51_babbage_get_dvfs_core_table(int *wp)
{
	*wp = ARRAY_SIZE(dvfs_core_setpoint);
	return dvfs_core_setpoint;
}

static struct platform_pwm_backlight_data mxc_pwm_backlight_data = {
	.pwm_id = 0,
	.max_brightness = 255,
	.dft_brightness = 128,
	.pwm_period_ns = 78770,
};

extern void mx5_ipu_reset(void);
static struct mxc_ipu_config mxc_ipu_data = {
	.rev = 2,
	.reset = mx5_ipu_reset,
};

extern void mx5_vpu_reset(void);
static struct mxc_vpu_platform_data mxc_vpu_data = {
	.iram_enable = false,
	.iram_size = 0x14000,
	.reset = mx5_vpu_reset,
};

/* workaround for ecspi chipselect pin may not keep correct level when idle */
static void mx51_babbage_gpio_spi_chipselect_active(int cspi_mode, int status,
					     int chipselect)
{
	switch (cspi_mode) {
	case 1:
		switch (chipselect) {
		case 0x1:
			{
			struct pad_desc cspi1_ss0 = MX51_PAD_CSPI1_SS0__CSPI1_SS0;

			mxc_iomux_v3_setup_pad(&cspi1_ss0);
			break;
			}
		case 0x2:
			{
			struct pad_desc cspi1_ss0_gpio = MX51_PAD_CSPI1_SS0__GPIO_4_24;

			mxc_iomux_v3_setup_pad(&cspi1_ss0_gpio);
			gpio_request(RDU_CSP1_SS0_GPIO, "cspi1-gpio");
			gpio_direction_output(RDU_CSP1_SS0_GPIO, 0);
			gpio_set_value(RDU_CSP1_SS0_GPIO, 1 & (~status));
			break;
			}
		default:
			break;
		}
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		break;
	}
}

static void mx51_babbage_gpio_spi_chipselect_inactive(int cspi_mode, int status,
					       int chipselect)
{
	switch (cspi_mode) {
	case 1:
		switch (chipselect) {
		case 0x1:
			break;
		case 0x2:
			gpio_free(RDU_CSP1_SS0_GPIO);
			break;

		default:
			break;
		}
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		break;
	}
}

static struct mxc_spi_master mxcspi1_data = {
	.maxchipselect = 4,
	.spi_version = 23,
	.chipselect_active = mx51_babbage_gpio_spi_chipselect_active,
	.chipselect_inactive = mx51_babbage_gpio_spi_chipselect_inactive,
};

static struct imxi2c_platform_data mxci2c_data = {
	.bitrate = 50000,
};

static struct tve_platform_data tve_data = {
	.dac_reg = "VVIDEO",
};

static struct mxc_dvfs_platform_data dvfs_core_data = {
	.reg_id = "SW1",
	.clk1_id = "cpu_clk",
	.clk2_id = "gpc_dvfs_clk",
	.gpc_cntr_offset = MXC_GPC_CNTR_OFFSET,
	.gpc_vcr_offset = MXC_GPC_VCR_OFFSET,
	.ccm_cdcr_offset = MXC_CCM_CDCR_OFFSET,
	.ccm_cacrr_offset = MXC_CCM_CACRR_OFFSET,
	.ccm_cdhipr_offset = MXC_CCM_CDHIPR_OFFSET,
	.prediv_mask = 0x1F800,
	.prediv_offset = 11,
	.prediv_val = 3,
	.div3ck_mask = 0xE0000000,
	.div3ck_offset = 29,
	.div3ck_val = 2,
	.emac_val = 0x08,
	.upthr_val = 25,
	.dnthr_val = 9,
	.pncthr_val = 33,
	.upcnt_val = 10,
	.dncnt_val = 10,
	.delay_time = 30,
};

static struct mxc_bus_freq_platform_data bus_freq_data = {
	.gp_reg_id = "SW1",
	.lp_reg_id = "SW2",
};

static struct mxc_dvfsper_data dvfs_per_data = {
	.reg_id = "SW2",
	.clk_id = "gpc_dvfs_clk",
	.gpc_cntr_reg_addr = MXC_GPC_CNTR,
	.gpc_vcr_reg_addr = MXC_GPC_VCR,
	.gpc_adu = 0x0,
	.vai_mask = MXC_DVFSPMCR0_FSVAI_MASK,
	.vai_offset = MXC_DVFSPMCR0_FSVAI_OFFSET,
	.dvfs_enable_bit = MXC_DVFSPMCR0_DVFEN,
	.irq_mask = MXC_DVFSPMCR0_FSVAIM,
	.div3_offset = 0,
	.div3_mask = 0x7,
	.div3_div = 2,
	.lp_high = 1250000,
	.lp_low = 1250000,
};

static struct resource mxcfb_resources[] = {
	[0] = {
	       .flags = IORESOURCE_MEM,
	       },
};

static struct mxc_fb_platform_data fb_data[] = {
	{
		.interface_pix_fmt = IPU_PIX_FMT_RGB24,
		.mode_str = "TOSHIBA89",
		.mode = video_modes,
		.num_modes = ARRAY_SIZE(video_modes),
	},
	{
		.interface_pix_fmt = IPU_PIX_FMT_RGB565,
		.mode_str = "CLAA-WVGA",
		.mode = video_modes,
		.num_modes = ARRAY_SIZE(video_modes),
	},
};

/** iim **********************************************************************/
static void mxc_iim_enable_fuse(void)
{
	u32 reg;

	if (!ccm_base)
		return;
	/* Enable fuse blown */
	reg = readl(ccm_base + 0x64);
	reg |= 0x10;
	writel(reg, ccm_base + 0x64);
}

static void mxc_iim_disable_fuse(void)
{
	u32 reg;

	/* Disable fuse blown */
	if (!ccm_base)
		return;

	reg = readl(ccm_base + 0x64);
	reg &= ~0x10;
	writel(reg, ccm_base + 0x64);
}

static struct mxc_iim_data iim_data = {
	.bank_start = MXC_IIM_MX51_BANK_START_ADDR,
	.bank_end   = MXC_IIM_MX51_BANK_END_ADDR,
	.enable_fuse = mxc_iim_enable_fuse,
	.disable_fuse = mxc_iim_disable_fuse,
};

/** i2c bitbang **************************************************************/
#include <linux/i2c-gpio.h>

static struct i2c_gpio_platform_data i2c_bitbang_data = {
	.sda_pin = RDU_I2C4_SDA,
	.scl_pin = RDU_I2C4_SCL,
	.udelay = 50,
	.timeout = 100,
	.sda_is_open_drain = 0,
	.scl_is_open_drain = 0,
	.scl_is_output_only = 1,
};

static struct platform_device i2c_bitbang_device = {
        .name = "i2c-gpio",
        .id = 2,
        .dev = {
                .platform_data = &i2c_bitbang_data,
        },
};


/** spi gpio ****************************************************************/
#include <linux/spi/spi_gpio.h>
#include <linux/eeprom_93xx46.h>

static struct spi_gpio_platform_data  spi_bitbang_data = {
	.sck =  RDU_EEPROM2_CLK,
	.mosi = RDU_EEPROM2_DI,
	.miso = RDU_EEPROM2_DO,
	.num_chipselect = 1
};

static struct platform_device spi_device = {
        .name = "spi_gpio",
        .id = 2,
        .dev = {
                .platform_data = &spi_bitbang_data,
        },
};

static struct eeprom_93xx46_platform_data microwire_eeprom_data = {
	.flags = EE_ADDR8 | EE_READONLY,
};


static struct spi_board_info microwire_eeprom_device[] = {
	{
	.modalias = "93xx46",
	.max_speed_hz = 500000,	/* max spi clock (SCK) speed in HZ */
	.bus_num = 2,
	.chip_select = 0,
	.mode = SPI_MODE_0 | SPI_CS_HIGH,
	.controller_data = (void *)RDU_EEPROM2_CS,
	.platform_data = &microwire_eeprom_data,
	},
};

static void setup_microwire_eeprom(void)
{
#ifdef CONFIG_EEPROM_93XX46
        spi_register_board_info(microwire_eeprom_device, ARRAY_SIZE(microwire_eeprom_device));
        platform_device_register(&spi_device);
#endif
}


/** mdio gpio ****************************************************************/
#include <linux/mdio-gpio.h>

static struct mdio_gpio_platform_data  bitbang_data = {
        .mdc =  RDU_MDC_1_8V,
        .mdio = RDU_MDIO_1_8V
};

static struct platform_device mdio_device = {
        .name = "mdio-gpio",
        .id = 1,
        .dev = {
                .platform_data = &bitbang_data,
        },
};

/** switch *******************************************************************/
#ifdef CONFIG_MARVELL_M88E6161
static struct mport rdu_switch00_ports[] = {
	{
		.port_mask = 0x1,
		.description = "RDU",
	},
	{
		.port_mask = 0x2,
		.description = "NetAux",
	},
	{
		.port_mask = 0x8,
		.description = "NetRight",
	},
	{
		.port_mask = 0x10,
		.description = "NetLeft",
	},
};

static struct mswitch rdu_switches[] = {
	{
		.port_count = 4,
		.ports = &rdu_switch00_ports[0],
	},
};

static struct marvellswitch_platform_data rdu_switch_data = {
	.switch_count = 1,
	.switches = rdu_switches,
};

static struct platform_device marvell_switch = {
	.name = "marvell-m88e6161-switch",
	.dev = {
		.platform_data = &rdu_switch_data,
	},
};
#endif
#ifdef CONFIG_NET_DSA
#include <net/dsa.h>

static struct dsa_chip_data marvell_switch_chip_data = {
       .mii_bus = &mdio_device.dev,
        .port_names[0]	= "cpu",
        .port_names[1]	= "netaux",
        .port_names[2]	= 0, // unused
        .port_names[3]	= "netright",
        .port_names[4]	= "netleft",
        .port_names[5]	= 0, // unused
};

static struct dsa_platform_data switch_data = {
        .netdev = &mxc_fec_device.dev,
        .nr_chips = 1,
        .chip = &marvell_switch_chip_data,
};

static struct platform_device dsa_device = {
        .name = "dsa",
        .dev = {
                .platform_data = &switch_data,
        },
};
#endif

static void setup_ethernet_switch(void)
{
        platform_device_register(&mdio_device);
#ifdef CONFIG_NET_DSA
	platform_device_register(&dsa_device);
#endif
#ifdef CONFIG_MARVELL_M88E6161
	platform_device_register(&marvell_switch);
#endif
}

/** framebuffer **************************************************************/
extern int primary_di;
static int __init mxc_init_fb(void)
{
	// IMS_PENDING: Do the right thing here
	if ( video_mode == -1 ) {
		printk(KERN_ERR"No detected display type! Defaulting to %s\n", video_modes[0].name);
	} else {
		printk("Configuring framebuffer for detected display %s\n", video_modes[video_mode].name );
		fb_data[0].mode_str = (char *)video_modes[video_mode].name;
		fb_data[0].mode = &video_modes[video_mode];
		fb_data[0].num_modes = 1;
	}

	if (primary_di) {
		printk(KERN_INFO "DI1 is primary\n");

		/* DI1 -> DP-BG channel: */
		mxc_fb_devices[1].num_resources = ARRAY_SIZE(mxcfb_resources);
		mxc_fb_devices[1].resource = mxcfb_resources;
		mxc_register_device(&mxc_fb_devices[1], &fb_data[1]);

		/* DI0 -> DC channel: */
		mxc_register_device(&mxc_fb_devices[0], &fb_data[0]);
	} else {
		printk(KERN_INFO "DI0 is primary\n");

		/* DI0 -> DP-BG channel: */
		mxc_fb_devices[0].num_resources = ARRAY_SIZE(mxcfb_resources);
		mxc_fb_devices[0].resource = mxcfb_resources;
		mxc_register_device(&mxc_fb_devices[0], &fb_data[0]);

		/* DI1 -> DC channel: */
		mxc_register_device(&mxc_fb_devices[1], &fb_data[1]);
	}

	/*
	 * DI0/1 DP-FG channel:
	 */
	mxc_register_device(&mxc_fb_devices[2], NULL);

	return 0;
}
device_initcall(mxc_init_fb);

/** 7180 *********************************************************************/
// pwdn: 0, power up device 
// pwdn: 1, power down device
// device power down is low-true
static void adv7180_pwdn(int pwdn)
{
	int power = gpio_get_value(RDU_ADV7180_PWRDWN);
	pr_debug( "%s@%d power %d->%d\n", __func__, __LINE__, power, pwdn ^ 1 );
	gpio_set_value(RDU_ADV7180_PWRDWN, (pwdn==0)?1:0 );
	if ( 0 == power ) // need delay if turning the power on
		mdelay(5);
}

static void adv7180_reset( void )
{
	pr_debug( KERN_DEBUG "%s@%d\n", __func__, __LINE__ );
	gpio_set_value(RDU_RST_CAPTURE_B, 0 );
	mdelay(5); // spec'd minimum reset of 5ms
	gpio_set_value(RDU_RST_CAPTURE_B, 1 );
	mdelay(5); // spec'd minimum reset recovery of 5ms
}

static void adv7180_platform_init( void )
{
	pr_debug( KERN_DEBUG "%s@%d\n", __func__, __LINE__ );
	gpio_request(RDU_RST_CAPTURE_B, "adv7180-reset");
	gpio_request(RDU_ADV7180_PWRDWN, "adv7180-pwrdwn");

	gpio_direction_output(RDU_RST_CAPTURE_B, 0);
	gpio_set_value(RDU_RST_CAPTURE_B, 0 );

	gpio_direction_output(RDU_ADV7180_PWRDWN, 0);
	gpio_set_value(RDU_ADV7180_PWRDWN, 0 );
}

static void adv7180_platform_release( void )
{
	pr_debug( KERN_DEBUG "%s@%d\n", __func__, __LINE__ );
	gpio_set_value(RDU_RST_CAPTURE_B, 0 );
	gpio_set_value(RDU_ADV7180_PWRDWN, 0 );
	gpio_free(RDU_RST_CAPTURE_B);
	gpio_free(RDU_ADV7180_PWRDWN);
}

static struct mxc_tvin_platform_data adv7180_data = {
	.dvddio_reg = NULL,
	.dvdd_reg = NULL,
	.avdd_reg = NULL,
	.pvdd_reg = NULL,
	.pwdn = &adv7180_pwdn,
	.reset = &adv7180_reset,
	.platform_init = &adv7180_platform_init,
	.platform_exit = &adv7180_platform_release,
	.cvbs = true,
	.mode = TVIN_ADV7180_COMPOSITE_AUTOM,
};

/** touchscreen **************************************************************/
#if defined(CONFIG_TOUCHSCREEN_ATMEL_MXT) || defined(CONFIG_TOUCHSCREEN_ATMEL_MXT_MODULE)
static const u8 init_vals_ver_22[] = {
	/* MXT_GEN_COMMAND(6) */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_GEN_POWER(7) */
	0xff, 0xff, 0x32,
	/* MXT_GEN_ACQUIRE(8) */
	0x5f, 0x05, 0x05, 0x01, 0x0a, 0x00, 0x01, 0x00,
	/* MXT_TOUCH_MULTI(9) */
	0x83, 0x00, 0x00, 0x13, 0x0b, 0x00, 0x01, 0x12, 0x02, 0x06,
	0x00, 0x01, 0x01, 0x00, 0x05, 0x0a, 0x0a, 0x0a, 0x00, 0x00,
	0x00, 0x00, 0x07, 0x07, 0x37, 0x37, 0x40, 0x00, 0x80, 0x00,
	0x00,
	/* MXT_TOUCH_KEYARRAY(15) */
	0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x41, 0x1e, 0x02, 0x00,
	0x00,
	/* MXT_SPT_COMMSCONFIG_T18 */
	0x00, 0x00, 
	/* MXT_SPT_GPIOPWM(19) */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_PROCI_GRIPFACE(20) */
	0x09, 0x0a, 0x0a, 0x0a, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00,
	/* MXT_PROCG_NOISE(22) */
	0x0d, 0x00, 0x00, 0x0a, 0x00, 0xf6, 0xff, 0x04, 0x08, 0x00,
	0x01, 0x0a, 0x0f, 0x14, 0x19, 0x1e, 0x04,
	/* MXT_TOUCH_PROXIMITY(23) */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_PROCI_ONETOUCH(24) */
	0x03, 0x0a, 0xff, 0x03, 0x00, 0x64, 0x64, 0x01, 0x0a, 0x14,
	0x28, 0x4b, 0x00, 0x02, 0x00, 0x64, 0x00, 0x19, 0x00,
	/* MXT_SPT_SELFTEST(25) */
	0x00, 0x00, 0xb0, 0x36, 0x40, 0x1f, 0xb0, 0x36, 0xf4, 0x01,
	0x00, 0x00, 0x00, 0x00,
	/* MXT_PROCI_TWOTOUCH(27) */
	0x03, 0x02, 0x00, 0xe0, 0x03, 0x23, 0x00,
	/* MXT_SPT_CTECONFIG(28) */
	0x00, 0x00, 0x03, 0x1e, 0x28, 0x00,
};

static struct mxt_platform_data Toshiba89_mxt_data = {
	.config = init_vals_ver_22,
	.config_length = ARRAY_SIZE(init_vals_ver_22),

	.x_line = 0x13,			// Number of horizontal sensor lines
	.y_line = 0x0b,			// Number of vertiacal sensor lines
	.x_size = 0x4ff,
	.y_size = 0x2ff,
	.blen = 0x01,
	.threshold = 0x12,
	.voltage = 0,
	.orient = 0x06,			// flip both X and Y access

	.irqflags = IRQF_TRIGGER_FALLING,
};

static const u8 init_vals_NEC12[] = {
	/* MXT_GEN_COMMAND(6) */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_GEN_POWER(7) */
	0xff, 0xff, 0xff,
	/* MXT_GEN_ACQUIRE(8) */
	0x55, 0x05, 0x01, 0x01, 0x32, 0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_TOUCH_MULTI(9) */
	0x83, 0x00, 0x00, 0x21, 0x2a, 0x00, 0x21, 0x23, 0x02, 0x03,
	0x00, 0x04, 0x01, 0x00, 0x04, 0x01, 0x01, 0x05, 0x00, 0x00,
	0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,
	/* MXT_TOUCH_KEYARRAY(15), Instance 0 */
	0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x41, 0x1e, 0x02, 0x00,
	0x00,
	/* MXT_TOUCH_KEYARRAY(15), Incstance 1 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00,
	/* MXT_SPT_COMMSCONFIG_T18 */
	0x00, 0x00, 
	/* MXT_PROCG_NOISE(22) */
	0x05, 0x00, 0x00, 0x0a, 0x00, 0xf6, 0xff, 0x04, 0x08, 0x00,
	0x01, 0x0a, 0x0f, 0x14, 0x19, 0x1e, 0x04,
	/* MXT_PROCI_ONETOUCH(24) */
	0x00, 0x02, 0xff, 0x03, 0x00, 0x64, 0x64, 0x01, 0x0a, 0x14,
	0x28, 0x4b, 0x00, 0x02, 0x00, 0x64, 0x00, 0x19, 0x00,
	/* MXT_SPT_SELFTEST(25) */
	0x03, 0x00, 0xd4, 0x30, 0x7c, 0x15, 0xb0, 0x36, 0xf4, 0x01,
	0x00, 0x00, 0x00, 0x00,
	/* MXT_PROCI_TWOTOUCH(27) */
	0x00, 0x00, 0x00, 0xe0, 0x03, 0x23, 0x00,
	/* MXT_SPT_CTECONFIG(28) */
	0x00, 0x00, 0xff, 0x19, 0x1e, 0x00,
	/* MXT_PROCI_GRIPSUPPRESSION_T40, Instance 0 */
	0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_PROCI_PALMSUPPRESSION_T41, Instance 0 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_SPT_DIGITIZER_T43, Instance 0 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

static struct mxt_platform_data NEC12_mxt_data = {
	.config = init_vals_NEC12,
	.config_length = ARRAY_SIZE(init_vals_NEC12),

	.x_line = 0x21,			// Number of horizontal sensor lines
	.y_line = 0x2a,			// Number of vertiacal sensor lines
	.x_size = 0x320,
	.y_size = 0x500,
	.blen = 0x21,
	.threshold = 0x2d,
	.voltage = 0,
	.orient = 0x03,			// flip both X and Y access

	.irqflags = IRQF_TRIGGER_FALLING,
};

static const u8 init_vals_NEC15[] = {
	/* MXT_GEN_COMMAND(6) */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_GEN_POWER(7) */
	0xff, 0xff, 0xff,
	/* MXT_GEN_ACQUIRE(8) */
	0x5f, 0x05, 0x01, 0x01, 0x32, 0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_TOUCH_MULTI(9) */
	0x83, 0x00, 0x00, 0x21, 0x2a, 0x00, 0x21, 0x2d, 0x02, 0x03,
	0x00, 0x04, 0x01, 0x00, 0x04, 0x01, 0x05, 0x05, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x2c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,
	/* MXT_TOUCH_KEYARRAY(15), Instance 0 */
	0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x41, 0x1e, 0x02, 0x00,
	0x00,
	/* MXT_TOUCH_KEYARRAY(15), Incstance 1 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00,
	/* MXT_SPT_COMMSCONFIG_T18 */
	0x00, 0x00, 
	/* MXT_PROCG_NOISE(22) */
	0x05, 0x00, 0x00, 0x0a, 0x00, 0xf6, 0xff, 0x04, 0x08, 0x00,
	0x01, 0x0a, 0x0f, 0x14, 0x19, 0x1e, 0x04,
	/* MXT_PROCI_ONETOUCH(24) */
	0x00, 0x02, 0xff, 0x03, 0x00, 0x64, 0x64, 0x01, 0x0a, 0x14,
	0x28, 0x4b, 0x00, 0x02, 0x00, 0x64, 0x00, 0x19, 0x00,
	/* MXT_SPT_SELFTEST(25) */
	0x03, 0x00, 0xb0, 0x36, 0x40, 0x1f, 0xb0, 0x36, 0xf4, 0x01,
	0x00, 0x00, 0x00, 0x00,
	/* MXT_PROCI_TWOTOUCH(27) */
	0x00, 0x00, 0x00, 0xe0, 0x03, 0x23, 0x00,
	/* MXT_SPT_CTECONFIG(28) */
	0x00, 0x00, 0xff, 0x19, 0x1e, 0x00,
	/* MXT_PROCI_GRIPSUPPRESSION_T40, Instance 0 */
	0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_PROCI_PALMSUPPRESSION_T41, Instance 0 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	/* MXT_SPT_DIGITIZER_T43, Instance 0 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

static struct mxt_platform_data NEC15_mxt_data = {
	.config = init_vals_NEC15,
	.config_length = ARRAY_SIZE(init_vals_NEC15),

	.x_line = 0x21,			// Number of vertical sensor lines
	.y_line = 0x2a,			// Number of horizontal sensor lines
	.x_size = 0x300,
	.y_size = 0x500,
	.blen = 0x21,
	.threshold = 0x2d,
	.voltage = 0,
	.orient = 0x03,			// rotate 90 degrees counter clockwise

	.irqflags = IRQF_TRIGGER_FALLING,
};

#elif defined(CONFIG_TOUCHSCREEN_QT602240) || defined(CONFIG_TOUCHSCREEN_QT602240_MODULE)
static struct qt602240_platform_data qt602240_data = {
	.x_line = 0x13,			// Number of horizontal sensor lines
	.y_line = 0x0b,			// Number of vertiacal sensor lines
	.x_size = 0x4ff,
	.y_size = 0x2ff,
	.blen = 0x01,
	.threshold = 0x14,
	.voltage = 0,
	.orient = 0x06,			// flip both X and Y access
};
#endif

#if defined(CONFIG_TOUCHSCREEN_SYNAPTICS_I2C_RMI4) || \
    defined(CONFIG_TOUCHSCREEN_SYNAPTICS_I2C_RMI4_MODULE)

#include <linux/input/synaptics_i2c_rmi4.h>

static struct synaptics_rmi4_platform_data Toshiba89_synaptics_rmi4_i2c_pdata = {
	.x_size		= 1280,
	.y_size		= 768,
	.x_flip		= false,
	.y_flip		= true,
	.swap_xy	= true,
	.irq_type	= IRQF_TRIGGER_FALLING,
};

static struct synaptics_rmi4_platform_data NEC12_synaptics_rmi4_i2c_pdata = {
	.x_size		= 1280,
	.y_size		= 800,
	.x_flip		= false,
	.y_flip		= true,
	.swap_xy	= true,
	.irq_type	= IRQF_TRIGGER_FALLING,
};

static struct synaptics_rmi4_platform_data Chimei15_synaptics_rmi4_i2c_pdata = {
	.x_size		= 1280,
	.y_size		= 800,
	.x_flip		= false,
	.y_flip		= true,
	.swap_xy	= true,
	.irq_type	= IRQF_TRIGGER_FALLING,
};

#endif

/** i2c **********************************************************************/
static struct tpa6130a2_platform_data tpa6130a2_data = {
	.id = TPA6130A2,
	.power_gpio = -1, // this pin is connected but controlled externally on Rev C.
//	.shutdown_in = HP_AMP_SHUTDOWN_B, // only for rev C
};

enum {
	I2C1_BOARDINFOIDX_SGTL5000 = 0,
	I2C1_BOARDINFOIDX_TPA6130A2,
	I2C1_BOARDINFOIDX_ADV7180,
	I2C1_BOARDINFOIDX_DS1341,
#if defined(CONFIG_TOUCHSCREEN_ATMEL_MXT) || defined(CONFIG_TOUCHSCREEN_ATMEL_MXT_MODULE)
	I2C1_BOARDINFOIDX_ATMEL_MXT224_TS,
	I2C1_BOARDINFOIDX_ATMEL_MXT1386_TS,
#elif defined(CONFIG_TOUCHSCREEN_QT602240) || defined(CONFIG_TOUCHSCREEN_QT602240_MODULE)
	I2C1_BOARDINFOIDX_QT602240_TS,
#endif
#if defined(CONFIG_TOUCHSCREEN_SYNAPTICS_I2C_RMI4) || defined(CONFIG_TOUCHSCREEN_SYNAPTICS_I2C_RMI4_MODULE)
	I2C1_BOARDINFOIDX_SYNAPTICS_RMI4_TS,
#endif
};

static struct i2c_board_info mxc_i2c1_board_info[] __initdata = {
	{
		.type = "sgtl5000-i2c",
		.addr = 0x0a,
	},
	{
		.type = "tpa6130a2",
		.addr = 0x60,
		.platform_data = &tpa6130a2_data,
	},
	{
		.type = "adv7180",
		.addr = 0x21,
		.platform_data = &adv7180_data,
	},
	{
		.type = "ds1341",
		.addr = 0x68,
	},
#if defined(CONFIG_TOUCHSCREEN_ATMEL_MXT) || defined(CONFIG_TOUCHSCREEN_ATMEL_MXT_MODULE)
	{
		.type = "atmel_mxt_ts",
		.addr = 0x4b,
		.platform_data = &Toshiba89_mxt_data,
		.irq = gpio_to_irq(RDU_INT_TOUCH_B),
	},
	{
		.type = "atmel_mxt_ts",
		.addr = 0x4c,
		.platform_data = &NEC12_mxt_data,
		.irq = gpio_to_irq(RDU_INT_TOUCH_B),
	},
#elif defined(CONFIG_TOUCHSCREEN_QT602240) || defined(CONFIG_TOUCHSCREEN_QT602240_MODULE)
	{
		.type = "qt602240_ts",
		.addr = 0x4b,
		.platform_data = &qt602240_data,
	},
#endif
#if defined(CONFIG_TOUCHSCREEN_SYNAPTICS_I2C_RMI4) || \
    defined(CONFIG_TOUCHSCREEN_SYNAPTICS_I2C_RMI4_MODULE)
	{
		I2C_BOARD_INFO("synaptics_rmi4_i2c", 0x20),
		.irq = gpio_to_irq(RDU_INT_TOUCH_B),
		/* Default for now to the Toshiba 8.9" unit. It will be set
		 * by display type later */
		.platform_data = &Toshiba89_synaptics_rmi4_i2c_pdata,
	},
#endif
};

static struct mtd_partition mxc_spi_nor_partitions[] = {
	{
	 .name = "NOR-config",
	 .offset = 0,
	 .size = 0x000000400,},
	{
	 .name = "NOR-bootloader",
	 .offset = MTDPART_OFS_APPEND,
	 .size = 0x0000FFC00,},
	{
	 .name = "NOR-kernel",
	 .offset = MTDPART_OFS_APPEND,
	 .size = 0x00300000,},
	{
	 .name = "NOR-rootfs",
	 .offset = MTDPART_OFS_APPEND,
	 .size = MTDPART_SIZ_FULL,},
};

static struct mtd_partition mxc_dataflash_partitions[] = {
	{
	 .name = "NOR-config",
	 .offset = 0,
	 .size = 0x000000400,},
	{
	 .name = "NOR-bootloader",
	 .offset = MTDPART_OFS_APPEND,
	 .size = 0x0000FFC00,},
	{
	 .name = "NOR-kernel",
	 .offset = MTDPART_OFS_APPEND,
	 .size = 0x00300000,},
	{
	 .name = "NOR-rootfs",
	 .offset = MTDPART_OFS_APPEND,
	 .size = MTDPART_SIZ_FULL,},
};

static struct flash_platform_data mxc_spi_flash_data[] = {
	{
	 .name = "mxc_spi_nor",
	 .parts = mxc_spi_nor_partitions,
	 .nr_parts = ARRAY_SIZE(mxc_spi_nor_partitions),
	 .type = "sst25vf016b",},
	{
	 .name = "mxc_dataflash",
	 .parts = mxc_dataflash_partitions,
	 .nr_parts = ARRAY_SIZE(mxc_dataflash_partitions),
	 .type = "at45db642d",}
};

#if 0
static struct spi_board_info mxc_spi_nor_device[] __initdata = {
	{
	 .modalias = "mxc_spi_nor",
	 .max_speed_hz = 25000000,	/* max spi clock (SCK) speed in HZ */
	 .bus_num = 1,
	 .chip_select = 1,
	 .platform_data = &mxc_spi_flash_data[0],
	},
};
#endif
static struct spi_board_info mxc_dataflash_device[] __initdata = {
	{
	 .modalias = "mxc_dataflash",
	 .max_speed_hz = 25000000,	/* max spi clock (SCK) speed in HZ */
	 .bus_num = 1,
	 .chip_select = 1,
	 .platform_data = &mxc_spi_flash_data[1],},
};


static struct i2c_board_info mxc_bitbang_i2c_board_info[] __initdata = {
    {
        .type = "sgtl5000-i2c",
        .addr = 0x0a,
    },
};

/** mmc/hd *******************************************************************/
static int sdhc_write_protect(struct device *dev)
{
	unsigned short rc = 0;
	switch(to_platform_device(dev)->id)
	{
	case 0:
		// SD-1 (On-board eMMC.  It is never write protected.) 
		rc = 0;
		break;

	case 1:
		// SD-2 (SD Card slot)
		rc = gpio_get_value(RDU_SD2_WP);
		break;

	case 2:
		// SD-3 (SD Card slot)
		rc = gpio_get_value(RDU_SD3_WP);
		break;
	}
	return rc;
}

static unsigned int sdhc_get_card_det_status(struct device *dev)
{
	int ret;
		
	ret = 1;
	switch(to_platform_device(dev)->id)
	{
	case 0:
		// SD-1 (On-board eMMC.  It is always present.)
		ret = 0; 
		break;

	case 1:
		// SD-2 (SD Card slot)
		ret = gpio_get_value(RDU_SD2_CD);
		break;

	case 2:
		// SD-3 (SD Card slot)
		ret = gpio_get_value(RDU_SD3_CD);
		break;
	}
	return ret;
}

static struct mxc_mmc_platform_data mmc1_data = {
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
	    MMC_VDD_31_32,
	.caps = 0,	// Default to 0 as this will be set in __init mxc_board_init
				// based on a passed in parameter from U-boot
	.min_clk = 150000,
	.max_clk = 52000000,
	.card_inserted_state = 0,
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
	.power_mmc = NULL,
};

static struct mxc_mmc_platform_data mmc2_data = {
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
	    MMC_VDD_31_32,
	.caps = MMC_CAP_4_BIT_DATA,
	.min_clk = 150000,
	.max_clk = 50000000,
	.card_inserted_state = 0,
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
};

static struct mxc_mmc_platform_data mmc3_data = {
   	// IMS_DEBUG: try 3.3 
	//.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
	//    MMC_VDD_31_32,
   	.ocr_mask = MMC_VDD_32_33,
	.caps = MMC_CAP_4_BIT_DATA,		
	.min_clk = 150000,
	.max_clk = 50000000,
	.card_inserted_state = 0, 
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
};

/** audio ********************************************************************/
#include <sound/soc.h>
extern int tpa6130a2_stereo_enable(struct snd_soc_codec *codec, int enable);
static int mxc_sgtl5000_amp_enable(int enable)
{
	pr_debug(KERN_INFO"%s: %s\n", __func__, enable?"enable":"disable" );
	return tpa6130a2_stereo_enable( NULL, enable );
}

// clock enable is high true
static int mxc_sgtl5000_clock_enable(int enable)
{
	int enabled = gpio_get_value(RDU_AUDIO_CLK_EN_B);
	pr_debug(KERN_DEBUG"%s: %s\n", __func__, enable?"enable":"disable");
	gpio_set_value(RDU_AUDIO_CLK_EN_B, !enable);
	if ( enable && !enabled )
		udelay(5); // spec calls for minimum delay of 1uS
	return 0;
}

static int headphone_det_status(void)
{
	return 1; // Always present
}

static struct mxc_audio_platform_data sgtl5000_data = {
	.ssi_num = 1,
	.src_port = 2,
	.ext_port = 3,
	.hp_status = headphone_det_status,
	.amp_enable = mxc_sgtl5000_amp_enable,
	.clock_enable = mxc_sgtl5000_clock_enable,
	.sysclk = 26000000,
};

static struct platform_device mxc_sgtl5000_device = {
	.name = "imx-3stack-sgtl5000",
};

/** system type **************************************************************/
struct gpio systemType_gpio[] = {
	{ .gpio = RDU_SYSTEM_TYPE_3, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_3", },
	{ .gpio = RDU_SYSTEM_TYPE_2, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_2", },
	{ .gpio = RDU_SYSTEM_TYPE_1, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_1", },
	{ .gpio = RDU_SYSTEM_TYPE_0, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_0", },
};

// these definitions should be moved somewhere board specific that makes them 
// available to anyone who might call IMSSystemType
#define SYSTEM_TYPE_MASK	0x3
#define	RDU_REV_B	(0b0000 & SYSTEM_TYPE_MASK)
#define	RDU_REV_C	(0b1101 & SYSTEM_TYPE_MASK)
#define SCU_MEZZ	(0b0010 & SYSTEM_TYPE_MASK)

static char *systemType_toStr( int type )
{
	switch ( type ) {
	case RDU_REV_B: return "RDU Rev B";
	case RDU_REV_C: return "RDU Rev C";
	case SCU_MEZZ: return "SCU Mezz";
	default: return "Unknown";
	}
}

int IMSSystemType( void )
{
	static int type = -1;
	if ( type == -1 ) {
		type =	(gpio_get_value( RDU_SYSTEM_TYPE_3 ) << 3 ) |
			(gpio_get_value( RDU_SYSTEM_TYPE_2 ) << 2 ) |
			(gpio_get_value( RDU_SYSTEM_TYPE_1 ) << 1 ) |
			(gpio_get_value( RDU_SYSTEM_TYPE_0 ) << 0 );

		// NOTE: masking off the top bits because of varying configurations in populated resistors.
		type &= SYSTEM_TYPE_MASK;

		printk(KERN_INFO"%s: detected board type %s(%02x)\n", __func__, systemType_toStr( type ), type);
	}
	return type;
}
EXPORT_SYMBOL(IMSSystemType);

static int boardType_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	int type = IMSSystemType();
	len += sprintf(buf+len,"%s(%02x)\n", systemType_toStr( type ), type);
	*eof = 1;
	return len;
}


/*****************************************************************************/

/*!
 * Board specific fixup function. It is called by \b setup_arch() in
 * setup.c file very early on during kernel starts. It allows the user to
 * statically fill in the proper values for the passed-in parameters. None of
 * the parameters is used currently.
 *
 * @param  desc         pointer to \b struct \b machine_desc
 * @param  tags         pointer to \b struct \b tag
 * @param  cmdline      pointer to the command line
 * @param  mi           pointer to \b struct \b meminfo
 */
static void __init fixup_mxc_board(struct machine_desc *desc, struct tag *tags,
				   char **cmdline, struct meminfo *mi)
{
	char *str;
	struct tag *t;
	struct tag *mem_tag = 0;
	int total_mem = SZ_512M;
	int left_mem = 0;
	int gpu_mem = SZ_64M;
	int fb_mem = SZ_32M;

	mxc_set_cpu_type(MXC_CPU_MX51);

	get_cpu_wp = mx51_babbage_get_cpu_wp;
	set_num_cpu_wp = mx51_babbage_set_num_cpu_wp;
	get_dvfs_core_wp = mx51_babbage_get_dvfs_core_table;
	num_cpu_wp = ARRAY_SIZE(cpu_wp_auto);

	for_each_tag(mem_tag, tags) {
		if (mem_tag->hdr.tag == ATAG_MEM) {
			total_mem = mem_tag->u.mem.size;
			left_mem = total_mem - gpu_mem - fb_mem;
			break;
		}
	}

	for_each_tag(t, tags) {
		if (t->hdr.tag == ATAG_CMDLINE) {
			str = t->u.cmdline.cmdline;
			str = strstr(str, "mem=");
			if (str != NULL) {
				str += 4;
				left_mem = memparse(str, &str);
				if (left_mem == 0 || left_mem > total_mem)
					left_mem = total_mem - gpu_mem - fb_mem;
			}

			str = t->u.cmdline.cmdline;
			str = strstr(str, "gpu_memory=");
			if (str != NULL) {
				str += 11;
				gpu_mem = memparse(str, &str);
			}

			break;
		}
	}

	if (mem_tag) {
		fb_mem = total_mem - left_mem - gpu_mem;
		if (fb_mem < 0) {
			gpu_mem = total_mem - left_mem;
			fb_mem = 0;
		}
		mem_tag->u.mem.size = left_mem;

		/*reserve memory for gpu*/
		gpu_device.resource[5].start =
				mem_tag->u.mem.start + left_mem;
		gpu_device.resource[5].end =
				gpu_device.resource[5].start + gpu_mem - 1;
#if defined(CONFIG_FB_MXC_SYNC_PANEL) || \
	defined(CONFIG_FB_MXC_SYNC_PANEL_MODULE)
		if (fb_mem) {
			mxcfb_resources[0].start =
				gpu_device.resource[5].end + 1;
			mxcfb_resources[0].end =
				mxcfb_resources[0].start + fb_mem - 1;
		} else {
			mxcfb_resources[0].start = 0;
			mxcfb_resources[0].end = 0;
		}
#endif
	}

	// parse the command line for the video options
	str = t->u.cmdline.cmdline;
	str = strstr(str,"video=");
	if ( str ) { // video substring found
		int i;
		for ( i = 0; i < ARRAY_SIZE( video_modes ); i++ ) {
			if ( strstr( str, video_modes[i].name )) {
				video_mode = i;
				printk("%s: found display type %s\n", __func__, video_modes[i].name );
				break;
			}
		}
	}

	// parse the command line for emmc1 bus bit width
	str = t->u.cmdline.cmdline;
	str = strstr(str,"mmc1BusWidth=");
	if ( str ) { // mmc1 bus width substring found
		int i;
		for ( i = 0; i < ARRAY_SIZE( mmc_bus_widths ); i++ ) {
			if ( strstr( str, mmc_bus_widths[i].name )) {
				mmc1_mode = i;
				printk("%s: found mmc bus width: %s\n", __func__, mmc_bus_widths[i].name );
				break;
			}
		}
	}
}

#define PWGT1SPIEN (1<<15)
#define PWGT2SPIEN (1<<16)
#define USEROFFSPI (1<<3)

static void mxc_power_off(void)
{
	/* We can do power down one of two ways:
	   Set the power gating
	   Set USEROFFSPI */

	/* Set the power gate bits to power down */
	pmic_write_reg(REG_POWER_MISC, (PWGT1SPIEN|PWGT2SPIEN),
		(PWGT1SPIEN|PWGT2SPIEN));
}

/*!
 * Power Key interrupt handler.
 */
static irqreturn_t power_key_int(int irq, void *dev_id)
{
	pr_info(KERN_INFO "PWR key pressed\n");
	return 0;
}

/*!
 * Power Key initialization.
 */
static int __init mxc_init_power_key(void)
{
	/* Set power key as wakeup resource */
	int irq, ret;

	irq = IOMUX_TO_IRQ_V3(RDU_SYS_ON_OFF_REQ);

	set_irq_type(irq, IRQF_TRIGGER_RISING);
	ret = request_irq(irq, power_key_int, 0, "power_key", 0);
	if (ret)
		pr_info("register on-off key interrupt failed\n");
	else
		enable_irq_wake(irq);
	return ret;
}
late_initcall(mxc_init_power_key);

static void __init mx51_babbage_io_init(void)
{
	mxc_iomux_v3_setup_multiple_pads(mx51babbage_pads,
					ARRAY_SIZE(mx51babbage_pads));
	/* SYSTEM TYPE */
	gpio_request_array( systemType_gpio, ARRAY_SIZE(systemType_gpio));

	/* PMIC interrupt */
	gpio_request(RDU_INT_FROM_PMIC, "INT_FROM_PMIC");
	gpio_direction_input(RDU_INT_FROM_PMIC);

	/* SD Card Slots */
	gpio_request(RDU_SD2_CD,   "SD2_CD");
	gpio_request(RDU_SD2_WP,   "SD2_WP");
	gpio_request(RDU_SD3_CD,   "SD3_CD");
	gpio_request(RDU_SD3_WP,   "SD3_WP");
	gpio_direction_input(RDU_SD2_CD);
	gpio_direction_input(RDU_SD2_WP);
	gpio_direction_input(RDU_SD3_CD);
	gpio_direction_input(RDU_SD3_WP);

	/* reset usbh1 hub */
	gpio_request(RDU_RST_USB1_PHY, "RST_USB1_PHY");
	gpio_direction_output(RDU_RST_USB1_PHY, 0);
	gpio_set_value(RDU_RST_USB1_PHY, 0);
	msleep(1);
	gpio_set_value(RDU_RST_USB1_PHY, 1);
	gpio_free(RDU_RST_USB1_PHY);

	/* reset usbh2 hub */
	gpio_request(RDU_RST_USB2_PHY, "RST_USB2_PHY");
	gpio_direction_output(RDU_RST_USB2_PHY, 0);
	gpio_set_value(RDU_RST_USB2_PHY, 0);
	msleep(1);
	gpio_set_value(RDU_RST_USB2_PHY, 1);
	gpio_free(RDU_RST_USB2_PHY);

	/* reset FEC PHY */
	gpio_request(RDU_RST_ENET_B, "RST_ENET_B");
	gpio_direction_output(RDU_RST_ENET_B, 0);
	msleep(10);
	gpio_set_value(RDU_RST_ENET_B, 1);
	gpio_free(RDU_RST_ENET_B);

	/* Drive 26M_OSC_EN line high */
	gpio_request(RDU_26M_OSC_EN, "26M_OSC_EN");
	gpio_direction_output(RDU_26M_OSC_EN, 1);

	/* Drive USB_CLK_EN_B line low */
	gpio_request(RDU_USB_CLK_EN_B, "USB_CLK_EN_B");
	gpio_direction_output(RDU_USB_CLK_EN_B, 0);

	/* audio_clk_en_b */
	gpio_request(RDU_AUDIO_CLK_EN_B, "AUDIO_CLK_EN_B");
	gpio_direction_output(RDU_AUDIO_CLK_EN_B, 0);

	/* power key */
	gpio_request(RDU_SYS_ON_OFF_REQ, "SYS_ON_OFF_REQ");
	gpio_direction_input(RDU_SYS_ON_OFF_REQ);

	/* LCD related gpio */
	gpio_request(RDU_LVDS_PWR_DWN_B, "LVDS_PWR_DWN_B");
	gpio_direction_output(RDU_LVDS_PWR_DWN_B, 0);
	mxc_gpio_set_name(RDU_LVDS_PWR_DWN_B, "lvds_pwr_dwn_b");
	gpio_export(RDU_LVDS_PWR_DWN_B, 0);

	/* Reset touch screen */
	gpio_request(RDU_RST_TOUCH_B, "RST_TOUCH_B");
	gpio_direction_output(RDU_RST_TOUCH_B, 0);
	msleep(65);
	gpio_set_value(RDU_RST_TOUCH_B, 1);
	gpio_free(RDU_RST_TOUCH_B);

	/*
	 * Atmel Max Touch touchscreen interrupt, also used on boards
	 * with Synaptics RMI4 touchscreens.
	 */
	gpio_request(RDU_INT_TOUCH_B, "INT_TOUCH_B");
	gpio_direction_input(RDU_INT_TOUCH_B);

	/* Turn off diagnostic LED */
	gpio_request(RDU_DIAG_LED_GPIO, "DIAG_LED_GPIO");
	gpio_direction_output(RDU_DIAG_LED_GPIO, 0);
	gpio_free(RDU_DIAG_LED_GPIO);
		
	/* Enable RS485_2_TX_ENABLE and set in RX state as default */
	gpio_request(RDU_RS485_2_TX_ENABLE, "RS485_2_TX_ENABLE");
	gpio_direction_output(RDU_RS485_2_TX_ENABLE, 0);

	/* Switch Interrupt GPIO */
	gpio_request(RDU_DDS_SWITCH_INT_B, "RDU_DDS_SWITCH_INT_B");
	gpio_direction_input(RDU_DDS_SWITCH_INT_B);
	mxc_gpio_set_name(RDU_DDS_SWITCH_INT_B, "switch_int_b");
	gpio_export(RDU_DDS_SWITCH_INT_B, 0);

	/* Register I2C4 signals */
	//gpio_request(RDU_I2C4_SDA, "I2C4_SDA");
	//gpio_direction_input(RDU_I2C4_SDA);
	//gpio_request(RDU_I2C4_SCL, "I2C4_SCL");
	//gpio_direction_output(RDU_I2C4_SCL, 1);

	if ( RDU_REV_C == IMSSystemType() ) {
		/* amplifier external power control indicator */
		gpio_request(HP_AMP_SHUTDOWN_B, "EXT_AMP_SHUTDOWN");
		gpio_direction_input(HP_AMP_SHUTDOWN_B);
	}
}

static int _reset_reason;
static int resetReason_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	switch (_reset_reason) {
	case 0x09:
		len += sprintf(buf+len, "Reset reason: User\n");
		break;
	case 0x01:
		len += sprintf(buf+len, "Reset reason: Power-on\n");
		break;
	case 0x10:
	case 0x11:
		len += sprintf(buf+len, "Reset reason: WDOG\n");
		break;
	default:
		len += sprintf(buf+len, "Reset reason unknown: 0x%x\n", _reset_reason);
		break;
	}
	
	*eof = 1;
	return len;
}

/*!
 * Board specific initialization.
 */
static void __init mxc_board_init(void)
{
	void *memptr;

	mxc_ipu_data.di_clk[0] = clk_get(NULL, "ipu_di0_clk");
	mxc_ipu_data.di_clk[1] = clk_get(NULL, "ipu_di1_clk");
	mxc_ipu_data.csi_clk[0] = clk_get(NULL, "csi_mclk1");
	mxc_ipu_data.csi_clk[1] = clk_get(NULL, "csi_mclk2");

	/* SD card detect irqs */
	mxcsdhc1_device.resource[2].start = IOMUX_TO_IRQ_V3(RDU_BABBAGE_BOARD_TYPE);   // IMS_PENDING: Use this signal for now as a harmless placeholder 
	mxcsdhc1_device.resource[2].end   = IOMUX_TO_IRQ_V3(RDU_BABBAGE_BOARD_TYPE);   // IMS_PENDING: Use this signal for now as a harmless placeholder 
	mxcsdhc2_device.resource[2].start = IOMUX_TO_IRQ_V3(RDU_SD2_CD);
	mxcsdhc2_device.resource[2].end   = IOMUX_TO_IRQ_V3(RDU_SD2_CD);
	// IMS_PENDING: Testing now
	mxcsdhc3_device.resource[2].start = IOMUX_TO_IRQ_V3(RDU_SD3_CD);
	mxcsdhc3_device.resource[2].end   = IOMUX_TO_IRQ_V3(RDU_SD3_CD); 

	mxc_cpu_common_init();
	mx51_babbage_io_init();

	mxc_register_device(&mxc_dma_device, NULL);
	mxc_register_device(&mxc_wdt_device, NULL);
	mxc_register_device(&mxcspi1_device, &mxcspi1_data);
	mxc_register_device(&mxci2c_devices[0], &mxci2c_data);
	mxc_register_device(&mxci2c_devices[1], &mxci2c_data);
	mxc_register_device(&mxc_rtc_device, NULL);
	mxc_register_device(&mxc_ipu_device, &mxc_ipu_data);
	mxc_register_device(&mxc_tve_device, &tve_data);
	mxc_register_device(&mxcvpu_device, &mxc_vpu_data);
	mxc_register_device(&gpu_device, NULL);
	mxc_register_device(&mxcscc_device, NULL);
	mxc_register_device(&mx51_lpmode_device, NULL);
	mxc_register_device(&busfreq_device, &bus_freq_data);
	mxc_register_device(&sdram_autogating_device, NULL);
	mxc_register_device(&mxc_dvfs_core_device, &dvfs_core_data);
	mxc_register_device(&mxc_dvfs_per_device, &dvfs_per_data);
	mxc_register_device(&mxc_iim_device, &iim_data);
	mxc_register_device(&mxc_pwm1_device, NULL);
	mxc_register_device(&mxc_pwm1_backlight_device,
		&mxc_pwm_backlight_data);
	
	// With Rev D and prior boards there are soldering issues with emmc1
	// To help workaround this issue emmc1 will be run in 1 or 4 bit mode
	// based on a command line parameter passed in from U-boot
	mmc1_data.caps |= mmc_bus_widths[mmc1_mode].bus_width;
	mxc_register_device(&mxcsdhc1_device, &mmc1_data);
	
	mxc_register_device(&mxcsdhc2_device, &mmc2_data);
	mxc_register_device(&mxcsdhc3_device, &mmc3_data);
	
	mxc_register_device(&mxc_ssi1_device, NULL);
	mxc_register_device(&mxc_ssi2_device, NULL);
	mxc_register_device(&mxc_ssi3_device, NULL);
	mxc_register_device(&mxc_fec_device, NULL);
	mxc_register_device(&mxc_v4l2_device, NULL);
	mxc_register_device(&mxc_v4l2out_device, NULL);

	platform_device_register(&i2c_bitbang_device);

	mx51_babbage_init_mc13892();


	if ( RDU_REV_C == IMSSystemType() ) {
		spi_register_board_info(mxc_dataflash_device,
			ARRAY_SIZE(mxc_dataflash_device));
		tpa6130a2_data.shutdown_in = HP_AMP_SHUTDOWN_B;
	}

	if (video_mode == VIDEOMODESIDX_TOSIHBA89 ) {
		mxc_i2c1_board_info[I2C1_BOARDINFOIDX_SYNAPTICS_RMI4_TS].platform_data =
			&Toshiba89_synaptics_rmi4_i2c_pdata;
	}
	else if (video_mode == VIDEOMODESIDX_NEC12 ) {
		mxc_i2c1_board_info[I2C1_BOARDINFOIDX_SYNAPTICS_RMI4_TS].platform_data =
			&NEC12_synaptics_rmi4_i2c_pdata;
	}
	else if (video_mode == VIDEOMODESIDX_CHIMEI15 ) {
		mxc_i2c1_board_info[I2C1_BOARDINFOIDX_SYNAPTICS_RMI4_TS].platform_data =
			&Chimei15_synaptics_rmi4_i2c_pdata;
	}
	else if (video_mode == VIDEOMODESIDX_NEC15 ) {
		// the same atmel touchscreen controller is used for the 12" and 15" displays
		// if the detected display is the 15", then change the platform configuration from
		// the default (12") to the configuration for the 15".
		mxc_i2c1_board_info[I2C1_BOARDINFOIDX_ATMEL_MXT1386_TS].platform_data =
			&NEC15_mxt_data;
	}

	i2c_register_board_info(1, mxc_i2c1_board_info,
		ARRAY_SIZE(mxc_i2c1_board_info));

	i2c_register_board_info(2, mxc_bitbang_i2c_board_info,
		ARRAY_SIZE(mxc_bitbang_i2c_board_info));

	pm_power_off = mxc_power_off;

	mxc_register_device(&mxc_sgtl5000_device, &sgtl5000_data);

	mx5_usb_dr_init();
	mx5_usbh1_init();
	mx5_usbh2_init();
	setup_microwire_eeprom();
	setup_ethernet_switch();

	memptr = ioremap( MX51_SRC_BASE_ADDR, 0x1000 );
	_reset_reason = readl(memptr + 0x8);
	iounmap( memptr );

	{
		struct proc_dir_entry *proc_dir = proc_mkdir( "rave", NULL );
		create_proc_read_entry( "reset_reason",
				0,		/* default mode */
				proc_dir,	/* parent dir */
				resetReason_proc,
				NULL );		/* client data */

		create_proc_read_entry( "board_type",
				0,		/* default mode */
				proc_dir,	/* parent dir */
				boardType_proc,
				NULL );		/* client data */
	}
}

static void __init mx51_babbage_timer_init(void)
{
	struct clk *uart_clk;

	/* Change the CPU voltages for TO2*/
	if (mx51_revision() == IMX_CHIP_REVISION_2_0) {
		cpu_wp_auto[0].cpu_voltage = 1175000;
		cpu_wp_auto[1].cpu_voltage = 1100000;
		cpu_wp_auto[2].cpu_voltage = 1000000;
	}

	mx51_clocks_init(32768, 24000000, 22579200, 24576000);

	uart_clk = clk_get_sys("mxcintuart.0", NULL);
	early_console_setup(UART1_BASE_ADDR, uart_clk);
}

static struct sys_timer mxc_timer = {
	.init	= mx51_babbage_timer_init,
};

/*
 * The following uses standard kernel macros define in arch.h in order to
 * initialize __mach_desc_MX51_BABBAGE data structure.
 */
/* *INDENT-OFF* */
MACHINE_START(MX51_BABBAGE, "Freescale MX51 Babbage Board")
	/* Maintainer: Freescale Semiconductor, Inc. */
	.phys_io	= AIPS1_BASE_ADDR,
	.io_pg_offst	= ((AIPS1_BASE_ADDR_VIRT) >> 18) & 0xfffc,
	.fixup = fixup_mxc_board,
	.map_io = mx5_map_io,
	.init_irq = mx5_init_irq,
	.init_machine = mxc_board_init,
	.timer = &mxc_timer,
MACHINE_END
