#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/i2c/at24.h>
#include <linux/crc-itu-t.h>

#define MAX_PART_NUM_LENGTH		20
#define MAX_SERIAL_NUM_LENGTH	10
#define MAX_DOM_LENGTH			5
#define MAX_REVISION_LENGTH	    5

typedef struct __attribute__ ((packed)) {
	char        partNumber[MAX_PART_NUM_LENGTH];
	char        serialNumber[MAX_SERIAL_NUM_LENGTH];
	char        boardInitialDom[MAX_DOM_LENGTH];
	char        boardInitialRev[MAX_REVISION_LENGTH];
	char        boardUpdatedDom[MAX_DOM_LENGTH];
	char        boardUpdatedRev[MAX_REVISION_LENGTH];
    u32          mod;
	u16          crc16;
} NameplateEeprom_S;

NameplateEeprom_S nameplate_eeprom;
char modString[11] = {0};

static int populate_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	len += sprintf(buf+len,"%s\n", (char *)data_param);
	*eof = 1;
	return len;
}


// This is the callback function when a a specifc at24 eeprom is found.
// Its reads out the eeprom contents via the read function passed back in via
// struct memory_accessor. It then calls part_number_proc, serial_number_proc,
// and dom_proc to populate the procfs entries for each specific field.
void scu2_esb_mezz_namepate(struct memory_accessor *mem_accessor, struct proc_dir_entry *rave_proc_dir) {
    u16 calcCrc = 0xFFFF;
	memset(&nameplate_eeprom, 0, sizeof(NameplateEeprom_S));

	// Read the Data structure from the EEPROM
	if(mem_accessor->read( mem_accessor, (char *)&nameplate_eeprom, 0, sizeof(NameplateEeprom_S)) <=0) {
		// The read failed. Make sure that the displayed data is just NULL characters
		memset(&nameplate_eeprom, 0, sizeof(NameplateEeprom_S));
	}

	// Validate the CRC-16
	calcCrc = crc_itu_t(calcCrc, (const u8 *)&nameplate_eeprom, sizeof(NameplateEeprom_S));
	if(calcCrc != 0) {
		// The validation failed. Make sure that the displayed data is just NULL characters
		memset(&nameplate_eeprom, 0, sizeof(NameplateEeprom_S));
	}

	create_proc_read_entry( "part_number",
			0,		/* default mode */
			rave_proc_dir,	/* parent dir */
			populate_proc,
			nameplate_eeprom.partNumber );		/* client data */

	create_proc_read_entry( "serial_number",
			0,		/* default mode */
			rave_proc_dir,	/* parent dir */
			populate_proc,
			nameplate_eeprom.serialNumber );		/* client data */

	create_proc_read_entry( "date_of_manufacture",
			0,		/* default mode */
			rave_proc_dir,	/* parent dir */
			populate_proc,
			nameplate_eeprom.boardInitialDom );		/* client data */

	create_proc_read_entry( "revision",
			0,		/* default mode */
			rave_proc_dir,	/* parent dir */
			populate_proc,
			nameplate_eeprom.boardInitialRev );		/* client data */

	create_proc_read_entry( "updated_date_of_manufacture",
			0,		/* default mode */
			rave_proc_dir,	/* parent dir */
			populate_proc,
			nameplate_eeprom.boardUpdatedDom );		/* client data */

	create_proc_read_entry( "updated_revision",
			0,		/* default mode */
			rave_proc_dir,	/* parent dir */
			populate_proc,
			nameplate_eeprom.boardUpdatedRev );		/* client data */

	snprintf( modString, 11, "0x%08X", nameplate_eeprom.mod );
	create_proc_read_entry( "mod",
			0,		/* default mode */
			rave_proc_dir,	/* parent dir */
			populate_proc,
			modString );		/* client data */
}


