/*
 * Copyright 2004-2011 Freescale Semiconductor, Inc.
 * Copyright (C) 2008 by Sascha Hauer <kernel@pengutronix.de>
 * Copyright (C) 2009 by Jan Weitzel Phytec Messtechnik GmbH,
 *                       <armlinux@phytec.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/gpio.h>

#include <mach/hardware.h>
#include <asm/mach/map.h>
#include <mach/iomux-v3.h>

static void __iomem *base;

/*
 * Read a single pad in the iomuxer
 */
int mxc_iomux_v3_get_pad(struct pad_desc *pad)
{
	pad->mux_mode = __raw_readl(base + pad->mux_ctrl_ofs) & 0xFF;
	pad->pad_ctrl = __raw_readl(base + pad->pad_ctrl_ofs) & 0x1FFFF;
	pad->select_input = __raw_readl(base + pad->select_input_ofs) & 0x7;

	return 0;
}
EXPORT_SYMBOL(mxc_iomux_v3_get_pad);

/*
 * Read multiple pads in the iomuxer
 */
int mxc_iomux_v3_get_multiple_pads(struct pad_desc *pad_list, unsigned count)
{
	struct pad_desc *p = pad_list;
	int i;
	int ret;

	for (i = 0; i < count; i++) {
		mxc_iomux_v3_get_pad(p);
		p++;
	}
	return 0;
}
EXPORT_SYMBOL(mxc_iomux_v3_get_multiple_pads);
/*
 * setups a single pad in the iomuxer
 */
int mxc_iomux_v3_setup_pad(struct pad_desc *pad)
{
	if (pad->mux_ctrl_ofs)
		__raw_writel(pad->mux_mode, base + pad->mux_ctrl_ofs);

	if (pad->select_input_ofs)
		__raw_writel(pad->select_input,
				base + pad->select_input_ofs);

	if (!(pad->pad_ctrl & NO_PAD_CTRL) && pad->pad_ctrl_ofs)
		__raw_writel(pad->pad_ctrl, base + pad->pad_ctrl_ofs);
	return 0;
}
EXPORT_SYMBOL(mxc_iomux_v3_setup_pad);

//IMS_PATCH---------------------------------------
void mxc_iomux_v3_verify_pads(struct pad_desc *pad_list, unsigned count)
{
	struct pad_desc *p = pad_list;
	int i;
    unsigned int mux_mode;
    unsigned int select_input;
    unsigned int pad_ctrl;
    


	for (i = 0; i < count; i++) {

        mux_mode = select_input = pad_ctrl = 0;

    	if (p->mux_ctrl_ofs)
        {
    		//__raw_writel(pad->mux_mode, base + pad->mux_ctrl_ofs);
            mux_mode = __raw_readl(base + p->mux_ctrl_ofs);
        }
    
    	if (p->select_input_ofs)
        {
    		//__raw_writel(pad->select_input,  base + pad->select_input_ofs);
            select_input = __raw_readl(base + p->select_input_ofs);
        }
    
    	if (!(p->pad_ctrl & NO_PAD_CTRL) && p->pad_ctrl_ofs)
        {
    		//__raw_writel(pad->pad_ctrl, base + pad->pad_ctrl_ofs);
            pad_ctrl = __raw_readl( base + p->pad_ctrl_ofs);
        }

		printk(KERN_INFO "%3.3d pad %4.4x %5.5x mux %4.4x %4.4x  sel %4.4x %4.4x",
			   i,
               p->pad_ctrl_ofs,
               pad_ctrl,
               p->mux_ctrl_ofs,
               mux_mode,
               p->select_input_ofs,
               select_input);

		p++;
    }
    return;
}
//IMS_PATCH---------------------------------------

int mxc_iomux_v3_setup_multiple_pads(struct pad_desc *pad_list, unsigned count)
{
	struct pad_desc *p = pad_list;
	int i;
	int ret;

	for (i = 0; i < count; i++) {
//IMS_PATCH---------------------------------------
        //%%jws%%
        /*
		printk(KERN_INFO "%3.3d pad %4.4x %5.5x mux %4.4x %4.4x  sel %4.4x %4.4x",
			   i,
			   p->pad_ctrl_ofs,
			   p->pad_ctrl,
			   p->mux_ctrl_ofs,
			   p->mux_mode,
			   p->select_input_ofs,
			   p->select_input);
        */
//IMS_PATCH---------------------------------------
		ret = mxc_iomux_v3_setup_pad(p);
		if (ret)
			return ret;
		p++;
	}
	return 0;
}
EXPORT_SYMBOL(mxc_iomux_v3_setup_multiple_pads);

int mxc_iomux_v3_setup_pad_ext(struct pad_cfg *pd)
{
	struct pad_desc *pad = &(pd->pd);

	if (pad->mux_ctrl_ofs)
		__raw_writel(pad->mux_mode, base + pad->mux_ctrl_ofs);

	if (pad->select_input_ofs)
		__raw_writel(pad->select_input,
				base + pad->select_input_ofs);

	if (pd->pad_ctrl && pad->pad_ctrl_ofs)
		__raw_writel(pd->pad_ctrl, base + pad->pad_ctrl_ofs);
	else if (!(pad->pad_ctrl & NO_PAD_CTRL) && pad->pad_ctrl_ofs)
		__raw_writel(pad->pad_ctrl, base + pad->pad_ctrl_ofs);
	return 0;
}
EXPORT_SYMBOL(mxc_iomux_v3_setup_pad_ext);

int mxc_iomux_v3_setup_multiple_pads_ext(struct pad_cfg *pad_list,
						unsigned count)
{
	struct pad_cfg *p = pad_list;
	int i;
	int ret;

	for (i = 0; i < count; i++) {
		ret = mxc_iomux_v3_setup_pad_ext(p);
		if (ret)
			return ret;
		p++;
	}
	return 0;
}
EXPORT_SYMBOL(mxc_iomux_v3_setup_multiple_pads_ext);

void mxc_iomux_v3_init(void __iomem *iomux_v3_base)
{
	base = iomux_v3_base;
}
